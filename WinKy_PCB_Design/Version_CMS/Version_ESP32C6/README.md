# WinKy ESP32


<img src="pictures/C3/winky-enclosure.png" alt="Winky ESP32 Into enclosure">

WinKy is an Open-source project which creates a Wifi end-device to connect with Linky meter in France.

This project is based on awesome work done by Jérôme Ferrari, explanations are at https://miniprojets.net/index.php/2021/09/08/winky-open-source-projet-pour-linky-avec-wifi/

Original Jérôme's repository is located [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/winky)

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

The Code is licensed under a
[Creative Commons Attribution-Non-Commercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

This ESP32-C3 ESP32-C6 Hardware: [![CC BY-NC-ND 4.0][cc-by-nc-nd-shield]][cc-by-nc-nd]

The Hardware is licensed under a
[Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License][cc-by-nc-sa].


[![CC BY-NC-ND 4.0][cc-by-nc-nd-image]][cc-by-nc-nd]

[cc-by-nc-nd]: https://creativecommons.org/licenses/by-nc-nd/4.0/
[cc-by-nc-nd-image]: https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png
[cc-by-nc-nd-shield]: https://img.shields.io/badge/License-CC%20BY--NC--ND%204.0-lightgrey.svg


This End-device was created by Ferrari Jérôme CNRS/UGA/G-INP – G2ELAB, and ESP32-C3 ESP32-C6 all in one Hardware have been designed by [Charles-Henri Hallard][1] with permission of original authors and partners

<img src="https://www.cnrs.fr/themes/custom/cnrs/logo.svg" width="15%" height="15%">&nbsp;&nbsp;
<img src="https://www.grenoble-inp.fr/uas/alias1/LOGO/Grenoble+INP+-+%C3%89tablissement+%28couleur%2C+RVB%2C+120px%29.png">&nbsp;&nbsp;
<img src="https://g2elab.grenoble-inp.fr/uas/alias31/LOGO/Logo-g2elab-couleur.png">
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1D0TxrJin49KlU9Oo263OgjsanU9vKQZhKJ9LZsMjxxggnCEKO7NDsN-rW6CEXGJbLEE&usqp=CAU">
<!--[CNRS-logo]: https://www.cnrs.fr/themes/custom/cnrs/logo.svg-->


This all in one board is used to get French energy meter called Teleinfo (AKA TIC) and has the following features:

# ESP32-C3

- ESP32-C3 with 4Mb Flash
- Self Powered From Linky Interface
- Teleinfo Reader interface
- 2 x RGB User Led
- Blue LED to see if Teleinfo signal is received
- QWIIC/STEMMA I2C header sensors connector
- USB-C Connector
- Reset and Flash Button
- FTDI Serial pinout to get core UART RX/TX pins
- Some Free GPIO exposed as solder PAD
- Resistor divider to monitor voltage across supercap and VUSB
- Several Super CAPS footprint

It is a plug and play board, nothing to solder or assemble.

# ESP32-C6

- ESP32-C6 with 4Mb Flash
- Self Powered From Linky Interface
- Teleinfo Reader interface
- 2 x RGB User Led
- Blue LED to see if Teleinfo signal is received
- QWIIC/STEMMA I2C header sensors connector
- USB-C Connector
- Reset and Flash Button
- FTDI Serial pinout to get core UART RX/TX pins
- Some Free GPIO exposed as solder PAD
- Resistor divider to monitor voltage across Linky Power, Super Cap and VUSB
- Several Super CAPS footprint

It is a plug and play board, nothing to solder or assemble.


# History


**V1.3 ESP32-C6**

- added 10K pullup resistor on IO9 button
- some routing change to easier production


**V1.2 ESP32-C6**

- moved super capacitor footprint to left to reduce total size
- added resistor 470K/100K divider accros VIN from Linky
- rerouted to use ESP32-C6 module
- FTDI Connector 3.3V disabled (enable with solder pad)
- FTDI Connector 5.0V disabled (enable with solder pad)
- Exposed GPIO23 can be connected to ground (avoid Tasmota DeepSleep)
- changed VUSB divider to 10K/10K to reduce ADC voltage read when no USB (diode leak)

**V1.1 ESP32-C3**

- added 2nd RGB LED for today/tomorrow color on Tempo contract
- added solder pad to select if RGB Leds are powered with ESP32 3.3V or Supercaps (Always ON mode)
- fixed some silk
- fixed I2/I1/A pins order that were reversed


**V1.0 ESP32-C3**

First release based on ESP32 C3 (not produced)

# Detailed Description

Look at the schematics for more informations, easy to understand. Wiring is as follow:

| Pin Function  | ESP32-C3 | ESP32-C6 | 
|  :---          | :---: | :---: | 
| 5V ADC with 10K/10K resistors divider | GPIO1 | GPIO1 | 
| Adressable RGB Leds | GPIO2 | GPIO2 | 
| Téléinfo Rx    | GPIO3 | GPIO4 | 
| Supercap ADC with 470K/470K resistors divider | GPIO4 | GPIO3 | 
| Linky VDC ADC with 470K/100K resistors divider | None | GPIO5 | 
| I2C SDA        | GPIO5 | GPIO6 |  
| I2C SDL        | GPIO6 | GPIO7 |  
| Button (flash) | GPIO9 | GPIO9 | 
| Free with solder pad | GPIO7 | GPIO9 | 
| Free with solder pad | GPIO10 | GPIO18 | 
| Free with solder pad | None | GPIO23 | 

# Schematics

## ESP32-C3 

<img src="pictures/C3/winky-sch.png" alt="Winky ESP32 Schematics">

## ESP32-C6

<img src="pictures/C6/winky-sch.png" alt="Winky ESP32 Schematics">

# Boards

## ESP32-C3 

<img src="pictures/C3/winky-top.png" alt="Winky ESP32 Top">

<img src="pictures/C3/winky-bot.png" alt="Winky ESP32 Bottom">

## ESP32-C6

<img src="pictures/C6/winky-top.png" alt="Winky ESP32 Top">

<img src="pictures/C6/winky-bot.png" alt="Winky ESP32 Bottom">


# Firmware 

It's compatible with Arduino IDE, Visual Studio Code but C3 also Micro Python since this board use same CPU than Adafruit [QT-PY-ESP32-C3](https://learn.adafruit.com/adafruit-qt-py-esp32-c3-wifi-dev-board)

You can write your own and use with [LibTeleinfo](https://github.com/hallard/LibTeleinfo) library I wrote if you want to get rid of driving teleinfo stuff (chekout some [examples](ttps://github.com/hallard/LibTeleinfo/tree/master/examples)).

## Basic sample copde

Working code using VScode + platform.io located [here](https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/winky/-/tree/main/WinKy_Firmware/esp32) 

## Tasmota

But I strongly suggest using amazing [Tasmota](https://tasmota.github.io/docs/) firmware, all is already done and well done.

Please check Teleinfo official tasmota [documentation](https://tasmota.github.io/docs/Teleinfo/) so see how to configure your device depending on smartmeter type and what options you need.


For now Winky has been successfully tested with tasmota firmware (USB-C Powered) please check the Denky D4 [firmware documentation](https://github.com/hallard/Denky-D4#firmware) it's the same process and you'll find on the Denky D4 [repository](https://github.com/hallard/Denky-D4) all necessary documentation to work with Tasmota including some Berry scripting examples.

### Tasmota template

I strongly suggest using awesome Tamsota [autoconf](https://github.com/tasmota/autoconf) feature that works right out of the box for C3 and C6, but if you need Tasmota template for Winky, here they are

#### ESP32-C3

```json
{"NAME":"Winky","GPIO":[1,4704,1376,5632,4705,640,608,1,1,32,1,0,0,0,0,0,0,0,1,1,1,1],"FLAG":0,"BASE":1}
```

#### ESP32-C6

```json
{"NAME":"Winky","GPIO":[1,4704,1376,4705,5632,4706,640,608,1,32,1,0,0,0,0,0,0,0,1,1,1,1,1,4096,0,0,0,0,0,0,0],"FLAG":0,"BASE":1}
```

# Support and discussion

If you have any issue or just want to discuss on this project, please use official  [forum](https://miniprojets.net/index.php/forum/)

# License

<img alt="Creative Commons Attribution-NonCommercial 4.0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png">   

This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/)    


<!--
# Lazy building your own? 

You can order this module (V1.1 only) fully assembled with some extra on (tindie)[https://www.tindie.com/products/]

<a href="https://www.tindie.com/products/28907/"><img src="https://d2ss6ovg47m0r5.cloudfront.net/badges/tindie-mediums.png" alt="I sell on Tindie" width="150" height="78"></a>

-->

# Misc

See news and other projects on my [blog][1] 
 
[1]: https://hallard.me










