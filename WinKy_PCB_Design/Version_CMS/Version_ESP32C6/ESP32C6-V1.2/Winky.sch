<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="25" unitdist="mil" unit="mil" style="dots" multiple="1" display="yes" altdistance="5" altunitdist="mil" altunit="mil"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="yes" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="yes" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="yes" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="yes" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="yes" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="yes" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="yes" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="yes" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="yes" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="yes" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="yes" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="yes" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="yes" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="3" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="3" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="3" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="48" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="40" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="43" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="31" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="yes" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="3" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="yes" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="yes" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="yes" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="yes" active="no"/>
<layer number="58" name="b3D" color="7" fill="1" visible="yes" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="61" name="stand" color="7" fill="1" visible="yes" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="156" name="tAssmbl" color="7" fill="1" visible="yes" active="yes"/>
<layer number="157" name="bAssmbl" color="7" fill="1" visible="yes" active="yes"/>
<layer number="158" name="FaceMMeas" color="3" fill="1" visible="no" active="no"/>
<layer number="159" name="Geh-Bear2" color="1" fill="7" visible="no" active="no"/>
<layer number="160" name="FAB" color="7" fill="1" visible="no" active="yes"/>
<layer number="161" name="tomplace2" color="7" fill="1" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="180" name="Outline" color="0" fill="1" visible="no" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="252" name="RM45" color="7" fill="1" visible="no" active="yes"/>
<layer number="253" name="Extra" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="_c2h">
<packages>
<package name="0805">
<rectangle x1="-1" y1="-0.625" x2="1" y2="0.625" layer="51"/>
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1" roundness="25"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1" roundness="25"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="AXIAL-1/4W">
<pad name="P$1" x="-3.81" y="0" drill="0.8"/>
<pad name="P$2" x="3.81" y="0" drill="0.8"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
SMD</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0" layer="48"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0" layer="48"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.254" layer="21"/>
<rectangle x1="-0.538125" y1="-0.3048" x2="-0.238125" y2="0.2951" layer="51"/>
<rectangle x1="0.242925" y1="-0.3048" x2="0.542925" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.534125" y="0" dx="0.56" dy="0.6" layer="1" roundness="30"/>
<smd name="2" x="0.534125" y="0" dx="0.56" dy="0.6" layer="1" roundness="30"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.254" layer="21"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1.016" dy="0.9652" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.016" dy="0.9652" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="0805-RES">
<wire x1="-0.1" y1="0.7" x2="-0.1" y2="0.5" width="0.2" layer="21"/>
<wire x1="-0.1" y1="0.5" x2="0.3" y2="0.2" width="0.2" layer="21"/>
<wire x1="0.3" y1="0.2" x2="-0.3" y2="0" width="0.2" layer="21"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="-0.3" width="0.2" layer="21"/>
<wire x1="0.3" y1="-0.3" x2="-0.1" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.1" y1="-0.5" x2="-0.1" y2="-0.7" width="0.2" layer="21"/>
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1" roundness="25"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1" roundness="25"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-0.635" x2="-0.508" y2="0.635" layer="51"/>
<rectangle x1="0.508" y1="-0.635" x2="1.016" y2="0.635" layer="51"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="RES_UPRIGHT">
<wire x1="-2.5025" y1="0.977" x2="-2.5025" y2="-0.977" width="0.2032" layer="21"/>
<wire x1="-2.5025" y1="-0.977" x2="2.5025" y2="-0.977" width="0.2032" layer="21"/>
<wire x1="2.5025" y1="-0.977" x2="2.5025" y2="0.977" width="0.2032" layer="21"/>
<wire x1="2.5025" y1="0.977" x2="-2.5025" y2="0.977" width="0.2032" layer="21"/>
<pad name="1" x="-1.5475" y="0" drill="0.762" diameter="1.524"/>
<pad name="2" x="1.5475" y="0" drill="0.762" diameter="1.524"/>
<text x="-1.5837" y="1.2727" size="0.6096" layer="25">&gt;NAME</text>
<text x="-2.058" y="-2.0161" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="1206-RES">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="0.1" y1="-0.7" x2="0.1" y2="-0.5" width="0.2" layer="25"/>
<wire x1="0.1" y1="-0.5" x2="-0.3" y2="-0.2" width="0.2" layer="25"/>
<wire x1="-0.3" y1="-0.2" x2="0.3" y2="0" width="0.2" layer="25"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.3" width="0.2" layer="25"/>
<wire x1="-0.3" y1="0.3" x2="0.1" y2="0.5" width="0.2" layer="25"/>
<wire x1="0.1" y1="0.5" x2="0.1" y2="0.7" width="0.2" layer="25"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
SMD</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0" layer="48"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0" layer="48"/>
<wire x1="-0.127" y1="0.254" x2="0.127" y2="0.254" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="-0.254" x2="0.127" y2="-0.254" width="0.2032" layer="21"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.55" y="0" dx="0.5" dy="0.6" layer="1" roundness="30"/>
<smd name="2" x="0.55" y="0" dx="0.5" dy="0.6" layer="1" roundness="30"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-0.127" y1="-0.254" x2="0.127" y2="-0.254" width="0.254" layer="21"/>
<wire x1="-0.127" y1="0.254" x2="0.127" y2="0.254" width="0.254" layer="21"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1.016" dy="0.9652" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.016" dy="0.9652" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="0805-CAP">
<wire x1="-0.2" y1="0.5" x2="-0.2" y2="-0.5" width="0.2" layer="21"/>
<wire x1="0.2" y1="0.5" x2="0.2" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.3" y1="0" x2="-0.5" y2="0" width="0.2" layer="21"/>
<wire x1="0.3" y1="0" x2="0.5" y2="0" width="0.2" layer="21"/>
<smd name="1" x="-1" y="0" dx="0.9016" dy="1.3" layer="1"/>
<smd name="2" x="1" y="0" dx="0.9016" dy="1.3" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-0.635" x2="-0.508" y2="0.635" layer="51"/>
<rectangle x1="0.508" y1="-0.635" x2="1.016" y2="0.635" layer="51"/>
</package>
<package name="0805-CAP-LARGEPADS">
<wire x1="-0.2" y1="0.5" x2="-0.2" y2="-0.5" width="0.2" layer="21"/>
<wire x1="0.2" y1="0.5" x2="0.2" y2="-0.5" width="0.2" layer="21"/>
<wire x1="-0.3" y1="0" x2="-0.5" y2="0" width="0.2" layer="21"/>
<wire x1="0.3" y1="0" x2="0.5" y2="0" width="0.2" layer="21"/>
<rectangle x1="-1" y1="-0.625" x2="1" y2="0.625" layer="51"/>
<smd name="1" x="-1.127" y="0" dx="1.1556" dy="1.3" layer="1"/>
<smd name="2" x="1.127" y="0" dx="1.1556" dy="1.3" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1206-CAP">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-0.2" y1="0.627" x2="-0.2" y2="-0.627" width="0.2" layer="25"/>
<wire x1="0.2" y1="0.627" x2="0.2" y2="-0.627" width="0.2" layer="25"/>
<wire x1="-0.3" y1="0" x2="-0.5" y2="0" width="0.2" layer="25"/>
<wire x1="0.3" y1="0" x2="0.5" y2="0" width="0.2" layer="25"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="0402-CAP-OK">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0" layer="48"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0" layer="48"/>
<wire x1="-0.127" y1="0.254" x2="0.127" y2="0.254" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="-0.254" x2="0.127" y2="-0.254" width="0.2032" layer="21"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.5207" y="0" dx="0.56" dy="0.6" layer="1" roundness="30"/>
<smd name="2" x="0.5246" y="0" dx="0.56" dy="0.6" layer="1" roundness="30"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SOT-323-3">
<wire x1="1.1224" y1="0.6604" x2="1.1224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.1224" y1="-0.6604" x2="-1.1224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1224" y1="-0.6604" x2="-1.1224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.1224" y1="0.6604" x2="1.1224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.673" y1="0.7" x2="-1.1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.1" y1="0.7" x2="-1.1" y2="-0.354" width="0.2032" layer="21"/>
<wire x1="0.673" y1="0.7" x2="1.1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.354" width="0.2032" layer="21"/>
<smd name="1" x="-0.65" y="-0.925" dx="0.7" dy="0.7" layer="1"/>
<smd name="2" x="0.65" y="-0.925" dx="0.7" dy="0.7" layer="1"/>
<smd name="3" x="0" y="0.925" dx="0.7" dy="0.7" layer="1"/>
<text x="-1.27" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.27" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT-23">
<description>&lt;b&gt;SOT23&lt;/b&gt;</description>
<wire x1="-0.1905" y1="-0.635" x2="0.1905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="-0.254" x2="1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="0.6985" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0.635" x2="-1.4605" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.4605" y1="0.635" x2="-1.4605" y2="-0.254" width="0.127" layer="21"/>
<smd name="3" x="0" y="1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="2" x="0.889" y="-1.016" dx="1.016" dy="1.143" layer="1"/>
<smd name="1" x="-0.889" y="-1.016" dx="1.016" dy="1.143" layer="1" rot="R180"/>
<text x="-1.905" y="1.905" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<rectangle x1="-1.524" y1="-1.651" x2="1.524" y2="1.651" layer="39"/>
</package>
<package name="0603-MATCHNET">
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.7357" y="0" dx="1" dy="1" layer="1"/>
<smd name="2" x="0.7484" y="0" dx="1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<polygon width="0.0508" layer="41">
<vertex x="-0.09525" y="0.53975"/>
<vertex x="0.09525" y="0.53975"/>
<vertex x="0.09525" y="-0.53975"/>
<vertex x="-0.09525" y="-0.53975"/>
</polygon>
</package>
<package name="0402-RES-OK">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0" layer="48"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0" layer="48"/>
<wire x1="-0.0889" y1="0" x2="0.0889" y2="0" width="0.254" layer="21"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.53095" y="0" dx="0.56" dy="0.6" layer="1" roundness="30"/>
<smd name="2" x="0.53095" y="0" dx="0.56" dy="0.6" layer="1" roundness="30"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST04_1MM_VERT">
<description>&lt;h3&gt;Vertical Qwiic Connector&lt;/h3&gt;

&lt;p&gt;Physical and Electrical Charactersistics&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Voltage Rating: 50V&lt;/li&gt;
&lt;li&gt;Current Rating 1.0A&lt;/li&gt;
&lt;li&gt;Contact Resistance: 20 milliohms&lt;/li&gt;
&lt;li&gt;ROHS Compliant&lt;/li&gt;
&lt;li&gt;SMD Pitch: 1mm&lt;/li&gt;
&lt;li&gt;Small pad size: .6mm x 1.55mm&lt;/li&gt;
&lt;li&gt;Large pad size: 1.2mm x 2.0mm&lt;/li&gt;
&lt;li&gt;&lt;/li&gt;
&lt;li&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
<circle x="-1.948" y="-0.43" radius="0.2032" width="0" layer="21"/>
<wire x1="1.912" y1="3.87" x2="-1.878" y2="3.87" width="0.2032" layer="21"/>
<wire x1="-3.218" y1="1.33" x2="-3.218" y2="0.68" width="0.2032" layer="21"/>
<wire x1="-3.218" y1="0.68" x2="-2.458" y2="0.68" width="0.2032" layer="21"/>
<wire x1="3.272" y1="1.35" x2="3.272" y2="0.66" width="0.2032" layer="21"/>
<wire x1="3.272" y1="0.66" x2="2.542" y2="0.66" width="0.2032" layer="21"/>
<wire x1="-2.921" y1="3.556" x2="-3.302" y2="3.556" width="0.1" layer="51"/>
<wire x1="-3.302" y1="3.556" x2="-3.302" y2="3.175" width="0.1" layer="51"/>
<wire x1="-3.302" y1="3.175" x2="-2.921" y2="3.175" width="0.1" layer="51"/>
<wire x1="2.945" y1="3.154" x2="3.326" y2="3.154" width="0.1" layer="51"/>
<wire x1="3.326" y1="3.154" x2="3.326" y2="3.535" width="0.1" layer="51"/>
<wire x1="3.326" y1="3.535" x2="2.945" y2="3.535" width="0.1" layer="51"/>
<smd name="1" x="-1.478" y="0.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-0.478" y="0.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="0.522" y="0.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="1.522" y="0.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="NC1" x="-2.778" y="3.2" dx="1.2" dy="2" layer="1"/>
<smd name="NC2" x="2.822" y="3.2" dx="1.2" dy="2" layer="1"/>
<text x="0.016" y="4.56633125" size="0.6096" layer="25">&gt;NAME</text>
<text x="0.016" y="-1.29633125" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="JST04_1MM_RA">
<description>&lt;h3&gt;SMD- 4 Pin Right Angle &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="2.5" y="-2.84" radius="0.1016" width="0.2032" layer="21"/>
<wire x1="1.5" y1="2.06" x2="-1.5" y2="2.06" width="0.2032" layer="21"/>
<wire x1="3" y1="-0.54" x2="3" y2="-2.19" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="-2.19" x2="-3" y2="-2.19" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.19" x2="-3" y2="-0.54" width="0.2032" layer="21"/>
<wire x1="3" y1="-2.19" x2="2.25" y2="-2.19" width="0.2032" layer="21"/>
<smd name="1" x="1.5" y="-2.54" dx="0.6" dy="1.35" layer="1" rot="R180"/>
<smd name="2" x="0.5" y="-2.54" dx="0.6" dy="1.35" layer="1" rot="R180"/>
<smd name="3" x="-0.5" y="-2.54" dx="0.6" dy="1.35" layer="1" rot="R180"/>
<smd name="4" x="-1.5" y="-2.54" dx="0.6" dy="1.35" layer="1" rot="R180"/>
<smd name="NC1" x="-2.8" y="1.135" dx="1.2" dy="2" layer="1" rot="R180"/>
<smd name="NC2" x="2.8" y="1.135" dx="1.2" dy="2" layer="1" rot="R180"/>
<text x="1.397" y="-0.381" size="0.6096" layer="25" font="vector" ratio="20" rot="R180">&gt;NAME</text>
<text x="1.651" y="0.762" size="0.6096" layer="27" font="vector" ratio="20" rot="R180">&gt;VALUE</text>
</package>
<package name="JST04_1MM_RA_STRESSRELIEF">
<description>Qwiic connector with milled cutout. Sliding the cable into this slot prevents the cable from coming unplugged.</description>
<circle x="-2.5" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.254" layer="21"/>
<wire x1="-2" y1="-10.16" x2="-2" y2="-8" width="0.3048" layer="20"/>
<wire x1="-2" y1="-8" x2="4" y2="-8" width="0.3048" layer="20"/>
<wire x1="4" y1="-8" x2="4" y2="-6" width="0.3048" layer="20"/>
<wire x1="4" y1="-6" x2="-4" y2="-6" width="0.3048" layer="20"/>
<wire x1="-4" y1="-6" x2="-4" y2="-10.16" width="0.3048" layer="20"/>
<rectangle x1="-4" y1="-8" x2="4" y2="-6" layer="46"/>
<rectangle x1="-4" y1="-10" x2="-2" y2="-8" layer="46"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<text x="-1.397" y="-2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SK6805-1515">
<description>&lt;h3&gt;WS2812-1515 - 1515 package for the WS2812&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 1 mm&lt;/li&gt;
&lt;li&gt;Area: 1.7 mm x 1.5 mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<smd name="2" x="0.475" y="0.48" dx="0.55" dy="0.55" layer="1"/>
<smd name="1" x="0.475" y="-0.48" dx="0.55" dy="0.55" layer="1"/>
<smd name="4" x="-0.475" y="-0.48" dx="0.55" dy="0.55" layer="1"/>
<smd name="3" x="-0.475" y="0.48" dx="0.55" dy="0.55" layer="1"/>
<wire x1="-0.8365" y1="0.8365" x2="0.8365" y2="0.8365" width="0.0762" layer="21"/>
<wire x1="0.8365" y1="-0.8365" x2="-0.8365" y2="-0.8365" width="0.0762" layer="21"/>
<wire x1="-0.8" y1="0.8" x2="-0.8" y2="-0.8" width="0.0762" layer="51"/>
<wire x1="-0.8" y1="-0.8" x2="0.8" y2="-0.8" width="0.0762" layer="51"/>
<wire x1="0.8" y1="-0.8" x2="0.8" y2="0.8" width="0.0762" layer="51"/>
<wire x1="0.8" y1="0.8" x2="-0.8" y2="0.8" width="0.0762" layer="51"/>
<wire x1="-0.8365" y1="-0.1" x2="-0.8365" y2="0.1" width="0.0762" layer="21"/>
<wire x1="0.8365" y1="-0.1" x2="0.8365" y2="0.1" width="0.0762" layer="21"/>
<circle x="1.00475" y="-1" radius="0.07" width="0.127" layer="21"/>
</package>
<package name="SW2-2.6-3.0X2.5X1.2+0.4MM">
<smd name="1" x="-1.9" y="0" dx="1.7" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1.9" y="0" dx="1.7" dy="1" layer="1" rot="R90"/>
<wire x1="-1.5" y1="-1.25" x2="1.5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.25" x2="1.5" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.25" x2="-1.5" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.25" x2="-1.5" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.2" x2="-1.5" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.2" x2="-1.5" y2="1" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.2" x2="1.5" y2="1" width="0.127" layer="21"/>
<rectangle x1="-1.5" y1="-1.2" x2="1.5" y2="1.2" layer="39"/>
<text x="-1.8" y="1.5" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.5" y="-2.54" size="0.635" layer="27" ratio="11">&gt;VALUE</text>
</package>
<package name="C498294">
<smd name="1" x="-1.8" y="0" dx="1.8" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="1.8" y="0" dx="1.8" dy="0.8" layer="1" rot="R90"/>
<wire x1="-1.5" y1="-1.25" x2="1.5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.25" x2="1.5" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.25" x2="-1.5" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.25" x2="-1.5" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.2" x2="-1.5" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.2" x2="-1.5" y2="1" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.2" x2="1.5" y2="1" width="0.127" layer="21"/>
<rectangle x1="-1.5" y1="-1.2" x2="1.5" y2="1.2" layer="39"/>
<text x="-1.6" y="1.5" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.5" y="-2.14" size="0.635" layer="27" ratio="11">&gt;VALUE</text>
<hole x="0" y="0" drill="0.69"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="3V3-EXT">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3V3-EXT" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="CAP">
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="MOSFET-N">
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.5875" y1="-1.905" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="1.5875" y2="1.905" width="0.254" layer="94"/>
<wire x1="2.2225" y1="-0.4445" x2="1.905" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.4445" x2="0.9525" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="2.2225" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="2.2225" y1="0.4445" x2="1.5875" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0.4445" x2="0.9525" y2="0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.4445" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.4445" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.3175" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.27" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="0.3175" x2="-1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="-1.5875" y1="0" x2="-0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-0.9525" y1="-0.3175" x2="-1.397" y2="0.127" width="0.254" layer="94"/>
<circle x="0" y="1.905" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="-1.905" radius="0.254" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="0" y="2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="2.2225" size="0.8128" layer="93">D</text>
<text x="-0.635" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-3.4925" y="0" size="0.8128" layer="93">G</text>
<pin name="G" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="I2C_STANDARD">
<description>&lt;h3&gt;SparkFun I&lt;sup&gt;2&lt;/sup&gt;C Standard Pinout Header&lt;/h3&gt;
&lt;p&gt;SparkFun has standardized on a pinout for all I&lt;sup&gt;2&lt;/sup&gt;C based sensor breakouts.&lt;br&gt;</description>
<wire x1="3.81" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<text x="-5.08" y="-5.334" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<text x="-5.08" y="7.874" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-4.572" y="2.54" size="1.778" layer="94" font="vector" align="center-left">SDA</text>
<text x="-4.572" y="0" size="1.778" layer="94" font="vector" align="center-left">VCC</text>
<text x="-4.572" y="-2.54" size="1.778" layer="94" font="vector" align="center-left">GND</text>
<text x="-4.572" y="5.08" size="1.778" layer="94" font="vector" align="center-left">SCL</text>
</symbol>
<symbol name="SK6805-EC15">
<description>&lt;h3&gt;WS28X1 RGB LED - I2C Control&lt;/h3&gt;
&lt;p&gt;4 pin RGB LED with I2C Controller built-in&lt;/p&gt;</description>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="-0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.778" x2="-0.508" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-3.048" x2="-0.508" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-4.318" x2="0.762" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="-0.508" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0.762" y1="-1.778" x2="0.762" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="0.762" y2="-4.318" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.048" x2="2.032" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-3.048" x2="-1.778" y2="-3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="4.318" x2="-0.508" y2="3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="3.048" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="0.762" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="-0.508" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.762" y1="4.318" x2="0.762" y2="3.048" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="2.032" y2="3.048" width="0.254" layer="94"/>
<wire x1="-0.508" y1="3.048" x2="-1.778" y2="3.048" width="0.254" layer="94"/>
<pin name="VDD" x="-15.24" y="5.08" visible="pin" length="short"/>
<pin name="VSS" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="DI" x="-15.24" y="-2.54" visible="pin" length="short"/>
<pin name="DO" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<text x="-2.54" y="8.382" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.778" layer="95" font="vector" align="bottom-center">&gt;VALUE</text>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
<text x="-1.524" y="5.5118" size="1.27" layer="94">RGB</text>
<text x="-3.175" y="-2.159" size="1.27" layer="94" rot="R90">SK680x</text>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.254" layer="94" style="shortdash"/>
</symbol>
<symbol name="BUTTON-2P-1">
<pin name="1" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-1.905" y1="0.635" x2="1.27" y2="1.905" width="0.1524" layer="94"/>
<circle x="-1.905" y="0" radius="0.635" width="0.1524" layer="94"/>
<circle x="1.905" y="0" radius="0.635" width="0.1524" layer="94"/>
<text x="-5.08" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1/4W" package="AXIAL-1/4W">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="UPRIGHT" package="RES_UPRIGHT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'1206-RES''" package="1206-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MATCHINGNETWORK" package="0603-MATCHNET">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="402-RES-OK" package="0402-RES-OK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3V3-EXT" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="3V3-EXT" symbol="3V3-EXT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-CAP" package="0805-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-CAP-LARGEPADS" package="0805-CAP-LARGEPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'1206-CAP'" package="1206-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP-OK" package="0402-CAP-OK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MATCHINGNETWORK" package="0603-MATCHNET">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-MOSFET-N-CH-20V-2.1A-CJ2302(SOT-23)" prefix="Q" uservalue="yes">
<description>305030015</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="0" y="0"/>
</gates>
<devices>
<device name="CJ2302" package="SOT-23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="CJ2302" constant="no"/>
<attribute name="VALUE" value="CJ2302" constant="no"/>
</technology>
</technologies>
</device>
<device name="BSS138" package="SOT-23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SOT-323-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="QWIIC_CONNECTOR" prefix="J" uservalue="yes">
<description>&lt;h3&gt;SparkFun I&lt;sup&gt;2&lt;/sup&gt;C Standard Qwiic Connector&lt;/h3&gt;
An SMD 1mm pitch JST connector makes it easy and quick (get it? Qwiic?) to connect I&lt;sup&gt;2&lt;/sup&gt;C devices to each other. The &lt;a href=”http://www.sparkfun.com/qwiic”&gt;Qwiic system&lt;/a&gt; enables fast and solderless connection between popular platforms and various sensors and actuators.

&lt;br&gt;&lt;br&gt;

We carry &lt;a href=”https://www.sparkfun.com/products/14204”&gt;200mm&lt;/a&gt;, &lt;a href=”https://www.sparkfun.com/products/14205”&gt;100mm&lt;/a&gt;, &lt;a href=”https://www.sparkfun.com/products/14206”&gt;50mm&lt;/a&gt;, and &lt;a href=”https://www.sparkfun.com/products/14207”&gt;breadboard friendly&lt;/a&gt; Qwiic cables. We also offer &lt;a href=”https://www.sparkfun.com/products/14323”&gt;10 pcs strips&lt;/a&gt; the SMD connectors.</description>
<gates>
<gate name="G$1" symbol="I2C_STANDARD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST04_1MM_VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14483" constant="no"/>
<attribute name="VALUE" value="Vertical Qwiic Connector" constant="no"/>
</technology>
</technologies>
</device>
<device name="JS-1MM" package="JST04_1MM_RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13694" constant="no"/>
<attribute name="VALUE" value="QWIIC_RIGHT_ANGLE" constant="no"/>
</technology>
</technologies>
</device>
<device name="SR" package="JST04_1MM_RA_STRESSRELIEF">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SK6805">
<gates>
<gate name="G$1" symbol="SK6805-EC15" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="1515" package="SK6805-1515">
<connects>
<connect gate="G$1" pin="DI" pad="1"/>
<connect gate="G$1" pin="DO" pad="3"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VSS" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMD-BUTTON(2P-3.0X2.5X1.2+0.4MM)" prefix="SW" uservalue="yes">
<description>311020047</description>
<gates>
<gate name="G$1" symbol="BUTTON-2P-1" x="0" y="0"/>
</gates>
<devices>
<device name="-B3U-1000P-2P-SMD" package="SW2-2.6-3.0X2.5X1.2+0.4MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="B3U-1000P-2P-SMD"/>
<attribute name="VALUE" value="B3U-1000P-2P-SMD" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="C498294">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="FR-A4L">
<rectangle x1="178.7652" y1="0" x2="179.3748" y2="20.32" layer="94"/>
<rectangle x1="225.7552" y1="-26.67" x2="226.3648" y2="67.31" layer="94" rot="R90"/>
<wire x1="225.29" y1="-0.1" x2="225.29" y2="5.08" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="273.05" y2="5.08" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="179.07" y2="5.08" width="0.1016" layer="94"/>
<wire x1="179.07" y1="10.16" x2="225.29" y2="10.16" width="0.1016" layer="94"/>
<wire x1="225.29" y1="10.16" x2="273.05" y2="10.16" width="0.1016" layer="94"/>
<wire x1="179.07" y1="15.24" x2="273.05" y2="15.24" width="0.1016" layer="94"/>
<wire x1="225.29" y1="5.08" x2="225.29" y2="10.16" width="0.1016" layer="94"/>
<wire x1="179.07" y1="19.05" x2="179.07" y2="20.32" width="0.6096" layer="94"/>
<wire x1="179.07" y1="20.32" x2="180.34" y2="20.32" width="0.6096" layer="94"/>
<text x="181.61" y="11.43" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="181.61" y="6.35" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="195.58" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="181.61" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="181.61" y="16.51" size="2.54" layer="94" font="vector">&gt;CNAME</text>
<text x="226.16" y="1.27" size="2.54" layer="94" font="vector">Rev:</text>
<text x="226.26" y="6.35" size="2.54" layer="94" font="vector">&gt;DESIGNER</text>
<text x="234.92" y="1.17" size="2.54" layer="94" font="vector">&gt;CREVISION</text>
<frame x1="-3.81" y1="-3.81" x2="276.86" y2="182.88" columns="8" rows="5" layer="94"/>
</symbol>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VIN">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VIN" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A4L" prefix="FRAME">
<description>&lt;b&gt;Schematic Frame-European Format&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
Standard A4 size frame in Landscape</description>
<gates>
<gate name="G$1" symbol="FR-A4L" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VIN" prefix="SUPPLY">
<description>Vin supply symbol</description>
<gates>
<gate name="G$1" symbol="VIN" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="optocoupler">
<description>&lt;b&gt;Opto Couplers&lt;/b&gt;&lt;p&gt;
Siemens, Hewlett-Packard, Texas Instuments, Sharp, Motorola&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL4-SMD">
<description>&lt;b&gt;Dual  In Line SMD&lt;/b&gt; 4 pol.&lt;p&gt;
Source: LITE-ON ELECTRONICS, LTV816.pdf</description>
<wire x1="2.315" y1="3.18" x2="2.315" y2="-2.164" width="0.1524" layer="21"/>
<wire x1="2.315" y1="-2.164" x2="2.315" y2="-3.18" width="0.1524" layer="21"/>
<wire x1="-2.315" y1="3.18" x2="2.315" y2="3.18" width="0.1524" layer="21"/>
<wire x1="2.315" y1="-3.18" x2="-2.315" y2="-3.18" width="0.1524" layer="21"/>
<wire x1="-2.315" y1="-3.18" x2="-2.315" y2="3.18" width="0.1524" layer="21"/>
<wire x1="2.315" y1="-2.164" x2="-2.315" y2="-2.164" width="0.1524" layer="21"/>
<smd name="1" x="-1.27" y="-4.77" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.27" y="-4.77" dx="1.6" dy="2" layer="1"/>
<smd name="3" x="1.27" y="4.77" dx="1.6" dy="2" layer="1" rot="R180"/>
<smd name="4" x="-1.27" y="4.77" dx="1.6" dy="2" layer="1" rot="R180"/>
<text x="-2.54" y="-3.175" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.81" y="-3.175" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.875" y1="-5.15" x2="-0.65" y2="-3.2" layer="51"/>
<rectangle x1="0.665" y1="-5.15" x2="1.89" y2="-3.2" layer="51"/>
<rectangle x1="0.65" y1="3.2" x2="1.875" y2="5.15" layer="51" rot="R180"/>
<rectangle x1="-1.89" y1="3.2" x2="-0.665" y2="5.15" layer="51" rot="R180"/>
</package>
<package name="DIL04">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="2.54" y1="2.921" x2="-2.54" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="2.54" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.54" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-1.27" y="-3.81" drill="0.8128" shape="offset" rot="R270"/>
<pad name="2" x="1.27" y="-3.81" drill="0.8128" shape="offset" rot="R270"/>
<pad name="3" x="1.27" y="3.81" drill="0.8128" shape="offset" rot="R90"/>
<pad name="4" x="-1.27" y="3.81" drill="0.8128" shape="offset" rot="R90"/>
<text x="4.191" y="-2.921" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-2.921" y="-2.667" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="OK-LD">
<wire x1="-6.985" y1="5.08" x2="9.525" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="-5.08" x2="9.525" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="9.525" y1="5.08" x2="9.525" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="-6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.175" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-1.143" x2="3.429" y2="0.254" width="0.1524" layer="94"/>
<wire x1="3.429" y1="0.254" x2="2.54" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.127" x2="3.048" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-0.635" x2="3.429" y2="0.254" width="0.1524" layer="94"/>
<wire x1="3.302" y1="1.397" x2="2.413" y2="1.016" width="0.1524" layer="94"/>
<wire x1="2.413" y1="1.016" x2="2.921" y2="0.508" width="0.1524" layer="94"/>
<wire x1="2.921" y1="0.508" x2="3.302" y2="1.397" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0" x2="3.302" y2="1.397" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-3.175" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="-3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-2.54" x2="-3.175" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="2.54" x2="-3.175" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="7.366" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="10.16" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="10.16" y2="2.54" width="0.1524" layer="94"/>
<wire x1="6.858" y1="-1.016" x2="7.366" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="7.366" y1="-2.286" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.366" y1="-2.286" x2="6.096" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="6.096" y1="-1.778" x2="6.858" y2="-1.016" width="0.1524" layer="94"/>
<circle x="-3.175" y="2.54" radius="0.254" width="0.4064" layer="94"/>
<circle x="-3.175" y="-2.54" radius="0.254" width="0.4064" layer="94"/>
<text x="-6.985" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.985" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="4.699" y1="-2.54" x2="5.461" y2="2.54" layer="94"/>
<pin name="AC1" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="AC2" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="E" x="12.7" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="C" x="12.7" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SFH620" prefix="OK">
<description>&lt;b&gt;Optocoupler, Phototransistor Output, AC Input&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/83675/sfh620.pdf</description>
<gates>
<gate name="G$1" symbol="OK-LD" x="0" y="0"/>
</gates>
<devices>
<device name="6" package="DIL4-SMD">
<connects>
<connect gate="G$1" pin="AC1" pad="1"/>
<connect gate="G$1" pin="AC2" pad="2"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="VISHAY/TELEFUNKEN" constant="no"/>
<attribute name="MPN" value="SFH6206-3" constant="no"/>
<attribute name="OC_FARNELL" value="1469585" constant="no"/>
<attribute name="OC_NEWARK" value="58K2214" constant="no"/>
</technology>
</technologies>
</device>
<device name="A" package="DIL04">
<connects>
<connect gate="G$1" pin="AC1" pad="1"/>
<connect gate="G$1" pin="AC2" pad="2"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="SFH620A-2" constant="no"/>
<attribute name="OC_FARNELL" value="1469594" constant="no"/>
<attribute name="OC_NEWARK" value="97K9546" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microbuilder">
<description>&lt;h2&gt;&lt;b&gt;microBuilder.eu&lt;/b&gt; Eagle Footprint Library&lt;/h2&gt;

&lt;p&gt;Footprints for common components used in our projects and products.  This is the same library that we use internally, and it is regularly updated.  The newest version can always be found at &lt;b&gt;www.microBuilder.eu&lt;/b&gt;.  If you find this library useful, please feel free to purchase something from our online store. Please also note that all holes are optimised for metric drill bits!&lt;/p&gt;

&lt;h3&gt;Obligatory Warning&lt;/h3&gt;
&lt;p&gt;While it probably goes without saying, there are no guarantees that the footprints or schematic symbols in this library are flawless, and we make no promises of fitness for production, prototyping or any other purpose. These libraries are provided for information puposes only, and are used at your own discretion.  While we make every effort to produce accurate footprints, and many of the items found in this library have be proven in production, we can't make any promises of suitability for a specific purpose. If you do find any errors, though, please feel free to contact us at www.microbuilder.eu to let us know about it so that we can update the library accordingly!&lt;/p&gt;

&lt;h3&gt;License&lt;/h3&gt;
&lt;p&gt;This work is placed in the public domain, and may be freely used for commercial and non-commercial work with the following conditions:&lt;/p&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
&lt;/p&gt;</description>
<packages>
<package name="SMADIODE">
<description>&lt;b&gt;SMA Surface Mount Diode&lt;/b&gt;</description>
<wire x1="-2.15" y1="1.3" x2="2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.3" x2="2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.3" x2="-2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.3" x2="-2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-3.789" y1="-1.394" x2="-3.789" y2="-1.146" width="0.127" layer="21"/>
<wire x1="-3.789" y1="-1.146" x2="-3.789" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-3.789" y1="1.6" x2="3.816" y2="1.6" width="0.2032" layer="21"/>
<wire x1="3.816" y1="1.6" x2="3.816" y2="1.394" width="0.2032" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="1.3365" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.816" y1="-1.6" x2="-3.789" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.789" y1="-1.6" x2="-3.789" y2="-1.146" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.508" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0" x2="0.254" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.762" width="0.2032" layer="21"/>
<smd name="C" x="-2.3495" y="0" dx="2.54" dy="2.54" layer="1"/>
<smd name="A" x="2.3495" y="0" dx="2.54" dy="2.54" layer="1" rot="R180"/>
<text x="-2.54" y="1.905" size="0.8128" layer="25" font="vector" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="0.4064" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.825" y1="-1.1" x2="-2.175" y2="1.1" layer="51"/>
<rectangle x1="2.175" y1="-1.1" x2="2.825" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.75" y1="-1.225" x2="-1.075" y2="1.225" layer="51"/>
</package>
<package name="DO-1N4148">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9"/>
<pad name="C" x="3.81" y="0" drill="0.9"/>
<text x="-2.286" y="1.143" size="0.8128" layer="25" ratio="18">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21">&gt;Value</text>
</package>
<package name="SOT23-R">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Reflow soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5724" y1="-0.6524" x2="-1.5724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="-1.5724" y1="0.6604" x2="-0.5636" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6524" width="0.2032" layer="21"/>
<wire x1="0.5636" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="0.3724" y1="-0.6604" x2="-0.3864" y2="-0.6604" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1" dx="0.635" dy="1.016" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.635" dy="1.016" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="0.635" dy="1.016" layer="1"/>
<text x="1.778" y="-0.127" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.778" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOD-523">
<description>SOD-523 (0.8x1.2mm)

&lt;p&gt;Source: http://www.rohm.com/products/databook/di/pdf/rb751s-40.pdf&lt;/p&gt;</description>
<smd name="K" x="0" y="0.75" dx="0.8" dy="0.6" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.6" layer="1"/>
<text x="0.716" y="0.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="0.716" y="-0.724" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="0.4254" y1="0.6" x2="0.4254" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.6" x2="-0.4" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-0.4254" y1="-0.6" x2="-0.4254" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.4" y1="0.6" x2="0.4" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.05" x2="0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="0.25" y1="-0.2" x2="-0.25" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-0.25" y1="-0.2" x2="0" y2="0.05" width="0.127" layer="21"/>
<rectangle x1="-0.1" y1="0.45" x2="0.1" y2="0.85" layer="51" rot="R270"/>
<rectangle x1="-0.1" y1="-0.85" x2="0.1" y2="-0.45" layer="51" rot="R270"/>
<rectangle x1="-0.1" y1="-0.2254" x2="0.1" y2="0.5746" layer="21" rot="R270"/>
<polygon width="0.0508" layer="21">
<vertex x="0" y="0.05"/>
<vertex x="0.25" y="-0.2"/>
<vertex x="-0.25" y="-0.2"/>
</polygon>
</package>
<package name="SOD-323">
<description>&lt;b&gt;SOD323&lt;/b&gt; (2.5x1.2mm)</description>
<smd name="C" x="-1.27" y="0" dx="1.35" dy="0.8" layer="1"/>
<smd name="A" x="1.27" y="0" dx="1.35" dy="0.8" layer="1"/>
<text x="-1.1" y="1" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.1" y="-1.792" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-1" y1="0.7" x2="1" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1" y1="0.7" x2="1" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.7" x2="-1" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.2032" layer="51"/>
<wire x1="-0.25" y1="0" x2="0.35" y2="0.4" width="0.2032" layer="21"/>
<wire x1="0.35" y1="0.4" x2="0.35" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="0.35" y1="-0.4" x2="-0.25" y2="0" width="0.2032" layer="21"/>
<rectangle x1="-0.45" y1="-0.5" x2="-0.25" y2="0.5" layer="21"/>
<polygon width="0.2032" layer="21">
<vertex x="-0.1" y="0"/>
<vertex x="0.2" y="0.2"/>
<vertex x="0.2" y="-0.2"/>
</polygon>
</package>
<package name="SOD-123">
<description>&lt;b&gt;SOD-123&lt;/b&gt;
&lt;p&gt;Source: http://www.diodes.com/datasheets/ds30139.pdf&lt;/p&gt;</description>
<smd name="C" x="-1.85" y="0" dx="1.4" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.85" y="0" dx="1.4" dy="1.4" layer="1" rot="R90"/>
<text x="-1.27" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-0.873" y1="0.7" x2="0.873" y2="0.7" width="0.2032" layer="21"/>
<wire x1="0.873" y1="0.7" x2="0.873" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="0.873" y1="-0.7" x2="-0.873" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-0.873" y1="-0.7" x2="-0.873" y2="0.7" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="0.4" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="0.3" y1="-0.4" x2="-0.3" y2="0" width="0.2032" layer="21"/>
<rectangle x1="-1.723" y1="-0.45" x2="-0.973" y2="0.4" layer="51"/>
<rectangle x1="0.973" y1="-0.45" x2="1.723" y2="0.4" layer="51"/>
<rectangle x1="-0.5" y1="-0.5" x2="-0.3" y2="0.5" layer="21"/>
<polygon width="0.2032" layer="21">
<vertex x="-0.1" y="0"/>
<vertex x="0.2" y="0.2"/>
<vertex x="0.2" y="-0.2"/>
</polygon>
</package>
<package name="SOT23-WIDE">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.2032" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.2032" layer="51"/>
<wire x1="-1.6724" y1="-0.6524" x2="-1.6724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="-1.6724" y1="0.6604" x2="-0.7136" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="1.6724" y1="0.6604" x2="1.6724" y2="-0.6524" width="0.2032" layer="21"/>
<wire x1="0.7136" y1="0.6604" x2="1.6724" y2="0.6604" width="0.2032" layer="21"/>
<wire x1="0.2224" y1="-0.6604" x2="-0.2364" y2="-0.6604" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1" dx="1" dy="1.27" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="1" dy="1.27" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="1" dy="1.27" layer="1"/>
<text x="1.905" y="0" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1.905" y="-0.635" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="DFN1006-2">
<smd name="C" x="0.35775" y="0" dx="0.65" dy="0.36" layer="1" rot="R270"/>
<smd name="A" x="-0.35775" y="0" dx="0.65" dy="0.36" layer="1" rot="R270"/>
<text x="1.905" y="-0.6985" size="0.8128" layer="25" ratio="18" rot="R180">&gt;NAME</text>
<text x="1.27" y="1.143" size="0.4064" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<wire x1="0.746" y1="0.22375" x2="0.746" y2="-0.2555" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="-0.4" x2="-0.3" y2="0.4" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="0.3" y2="0" width="0.2032" layer="51"/>
<rectangle x1="0.2365" y1="-0.5" x2="0.4365" y2="0.5" layer="51" rot="R180"/>
<polygon width="0.2032" layer="51">
<vertex x="0.1" y="0"/>
<vertex x="-0.2" y="-0.2"/>
<vertex x="-0.2" y="0.2"/>
</polygon>
</package>
<package name="SOLDERJUMPER_CLOSEDWIRE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="WIRE" x="0" y="0" dx="0.635" dy="0.2032" layer="1" cream="no"/>
<text x="-1.651" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SOLDERJUMPER_CLOSEDPERM">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="WIRE" x="0" y="0" dx="0.635" dy="1.6002" layer="1" cream="no"/>
<text x="-1.651" y="1.143" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SOLDERJUMPER_REFLOW">
<description>&lt;b&gt;Solder Jumper&lt;/b&gt; - Reflow</description>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SOLDERJUMPER_WAVE">
<description>&lt;b&gt;Solder Jumper&lt;/b&gt; - Wave</description>
<wire x1="1.905" y1="-1.524" x2="-1.905" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.524" x2="2.159" y2="1.27" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2.159" y1="1.27" x2="-1.905" y2="1.524" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-1.27" x2="-1.905" y2="-1.524" width="0.2032" layer="21" curve="90"/>
<wire x1="1.905" y1="-1.524" x2="2.159" y2="-1.27" width="0.2032" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.27" x2="2.159" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-1.27" x2="-2.159" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.524" x2="1.905" y2="1.524" width="0.2032" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.524" y1="0" x2="2.032" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.524" y1="0" x2="-2.032" y2="0" width="0.2032" layer="51"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.2032" layer="51" curve="-180"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.2032" layer="51" curve="180"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-2.159" y="1.778" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="1" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="0.762" y1="-0.762" x2="1.016" y2="0.762" layer="51"/>
<rectangle x1="1.016" y1="-0.635" x2="1.27" y2="0.635" layer="51"/>
<rectangle x1="1.27" y1="-0.508" x2="1.397" y2="0.508" layer="51"/>
<rectangle x1="1.397" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.016" y1="-0.762" x2="-0.762" y2="0.762" layer="51"/>
<rectangle x1="-1.27" y1="-0.635" x2="-1.016" y2="0.635" layer="51"/>
<rectangle x1="-1.397" y1="-0.508" x2="-1.27" y2="0.508" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.397" y2="0.254" layer="51"/>
<rectangle x1="0.9652" y1="-0.7112" x2="1.0922" y2="-0.5842" layer="51"/>
<rectangle x1="1.3462" y1="-0.3556" x2="1.4732" y2="-0.2286" layer="51"/>
<rectangle x1="1.3462" y1="0.2032" x2="1.4732" y2="0.3302" layer="51"/>
<rectangle x1="0.9652" y1="0.5842" x2="1.0922" y2="0.7112" layer="51"/>
<rectangle x1="-1.0922" y1="-0.7112" x2="-0.9652" y2="-0.5842" layer="51"/>
<rectangle x1="-1.4478" y1="-0.3302" x2="-1.3208" y2="-0.2032" layer="51"/>
<rectangle x1="-1.4732" y1="0.2032" x2="-1.3462" y2="0.3302" layer="51"/>
<rectangle x1="-1.1176" y1="0.5842" x2="-0.9906" y2="0.7112" layer="51"/>
</package>
<package name="SOLDERJUMPER_REFLOW_NOPASTE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1" cream="no"/>
<text x="-1.651" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SOLDERJUMPER_ARROW_NOPASTE">
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.2032" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.2032" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.2032" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="0.762" dy="1.524" layer="1" roundness="50" stop="no" cream="no"/>
<smd name="2" x="1.016" y="0" dx="0.762" dy="1.524" layer="1" roundness="50" stop="no" cream="no"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.651" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<polygon width="0.0508" layer="1">
<vertex x="-1.143" y="0.7366"/>
<vertex x="-0.5715" y="0.7366"/>
<vertex x="0.1651" y="0"/>
<vertex x="-0.5715" y="-0.7366"/>
<vertex x="-1.143" y="-0.7366"/>
</polygon>
<polygon width="0.0508" layer="1">
<vertex x="-0.127" y="0.7366"/>
<vertex x="0.5842" y="0"/>
<vertex x="-0.127" y="-0.7366"/>
<vertex x="1.143" y="-0.7366"/>
<vertex x="1.143" y="0.7366"/>
</polygon>
<rectangle x1="-1.4605" y1="-0.8255" x2="1.4605" y2="0.8255" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.524" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="3.3V">
<wire x1="-1.27" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-1.524" y="1.016" size="1.27" layer="96">&gt;VALUE</text>
<pin name="3.3V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="SOLDERJUMPER_CLOSED">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.4064" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="SOLDERJUMPER">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND">
<description>&lt;b&gt;GND&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V">
<description>&lt;b&gt;3.3V Supply&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Diode&lt;/b&gt;
&lt;p&gt;
&lt;h3&gt;SMA&lt;/h3&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Digikey #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;SSA34-E3&lt;/td&gt;
  &lt;td&gt;40V&lt;/td&gt;
  &lt;td&gt;3A&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;480mV @ 3A&lt;/td&gt;
  &lt;td&gt;200uA @ 40V&lt;/td&gt;
  &lt;td&gt;SSA34-E3/61TGITR-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;CDBA120-G&lt;/td&gt;
  &lt;td&gt;20V&lt;/td&gt;
  &lt;td&gt;1A&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;500mV @ 1A&lt;/td&gt;
  &lt;td&gt;500uA @ 20V&lt;/td&gt;
  &lt;td&gt;641-1014-6-ND&lt;/td&gt;
  &lt;td&gt;REEL&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;MBRA210&lt;/td&gt;
  &lt;td&gt;10V&lt;/td&gt;
  &lt;td&gt;2A&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;350mV @ 2A&lt;/td&gt;
  &lt;td&gt;700uA @ 10V&lt;/td&gt;
  &lt;td&gt;MBRA210LT3&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;h3&gt;SOD-123&lt;/h3&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Order #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;BAT54T1G&lt;/td&gt;
  &lt;td&gt;30V&lt;/td&gt;
  &lt;td&gt;200mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;800mV @ 200mA&lt;/td&gt;
  &lt;td&gt;2uA @ 25V&lt;/td&gt;
  &lt;td&gt;BAT54T1GOSTR-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;B0530W&lt;/td&gt;
  &lt;td&gt;30V&lt;/td&gt;
  &lt;td&gt;500mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;430mV @ 500mA&lt;/td&gt;
  &lt;td&gt;130uA @ 30V&lt;/td&gt;
  &lt;td&gt;B0530W-FDICT-ND&lt;/td&gt;
  &lt;td&gt;REEL&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;MBR120&lt;/td&gt;
  &lt;td&gt;1A&lt;/td&gt;
  &lt;td&gt;20V&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;340mV @ 1A&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;MBR120VLSFT1GOSCT-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;h3&gt;SOD-323&lt;/h3&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Order #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;PMEG2005EJ&lt;/td&gt;
  &lt;td&gt;20V&lt;/td&gt;
  &lt;td&gt;500mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;355mV @ 500mA&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;568-4110-1-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;ZLLS410&lt;/td&gt;
  &lt;td&gt;10V&lt;/td&gt;
  &lt;td&gt;570mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;380mV @ 570mA&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;ZLLS410CT-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;1N4148WS&lt;/td&gt;
  &lt;td&gt;75V&lt;/td&gt;
  &lt;td&gt;150mA&lt;/td&gt;
  &lt;td&gt;Silicon/Simple&lt;/td&gt;
  &lt;td&gt;1V&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;1N4148WSFSCT-ND&lt;/td&gt;
  &lt;td&gt;REEL&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;h3&gt;SOD-523&lt;/h3&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Order #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;BAT54XV2&lt;/td&gt;
  &lt;td&gt;30V&lt;/td&gt;
  &lt;td&gt;200mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;0.8V @ 100mA&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;BAT54XV2CT-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;TB751S&lt;/td&gt;
  &lt;td&gt;30V&lt;/td&gt;
  &lt;td&gt;30mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;RB751S-40TE61CT-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;h3&gt;SOT23-R/W&lt;/h3&gt;(R = Solder Paste/Reflow Ovens, W = Hand-Soldering)
&lt;br/&gt;
&lt;table cellpadding="5" width="600"&gt;
&lt;tr bgcolor="#DDDDDD"&gt;
  &lt;td&gt;&lt;b&gt;Model&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Volts&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Amps&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Type&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vf&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Vr&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Order #&lt;/b&gt;&lt;/td&gt;
  &lt;td&gt;&lt;b&gt;Notes&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
  &lt;td&gt;BAT54FILM&lt;/td&gt;
  &lt;td&gt;40V&lt;/td&gt;
  &lt;td&gt;300mA&lt;/td&gt;
  &lt;td&gt;Schottky&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
  &lt;td&gt;497-7162-1-ND&lt;/td&gt;
  &lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="SMADIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO-1N4148" package="DO-1N4148">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23_REFLOW" package="SOT23-R">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-523" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23_WIDE" package="SOT23-WIDE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DFN1006" package="DFN1006-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDERJUMPER_CLOSED" prefix="SJ" uservalue="yes">
<description>&lt;b&gt;Solder Jumper - Closed&lt;/b&gt;
&lt;p&gt;These solder jumpers are closed by default&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;CLOSEDWIRE&lt;/b&gt; - 10 mil trace between pads that can be cut and resoldered later&lt;/li&gt;
&lt;li&gt;&lt;b&gt;CLOSEDPERM&lt;/b&gt; - 63 mil trace between pads for a permanent connection (used to 'bridge' two signals, a shameless hack for Eagle).  Can double as a test point as well (no cream layer)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="SOLDERJUMPER_CLOSED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOLDERJUMPER_CLOSEDWIRE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PERM" package="SOLDERJUMPER_CLOSEDPERM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDERJUMPER" prefix="SJ" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;SMD Solder JUMPER&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;Solder the two pads together to create a connection, or remove the solder to break it.&lt;/p&gt;
&lt;b&gt;REFLOW&lt;/b&gt; - Use this footprint for solder paste and reflow ovens.&lt;br/&gt;
&lt;b&gt;WAVE&lt;/b&gt; - Use this footprint for hand-soldering (larger pads).
&lt;p&gt;&lt;b&gt;CLOSED&lt;/b&gt; - Has a trace between the two pads to ensure it is closed by default.  The trace needs to be cut to disable the jumper, and can be closed again by creating a solder bridge between the two pads.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="SOLDERJUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="REFLOW" package="SOLDERJUMPER_REFLOW">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WAVE" package="SOLDERJUMPER_WAVE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CLOSED" package="SOLDERJUMPER_CLOSEDWIRE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="REFLOW_NOPASTE" package="SOLDERJUMPER_REFLOW_NOPASTE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SOLDERJUMPER_ARROW_NOPASTE">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerIC">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find drivers, regulators, and amplifiers.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1.27" y1="0.4294" x2="1.27" y2="-0.4294" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="-0.4294" x2="-1.27" y2="0.4294" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.2684" y1="0.7088" x2="0.2684" y2="0.7088" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1524" layer="51"/>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<smd name="1" x="-0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<text x="-0.889" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<circle x="-1.6002" y="-1.016" radius="0.127" width="0" layer="21"/>
</package>
<package name="SC70">
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="21"/>
<smd name="1" x="-0.65" y="-0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<smd name="2" x="0" y="-0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<smd name="3" x="0.65" y="-0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<smd name="4" x="0.65" y="0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<smd name="5" x="-0.65" y="0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<text x="-0.889" y="1.524" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.254" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.85" y1="0.45" x2="-0.45" y2="1.2" layer="31"/>
<rectangle x1="0.45" y1="0.45" x2="0.85" y2="1.2" layer="31"/>
<rectangle x1="-0.8382" y1="-1.1684" x2="-0.508" y2="-0.4826" layer="31"/>
<rectangle x1="-0.1651" y1="-1.1684" x2="0.1651" y2="-0.4826" layer="31"/>
<rectangle x1="0.508" y1="-1.1684" x2="0.8382" y2="-0.4826" layer="31"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="1" y2="-0.625" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="V-REG-LDO">
<wire x1="-7.62" y1="-7.62" x2="5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-7.62" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<text x="-7.62" y="9.144" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-11.43" size="1.778" layer="96">&gt;VALUE</text>
<pin name="IN" x="-10.16" y="5.08" visible="pin" length="short" direction="in"/>
<pin name="GND" x="-10.16" y="-5.08" visible="pin" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="5.08" visible="pin" length="short" direction="pas" rot="R180"/>
<pin name="EN" x="-10.16" y="0" visible="pin" length="short" direction="in"/>
<pin name="BP" x="7.62" y="-5.08" visible="pin" length="short" direction="in" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="V_REG_LDO" prefix="U">
<description>&lt;b&gt;Voltage Regulator LDO&lt;/b&gt;
Standard 150mA LDO voltage regulator in SOT-23 layout. Micrel part MIC5205. BP (by-pass) pin is used to lower output noise with 470pF cap.</description>
<gates>
<gate name="G$1" symbol="V-REG-LDO" x="2.54" y="0"/>
</gates>
<devices>
<device name="SMD" package="SOT23-5">
<connects>
<connect gate="G$1" pin="BP" pad="4"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="SC70">
<connects>
<connect gate="G$1" pin="BP" pad="4"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="VREG-08428" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="inductors">
<description>&lt;b&gt;Inductors and Filters&lt;/b&gt;&lt;p&gt;
Based on the previous library ind-a.lbr&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="0402">
<description>&lt;b&gt;EMIFIL (R) Chip Ferrite Bead for GHz Noise&lt;/b&gt;&lt;p&gt;
Source: http://www.murata.com/ Ferrite Bead BLM15H.pdf</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.55" y="0" dx="0.5" dy="0.9" layer="1"/>
<smd name="2" x="0.55" y="0" dx="0.5" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="L">
<text x="-3.81" y="1.3716" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-0.889" x2="2.54" y2="0.889" layer="94"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BLM15H" prefix="L">
<description>&lt;b&gt;EMIFIL (R) Chip Ferrite Bead for GHz Noise&lt;/b&gt;&lt;p&gt;
Source: http://www.murata.com/ Ferrite Bead BLM15H.pdf</description>
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="B121SN1"/>
<technology name="B221SN1"/>
<technology name="D102SN1"/>
<technology name="D182SN1"/>
<technology name="D601SN1"/>
<technology name="G102SN1"/>
<technology name="G601SN1"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LowPowerLab">
<packages>
<package name="AXIAL-1/4W">
<pad name="P$1" x="-3.81" y="0" drill="0.8"/>
<pad name="P$2" x="3.81" y="0" drill="0.8"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
SMD</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0" layer="48"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0" layer="48"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0" layer="48"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.254" layer="21"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.55" y="0" dx="0.5" dy="0.6" layer="1" roundness="30"/>
<smd name="2" x="0.55" y="0" dx="0.5" dy="0.6" layer="1" roundness="30"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.254" layer="21"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1.016" dy="0.9652" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.016" dy="0.9652" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="0805">
<rectangle x1="-1" y1="-0.625" x2="1" y2="0.625" layer="51"/>
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1" roundness="25"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1" roundness="25"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0805-RES">
<wire x1="-0.1" y1="0.7" x2="-0.1" y2="0.5" width="0.2" layer="121"/>
<wire x1="-0.1" y1="0.5" x2="0.3" y2="0.2" width="0.2" layer="121"/>
<wire x1="0.3" y1="0.2" x2="-0.3" y2="0" width="0.2" layer="121"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="-0.3" width="0.2" layer="121"/>
<wire x1="0.3" y1="-0.3" x2="-0.1" y2="-0.5" width="0.2" layer="121"/>
<wire x1="-0.1" y1="-0.5" x2="-0.1" y2="-0.7" width="0.2" layer="121"/>
<rectangle x1="-1" y1="-0.625" x2="1" y2="0.625" layer="51"/>
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1" roundness="25"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1" roundness="25"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="RES_UPRIGHT">
<wire x1="-2.5025" y1="0.977" x2="-2.5025" y2="-0.977" width="0.2032" layer="21"/>
<wire x1="-2.5025" y1="-0.977" x2="2.5025" y2="-0.977" width="0.2032" layer="21"/>
<wire x1="2.5025" y1="-0.977" x2="2.5025" y2="0.977" width="0.2032" layer="21"/>
<wire x1="2.5025" y1="0.977" x2="-2.5025" y2="0.977" width="0.2032" layer="21"/>
<pad name="1" x="-1.5475" y="0" drill="0.762" diameter="1.524"/>
<pad name="2" x="1.5475" y="0" drill="0.762" diameter="1.524"/>
<text x="-1.5837" y="1.2727" size="0.6096" layer="25">&gt;NAME</text>
<text x="-2.058" y="-2.0161" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1/4W" package="AXIAL-1/4W">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="UPRIGHT" package="RES_UPRIGHT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="OPL_Optoelectronics">
<description>&lt;b&gt;Seeed Open Parts Library (OPL) for the Seeed Fusion PCB Assembly Service
&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/opl.html" title="https://www.seeedstudio.com/opl.html"&gt;Seeed Fusion PCBA OPL&lt;/a&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/fusion_pcb.html"&gt;Order PCB/PCBA&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com"&gt;www.seeedstudio.com&lt;/a&gt;
&lt;br&gt;&lt;/b&gt;</description>
<packages>
<package name="LED-0402">
<smd name="+" x="-0.4625" y="0" dx="0.5" dy="0.5" layer="1" roundness="50" rot="R90"/>
<smd name="-" x="0.4625" y="0" dx="0.5" dy="0.5" layer="1" roundness="50" rot="R90"/>
<text x="-1.905" y="0.635" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-1.27" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-0.762" y1="-0.4445" x2="-0.889" y2="-0.3175" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.889" y1="-0.3175" x2="-0.889" y2="0.3175" width="0.0762" layer="21"/>
<wire x1="-0.889" y1="0.3175" x2="-0.762" y2="0.4445" width="0.0762" layer="21" curve="-90"/>
<wire x1="-0.762" y1="0.4445" x2="0.5715" y2="0.4445" width="0.0762" layer="21"/>
<wire x1="0.5715" y1="0.4445" x2="0.889" y2="0.127" width="0.0762" layer="21"/>
<wire x1="0.889" y1="0.127" x2="0.889" y2="-0.127" width="0.0762" layer="21"/>
<wire x1="0.889" y1="-0.127" x2="0.5715" y2="-0.4445" width="0.0762" layer="21"/>
<wire x1="0.5715" y1="-0.4445" x2="-0.762" y2="-0.4445" width="0.0762" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED-1">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.143" y2="2.413" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="0.889" y2="4.445" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.127" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.524" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="1.397" y2="3.048" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-LED-CLEAR-BLUE(0402)" prefix="D" uservalue="yes">
<description>304090075</description>
<gates>
<gate name="G$1" symbol="LED-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-0402">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="BL-HB337A-AV-TRB" constant="no"/>
<attribute name="VALUE" value="BLUE-0402" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="USB-C-16P-4LAYER-ISOLATED">
<smd name="B6" x="0.75" y="3.47" dx="0.3" dy="1" layer="1"/>
<smd name="A7" x="0.25" y="3.47" dx="0.3" dy="1" layer="1"/>
<smd name="GND2" x="3.225" y="3.47" dx="0.6" dy="1" layer="1"/>
<smd name="VBUS2" x="2.45" y="3.47" dx="0.6" dy="1" layer="1"/>
<smd name="B5" x="1.75" y="3.47" dx="0.3" dy="1" layer="1"/>
<smd name="A8" x="1.25" y="3.47" dx="0.3" dy="1" layer="1"/>
<smd name="B7" x="-0.75" y="3.47" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="A6" x="-0.25" y="3.47" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="GND" x="-3.225" y="3.47" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="VBUS1" x="-2.45" y="3.47" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="B8" x="-1.75" y="3.47" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="A5" x="-1.25" y="3.47" dx="0.3" dy="1" layer="1" rot="R180"/>
<hole x="-2.89" y="2.365" drill="0.65"/>
<hole x="2.89" y="2.365" drill="0.65"/>
<wire x1="-4.62" y1="-1.59" x2="-4.62" y2="-0.97" width="0.01" layer="46"/>
<wire x1="-4.02" y1="-1.59" x2="-4.02" y2="-0.97" width="0.01" layer="46"/>
<wire x1="-4.62" y1="-0.97" x2="-4.02" y2="-0.97" width="0.01" layer="46" curve="-180"/>
<wire x1="-4.02" y1="-1.59" x2="-4.62" y2="-1.59" width="0.01" layer="46" curve="-180"/>
<wire x1="-4.32" y1="-0.485" x2="-4.32" y2="-2.085" width="0.01" layer="51"/>
<wire x1="-4.82" y1="-1.285" x2="-3.82" y2="-1.285" width="0.01" layer="51"/>
<wire x1="-4.62" y1="-1.285" x2="-4.02" y2="-1.285" width="0.01" layer="52"/>
<wire x1="-4.32" y1="-0.685" x2="-4.32" y2="-1.885" width="0.01" layer="52"/>
<wire x1="-4.82" y1="-1.585" x2="-3.82" y2="-1.585" width="0.01" layer="51"/>
<wire x1="-4.82" y1="-0.985" x2="-3.82" y2="-0.985" width="0.01" layer="51"/>
<wire x1="-4.62" y1="2.345" x2="-4.62" y2="3.445" width="0.01" layer="46"/>
<wire x1="-4.02" y1="2.345" x2="-4.02" y2="3.445" width="0.01" layer="46"/>
<wire x1="-4.02" y1="2.345" x2="-4.62" y2="2.345" width="0.01" layer="46" curve="-180"/>
<wire x1="-4.82" y1="2.895" x2="-3.82" y2="2.895" width="0.01" layer="51"/>
<wire x1="-4.62" y1="2.895" x2="-4.02" y2="2.895" width="0.01" layer="52"/>
<wire x1="-4.82" y1="2.295" x2="-3.82" y2="2.295" width="0.01" layer="51"/>
<wire x1="-4.82" y1="3.495" x2="-3.82" y2="3.495" width="0.01" layer="51"/>
<wire x1="-4.62" y1="3.445" x2="-4.02" y2="3.445" width="0.01" layer="46" curve="-180"/>
<wire x1="4.32" y1="3.745" x2="4.32" y2="2.045" width="0.01" layer="52"/>
<wire x1="4.02" y1="-1.59" x2="4.02" y2="-0.97" width="0.01" layer="46"/>
<wire x1="4.62" y1="-1.59" x2="4.62" y2="-0.97" width="0.01" layer="46"/>
<wire x1="4.02" y1="-0.97" x2="4.62" y2="-0.97" width="0.01" layer="46" curve="-180"/>
<wire x1="4.62" y1="-1.59" x2="4.02" y2="-1.59" width="0.01" layer="46" curve="-180"/>
<wire x1="4.32" y1="-0.485" x2="4.32" y2="-2.085" width="0.01" layer="51"/>
<wire x1="3.82" y1="-1.285" x2="4.82" y2="-1.285" width="0.01" layer="51"/>
<wire x1="4.02" y1="-1.285" x2="4.62" y2="-1.285" width="0.01" layer="52"/>
<wire x1="4.32" y1="-0.685" x2="4.32" y2="-1.885" width="0.01" layer="52"/>
<wire x1="3.82" y1="-1.585" x2="4.82" y2="-1.585" width="0.01" layer="51"/>
<wire x1="3.82" y1="-0.985" x2="4.82" y2="-0.985" width="0.01" layer="51"/>
<wire x1="4.02" y1="2.345" x2="4.02" y2="3.445" width="0.01" layer="46"/>
<wire x1="4.62" y1="2.345" x2="4.62" y2="3.445" width="0.01" layer="46"/>
<wire x1="4.62" y1="2.345" x2="4.02" y2="2.345" width="0.01" layer="46" curve="-180"/>
<wire x1="3.82" y1="2.895" x2="4.82" y2="2.895" width="0.01" layer="51"/>
<wire x1="4.02" y1="2.895" x2="4.62" y2="2.895" width="0.01" layer="52"/>
<wire x1="3.82" y1="2.295" x2="4.82" y2="2.295" width="0.01" layer="51"/>
<wire x1="3.82" y1="3.495" x2="4.82" y2="3.495" width="0.01" layer="51"/>
<wire x1="4.02" y1="3.445" x2="4.62" y2="3.445" width="0.01" layer="46" curve="-180"/>
<wire x1="-4.32" y1="3.945" x2="-4.32" y2="3.465" width="0.01" layer="51"/>
<wire x1="-4.32" y1="3.465" x2="-4.32" y2="1.845" width="0.01" layer="51"/>
<wire x1="-4.32" y1="3.745" x2="-4.32" y2="2.045" width="0.01" layer="52"/>
<wire x1="4.32" y1="3.945" x2="4.32" y2="3.465" width="0.01" layer="51"/>
<wire x1="4.32" y1="3.465" x2="4.32" y2="1.845" width="0.01" layer="51"/>
<wire x1="-4.32" y1="-3.885" x2="4.32" y2="-3.885" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-3.885" x2="-4.32" y2="3.465" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="3.465" x2="4.32" y2="3.465" width="0.1524" layer="51"/>
<wire x1="4.32" y1="3.465" x2="4.32" y2="-3.885" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-3.89" x2="4.32" y2="-3.89" width="0.1524" layer="51" curve="-21.282614"/>
<wire x1="-4.32" y1="1.61" x2="-4.32" y2="-0.19" width="0.2032" layer="21"/>
<wire x1="4.32" y1="1.61" x2="4.32" y2="-0.19" width="0.2032" layer="21"/>
<polygon width="0.01" layer="31">
<vertex x="-4.82" y="2.29"/>
<vertex x="-4.82" y="3.49" curve="-90"/>
<vertex x="-4.35" y="3.96" curve="-90"/>
<vertex x="-3.82" y="3.49"/>
<vertex x="-3.82" y="2.29" curve="-90"/>
<vertex x="-4.35" y="1.86" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="3.82" y="2.24"/>
<vertex x="3.82" y="3.44" curve="-90"/>
<vertex x="4.3" y="3.96" curve="-90"/>
<vertex x="4.82" y="3.49"/>
<vertex x="4.82" y="2.29" curve="-90"/>
<vertex x="4.3" y="1.86" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="3.82" y="-1.64"/>
<vertex x="3.82" y="-1.04" curve="-90"/>
<vertex x="4.3" y="-0.49" curve="-90"/>
<vertex x="4.82" y="-0.99"/>
<vertex x="4.82" y="-1.59" curve="-90"/>
<vertex x="4.35" y="-2.09" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="-4.82" y="-1.59"/>
<vertex x="-4.82" y="-0.99" curve="-90"/>
<vertex x="-4.3" y="-0.49" curve="-90"/>
<vertex x="-3.82" y="-0.99"/>
<vertex x="-3.82" y="-1.59" curve="-90"/>
<vertex x="-4.3" y="-2.09" curve="-90"/>
</polygon>
<text x="-2.54" y="5.08" size="0.762" layer="25" align="center">&gt;Name</text>
<text x="-2.54" y="-5.08" size="0.762" layer="27" align="center">&gt;Value</text>
<smd name="SHLD1" x="-4.32" y="2.895" dx="1" dy="2.1" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="SHLD2" x="4.32" y="2.895" dx="1" dy="2.1" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="SHLD3" x="-4.32" y="2.895" dx="1" dy="2.1" layer="16" roundness="100" stop="no" cream="no"/>
<smd name="SHLD4" x="4.32" y="2.895" dx="1" dy="2.1" layer="16" roundness="100" stop="no" cream="no"/>
<smd name="SHLD5" x="-4.32" y="-1.285" dx="1" dy="1.6" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<smd name="SHLD6" x="-4.32" y="-1.285" dx="1" dy="1.6" layer="16" roundness="100" stop="no" cream="no"/>
<smd name="SHLD7" x="4.32" y="-1.285" dx="1" dy="1.6" layer="16" roundness="100" stop="no" cream="no"/>
<smd name="SHLD8" x="4.32" y="-1.285" dx="1" dy="1.6" layer="1" roundness="100" rot="R180" stop="no" cream="no"/>
<rectangle x1="3.025" y1="3.215" x2="3.425" y2="3.715" layer="51"/>
<rectangle x1="2.25" y1="3.215" x2="2.65" y2="3.715" layer="51"/>
<rectangle x1="1.65" y1="3.21" x2="1.85" y2="3.71" layer="51"/>
<rectangle x1="1.15" y1="3.21" x2="1.35" y2="3.71" layer="51"/>
<rectangle x1="0.65" y1="3.215" x2="0.85" y2="3.715" layer="51"/>
<rectangle x1="0.15" y1="3.215" x2="0.35" y2="3.715" layer="51"/>
<rectangle x1="-0.35" y1="3.215" x2="-0.15" y2="3.715" layer="51"/>
<rectangle x1="-0.85" y1="3.215" x2="-0.65" y2="3.715" layer="51"/>
<rectangle x1="-1.35" y1="3.215" x2="-1.15" y2="3.715" layer="51"/>
<rectangle x1="-1.85" y1="3.215" x2="-1.65" y2="3.715" layer="51"/>
<rectangle x1="-2.65" y1="3.215" x2="-2.25" y2="3.715" layer="51"/>
<rectangle x1="-3.425" y1="3.215" x2="-3.025" y2="3.715" layer="51"/>
<polygon width="0.01" layer="29">
<vertex x="-4.92" y="2.29"/>
<vertex x="-4.92" y="3.49" curve="-90"/>
<vertex x="-4.32" y="4.045" curve="-90"/>
<vertex x="-3.72" y="3.49"/>
<vertex x="-3.72" y="2.29" curve="-90"/>
<vertex x="-4.32" y="1.745" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="3.72" y="2.29"/>
<vertex x="3.72" y="3.49" curve="-90"/>
<vertex x="4.32" y="4.045" curve="-90"/>
<vertex x="4.92" y="3.49"/>
<vertex x="4.92" y="2.29" curve="-90"/>
<vertex x="4.32" y="1.745" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="3.72" y="-1.59"/>
<vertex x="3.72" y="-0.99" curve="-90"/>
<vertex x="4.32" y="-0.385" curve="-90"/>
<vertex x="4.92" y="-0.99"/>
<vertex x="4.92" y="-1.59" curve="-90"/>
<vertex x="4.32" y="-2.185" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="-4.92" y="-1.59"/>
<vertex x="-4.92" y="-0.99" curve="-90"/>
<vertex x="-4.32" y="-0.385" curve="-90"/>
<vertex x="-3.72" y="-0.99"/>
<vertex x="-3.72" y="-1.59" curve="-90"/>
<vertex x="-4.32" y="-2.185" curve="-90"/>
</polygon>
<polygon width="0.0254" layer="2" pour="cutout">
<vertex x="-4.8" y="3.91"/>
<vertex x="-3.8" y="3.91"/>
<vertex x="-3.8" y="1.81"/>
<vertex x="-4.8" y="1.81"/>
</polygon>
<polygon width="0.0254" layer="2" pour="cutout">
<vertex x="-4.8" y="-0.49"/>
<vertex x="-3.8" y="-0.49"/>
<vertex x="-3.8" y="-2.09"/>
<vertex x="-4.8" y="-2.09"/>
</polygon>
<polygon width="0.0254" layer="2" pour="cutout">
<vertex x="3.8" y="3.91"/>
<vertex x="4.8" y="3.91"/>
<vertex x="4.8" y="1.81"/>
<vertex x="3.8" y="1.81"/>
</polygon>
<polygon width="0.0254" layer="2" pour="cutout">
<vertex x="3.8" y="-0.49"/>
<vertex x="4.8" y="-0.49"/>
<vertex x="4.8" y="-2.09"/>
<vertex x="3.8" y="-2.09"/>
</polygon>
<polygon width="0.0254" layer="15" pour="cutout">
<vertex x="-4.8" y="3.91"/>
<vertex x="-3.8" y="3.91"/>
<vertex x="-3.8" y="1.81"/>
<vertex x="-4.8" y="1.81"/>
</polygon>
<polygon width="0.0254" layer="15" pour="cutout">
<vertex x="-4.8" y="-0.49"/>
<vertex x="-3.8" y="-0.49"/>
<vertex x="-3.8" y="-2.09"/>
<vertex x="-4.8" y="-2.09"/>
</polygon>
<polygon width="0.0254" layer="15" pour="cutout">
<vertex x="3.8" y="3.91"/>
<vertex x="4.8" y="3.91"/>
<vertex x="4.8" y="1.81"/>
<vertex x="3.8" y="1.81"/>
</polygon>
<polygon width="0.0254" layer="15" pour="cutout">
<vertex x="3.8" y="-0.49"/>
<vertex x="4.8" y="-0.49"/>
<vertex x="4.8" y="-2.09"/>
<vertex x="3.8" y="-2.09"/>
</polygon>
</package>
<package name="USB-C-16P-2LAYER-PADS">
<smd name="B6" x="0.75" y="-0.34" dx="0.3" dy="1" layer="1"/>
<smd name="A7" x="0.25" y="-0.34" dx="0.3" dy="1" layer="1"/>
<smd name="GND2" x="3.225" y="-0.34" dx="0.6" dy="1" layer="1"/>
<smd name="VBUS2" x="2.45" y="-0.34" dx="0.6" dy="1" layer="1"/>
<smd name="B5" x="1.75" y="-0.34" dx="0.3" dy="1" layer="1"/>
<smd name="A8" x="1.25" y="-0.34" dx="0.3" dy="1" layer="1"/>
<smd name="B7" x="-0.75" y="-0.34" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="A6" x="-0.25" y="-0.34" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="GND" x="-3.225" y="-0.34" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="VBUS1" x="-2.45" y="-0.34" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="B8" x="-1.75" y="-0.34" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="A5" x="-1.25" y="-0.34" dx="0.3" dy="1" layer="1" rot="R180"/>
<hole x="-2.89" y="-1.445" drill="0.65"/>
<hole x="2.89" y="-1.445" drill="0.65"/>
<wire x1="-4.32" y1="-4.295" x2="-4.32" y2="-5.895" width="0.01" layer="51"/>
<wire x1="4.32" y1="-4.295" x2="4.32" y2="-5.895" width="0.01" layer="51"/>
<wire x1="-4.32" y1="-0.345" x2="-4.32" y2="-1.965" width="0.01" layer="51"/>
<wire x1="4.32" y1="-0.345" x2="4.32" y2="-1.965" width="0.01" layer="51"/>
<wire x1="-4.32" y1="-7.695" x2="4.32" y2="-7.695" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-7.695" x2="-4.32" y2="-0.345" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-0.345" x2="4.32" y2="-0.345" width="0.1524" layer="51"/>
<wire x1="4.32" y1="-0.345" x2="4.32" y2="-7.695" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-7.7" x2="4.32" y2="-7.7" width="0.1524" layer="51" curve="-21.282614"/>
<wire x1="-4.32" y1="-2.2" x2="-4.32" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.32" y1="-2.2" x2="4.32" y2="-4" width="0.2032" layer="21"/>
<polygon width="0.01" layer="31">
<vertex x="-4.82" y="-1.52"/>
<vertex x="-4.82" y="-0.32" curve="-90"/>
<vertex x="-4.35" y="0.15" curve="-90"/>
<vertex x="-3.82" y="-0.32"/>
<vertex x="-3.82" y="-1.52" curve="-90"/>
<vertex x="-4.35" y="-1.95" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="3.82" y="-1.57"/>
<vertex x="3.82" y="-0.37" curve="-90"/>
<vertex x="4.3" y="0.15" curve="-90"/>
<vertex x="4.82" y="-0.32"/>
<vertex x="4.82" y="-1.52" curve="-90"/>
<vertex x="4.3" y="-1.95" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="3.82" y="-5.45"/>
<vertex x="3.82" y="-4.85" curve="-90"/>
<vertex x="4.3" y="-4.3" curve="-90"/>
<vertex x="4.82" y="-4.8"/>
<vertex x="4.82" y="-5.4" curve="-90"/>
<vertex x="4.35" y="-5.9" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="-4.82" y="-5.4"/>
<vertex x="-4.82" y="-4.8" curve="-90"/>
<vertex x="-4.3" y="-4.3" curve="-90"/>
<vertex x="-3.82" y="-4.8"/>
<vertex x="-3.82" y="-5.4" curve="-90"/>
<vertex x="-4.3" y="-5.9" curve="-90"/>
</polygon>
<rectangle x1="3.025" y1="-0.595" x2="3.425" y2="-0.095" layer="51"/>
<rectangle x1="2.25" y1="-0.595" x2="2.65" y2="-0.095" layer="51"/>
<rectangle x1="1.65" y1="-0.6" x2="1.85" y2="-0.1" layer="51"/>
<rectangle x1="1.15" y1="-0.6" x2="1.35" y2="-0.1" layer="51"/>
<rectangle x1="0.65" y1="-0.595" x2="0.85" y2="-0.095" layer="51"/>
<rectangle x1="0.15" y1="-0.595" x2="0.35" y2="-0.095" layer="51"/>
<rectangle x1="-0.35" y1="-0.595" x2="-0.15" y2="-0.095" layer="51"/>
<rectangle x1="-0.85" y1="-0.595" x2="-0.65" y2="-0.095" layer="51"/>
<rectangle x1="-1.35" y1="-0.595" x2="-1.15" y2="-0.095" layer="51"/>
<rectangle x1="-1.85" y1="-0.595" x2="-1.65" y2="-0.095" layer="51"/>
<rectangle x1="-2.65" y1="-0.595" x2="-2.25" y2="-0.095" layer="51"/>
<rectangle x1="-3.425" y1="-0.595" x2="-3.025" y2="-0.095" layer="51"/>
<polygon width="0.01" layer="29">
<vertex x="-4.92" y="-1.52"/>
<vertex x="-4.92" y="-0.32" curve="-90"/>
<vertex x="-4.32" y="0.235" curve="-90"/>
<vertex x="-3.72" y="-0.32"/>
<vertex x="-3.72" y="-1.52" curve="-90"/>
<vertex x="-4.32" y="-2.065" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="3.72" y="-1.52"/>
<vertex x="3.72" y="-0.32" curve="-90"/>
<vertex x="4.32" y="0.235" curve="-90"/>
<vertex x="4.92" y="-0.32"/>
<vertex x="4.92" y="-1.52" curve="-90"/>
<vertex x="4.32" y="-2.065" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="3.72" y="-5.4"/>
<vertex x="3.72" y="-4.8" curve="-90"/>
<vertex x="4.32" y="-4.195" curve="-90"/>
<vertex x="4.92" y="-4.8"/>
<vertex x="4.92" y="-5.4" curve="-90"/>
<vertex x="4.32" y="-5.995" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="-4.92" y="-5.4"/>
<vertex x="-4.92" y="-4.8" curve="-90"/>
<vertex x="-4.32" y="-4.195" curve="-90"/>
<vertex x="-3.72" y="-4.8"/>
<vertex x="-3.72" y="-5.4" curve="-90"/>
<vertex x="-4.32" y="-5.995" curve="-90"/>
</polygon>
<pad name="SHLD3" x="-4.318" y="-5.095" drill="0.4" diameter="0.6" shape="long" rot="R90" stop="no"/>
<pad name="SHLD4" x="4.318" y="-5.095" drill="0.4" diameter="0.6" shape="long" rot="R90" stop="no"/>
<pad name="SHLD2" x="4.318" y="-0.915" drill="0.4" diameter="0.6" shape="long" rot="R90" stop="no"/>
<pad name="SHLD1" x="-4.318" y="-0.915" drill="0.4" diameter="0.6" shape="long" rot="R90" stop="no"/>
<polygon width="0.01" layer="1">
<vertex x="-3.818" y="-0.365"/>
<vertex x="-3.818" y="-1.465" curve="-90"/>
<vertex x="-4.318" y="-1.965" curve="-90"/>
<vertex x="-4.818" y="-1.465"/>
<vertex x="-4.818" y="-0.365" curve="-90"/>
<vertex x="-4.318" y="0.135" curve="-90"/>
</polygon>
<polygon width="0.01" layer="1">
<vertex x="4.818" y="-0.365"/>
<vertex x="4.818" y="-1.465" curve="-90"/>
<vertex x="4.318" y="-1.965" curve="-90"/>
<vertex x="3.818" y="-1.465"/>
<vertex x="3.818" y="-0.365" curve="-90"/>
<vertex x="4.318" y="0.135" curve="-90"/>
</polygon>
<polygon width="0.01" layer="16">
<vertex x="-4.818" y="-0.365"/>
<vertex x="-4.818" y="-1.465" curve="90"/>
<vertex x="-4.318" y="-1.965" curve="90"/>
<vertex x="-3.818" y="-1.465"/>
<vertex x="-3.818" y="-0.365" curve="90"/>
<vertex x="-4.318" y="0.135" curve="90"/>
</polygon>
<polygon width="0.01" layer="16">
<vertex x="3.818" y="-0.365"/>
<vertex x="3.818" y="-1.465" curve="90"/>
<vertex x="4.318" y="-1.965" curve="90"/>
<vertex x="4.818" y="-1.465"/>
<vertex x="4.818" y="-0.365" curve="90"/>
<vertex x="4.318" y="0.135" curve="90"/>
</polygon>
<polygon width="0.01" layer="1">
<vertex x="-4.318" y="-5.895" curve="-90"/>
<vertex x="-4.818" y="-5.395"/>
<vertex x="-4.818" y="-4.795" curve="-90"/>
<vertex x="-4.318" y="-4.295" curve="-90"/>
<vertex x="-3.818" y="-4.795"/>
<vertex x="-3.818" y="-5.395" curve="-90"/>
</polygon>
<polygon width="0.01" layer="16">
<vertex x="-4.818" y="-4.795"/>
<vertex x="-4.818" y="-5.395" curve="90"/>
<vertex x="-4.318" y="-5.895" curve="90"/>
<vertex x="-3.818" y="-5.395"/>
<vertex x="-3.818" y="-4.795" curve="90"/>
<vertex x="-4.318" y="-4.295" curve="90"/>
</polygon>
<polygon width="0.01" layer="16">
<vertex x="4.318" y="-5.895" curve="90"/>
<vertex x="4.818" y="-5.395"/>
<vertex x="4.818" y="-4.795" curve="90"/>
<vertex x="4.318" y="-4.295" curve="90"/>
<vertex x="3.818" y="-4.795"/>
<vertex x="3.818" y="-5.395" curve="90"/>
</polygon>
<polygon width="0.01" layer="1">
<vertex x="4.818" y="-4.795"/>
<vertex x="4.818" y="-5.395" curve="-90"/>
<vertex x="4.318" y="-5.895" curve="-90"/>
<vertex x="3.818" y="-5.395"/>
<vertex x="3.818" y="-4.795" curve="-90"/>
<vertex x="4.318" y="-4.295" curve="-90"/>
</polygon>
<text x="0" y="-2.54" size="0.762" layer="25" align="center">&gt;Name</text>
<text x="0" y="-3.81" size="0.762" layer="27" align="center">&gt;Value</text>
<wire x1="-4.318" y1="-0.365" x2="-4.318" y2="-1.465" width="0.6" layer="46"/>
<wire x1="4.318" y1="-0.365" x2="4.318" y2="-1.465" width="0.6" layer="46"/>
<wire x1="-4.318" y1="-4.78" x2="-4.318" y2="-5.4" width="0.6" layer="46"/>
<wire x1="4.318" y1="-4.78" x2="4.318" y2="-5.4" width="0.6" layer="46"/>
</package>
<package name="USB-C-16P_4LAYER-PADS">
<smd name="B6" x="0.75" y="-0.34" dx="0.3" dy="1" layer="1"/>
<smd name="A7" x="0.25" y="-0.34" dx="0.3" dy="1" layer="1"/>
<smd name="GND2" x="3.225" y="-0.34" dx="0.6" dy="1" layer="1"/>
<smd name="VBUS2" x="2.45" y="-0.34" dx="0.6" dy="1" layer="1"/>
<smd name="B5" x="1.75" y="-0.34" dx="0.3" dy="1" layer="1"/>
<smd name="A8" x="1.25" y="-0.34" dx="0.3" dy="1" layer="1"/>
<smd name="B7" x="-0.75" y="-0.34" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="A6" x="-0.25" y="-0.34" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="GND" x="-3.225" y="-0.34" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="VBUS1" x="-2.45" y="-0.34" dx="0.6" dy="1" layer="1" rot="R180"/>
<smd name="B8" x="-1.75" y="-0.34" dx="0.3" dy="1" layer="1" rot="R180"/>
<smd name="A5" x="-1.25" y="-0.34" dx="0.3" dy="1" layer="1" rot="R180"/>
<hole x="-2.89" y="-1.445" drill="0.65"/>
<hole x="2.89" y="-1.445" drill="0.65"/>
<wire x1="-4.32" y1="-4.295" x2="-4.32" y2="-5.895" width="0.01" layer="51"/>
<wire x1="4.32" y1="-4.295" x2="4.32" y2="-5.895" width="0.01" layer="51"/>
<wire x1="-4.32" y1="-0.345" x2="-4.32" y2="-1.965" width="0.01" layer="51"/>
<wire x1="4.32" y1="-0.345" x2="4.32" y2="-1.965" width="0.01" layer="51"/>
<wire x1="-4.32" y1="-7.695" x2="4.32" y2="-7.695" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-7.695" x2="-4.32" y2="-0.345" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-0.345" x2="4.32" y2="-0.345" width="0.1524" layer="51"/>
<wire x1="4.32" y1="-0.345" x2="4.32" y2="-7.695" width="0.1524" layer="51"/>
<wire x1="-4.32" y1="-7.7" x2="4.32" y2="-7.7" width="0.1524" layer="51" curve="-21.282614"/>
<wire x1="-4.32" y1="-2.2" x2="-4.32" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.32" y1="-2.2" x2="4.32" y2="-4" width="0.2032" layer="21"/>
<polygon width="0.01" layer="31">
<vertex x="-4.82" y="-1.52"/>
<vertex x="-4.82" y="-0.32" curve="-90"/>
<vertex x="-4.35" y="0.15" curve="-90"/>
<vertex x="-3.82" y="-0.32"/>
<vertex x="-3.82" y="-1.52" curve="-90"/>
<vertex x="-4.35" y="-1.95" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="3.82" y="-1.57"/>
<vertex x="3.82" y="-0.37" curve="-90"/>
<vertex x="4.3" y="0.15" curve="-90"/>
<vertex x="4.82" y="-0.32"/>
<vertex x="4.82" y="-1.52" curve="-90"/>
<vertex x="4.3" y="-1.95" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="3.82" y="-5.45"/>
<vertex x="3.82" y="-4.85" curve="-90"/>
<vertex x="4.3" y="-4.3" curve="-90"/>
<vertex x="4.82" y="-4.8"/>
<vertex x="4.82" y="-5.4" curve="-90"/>
<vertex x="4.35" y="-5.9" curve="-90"/>
</polygon>
<polygon width="0.01" layer="31">
<vertex x="-4.82" y="-5.4"/>
<vertex x="-4.82" y="-4.8" curve="-90"/>
<vertex x="-4.3" y="-4.3" curve="-90"/>
<vertex x="-3.82" y="-4.8"/>
<vertex x="-3.82" y="-5.4" curve="-90"/>
<vertex x="-4.3" y="-5.9" curve="-90"/>
</polygon>
<rectangle x1="3.025" y1="-0.595" x2="3.425" y2="-0.095" layer="51"/>
<rectangle x1="2.25" y1="-0.595" x2="2.65" y2="-0.095" layer="51"/>
<rectangle x1="1.65" y1="-0.6" x2="1.85" y2="-0.1" layer="51"/>
<rectangle x1="1.15" y1="-0.6" x2="1.35" y2="-0.1" layer="51"/>
<rectangle x1="0.65" y1="-0.595" x2="0.85" y2="-0.095" layer="51"/>
<rectangle x1="0.15" y1="-0.595" x2="0.35" y2="-0.095" layer="51"/>
<rectangle x1="-0.35" y1="-0.595" x2="-0.15" y2="-0.095" layer="51"/>
<rectangle x1="-0.85" y1="-0.595" x2="-0.65" y2="-0.095" layer="51"/>
<rectangle x1="-1.35" y1="-0.595" x2="-1.15" y2="-0.095" layer="51"/>
<rectangle x1="-1.85" y1="-0.595" x2="-1.65" y2="-0.095" layer="51"/>
<rectangle x1="-2.65" y1="-0.595" x2="-2.25" y2="-0.095" layer="51"/>
<rectangle x1="-3.425" y1="-0.595" x2="-3.025" y2="-0.095" layer="51"/>
<polygon width="0.01" layer="29">
<vertex x="-4.92" y="-1.52"/>
<vertex x="-4.92" y="-0.32" curve="-90"/>
<vertex x="-4.32" y="0.235" curve="-90"/>
<vertex x="-3.72" y="-0.32"/>
<vertex x="-3.72" y="-1.52" curve="-90"/>
<vertex x="-4.32" y="-2.065" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="3.72" y="-1.52"/>
<vertex x="3.72" y="-0.32" curve="-90"/>
<vertex x="4.32" y="0.235" curve="-90"/>
<vertex x="4.92" y="-0.32"/>
<vertex x="4.92" y="-1.52" curve="-90"/>
<vertex x="4.32" y="-2.065" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="3.72" y="-5.4"/>
<vertex x="3.72" y="-4.8" curve="-90"/>
<vertex x="4.32" y="-4.195" curve="-90"/>
<vertex x="4.92" y="-4.8"/>
<vertex x="4.92" y="-5.4" curve="-90"/>
<vertex x="4.32" y="-5.995" curve="-90"/>
</polygon>
<polygon width="0.01" layer="29">
<vertex x="-4.92" y="-5.4"/>
<vertex x="-4.92" y="-4.8" curve="-90"/>
<vertex x="-4.32" y="-4.195" curve="-90"/>
<vertex x="-3.72" y="-4.8"/>
<vertex x="-3.72" y="-5.4" curve="-90"/>
<vertex x="-4.32" y="-5.995" curve="-90"/>
</polygon>
<pad name="SHLD3" x="-4.318" y="-5.095" drill="0.4" diameter="0.6" shape="long" rot="R90" stop="no"/>
<pad name="SHLD4" x="4.318" y="-5.095" drill="0.4" diameter="0.6" shape="long" rot="R90" stop="no"/>
<pad name="SHLD2" x="4.318" y="-0.915" drill="0.4" diameter="0.6" shape="long" rot="R90" stop="no"/>
<pad name="SHLD1" x="-4.318" y="-0.915" drill="0.4" diameter="0.6" shape="long" rot="R90" stop="no"/>
<polygon width="0.01" layer="1">
<vertex x="-3.818" y="-0.365"/>
<vertex x="-3.818" y="-1.465" curve="-90"/>
<vertex x="-4.318" y="-1.965" curve="-90"/>
<vertex x="-4.818" y="-1.465"/>
<vertex x="-4.818" y="-0.365" curve="-90"/>
<vertex x="-4.318" y="0.135" curve="-90"/>
</polygon>
<polygon width="0.01" layer="1">
<vertex x="4.818" y="-0.365"/>
<vertex x="4.818" y="-1.465" curve="-90"/>
<vertex x="4.318" y="-1.965" curve="-90"/>
<vertex x="3.818" y="-1.465"/>
<vertex x="3.818" y="-0.365" curve="-90"/>
<vertex x="4.318" y="0.135" curve="-90"/>
</polygon>
<polygon width="0.01" layer="16">
<vertex x="-4.818" y="-0.365"/>
<vertex x="-4.818" y="-1.465" curve="90"/>
<vertex x="-4.318" y="-1.965" curve="90"/>
<vertex x="-3.818" y="-1.465"/>
<vertex x="-3.818" y="-0.365" curve="90"/>
<vertex x="-4.318" y="0.135" curve="90"/>
</polygon>
<polygon width="0.01" layer="16">
<vertex x="3.818" y="-0.365"/>
<vertex x="3.818" y="-1.465" curve="90"/>
<vertex x="4.318" y="-1.965" curve="90"/>
<vertex x="4.818" y="-1.465"/>
<vertex x="4.818" y="-0.365" curve="90"/>
<vertex x="4.318" y="0.135" curve="90"/>
</polygon>
<polygon width="0.01" layer="1">
<vertex x="-4.318" y="-5.895" curve="-90"/>
<vertex x="-4.818" y="-5.395"/>
<vertex x="-4.818" y="-4.795" curve="-90"/>
<vertex x="-4.318" y="-4.295" curve="-90"/>
<vertex x="-3.818" y="-4.795"/>
<vertex x="-3.818" y="-5.395" curve="-90"/>
</polygon>
<polygon width="0.01" layer="16">
<vertex x="-4.818" y="-4.795"/>
<vertex x="-4.818" y="-5.395" curve="90"/>
<vertex x="-4.318" y="-5.895" curve="90"/>
<vertex x="-3.818" y="-5.395"/>
<vertex x="-3.818" y="-4.795" curve="90"/>
<vertex x="-4.318" y="-4.295" curve="90"/>
</polygon>
<polygon width="0.01" layer="16">
<vertex x="4.318" y="-5.895" curve="90"/>
<vertex x="4.818" y="-5.395"/>
<vertex x="4.818" y="-4.795" curve="90"/>
<vertex x="4.318" y="-4.295" curve="90"/>
<vertex x="3.818" y="-4.795"/>
<vertex x="3.818" y="-5.395" curve="90"/>
</polygon>
<polygon width="0.01" layer="1">
<vertex x="4.818" y="-4.795"/>
<vertex x="4.818" y="-5.395" curve="-90"/>
<vertex x="4.318" y="-5.895" curve="-90"/>
<vertex x="3.818" y="-5.395"/>
<vertex x="3.818" y="-4.795" curve="-90"/>
<vertex x="4.318" y="-4.295" curve="-90"/>
</polygon>
<text x="0" y="-2.54" size="0.762" layer="25" align="center">&gt;Name</text>
<text x="0" y="-3.81" size="0.762" layer="27" align="center">&gt;Value</text>
<wire x1="-4.318" y1="-0.365" x2="-4.318" y2="-1.465" width="0.6" layer="46"/>
<wire x1="4.318" y1="-0.365" x2="4.318" y2="-1.465" width="0.6" layer="46"/>
<wire x1="-4.318" y1="-4.78" x2="-4.318" y2="-5.4" width="0.6" layer="46"/>
<wire x1="4.318" y1="-4.78" x2="4.318" y2="-5.4" width="0.6" layer="46"/>
<polygon width="0.001" layer="2">
<vertex x="-4.018" y="-0.365"/>
<vertex x="-4.018" y="-1.465" curve="-90"/>
<vertex x="-4.318" y="-1.765" curve="-90"/>
<vertex x="-4.618" y="-1.465"/>
<vertex x="-4.618" y="-0.365" curve="-90"/>
<vertex x="-4.318" y="-0.065" curve="-90"/>
</polygon>
<polygon width="0.001" layer="15">
<vertex x="-4.018" y="-0.365"/>
<vertex x="-4.018" y="-1.465" curve="-90"/>
<vertex x="-4.318" y="-1.765" curve="-90"/>
<vertex x="-4.618" y="-1.465"/>
<vertex x="-4.618" y="-0.365" curve="-90"/>
<vertex x="-4.318" y="-0.065" curve="-90"/>
</polygon>
<polygon width="0.001" layer="2">
<vertex x="4.618" y="-0.365"/>
<vertex x="4.618" y="-1.465" curve="-90"/>
<vertex x="4.318" y="-1.765" curve="-90"/>
<vertex x="4.018" y="-1.465"/>
<vertex x="4.018" y="-0.365" curve="-90"/>
<vertex x="4.318" y="-0.065" curve="-90"/>
</polygon>
<polygon width="0.001" layer="15">
<vertex x="4.618" y="-0.365"/>
<vertex x="4.618" y="-1.465" curve="-90"/>
<vertex x="4.318" y="-1.765" curve="-90"/>
<vertex x="4.018" y="-1.465"/>
<vertex x="4.018" y="-0.365" curve="-90"/>
<vertex x="4.318" y="-0.065" curve="-90"/>
</polygon>
<polygon width="0.001" layer="2">
<vertex x="-4.618" y="-5.4"/>
<vertex x="-4.618" y="-4.78" curve="-90"/>
<vertex x="-4.318" y="-4.48" curve="-90"/>
<vertex x="-4.018" y="-4.78"/>
<vertex x="-4.018" y="-5.4" curve="-90"/>
<vertex x="-4.318" y="-5.7" curve="-90"/>
</polygon>
<polygon width="0.001" layer="15">
<vertex x="-4.618" y="-5.4"/>
<vertex x="-4.618" y="-4.78" curve="-90"/>
<vertex x="-4.318" y="-4.48" curve="-90"/>
<vertex x="-4.018" y="-4.78"/>
<vertex x="-4.018" y="-5.4" curve="-90"/>
<vertex x="-4.318" y="-5.7" curve="-90"/>
</polygon>
<polygon width="0.001" layer="2">
<vertex x="4.318" y="-5.7" curve="-90"/>
<vertex x="4.018" y="-5.4"/>
<vertex x="4.018" y="-4.78" curve="-90"/>
<vertex x="4.318" y="-4.48" curve="-90"/>
<vertex x="4.618" y="-4.78"/>
<vertex x="4.618" y="-5.4" curve="-90"/>
</polygon>
<polygon width="0.001" layer="15">
<vertex x="4.318" y="-5.7" curve="-90"/>
<vertex x="4.018" y="-5.4"/>
<vertex x="4.018" y="-4.78" curve="-90"/>
<vertex x="4.318" y="-4.48" curve="-90"/>
<vertex x="4.618" y="-4.78"/>
<vertex x="4.618" y="-5.4" curve="-90"/>
</polygon>
</package>
<package name="1X05">
<description>&lt;h3&gt;Plated Through Hole - 5 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X05_1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 5 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;Pin pitch: 1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="-0.889" x2="4.445" y2="-0.635" width="0.127" layer="21"/>
<wire x1="4.445" y1="-0.635" x2="4.699" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.889" x2="5.461" y2="-0.889" width="0.127" layer="21"/>
<wire x1="5.461" y1="0.889" x2="4.699" y2="0.889" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.889" x2="4.445" y2="0.635" width="0.127" layer="21"/>
<wire x1="4.445" y1="0.635" x2="4.191" y2="0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="5.461" y1="0.889" x2="5.969" y2="0.381" width="0.127" layer="21"/>
<wire x1="5.969" y1="0.381" x2="5.969" y2="-0.381" width="0.127" layer="21"/>
<wire x1="5.969" y1="-0.381" x2="5.461" y2="-0.889" width="0.127" layer="21"/>
<pad name="5" x="5.08" y="0" drill="0.508" diameter="1"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.381" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X05_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 5 Pin with Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, locking pins in place during soldering.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X05_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 5 Pin Long Pad with Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, locking pins in place during soldering.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="9.144" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.127" x2="11.176" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.127" x2="11.43" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-1.1176" x2="11.1506" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.127" x2="11.43" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.8636" x2="11.1506" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.4191" x2="10.4521" y2="0.1651" layer="51"/>
<text x="-1.27" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X05_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 5 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X05_1MM">
<description>&lt;h3&gt;SMD - 5 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.sparkfun.com/datasheets/GPS/EM408-SMDConnector.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;li&gt;EM-408&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2" y1="2.921" x2="-2.08" y2="2.921" width="0.254" layer="21"/>
<wire x1="3.778" y1="0.762" x2="3.778" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.842" y1="-0.635" x2="-3.858" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-3.858" y1="-0.635" x2="-3.858" y2="0.762" width="0.254" layer="21"/>
<wire x1="3.778" y1="-0.635" x2="2.762" y2="-0.635" width="0.254" layer="21"/>
<smd name="NC2" x="3.3" y="2.225" dx="1.2" dy="1.8" layer="1" rot="R180"/>
<smd name="NC1" x="-3.3" y="2.225" dx="1.2" dy="1.8" layer="1" rot="R180"/>
<smd name="5" x="2" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="4" x="1" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="3" x="0" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="2" x="-1" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="1" x="-2" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<text x="-1.524" y="1.905" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="1.143" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<circle x="3.81" y="-1.27" radius="0" width="0.508" layer="21"/>
</package>
<package name="1X05_1MM_RA">
<description>&lt;h3&gt;SMD - 5 Pin Right Angle&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="-4.6" x2="2" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-3.5" y1="-2" x2="-3.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.75" y1="-0.35" x2="3.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="3.5" y1="-0.35" x2="3.5" y2="-2" width="0.254" layer="21"/>
<wire x1="-3.5" y1="-0.35" x2="-2.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-3" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-3.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="3.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-2" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="5" x="2" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X05_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 5 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-5-PTH">
<description>&lt;h3&gt;JST 5 Pin Right Angle Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-4" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="4" x="2" y="0" drill="0.7" diameter="1.6"/>
<pad name="5" x="4" y="0" drill="0.7" diameter="1.6"/>
<wire x1="-5.95" y1="-1.6" x2="-5.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="6" x2="5.95" y2="6" width="0.2032" layer="21"/>
<wire x1="5.95" y1="6" x2="5.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="-1.6" x2="-5.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="5.95" y1="-1.6" x2="5.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="-1.6" x2="-5.3" y2="0" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-1.6" x2="5.3" y2="0" width="0.2032" layer="21"/>
<text x="-1.397" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-5-PTH-VERT">
<description>&lt;h3&gt;JST 5 Pin Vertical Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.95" y1="-1.52" x2="-5.95" y2="2.98" width="0.2032" layer="21"/>
<wire x1="-5.95" y1="2.98" x2="5.95" y2="2.98" width="0.2032" layer="21"/>
<wire x1="5.95" y1="-1.52" x2="1" y2="-1.52" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.52" x2="-5.95" y2="-1.52" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.02" x2="1" y2="-1.02" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.02" x2="1" y2="-1.52" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.02" x2="-1" y2="-1.52" width="0.2032" layer="21"/>
<wire x1="5.95" y1="2.98" x2="5.95" y2="-1.52" width="0.2032" layer="21"/>
<pad name="1" x="-4" y="0.18" drill="0.7" diameter="1.6"/>
<pad name="2" x="-2" y="0.18" drill="0.7" diameter="1.6"/>
<pad name="3" x="0" y="0.18" drill="0.7" diameter="1.6"/>
<pad name="4" x="2" y="0.18" drill="0.7" diameter="1.6"/>
<pad name="5" x="4" y="0.18" drill="0.7" diameter="1.6"/>
<text x="-1.651" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-5">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -5 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 5&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="15.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="15.75" y1="3.4" x2="15.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="15.75" y1="-2.8" x2="15.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="15.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="15.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="15.75" y1="3.15" x2="16.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="16.25" y1="3.15" x2="16.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="16.25" y1="2.15" x2="15.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<text x="5.588" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="5.334" y="1.524" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X05_SMD_VERTICAL_COMBO">
<description>&lt;h3&gt;SMD - 4 Pin Vertical Connector&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:5&lt;/li&gt;
&lt;li&gt;SMD Pad count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="9.4726" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="9.4726" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.7928" y1="-1.29" x2="6.9072" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.7928" y1="1.25" x2="6.9072" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.2782" y1="-1.29" x2="4.3672" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<wire x1="11.53" y1="1.25" x2="11.53" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="11.53" y1="-1.25" x2="10.86" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="10.86" y1="1.25" x2="11.53" y2="1.25" width="0.1778" layer="21"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90" stop="no"/>
<smd name="3" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="4" x="7.62" y="1.652" dx="2" dy="1" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="5" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90" stop="no" cream="no"/>
<text x="-0.381" y="2.858" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-3.616" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<polygon width="0.01" layer="1">
<vertex x="2.04" y="2.65"/>
<vertex x="3.04" y="2.65"/>
<vertex x="3.04" y="0.65"/>
<vertex x="2.74" y="0.65"/>
<vertex x="2.74" y="-0.65"/>
<vertex x="3.04" y="-0.65"/>
<vertex x="3.04" y="-2.65"/>
<vertex x="2.04" y="-2.65"/>
<vertex x="2.04" y="-0.65"/>
<vertex x="2.34" y="-0.65"/>
<vertex x="2.34" y="0.65"/>
<vertex x="2.04" y="0.65"/>
</polygon>
<rectangle x1="1.92" y1="-2.76" x2="3.12" y2="-0.56" layer="29" rot="R180"/>
<rectangle x1="1.92" y1="-2.76" x2="3.12" y2="-0.56" layer="31" rot="R180"/>
<rectangle x1="1.94" y1="0.54" x2="3.14" y2="2.74" layer="29" rot="R180"/>
<rectangle x1="1.94" y1="0.54" x2="3.14" y2="2.74" layer="31" rot="R180"/>
<polygon width="0.01" layer="1">
<vertex x="-0.5" y="2.65"/>
<vertex x="0.5" y="2.65"/>
<vertex x="0.5" y="0.65"/>
<vertex x="0.2" y="0.65"/>
<vertex x="0.2" y="-0.65"/>
<vertex x="0.5" y="-0.65"/>
<vertex x="0.5" y="-2.65"/>
<vertex x="-0.5" y="-2.65"/>
<vertex x="-0.5" y="-0.65"/>
<vertex x="-0.2" y="-0.65"/>
<vertex x="-0.2" y="0.65"/>
<vertex x="-0.5" y="0.65"/>
</polygon>
<rectangle x1="-0.62" y1="-2.76" x2="0.58" y2="-0.56" layer="29" rot="R180"/>
<rectangle x1="-0.62" y1="-2.76" x2="0.58" y2="-0.56" layer="31" rot="R180"/>
<rectangle x1="-0.6" y1="0.54" x2="0.6" y2="2.74" layer="29" rot="R180"/>
<rectangle x1="-0.6" y1="0.54" x2="0.6" y2="2.74" layer="31" rot="R180"/>
<polygon width="0.01" layer="1">
<vertex x="4.59" y="2.65"/>
<vertex x="5.59" y="2.65"/>
<vertex x="5.59" y="0.65"/>
<vertex x="5.29" y="0.65"/>
<vertex x="5.29" y="-0.65"/>
<vertex x="5.59" y="-0.65"/>
<vertex x="5.59" y="-2.65"/>
<vertex x="4.59" y="-2.65"/>
<vertex x="4.59" y="-0.65"/>
<vertex x="4.89" y="-0.65"/>
<vertex x="4.89" y="0.65"/>
<vertex x="4.59" y="0.65"/>
</polygon>
<rectangle x1="4.47" y1="-2.76" x2="5.67" y2="-0.56" layer="29" rot="R180"/>
<rectangle x1="4.47" y1="-2.76" x2="5.67" y2="-0.56" layer="31" rot="R180"/>
<rectangle x1="4.49" y1="0.54" x2="5.69" y2="2.74" layer="29" rot="R180"/>
<rectangle x1="4.49" y1="0.54" x2="5.69" y2="2.74" layer="31" rot="R180"/>
<polygon width="0.01" layer="1">
<vertex x="7.12" y="2.65"/>
<vertex x="8.12" y="2.65"/>
<vertex x="8.12" y="0.65"/>
<vertex x="7.82" y="0.65"/>
<vertex x="7.82" y="-0.65"/>
<vertex x="8.12" y="-0.65"/>
<vertex x="8.12" y="-2.65"/>
<vertex x="7.12" y="-2.65"/>
<vertex x="7.12" y="-0.65"/>
<vertex x="7.42" y="-0.65"/>
<vertex x="7.42" y="0.65"/>
<vertex x="7.12" y="0.65"/>
</polygon>
<rectangle x1="7" y1="-2.76" x2="8.2" y2="-0.56" layer="29" rot="R180"/>
<rectangle x1="7" y1="-2.76" x2="8.2" y2="-0.56" layer="31" rot="R180"/>
<rectangle x1="7.02" y1="0.54" x2="8.22" y2="2.74" layer="29" rot="R180"/>
<rectangle x1="7.02" y1="0.54" x2="8.22" y2="2.74" layer="31" rot="R180"/>
<polygon width="0.01" layer="1">
<vertex x="9.66" y="2.65"/>
<vertex x="10.66" y="2.65"/>
<vertex x="10.66" y="0.65"/>
<vertex x="10.36" y="0.65"/>
<vertex x="10.36" y="-0.65"/>
<vertex x="10.66" y="-0.65"/>
<vertex x="10.66" y="-2.65"/>
<vertex x="9.66" y="-2.65"/>
<vertex x="9.66" y="-0.65"/>
<vertex x="9.96" y="-0.65"/>
<vertex x="9.96" y="0.65"/>
<vertex x="9.66" y="0.65"/>
</polygon>
<rectangle x1="9.54" y1="-2.76" x2="10.74" y2="-0.56" layer="29" rot="R180"/>
<rectangle x1="9.54" y1="-2.76" x2="10.74" y2="-0.56" layer="31" rot="R180"/>
<rectangle x1="9.56" y1="0.54" x2="10.76" y2="2.74" layer="29" rot="R180"/>
<rectangle x1="9.56" y1="0.54" x2="10.76" y2="2.74" layer="31" rot="R180"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R90" stop="no"/>
</package>
</packages>
<symbols>
<symbol name="USB-C-MINIMUM">
<description>&lt;h3&gt;USB - C 16 Pin&lt;/h3&gt;
Exposes the minimal pins needed to implement a USB 2.x legacy device.</description>
<wire x1="-2.54" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<text x="0" y="0" size="2.54" layer="94" rot="R270" align="center">USB-C</text>
<text x="-2.54" y="-12.446" size="1.778" layer="96" font="vector" rot="MR180" align="top-left">&gt;VALUE</text>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="10.414" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="GND" x="12.7" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="VBUS" x="12.7" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="CC1" x="12.7" y="0" visible="pin" length="short" rot="R180"/>
<pin name="D+" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="D-" x="12.7" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="CC2" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="SHLD" x="12.7" y="-5.08" visible="pin" length="short" rot="R180"/>
</symbol>
<symbol name="CONN_05">
<description>&lt;h3&gt;5 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-7.62" x2="-2.54" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-2.54" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB_C" prefix="J" uservalue="yes">
<description>&lt;h3&gt;USB Type C 16Pin Connector&lt;/h3&gt;

Super Speed pins not available on the 16-pin purely SMD connector so this part is best for USB 2.0 implementations. D1 and D2 are tied together enabling D+/- no matter which way the cable is plugged into the connector. The two channel configuration pins (CC1/2) are exposed. These are normally connected to ground via 5.1k resistors but can be reconfigured for high current/high power applications.

&lt;h2&gt;&lt;i&gt;**4-LAYER BOARD WARNING!!**&lt;/i&gt;&lt;/h2&gt;

If designing 4-layer board, make sure to select the variant &lt;strong&gt;_4-LAYER-ISOLATED&lt;/strong&gt;. The linked footprint (USB-C-16-4LAYER-ISOLATED) includes some &lt;i&gt;very&lt;/i&gt; necessary cutout polygons in the inner layers (2 and 15). This prevents the PTH mounting shield legs on the USB connector from electrically connecting any other polygons on the inner layers (usually VCC and GND).</description>
<gates>
<gate name="J1" symbol="USB-C-MINIMUM" x="0" y="0"/>
</gates>
<devices>
<device name="_4-LAYER-ISOLATED" package="USB-C-16P-4LAYER-ISOLATED">
<connects>
<connect gate="J1" pin="CC1" pad="A5"/>
<connect gate="J1" pin="CC2" pad="B5"/>
<connect gate="J1" pin="D+" pad="A6 B6"/>
<connect gate="J1" pin="D-" pad="A7 B7"/>
<connect gate="J1" pin="GND" pad="GND GND2"/>
<connect gate="J1" pin="SHLD" pad="SHLD1 SHLD2 SHLD3 SHLD4 SHLD5 SHLD6 SHLD7 SHLD8"/>
<connect gate="J1" pin="VBUS" pad="VBUS1 VBUS2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14122" constant="no"/>
<attribute name="VALUE" value="USB Female Type C Connector" constant="no"/>
</technology>
</technologies>
</device>
<device name="_2-LAYER_PADS" package="USB-C-16P-2LAYER-PADS">
<connects>
<connect gate="J1" pin="CC1" pad="A5"/>
<connect gate="J1" pin="CC2" pad="B5"/>
<connect gate="J1" pin="D+" pad="A6 B6"/>
<connect gate="J1" pin="D-" pad="A7 B7"/>
<connect gate="J1" pin="GND" pad="GND GND2"/>
<connect gate="J1" pin="SHLD" pad="SHLD1 SHLD2 SHLD3 SHLD4"/>
<connect gate="J1" pin="VBUS" pad="VBUS1 VBUS2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14122"/>
</technology>
</technologies>
</device>
<device name="_4-LAYER_PADS" package="USB-C-16P_4LAYER-PADS">
<connects>
<connect gate="J1" pin="CC1" pad="A5"/>
<connect gate="J1" pin="CC2" pad="B5"/>
<connect gate="J1" pin="D+" pad="A6 B6"/>
<connect gate="J1" pin="D-" pad="A7 B7"/>
<connect gate="J1" pin="GND" pad="GND GND2"/>
<connect gate="J1" pin="SHLD" pad="SHLD1 SHLD2 SHLD3 SHLD4"/>
<connect gate="J1" pin="VBUS" pad="VBUS1 VBUS2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14122"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_05" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="CONN_05" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.27MM" package="1X05_1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X05_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X05_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_LONGPADS" package="1X05_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X05_1MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08528" constant="no"/>
<attribute name="SF_ID" value="GPS-08288" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD2" package="1X05_1MM_RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_SILK" package="1X05_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST" package="JST-5-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09917" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-VERT" package="JST-5-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="Combine 8288+8399" constant="no"/>
</technology>
</technologies>
</device>
<device name="VERT_FEMALE_HEADER" package="1X05_SMD_VERTICAL_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-15262" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="TC33X">
<wire x1="-1.45" y1="1.75" x2="-1.45" y2="-1.65" width="0.254" layer="51"/>
<wire x1="-1.45" y1="-1.65" x2="1.45" y2="-1.65" width="0.254" layer="51"/>
<wire x1="1.45" y1="-1.65" x2="1.45" y2="1.75" width="0.254" layer="51"/>
<wire x1="1.45" y1="1.75" x2="-1.45" y2="1.75" width="0.254" layer="51"/>
<wire x1="-1.45" y1="-0.4" x2="-1.45" y2="1.75" width="0.254" layer="21"/>
<wire x1="-1.45" y1="1.75" x2="-0.85" y2="1.75" width="0.254" layer="21"/>
<wire x1="1.45" y1="-0.4" x2="1.45" y2="1.75" width="0.254" layer="21"/>
<wire x1="1.45" y1="1.75" x2="0.85" y2="1.75" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="2" x="0" y="1.5" dx="1.5" dy="1.6" layer="1"/>
<smd name="1" x="-1" y="-1.825" dx="1.2" dy="1.2" layer="1"/>
<smd name="3" x="1" y="-1.825" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.905" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.175" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.15" y1="-0.15" x2="1.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-1.15" x2="0.15" y2="1.15" layer="51"/>
</package>
<package name="TRIM-3386">
<pad name="2" x="0" y="-2.8575" drill="0.9" diameter="1.778"/>
<pad name="1" x="-2.54" y="-2.8575" drill="0.9" diameter="1.778"/>
<pad name="3" x="2.54" y="-2.8575" drill="0.9" diameter="1.778"/>
<wire x1="-4.7625" y1="-4.7625" x2="4.7625" y2="-4.7625" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-4.7625" x2="4.7625" y2="4.7625" width="0.127" layer="21"/>
<wire x1="4.7625" y1="4.7625" x2="-4.7625" y2="4.7625" width="0.127" layer="21"/>
<wire x1="-4.7625" y1="4.7625" x2="-4.7625" y2="-4.7625" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.302" width="0.127" layer="21"/>
<text x="-5.08" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.35" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TRIMPOT">
<wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="-1.8796" y2="1.7526" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="-0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-3.048" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.508" x2="-2.032" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.1597" y1="1.2939" x2="-1.7018" y2="2.2352" width="0.1524" layer="94"/>
<text x="-5.969" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="A" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="E" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="S" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRIMPOT" prefix="TM" uservalue="yes">
<description>SMT trimmer potentiometer part number TC33X
&lt;p&gt;http://www.ladyada.net/library/eagle&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TRIMPOT" x="0" y="0"/>
</gates>
<devices>
<device name="TC33X" package="TC33X">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3386" package="TRIM-3386">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Particle">
<packages>
<package name="TP35_BOT">
<smd name="P$1" x="0" y="0" dx="0.889" dy="0.889" layer="16" cream="no"/>
</package>
<package name="TP70_BOT">
<smd name="P$1" x="0" y="0" dx="1.778" dy="1.778" layer="16" cream="no"/>
</package>
<package name="TP60_BOT">
<smd name="P$1" x="0" y="0" dx="1.524" dy="1.524" layer="16" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="TESTPAD">
<pin name="TP" x="2.54" y="0" visible="off" length="short" rot="R180"/>
<circle x="0" y="0" radius="0.508" width="0.889" layer="94"/>
<text x="-1.27" y="1.27" size="1.778" layer="95" distance="10" rot="R180">TP</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TP">
<description>Test Points of all sizes and shapes</description>
<gates>
<gate name="G$1" symbol="TESTPAD" x="-2.54" y="0"/>
</gates>
<devices>
<device name="1" package="TP35_BOT">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2" package="TP70_BOT">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3" package="TP60_BOT">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="3.3V">
<description>&lt;h3&gt;3.3V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" font="vector" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="DGND">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="3.3V" prefix="SUPPLY">
<description>&lt;h3&gt;3.3V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 3.3V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SteveMarple">
<packages>
<package name="PAD">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="PAD-SMALL">
<pad name="1" x="0" y="0" drill="0.7" diameter="1.27"/>
</package>
<package name="TO-92">
<description>&lt;b&gt;TO 92&lt;/b&gt;</description>
<wire x1="-2.0946" y1="-1.651" x2="-1.016" y2="2.5485" width="0.2032" layer="21" curve="-110"/>
<wire x1="1.016" y1="2.5484" x2="2.0945" y2="-1.651" width="0.2032" layer="21" curve="-110"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-2.6549" y1="-0.254" x2="-2.413" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="-0.1016" y1="-0.254" x2="0.1016" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="2.413" y1="-0.254" x2="2.6549" y2="-0.254" width="0.2032" layer="21"/>
<pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.8796"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.635" size="0.4064" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.27" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SOT23-3-HAND">
<wire x1="-0.7112" y1="-1.5748" x2="0.7112" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="0.7112" y1="1.5748" x2="-0.7112" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-0.7112" y1="-1.4986" x2="0.7112" y2="-1.4986" width="0" layer="51"/>
<wire x1="0.7112" y1="-1.4986" x2="0.7112" y2="-0.254" width="0" layer="51"/>
<wire x1="0.7112" y1="1.4986" x2="0.3048" y2="1.4986" width="0" layer="51"/>
<wire x1="0.3048" y1="1.4986" x2="-0.3048" y2="1.4986" width="0" layer="51"/>
<wire x1="-0.3048" y1="1.4986" x2="-0.7112" y2="1.4986" width="0" layer="51"/>
<wire x1="-0.7112" y1="1.4986" x2="-0.7112" y2="1.27" width="0" layer="51"/>
<wire x1="-0.7112" y1="1.27" x2="-0.7112" y2="0.762" width="0" layer="51"/>
<wire x1="-0.7112" y1="0.762" x2="-0.7112" y2="-0.762" width="0" layer="51"/>
<wire x1="-0.7112" y1="1.27" x2="-1.2446" y2="1.27" width="0" layer="51"/>
<wire x1="-1.2446" y1="1.27" x2="-1.2446" y2="0.762" width="0" layer="51"/>
<wire x1="-1.2446" y1="0.762" x2="-0.7112" y2="0.762" width="0" layer="51"/>
<wire x1="-0.7112" y1="-1.4986" x2="-0.7112" y2="-1.27" width="0" layer="51"/>
<wire x1="-0.7112" y1="-1.27" x2="-0.7112" y2="-0.762" width="0" layer="51"/>
<wire x1="-0.7112" y1="-0.762" x2="-1.2446" y2="-0.762" width="0" layer="51"/>
<wire x1="-1.2446" y1="-0.762" x2="-1.2446" y2="-1.27" width="0" layer="51"/>
<wire x1="-1.2446" y1="-1.27" x2="-0.7112" y2="-1.27" width="0" layer="51"/>
<wire x1="0.7112" y1="-0.254" x2="0.7112" y2="0.254" width="0" layer="51"/>
<wire x1="0.7112" y1="0.254" x2="0.7112" y2="1.4986" width="0" layer="51"/>
<wire x1="0.7112" y1="-0.254" x2="1.2446" y2="-0.254" width="0" layer="51"/>
<wire x1="1.2446" y1="-0.254" x2="1.2446" y2="0.254" width="0" layer="51"/>
<wire x1="1.2446" y1="0.254" x2="0.7112" y2="0.254" width="0" layer="51"/>
<wire x1="0.3048" y1="1.4986" x2="-0.3048" y2="1.4986" width="0" layer="51" curve="-180"/>
<smd name="1" x="-1.651" y="1.016" dx="2.5908" dy="0.5588" layer="1"/>
<smd name="2" x="-1.651" y="-1.016" dx="2.5908" dy="0.5588" layer="1"/>
<smd name="3" x="1.651" y="0" dx="2.5908" dy="0.5588" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOT23-3-ROTATED">
<wire x1="0.7112" y1="0.6096" x2="0.7112" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="-0.1016" y1="-1.4986" x2="0.7112" y2="-1.4986" width="0.1524" layer="21"/>
<wire x1="0.7112" y1="-1.4986" x2="0.7112" y2="-0.6096" width="0.1524" layer="21"/>
<wire x1="0.7112" y1="1.4986" x2="0.3048" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.4986" x2="-0.1016" y2="1.4986" width="0.1524" layer="21"/>
<wire x1="-0.7112" y1="0.4064" x2="-0.7112" y2="-0.4064" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.4986" x2="-0.0254" y2="1.1938" width="0.1524" layer="21" curve="-94.994823"/>
<wire x1="-0.7112" y1="-1.4986" x2="0.7112" y2="-1.4986" width="0" layer="51"/>
<wire x1="0.7112" y1="-1.4986" x2="0.7112" y2="-0.254" width="0" layer="51"/>
<wire x1="0.7112" y1="1.4986" x2="0.3048" y2="1.4986" width="0" layer="51"/>
<wire x1="0.3048" y1="1.4986" x2="-0.3048" y2="1.4986" width="0" layer="51"/>
<wire x1="-0.3048" y1="1.4986" x2="-0.7112" y2="1.4986" width="0" layer="51"/>
<wire x1="-0.7112" y1="1.4986" x2="-0.7112" y2="1.27" width="0" layer="51"/>
<wire x1="-0.7112" y1="1.27" x2="-0.7112" y2="0.762" width="0" layer="51"/>
<wire x1="-0.7112" y1="0.762" x2="-0.7112" y2="-0.762" width="0" layer="51"/>
<wire x1="-0.7112" y1="1.27" x2="-1.2446" y2="1.27" width="0" layer="51"/>
<wire x1="-1.2446" y1="1.27" x2="-1.2446" y2="0.762" width="0" layer="51"/>
<wire x1="-1.2446" y1="0.762" x2="-0.7112" y2="0.762" width="0" layer="51"/>
<wire x1="-0.7112" y1="-1.4986" x2="-0.7112" y2="-1.27" width="0" layer="51"/>
<wire x1="-0.7112" y1="-1.27" x2="-0.7112" y2="-0.762" width="0" layer="51"/>
<wire x1="-0.7112" y1="-0.762" x2="-1.2446" y2="-0.762" width="0" layer="51"/>
<wire x1="-1.2446" y1="-0.762" x2="-1.2446" y2="-1.27" width="0" layer="51"/>
<wire x1="-1.2446" y1="-1.27" x2="-0.7112" y2="-1.27" width="0" layer="51"/>
<wire x1="0.7112" y1="-0.254" x2="0.7112" y2="0.254" width="0" layer="51"/>
<wire x1="0.7112" y1="0.254" x2="0.7112" y2="1.4986" width="0" layer="51"/>
<wire x1="0.7112" y1="-0.254" x2="1.2446" y2="-0.254" width="0" layer="51"/>
<wire x1="1.2446" y1="-0.254" x2="1.2446" y2="0.254" width="0" layer="51"/>
<wire x1="1.2446" y1="0.254" x2="0.7112" y2="0.254" width="0" layer="51"/>
<wire x1="0.3048" y1="1.4986" x2="-0.3048" y2="1.4986" width="0" layer="51" curve="-180"/>
<smd name="1" x="-1.016" y="1.016" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="2" x="-1.016" y="-1.016" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="3" x="1.016" y="0" dx="1.3208" dy="0.5588" layer="1"/>
<text x="-1.8542" y="1.4478" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-1.8542" y="1.4478" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="2.54" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-4.445" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PAD">
<pin name="1" x="0" y="0" visible="off" length="point"/>
<circle x="0" y="0" radius="0.762" width="0.254" layer="94"/>
</symbol>
<symbol name="MCP1702">
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="2.54" width="0.4064" layer="94"/>
<wire x1="7.62" y1="2.54" x2="-7.62" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<text x="-7.62" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">GND</text>
<pin name="VIN" x="-10.16" y="0" visible="pin" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="VOUT" x="10.16" y="0" visible="pin" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PAD">
<gates>
<gate name="G$1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL" package="PAD-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP1702" prefix="IC" uservalue="yes">
<description>&lt;b&gt;Microchip MCP1702&lt;/b&gt;&lt;br/&gt;
250mA low quiescent current (2uA) LDO voltage regulator.</description>
<gates>
<gate name="G$1" symbol="MCP1702" x="0" y="0"/>
</gates>
<devices>
<device name="TO92" package="TO-92">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23-HAND" package="SOT23-3-HAND">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23" package="SOT23-3-ROTATED">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MCP1812" package="SOT23-3-ROTATED">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="VIN" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-IC-Power">
<description>&lt;h3&gt;SparkFun Power Driver and Management ICs&lt;/h3&gt;
In this library you'll find anything that has to do with power delivery, or making power supplies.
&lt;p&gt;Contents:
&lt;ul&gt;&lt;li&gt;LDOs&lt;/li&gt;
&lt;li&gt;Boost/Buck controllers&lt;/li&gt;
&lt;li&gt;Charge pump controllers&lt;/li&gt;
&lt;li&gt;Power sequencers&lt;/li&gt;
&lt;li&gt;Power switches&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SOT23-3">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<text x="-1.905" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="2.54" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="BD48XXX">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="-5.08" y="5.588" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="-7.62" y="2.54" visible="pin" length="short" direction="in"/>
<pin name="GND" x="-7.62" y="-2.54" visible="pin" length="short" direction="in"/>
<pin name="VOUT" x="7.62" y="0" visible="pin" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="V_SUPERVISOR" prefix="U">
<description>&lt;h3&gt;BD48xxx - Voltage Detector IC&lt;/h3&gt;
&lt;p&gt;ROHM’s BD48xxx and BD49xxx series are highly
accurate, low-current Voltage Detector IC series. The
family includes BD48xxx devices with N-channel open
drain output and BD49xxx devices with CMOS output.
The devices are available for specific detection voltages
ranging from 2.3V to 6.0V in increments of 0.1V.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="BD48XXX" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SOT23-3">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VCC" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-14809" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Jumpers">
<description>&lt;h3&gt;SparkFun Jumpers&lt;/h3&gt;
In this library you'll find jumpers, or other semipermanent means of changing current paths. The least permanent form is the solder jumper. These can be changed by adding, removing, or moving solder. In cases that are less likely to be changed we have jumpers that are connected with traces. These can be cut with a razor, or reconnected with solder. Reference designator JP.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SMT-JUMPER_3_1-NC_TRACE_SILK">
<wire x1="1.27" y1="-1.016" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.016" x2="1.7272" y2="0.5588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.7272" y1="0.5588" x2="-1.27" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.7272" y1="-0.5588" x2="-1.27" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.27" y1="-1.016" x2="1.7272" y2="-0.5588" width="0.1524" layer="21" curve="90"/>
<wire x1="1.7272" y1="-0.5588" x2="1.7272" y2="0.5588" width="0.1524" layer="21"/>
<wire x1="-1.7272" y1="-0.5588" x2="-1.7272" y2="0.5588" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.016" x2="1.27" y2="1.016" width="0.1524" layer="21"/>
<smd name="1" x="-1.0414" y="0" dx="0.6604" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.6604" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="1.0414" y="0" dx="0.6604" dy="1.27" layer="1" cream="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="29">
<vertex x="0.3175" y="0.127"/>
<vertex x="0.6985" y="0.127"/>
<vertex x="0.6985" y="-0.127"/>
<vertex x="0.3175" y="-0.127"/>
</polygon>
<polygon width="0.0127" layer="1">
<vertex x="-0.1905" y="0.127"/>
<vertex x="0.70485" y="0.127"/>
<vertex x="0.70485" y="-0.127"/>
<vertex x="-0.1905" y="-0.127"/>
</polygon>
</package>
<package name="SMT-JUMPER_3_1-NC_TRACE_NO-SILK">
<smd name="1" x="-1.0414" y="0" dx="0.6604" dy="1.27" layer="1" cream="no"/>
<smd name="2" x="0" y="0" dx="0.6604" dy="1.27" layer="1" cream="no"/>
<smd name="3" x="1.0414" y="0" dx="0.6604" dy="1.27" layer="1" cream="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="29">
<vertex x="0.3175" y="0.127"/>
<vertex x="0.6985" y="0.127"/>
<vertex x="0.6985" y="-0.127"/>
<vertex x="0.3175" y="-0.127"/>
</polygon>
<polygon width="0.0127" layer="1">
<vertex x="-0.1905" y="0.127"/>
<vertex x="0.70485" y="0.127"/>
<vertex x="0.70485" y="-0.127"/>
<vertex x="-0.1905" y="-0.127"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="SMT-JUMPER_3_1-NC_TRACE">
<wire x1="-0.635" y1="-1.397" x2="0.635" y2="-1.397" width="1.27" layer="94" curve="180" cap="flat"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="2.54" y="0.381" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="2.54" y="-0.381" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.635" x2="1.27" y2="0.635" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JUMPER-SMT_3_1-NC_TRACE" prefix="JP">
<description>&lt;h3&gt;Normally closed trace jumper (1 of 2 connections)&lt;/h3&gt;
&lt;p&gt;This jumper has a trace between two pads so it's normally closed (NC). The other connection is normally open (NO). Use a razor knife to open the connection. For best results follow the IPC guidelines for cutting traces:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Cutout at least 0.063 mm (0.005 in).&lt;/li&gt;
&lt;li&gt;Remove all loose material to clean up the cut area.&lt;/li&gt;
&lt;li&gt;Seal the cut with an approved epoxy.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Reapply solder to reclose the connection, or to close the NO connection.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SMT-JUMPER_3_1-NC_TRACE" x="0" y="0"/>
</gates>
<devices>
<device name="_SILK" package="SMT-JUMPER_3_1-NC_TRACE_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_NO-SILK" package="SMT-JUMPER_3_1-NC_TRACE_NO-SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ESP32-C6-MINI-1">
<packages>
<package name="ESP32-C6-MINI-1">
<smd name="1" x="-5.9" y="4" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="2" x="-5.9" y="3.2" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="3" x="-5.9" y="2.4" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="4" x="-5.9" y="1.6" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="5" x="-5.9" y="0.8" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="6" x="-5.9" y="0" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="7" x="-5.9" y="-0.8" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="8" x="-5.9" y="-1.6" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="9" x="-5.9" y="-2.4" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="10" x="-5.9" y="-3.2" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="11" x="-5.9" y="-4" dx="0.4" dy="0.8" layer="1" rot="R90" cream="no"/>
<smd name="12" x="-4.8" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="13" x="-4" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="14" x="-3.2" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="15" x="-2.4" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="16" x="-1.6" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="17" x="-0.8" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="18" x="0" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="19" x="0.8" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="20" x="1.6" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="21" x="2.4" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="22" x="3.2" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="23" x="4" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="24" x="4.8" y="-4.9" dx="0.4" dy="0.8" layer="1" rot="R180" cream="no"/>
<smd name="25" x="5.9" y="-4" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="26" x="5.9" y="-3.2" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="27" x="5.9" y="-2.4" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="28" x="5.9" y="-1.6" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="29" x="5.9" y="-0.8" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="30" x="5.9" y="0" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="31" x="5.9" y="0.8" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="32" x="5.9" y="1.6" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="33" x="5.9" y="2.4" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="34" x="5.9" y="3.2" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="35" x="5.9" y="4" dx="0.4" dy="0.8" layer="1" rot="R270" cream="no"/>
<smd name="36" x="4.8" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="37" x="4" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="38" x="3.2" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="39" x="2.4" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="40" x="1.6" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="41" x="0.8" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="42" x="0" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="43" x="-0.8" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="44" x="-1.6" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="45" x="-2.4" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="46" x="-3.2" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="47" x="-4" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="48" x="-4.8" y="4.9" dx="0.4" dy="0.8" layer="1" cream="no"/>
<smd name="49.9" x="0" y="0" dx="1.45" dy="1.45" layer="1" cream="no"/>
<smd name="50" x="5.95" y="4.95" dx="0.7" dy="0.7" layer="1" cream="no"/>
<smd name="51" x="5.95" y="-4.95" dx="0.7" dy="0.7" layer="1" cream="no"/>
<smd name="52" x="-5.95" y="-4.95" dx="0.7" dy="0.7" layer="1" cream="no"/>
<smd name="53" x="-5.95" y="4.95" dx="0.7" dy="0.7" layer="1" cream="no"/>
<smd name="49.1" x="-1.975" y="1.975" dx="1.45" dy="1.45" layer="1" cream="no"/>
<smd name="49.2" x="-1.975" y="0" dx="1.45" dy="1.45" layer="1" cream="no"/>
<smd name="49.3" x="-1.975" y="-1.975" dx="1.45" dy="1.45" layer="1" cream="no"/>
<smd name="49.4" x="0" y="-1.975" dx="1.45" dy="1.45" layer="1" cream="no"/>
<smd name="49.5" x="1.975" y="-1.975" dx="1.45" dy="1.45" layer="1" cream="no"/>
<smd name="49.6" x="1.975" y="0" dx="1.45" dy="1.45" layer="1" cream="no"/>
<smd name="49.7" x="1.975" y="1.975" dx="1.45" dy="1.45" layer="1" cream="no"/>
<smd name="49.8" x="0" y="1.975" dx="1.45" dy="1.45" layer="1" cream="no"/>
<wire x1="-6.6" y1="-5.6" x2="6.6" y2="-5.6" width="0.1" layer="51"/>
<wire x1="6.6" y1="-5.6" x2="6.6" y2="5.6" width="0.1" layer="51"/>
<wire x1="6.6" y1="5.6" x2="6.6" y2="11" width="0.1" layer="51"/>
<wire x1="6.6" y1="11" x2="-6.6" y2="11" width="0.1" layer="51"/>
<wire x1="-6.6" y1="11" x2="-6.6" y2="5.6" width="0.1" layer="51"/>
<wire x1="-6.6" y1="5.6" x2="-6.6" y2="-5.6" width="0.1" layer="51"/>
<wire x1="-6.6" y1="5.6" x2="6.6" y2="5.6" width="0.1" layer="51"/>
<rectangle x1="-6.6" y1="-5.6" x2="6.6" y2="11" layer="39"/>
<rectangle x1="-6.6" y1="5.6" x2="6.6" y2="11" layer="41"/>
<rectangle x1="-6.25" y1="3.85" x2="-5.55" y2="4.15" layer="31"/>
<rectangle x1="-6.25" y1="3.05" x2="-5.55" y2="3.35" layer="31"/>
<rectangle x1="-6.25" y1="2.25" x2="-5.55" y2="2.55" layer="31"/>
<rectangle x1="-6.25" y1="1.45" x2="-5.55" y2="1.75" layer="31"/>
<rectangle x1="-6.25" y1="0.65" x2="-5.55" y2="0.95" layer="31"/>
<rectangle x1="-6.25" y1="-0.15" x2="-5.55" y2="0.15" layer="31"/>
<rectangle x1="-6.25" y1="-0.95" x2="-5.55" y2="-0.65" layer="31"/>
<rectangle x1="-6.25" y1="-1.75" x2="-5.55" y2="-1.45" layer="31"/>
<rectangle x1="-6.25" y1="-2.55" x2="-5.55" y2="-2.25" layer="31"/>
<rectangle x1="-6.25" y1="-3.35" x2="-5.55" y2="-3.05" layer="31"/>
<rectangle x1="-6.25" y1="-4.15" x2="-5.55" y2="-3.85" layer="31"/>
<rectangle x1="5.55" y1="-4.15" x2="6.25" y2="-3.85" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="-3.35" x2="6.25" y2="-3.05" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="-2.55" x2="6.25" y2="-2.25" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="-1.75" x2="6.25" y2="-1.45" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="-0.95" x2="6.25" y2="-0.65" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="-0.15" x2="6.25" y2="0.15" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="0.65" x2="6.25" y2="0.95" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="1.45" x2="6.25" y2="1.75" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="2.25" x2="6.25" y2="2.55" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="3.05" x2="6.25" y2="3.35" layer="31" rot="R180"/>
<rectangle x1="5.55" y1="3.85" x2="6.25" y2="4.15" layer="31" rot="R180"/>
<rectangle x1="2.85" y1="-5.05" x2="3.55" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="2.05" y1="-5.05" x2="2.75" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="1.25" y1="-5.05" x2="1.95" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="0.45" y1="-5.05" x2="1.15" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="-0.35" y1="-5.05" x2="0.35" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="-1.15" y1="-5.05" x2="-0.45" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="-1.95" y1="-5.05" x2="-1.25" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="-2.75" y1="-5.05" x2="-2.05" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="-3.55" y1="-5.05" x2="-2.85" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="-4.35" y1="-5.05" x2="-3.65" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="-5.15" y1="-5.05" x2="-4.45" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="3.65" y1="-5.05" x2="4.35" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="4.45" y1="-5.05" x2="5.15" y2="-4.75" layer="31" rot="R270"/>
<rectangle x1="-3.55" y1="4.75" x2="-2.85" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="-2.75" y1="4.75" x2="-2.05" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="-1.95" y1="4.75" x2="-1.25" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="-1.15" y1="4.75" x2="-0.45" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="-0.35" y1="4.75" x2="0.35" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="0.45" y1="4.75" x2="1.15" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="1.25" y1="4.75" x2="1.95" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="2.05" y1="4.75" x2="2.75" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="2.85" y1="4.75" x2="3.55" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="3.65" y1="4.75" x2="4.35" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="4.45" y1="4.75" x2="5.15" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="-4.35" y1="4.75" x2="-3.65" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="-5.15" y1="4.75" x2="-4.45" y2="5.05" layer="31" rot="R90"/>
<rectangle x1="5.65" y1="4.65" x2="6.25" y2="5.25" layer="31"/>
<rectangle x1="-6.25" y1="4.65" x2="-5.65" y2="5.25" layer="31"/>
<rectangle x1="5.65" y1="-5.25" x2="6.25" y2="-4.65" layer="31"/>
<rectangle x1="-6.25" y1="-5.25" x2="-5.65" y2="-4.65" layer="31"/>
<rectangle x1="-2.4" y1="1.6" x2="-1.6" y2="2.4" layer="31"/>
<rectangle x1="-2.4" y1="-0.4" x2="-1.6" y2="0.4" layer="31"/>
<rectangle x1="-2.4" y1="-2.4" x2="-1.6" y2="-1.6" layer="31"/>
<rectangle x1="-0.4" y1="-2.4" x2="0.4" y2="-1.6" layer="31"/>
<rectangle x1="1.6" y1="-2.4" x2="2.4" y2="-1.6" layer="31"/>
<rectangle x1="1.6" y1="-0.4" x2="2.4" y2="0.4" layer="31"/>
<rectangle x1="1.6" y1="1.6" x2="2.4" y2="2.4" layer="31"/>
<rectangle x1="-0.4" y1="1.6" x2="0.4" y2="2.4" layer="31"/>
<rectangle x1="-0.4" y1="-0.4" x2="0.4" y2="0.4" layer="31"/>
<text x="0" y="11.5" size="0.6096" layer="25" font="vector" ratio="20" align="center">&gt;Name</text>
<text x="0" y="-6.5" size="0.6096" layer="27" font="vector" ratio="20" align="center">&gt;Value</text>
<wire x1="-6" y1="-5.8" x2="-6.8" y2="-5.8" width="0.2" layer="21"/>
<wire x1="-6.8" y1="-5.8" x2="-6.8" y2="-5" width="0.2" layer="21"/>
<wire x1="6" y1="-5.8" x2="6.8" y2="-5.8" width="0.2" layer="21"/>
<wire x1="6.8" y1="-5.8" x2="6.8" y2="-5" width="0.2" layer="21"/>
<wire x1="-6.8" y1="4.4" x2="-6.8" y2="5.4" width="0.2" layer="21"/>
<wire x1="6.8" y1="4.4" x2="6.8" y2="5.4" width="0.2" layer="21"/>
<rectangle x1="-6.6" y1="5.6" x2="6.6" y2="11" layer="42" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="ESP32-C6-MINI-1">
<pin name="GND" x="-20.32" y="-30.48" length="short"/>
<pin name="3V3" x="-20.32" y="27.94" length="short"/>
<pin name="XTAL_32K_P/IO0" x="20.32" y="27.94" length="short" rot="R180"/>
<pin name="XTAL_32K_N/IO1" x="20.32" y="25.4" length="short" rot="R180"/>
<pin name="EN" x="-20.32" y="22.86" length="short"/>
<pin name="IO2" x="20.32" y="22.86" length="short" rot="R180"/>
<pin name="IO3" x="20.32" y="20.32" length="short" rot="R180"/>
<pin name="MTMS/LP_RX/IO4" x="20.32" y="17.78" length="short" rot="R180"/>
<pin name="MTDI/LP_TX/IO5" x="20.32" y="15.24" length="short" rot="R180"/>
<pin name="MTCK/LP_SDA/IO6" x="20.32" y="12.7" length="short" rot="R180"/>
<pin name="MTDO/LP_SCL/IO7" x="20.32" y="10.16" length="short" rot="R180"/>
<pin name="IO8" x="20.32" y="7.62" length="short" rot="R180"/>
<pin name="IO9" x="20.32" y="5.08" length="short" rot="R180"/>
<pin name="USB_D-/IO12" x="20.32" y="-2.54" length="short" rot="R180"/>
<pin name="USB_D+/IO13" x="20.32" y="-5.08" length="short" rot="R180"/>
<pin name="IO14" x="20.32" y="-7.62" length="short" rot="R180"/>
<pin name="IO15" x="20.32" y="-10.16" length="short" rot="R180"/>
<pin name="SDIO_CMD/IO18" x="20.32" y="-17.78" length="short" rot="R180"/>
<pin name="SDIO_CLK/IO19" x="20.32" y="-20.32" length="short" rot="R180"/>
<pin name="SDIO_D0/IO20" x="20.32" y="-22.86" length="short" rot="R180"/>
<pin name="SDIO_D1/IO21" x="20.32" y="-25.4" length="short" rot="R180"/>
<pin name="SDIO_D2/IO22" x="20.32" y="-27.94" length="short" rot="R180"/>
<pin name="SDIO_D3/IO23" x="20.32" y="-30.48" length="short" rot="R180"/>
<pin name="TX0/IO16" x="20.32" y="-12.7" length="short" rot="R180"/>
<pin name="RX0/IO17" x="20.32" y="-15.24" length="short" rot="R180"/>
<text x="-10.16" y="20.32" size="1.27" layer="97" font="vector" rot="R90" align="top-center">ADC</text>
<wire x1="-5.08" y1="12.7" x2="-7.62" y2="12.7" width="0.254" layer="97"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="15.24" width="0.254" layer="97"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="17.78" width="0.254" layer="97"/>
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="20.32" width="0.254" layer="97"/>
<wire x1="-7.62" y1="20.32" x2="-7.62" y2="22.86" width="0.254" layer="97"/>
<wire x1="-7.62" y1="22.86" x2="-7.62" y2="25.4" width="0.254" layer="97"/>
<wire x1="-7.62" y1="25.4" x2="-7.62" y2="27.94" width="0.254" layer="97"/>
<wire x1="-7.62" y1="27.94" x2="-5.08" y2="27.94" width="0.254" layer="97"/>
<wire x1="-7.62" y1="25.4" x2="-5.08" y2="25.4" width="0.254" layer="97"/>
<wire x1="-7.62" y1="22.86" x2="-5.08" y2="22.86" width="0.254" layer="97"/>
<wire x1="-7.62" y1="20.32" x2="-5.08" y2="20.32" width="0.254" layer="97"/>
<wire x1="-7.62" y1="17.78" x2="-5.08" y2="17.78" width="0.254" layer="97"/>
<wire x1="-7.62" y1="15.24" x2="-5.08" y2="15.24" width="0.254" layer="97"/>
<text x="-10.16" y="7.62" size="1.27" layer="97" font="vector" align="center">Strapping
pin defaults</text>
<text x="-12.7" y="0" size="1.27" layer="97" font="vector" align="center-right">IO4
IO5
IO8
IO9
IO15</text>
<text x="-7.62" y="0" size="1.27" layer="97" font="vector" align="center-left">Float
Float
Float
Pull-up
Float</text>
<text x="-10.16" y="0" size="1.27" layer="97" font="vector" align="center">---
---
---
---
---</text>
<wire x1="-17.78" y1="30.48" x2="-17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-33.02" x2="17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="-33.02" x2="17.78" y2="30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="30.48" x2="-17.78" y2="30.48" width="0.254" layer="94"/>
<text x="-17.78" y="33.02" size="1.778" layer="95" font="vector" align="top-left">&gt;Name</text>
<text x="-17.78" y="-35.56" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP32-C6-MINI-1" prefix="U">
<gates>
<gate name="G$1" symbol="ESP32-C6-MINI-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESP32-C6-MINI-1">
<connects>
<connect gate="G$1" pin="3V3" pad="3"/>
<connect gate="G$1" pin="EN" pad="8"/>
<connect gate="G$1" pin="GND" pad="1 2 11 14 36 37 38 39 40 41 42 43 44 45 46 47 48 49.1 49.2 49.3 49.4 49.5 49.6 49.7 49.8 49.9 50 51 52 53"/>
<connect gate="G$1" pin="IO14" pad="19"/>
<connect gate="G$1" pin="IO15" pad="20"/>
<connect gate="G$1" pin="IO2" pad="5"/>
<connect gate="G$1" pin="IO3" pad="6"/>
<connect gate="G$1" pin="IO8" pad="22"/>
<connect gate="G$1" pin="IO9" pad="23"/>
<connect gate="G$1" pin="MTCK/LP_SDA/IO6" pad="15"/>
<connect gate="G$1" pin="MTDI/LP_TX/IO5" pad="10"/>
<connect gate="G$1" pin="MTDO/LP_SCL/IO7" pad="16"/>
<connect gate="G$1" pin="MTMS/LP_RX/IO4" pad="9"/>
<connect gate="G$1" pin="RX0/IO17" pad="30"/>
<connect gate="G$1" pin="SDIO_CLK/IO19" pad="25"/>
<connect gate="G$1" pin="SDIO_CMD/IO18" pad="24"/>
<connect gate="G$1" pin="SDIO_D0/IO20" pad="26"/>
<connect gate="G$1" pin="SDIO_D1/IO21" pad="27"/>
<connect gate="G$1" pin="SDIO_D2/IO22" pad="28"/>
<connect gate="G$1" pin="SDIO_D3/IO23" pad="29"/>
<connect gate="G$1" pin="TX0/IO16" pad="31"/>
<connect gate="G$1" pin="USB_D+/IO13" pad="18"/>
<connect gate="G$1" pin="USB_D-/IO12" pad="17"/>
<connect gate="G$1" pin="XTAL_32K_N/IO1" pad="13"/>
<connect gate="G$1" pin="XTAL_32K_P/IO0" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-19033"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="wirepad">
<description>&lt;b&gt;Single Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="AUTO/0,70-ROUND">
<pad name="1" x="0" y="0" drill="0.7" diameter="1.27"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="AUTO/0,85-ROUND">
<pad name="1" x="0" y="0" drill="0.85"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="1,6/0,8">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-0.762" y1="0.762" x2="-0.508" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.508" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.508" x2="0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="-0.762" y2="-0.508" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PAD">
<wire x1="-1.016" y1="1.016" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<text x="-1.143" y="1.8542" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.143" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1,6/0,8" prefix="PAD" uservalue="yes">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<gates>
<gate name="P" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="OCTO" package="1,6/0,8">
<connects>
<connect gate="P" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0,85" package="AUTO/0,85-ROUND">
<connects>
<connect gate="P" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0,70" package="AUTO/0,70-ROUND">
<connects>
<connect gate="P" pin="P" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0.254">
<clearance class="0" value="0.1524"/>
</class>
<class number="1" name="power" width="0.2032" drill="0">
<clearance class="1" value="0.1524"/>
</class>
</classes>
<parts>
<part name="R1" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="4.7K"/>
<part name="R2" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="4.7K"/>
<part name="NAME" library="SparkFun-Aesthetics" deviceset="FRAME-A4L" device="" value="Value">
<attribute name="CNAME" value="WinKy"/>
<attribute name="CREVISION" value="1.2"/>
<attribute name="DESIGNER" value="Charles-Henri Hallard"/>
</part>
<part name="P+7" library="_c2h" deviceset="3V3-EXT" device="" value="3V3"/>
<part name="OK1" library="optocoupler" deviceset="SFH620" device="6" value="LTV-814"/>
<part name="R4" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="3K3"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="Q2" library="_c2h" deviceset="SMD-MOSFET-N-CH-20V-2.1A-CJ2302(SOT-23)" device="" value="BSS138"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="R5" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="10K"/>
<part name="3V3" library="_c2h" deviceset="3V3-EXT" device="" value="3V3"/>
<part name="GND15" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C1" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="100nF"/>
<part name="GND14" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY7" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="V_USB"/>
<part name="U3" library="SparkFun-PowerIC" deviceset="V_REG_LDO" device="SMD" value="AP2112K">
<attribute name="PROD_ID" value="VREG-12457"/>
</part>
<part name="U$5" library="microbuilder" deviceset="3.3V" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="L2" library="inductors" deviceset="BLM15H" device="" technology="B121SN1" value="BLM15HB"/>
<part name="C2" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="1uF"/>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="L1" library="inductors" deviceset="BLM15H" device="" technology="B121SN1" value="BLM15HB"/>
<part name="C5" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="100nF"/>
<part name="R8" library="LowPowerLab" deviceset="RESISTOR" device="0402-RES" value="1K"/>
<part name="LTIC" library="OPL_Optoelectronics" deviceset="SMD-LED-CLEAR-BLUE(0402)" device="" value="BLUE"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="J1" library="SparkFun-Connectors" deviceset="USB_C" device="_4-LAYER-ISOLATED" value="US Type C"/>
<part name="GND33" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R9" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="5.1K"/>
<part name="R7" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="5.1K"/>
<part name="J2" library="_c2h" deviceset="QWIIC_CONNECTOR" device="" value="Qwiic"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="LED1" library="_c2h" deviceset="SK6805" device="1515"/>
<part name="R3" library="adafruit" deviceset="TRIMPOT" device="TC33X" value="2K"/>
<part name="TGG" library="Particle" deviceset="TP" device="3"/>
<part name="U$11" library="Particle" deviceset="TP" device="3"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="U$3" library="Particle" deviceset="TP" device="3"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="SLED" library="microbuilder" deviceset="SOLDERJUMPER_CLOSED" device=""/>
<part name="U$2" library="microbuilder" deviceset="GND" device=""/>
<part name="RST" library="_c2h" deviceset="SMD-BUTTON(2P-3.0X2.5X1.2+0.4MM)" device="" value="B3U-1000P-2P-SMD"/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" deviceset="3.3V" device="" value="3V3"/>
<part name="R12" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="10K"/>
<part name="C6" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="1uF"/>
<part name="U$8" library="microbuilder" deviceset="GND" device=""/>
<part name="SUPPLY5" library="SparkFun-PowerSymbols" deviceset="3.3V" device="" value="3V3"/>
<part name="R11" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="10K"/>
<part name="IO9" library="_c2h" deviceset="SMD-BUTTON(2P-3.0X2.5X1.2+0.4MM)" device="" value="B3U-1000P-2P-SMD"/>
<part name="C7" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="100nF"/>
<part name="C3" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="10uF"/>
<part name="SUPPLY3" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="VIN"/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="U$4" library="SteveMarple" deviceset="PAD" device=""/>
<part name="U$7" library="SteveMarple" deviceset="PAD" device=""/>
<part name="U$9" library="SteveMarple" deviceset="PAD" device=""/>
<part name="D3" library="microbuilder" deviceset="DIODE" device="SOD-323" value="CUS10S30"/>
<part name="D4" library="microbuilder" deviceset="DIODE" device="SOD-323" value="BAT46"/>
<part name="D5" library="microbuilder" deviceset="DIODE" device="SOD-323" value="BAT46"/>
<part name="D6" library="microbuilder" deviceset="DIODE" device="SOD-323" value="BAT48"/>
<part name="P+1" library="_c2h" deviceset="3V3-EXT" device="" value="VLED"/>
<part name="GND4" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="U1" library="SteveMarple" deviceset="MCP1702" device="SOT23" value="MCP1702-5002"/>
<part name="D1" library="microbuilder" deviceset="DIODE" device="SOD-323" value="CUS10S30"/>
<part name="GND5" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C4" library="_c2h" deviceset="CAP" device="0805" value="10uF 25V"/>
<part name="GND6" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="D2" library="microbuilder" deviceset="DIODE" device="SOD-323" value="CUS10S30"/>
<part name="SUPPLY1" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="V_TIC"/>
<part name="SUPPLY4" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="V_SCAP"/>
<part name="R6" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="470K"/>
<part name="R10" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="470K"/>
<part name="GND16" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="U2" library="SparkFun-IC-Power" deviceset="V_SUPERVISOR" device="" value="APX803L40-33"/>
<part name="J3" library="SparkFun-Connectors" deviceset="CONN_05" device=""/>
<part name="C10" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="1uF"/>
<part name="GND18" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R13" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="100K"/>
<part name="C9" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="10uF"/>
<part name="GND19" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY6" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="V_SCAP"/>
<part name="U$1" library="microbuilder" deviceset="GND" device=""/>
<part name="U$10" library="microbuilder" deviceset="GND" device=""/>
<part name="U$13" library="microbuilder" deviceset="GND" device=""/>
<part name="R14" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="10K"/>
<part name="R15" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="10K"/>
<part name="GND20" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C8" library="_c2h" deviceset="CAP" device="0402-CAP-OK" value="100nF"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="LED2" library="_c2h" deviceset="SK6805" device="1515"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="D7" library="microbuilder" deviceset="DIODE" device="SOD-523" value="1N4148"/>
<part name="VLED" library="SparkFun-Jumpers" deviceset="JUMPER-SMT_3_1-NC_TRACE" device="_NO-SILK" value="TIC-RX"/>
<part name="SUPPLY8" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="VIN"/>
<part name="U$6" library="microbuilder" deviceset="3.3V" device=""/>
<part name="U4" library="ESP32-C6-MINI-1" deviceset="ESP32-C6-MINI-1" device=""/>
<part name="GND23" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="PAD1" library="wirepad" deviceset="1,6/0,8" device="0,70"/>
<part name="PAD2" library="wirepad" deviceset="1,6/0,8" device="0,70"/>
<part name="PAD3" library="wirepad" deviceset="1,6/0,8" device="0,70"/>
<part name="PAD4" library="wirepad" deviceset="1,6/0,8" device="0,70"/>
<part name="SUPPLY9" library="SparkFun-PowerSymbols" deviceset="3.3V" device="" value="3V3"/>
<part name="U$14" library="Particle" deviceset="TP" device="3" value="TP3"/>
<part name="SDS" library="microbuilder" deviceset="SOLDERJUMPER" device=""/>
<part name="S5V" library="microbuilder" deviceset="SOLDERJUMPER" device=""/>
<part name="R16" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="100K"/>
<part name="R17" library="_c2h" deviceset="RESISTOR" device="0402-RES" value="470K"/>
<part name="GND24" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="SUPPLY10" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="V_TIC"/>
<part name="S3V" library="microbuilder" deviceset="SOLDERJUMPER" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="195.58" y="16.51" size="2.54" layer="94" font="vector" rot="MR0">File:</text>
<text x="233.045" y="73.66" size="3.81" layer="94" font="vector" ratio="15" rot="R180">Teleinfo</text>
<text x="50.8" y="59.69" size="3.81" layer="94" font="vector" ratio="15" rot="R180">I2C / GPIO</text>
<text x="173.355" y="145.415" size="1.778" layer="97" align="top-center">600mA out
6V max in</text>
<text x="212.09" y="116.84" size="3.81" layer="94" font="vector" ratio="15">RGB LEDs</text>
<text x="190.5" y="37.465" size="2.54" layer="95" rot="R90">Teleinfo</text>
<wire x1="5.08" y1="61.595" x2="62.865" y2="61.595" width="0.8128" layer="94" style="shortdash"/>
<wire x1="172.72" y1="75.565" x2="184.15" y2="75.565" width="0.8128" layer="94" style="shortdash"/>
<wire x1="184.15" y1="75.565" x2="184.15" y2="26.035" width="0.8128" layer="94" style="shortdash"/>
<wire x1="184.15" y1="75.565" x2="277.495" y2="75.565" width="0.8128" layer="94" style="shortdash"/>
<wire x1="196.85" y1="122.555" x2="278.765" y2="122.555" width="0.8128" layer="94" style="shortdash"/>
<wire x1="172.72" y1="122.555" x2="172.72" y2="91.44" width="0.8128" layer="94" style="shortdash"/>
<text x="17.78" y="177.165" size="3.81" layer="94" font="vector" ratio="15">Linky / USB Power Path 5V</text>
<text x="115.57" y="86.995" size="3.81" layer="94" font="vector" ratio="15" align="center">ESP32</text>
<wire x1="172.72" y1="91.44" x2="172.72" y2="75.565" width="0.8128" layer="94" style="shortdash"/>
<wire x1="62.865" y1="75.565" x2="62.865" y2="61.595" width="0.8128" layer="94" style="shortdash"/>
<text x="205.74" y="170.815" size="1.778" layer="94" font="vector" ratio="10">Viking SC5V5Z105Z 1.0F 5.5V 17x8.5x21</text>
<text x="205.105" y="167.64" size="1.778" layer="94" font="vector" ratio="10">AVX SCMR18D105PRBB0 1.0F 5.4V 16x8x18</text>
<text x="208.915" y="164.465" size="1.778" layer="94" font="vector" ratio="10">KEMET FG0H105ZF 1.0F 5.5V 16.5x19</text>
<text x="216.535" y="176.53" size="3.81" layer="94" font="vector" ratio="15">Super Caps</text>
<wire x1="62.865" y1="61.595" x2="62.865" y2="5.08" width="0.8128" layer="94" style="shortdash"/>
<wire x1="196.85" y1="183.515" x2="196.85" y2="122.555" width="0.8128" layer="94" style="shortdash"/>
<wire x1="172.72" y1="122.555" x2="196.85" y2="122.555" width="0.8128" layer="94" style="shortdash"/>
<text x="149.225" y="177.165" size="3.81" layer="94" font="vector" ratio="15">ESP32 Power </text>
<wire x1="78.105" y1="91.44" x2="172.72" y2="91.44" width="0.8128" layer="94" style="shortdash"/>
<wire x1="78.105" y1="91.44" x2="62.865" y2="75.565" width="0.8128" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="R1" gate="G$1" x="36.83" y="38.735" smashed="yes" rot="R90">
<attribute name="NAME" x="35.3314" y="37.465" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.132" y="36.195" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="29.21" y="38.735" smashed="yes" rot="R90">
<attribute name="NAME" x="27.7114" y="37.465" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="32.512" y="36.195" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="NAME" gate="G$1" x="5.08" y="5.08" smashed="yes">
<attribute name="DRAWING_NAME" x="196.85" y="16.51" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="186.69" y="11.43" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="200.66" y="6.35" size="2.54" layer="94" font="vector"/>
<attribute name="DESIGNER" x="231.34" y="11.43" size="2.54" layer="94" font="vector"/>
<attribute name="CNAME" x="186.69" y="21.59" size="2.54" layer="94" font="vector"/>
<attribute name="CREVISION" x="240" y="6.25" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="P+7" gate="3V3-EXT" x="29.21" y="46.99" smashed="yes">
<attribute name="VALUE" x="26.924" y="50.546" size="1.778" layer="96"/>
</instance>
<instance part="OK1" gate="G$1" x="224.155" y="45.085" smashed="yes">
<attribute name="NAME" x="217.17" y="50.8" size="1.778" layer="95"/>
<attribute name="VALUE" x="219.71" y="37.465" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="239.395" y="37.465" smashed="yes" rot="R90">
<attribute name="NAME" x="237.8964" y="36.195" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="242.697" y="36.195" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="239.395" y="29.21"/>
<instance part="Q2" gate="G$1" x="248.285" y="42.545" smashed="yes">
<attribute name="NAME" x="250.825" y="43.815" size="1.778" layer="95" ratio="10"/>
<attribute name="VALUE" x="249.555" y="38.1" size="1.778" layer="96" ratio="10"/>
</instance>
<instance part="GND10" gate="1" x="248.285" y="29.21"/>
<instance part="R5" gate="G$1" x="248.285" y="57.15" smashed="yes" rot="R90">
<attribute name="NAME" x="246.7864" y="55.88" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="251.587" y="55.88" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="3V3" gate="3V3-EXT" x="248.285" y="68.58" smashed="yes">
<attribute name="VALUE" x="245.999" y="72.136" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="161.925" y="137.16" smashed="yes">
<attribute name="VALUE" x="159.385" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="114.3" y="142.24" smashed="yes" rot="MR0">
<attribute name="NAME" x="113.411" y="145.161" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="118.999" y="147.701" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="GND14" gate="1" x="114.3" y="133.35" smashed="yes">
<attribute name="VALUE" x="111.76" y="130.81" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="64.77" y="123.825" smashed="yes">
<attribute name="VALUE" x="61.849" y="127.381" size="1.778" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="173.355" y="158.115" smashed="yes">
<attribute name="PROD_ID" x="173.355" y="158.115" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="165.735" y="166.624" size="1.778" layer="95"/>
<attribute name="VALUE" x="165.735" y="146.685" size="1.778" layer="96"/>
</instance>
<instance part="U$5" gate="G$1" x="187.325" y="167.005" smashed="yes">
<attribute name="VALUE" x="185.801" y="168.021" size="1.27" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="36.83" y="95.25" smashed="yes">
<attribute name="VALUE" x="34.29" y="92.71" size="1.778" layer="96"/>
</instance>
<instance part="L2" gate="G$1" x="36.83" y="104.14" smashed="yes" rot="R270">
<attribute name="NAME" x="33.7566" y="105.41" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="41.021" y="97.79" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="187.325" y="154.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="186.436" y="157.861" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="192.024" y="158.496" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="GND2" gate="1" x="187.325" y="137.16" smashed="yes">
<attribute name="VALUE" x="184.785" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="L1" gate="G$1" x="48.895" y="121.92" smashed="yes">
<attribute name="NAME" x="47.625" y="123.9266" size="1.778" layer="95"/>
<attribute name="VALUE" x="55.245" y="120.396" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C5" gate="G$1" x="192.405" y="89.535" smashed="yes">
<attribute name="NAME" x="195.326" y="92.456" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="190.119" y="86.614" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R8" gate="G$1" x="260.985" y="61.595" smashed="yes" rot="R90">
<attribute name="NAME" x="259.4864" y="60.325" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="264.287" y="60.325" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LTIC" gate="G$1" x="260.985" y="52.07" smashed="yes" rot="R270">
<attribute name="NAME" x="266.065" y="53.975" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="255.905" y="54.61" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="GND7" gate="1" x="28.575" y="95.25"/>
<instance part="J1" gate="J1" x="15.875" y="114.3" smashed="yes">
<attribute name="VALUE" x="12.446" y="101.854" size="1.778" layer="96" font="vector" rot="MR180" align="top-left"/>
<attribute name="NAME" x="17.907" y="124.968" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="GND33" gate="1" x="48.895" y="95.25" smashed="yes">
<attribute name="VALUE" x="46.355" y="92.71" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="52.705" y="107.315" smashed="yes" rot="R90">
<attribute name="NAME" x="49.403" y="108.839" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="53.848" y="109.855" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R7" gate="G$1" x="45.72" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="42.418" y="108.204" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="46.863" y="109.855" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J2" gate="G$1" x="13.97" y="27.94"/>
<instance part="GND9" gate="1" x="232.41" y="86.995"/>
<instance part="LED1" gate="G$1" x="218.44" y="85.725" smashed="yes">
<attribute name="NAME" x="217.17" y="94.107" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="215.9" y="78.105" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R3" gate="G$1" x="207.645" y="37.465" smashed="yes" rot="MR90">
<attribute name="NAME" x="203.327" y="33.782" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="208.661" y="33.909" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="TGG" gate="G$1" x="55.245" y="22.86" rot="R180"/>
<instance part="U$11" gate="G$1" x="55.245" y="26.67" rot="R180"/>
<instance part="GND12" gate="1" x="49.53" y="17.145"/>
<instance part="U$3" gate="G$1" x="55.245" y="30.48" rot="R180"/>
<instance part="GND13" gate="1" x="27.305" y="17.145"/>
<instance part="SLED" gate="G$1" x="255.27" y="67.31" rot="R180"/>
<instance part="U$2" gate="G$1" x="76.2" y="23.495" smashed="yes">
<attribute name="VALUE" x="74.676" y="21.59" size="1.27" layer="96"/>
</instance>
<instance part="RST" gate="G$1" x="76.2" y="33.02" smashed="yes" rot="R270">
<attribute name="NAME" x="70.485" y="37.465" size="2.54" layer="95" ratio="10" rot="R270"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="84.455" y="80.01" smashed="yes">
<attribute name="VALUE" x="84.582" y="83.058" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R12" gate="G$1" x="83.82" y="46.99" smashed="yes" rot="R270">
<attribute name="NAME" x="82.042" y="45.466" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="87.122" y="44.45" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="83.82" y="36.83" smashed="yes" rot="R180">
<attribute name="NAME" x="85.979" y="31.369" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="79.121" y="38.481" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$8" gate="G$1" x="83.82" y="23.495" smashed="yes">
<attribute name="VALUE" x="82.296" y="21.59" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="83.82" y="53.34" smashed="yes">
<attribute name="VALUE" x="83.947" y="56.388" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R11" gate="G$1" x="153.035" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="151.257" y="59.436" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="156.337" y="59.055" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IO9" gate="G$1" x="149.225" y="45.72" smashed="yes" rot="R270">
<attribute name="NAME" x="151.765" y="48.895" size="2.54" layer="95" ratio="10" rot="R270"/>
</instance>
<instance part="C7" gate="G$1" x="88.265" y="68.58" smashed="yes">
<attribute name="NAME" x="89.281" y="74.041" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="85.979" y="66.929" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="79.375" y="68.58" smashed="yes">
<attribute name="NAME" x="79.756" y="74.041" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="77.089" y="67.564" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="106.045" y="168.91" smashed="yes">
<attribute name="VALUE" x="104.394" y="172.466" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="120.015" y="133.35" smashed="yes">
<attribute name="VALUE" x="117.475" y="130.81" size="1.778" layer="96"/>
</instance>
<instance part="U$4" gate="G$1" x="30.48" y="150.495"/>
<instance part="U$7" gate="G$1" x="16.51" y="150.495"/>
<instance part="U$9" gate="G$1" x="24.13" y="150.495" rot="R180"/>
<instance part="D3" gate="G$1" x="13.97" y="155.575" smashed="yes" rot="R90">
<attribute name="NAME" x="17.145" y="154.305" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="9.525" y="155.575" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="D4" gate="G$1" x="13.97" y="146.05" smashed="yes" rot="R90">
<attribute name="NAME" x="17.145" y="144.78" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="D5" gate="G$1" x="33.655" y="146.05" smashed="yes" rot="R90">
<attribute name="NAME" x="31.75" y="144.78" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="D6" gate="G$1" x="33.655" y="156.21" smashed="yes" rot="R90">
<attribute name="NAME" x="31.75" y="154.94" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="P+1" gate="3V3-EXT" x="210.185" y="107.315" smashed="yes">
<attribute name="VALUE" x="207.264" y="110.236" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="24.13" y="137.795"/>
<instance part="U1" gate="G$1" x="68.58" y="163.195" smashed="yes">
<attribute name="NAME" x="61.595" y="155.575" size="1.778" layer="95"/>
<attribute name="VALUE" x="60.071" y="166.37" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="85.09" y="121.92" smashed="yes">
<attribute name="NAME" x="84.455" y="125.095" size="1.27" layer="95"/>
<attribute name="VALUE" x="80.645" y="118.11" size="1.27" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="68.58" y="137.795"/>
<instance part="C4" gate="G$1" x="55.88" y="149.86" smashed="yes">
<attribute name="NAME" x="56.642" y="152.527" size="1.778" layer="95"/>
<attribute name="VALUE" x="53.213" y="145.796" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND6" gate="1" x="55.88" y="137.795"/>
<instance part="GND8" gate="1" x="220.345" y="128.905"/>
<instance part="GND11" gate="1" x="247.015" y="128.905"/>
<instance part="D2" gate="G$1" x="99.695" y="163.195" smashed="yes">
<attribute name="NAME" x="98.425" y="160.02" size="1.27" layer="95"/>
<attribute name="VALUE" x="94.615" y="165.735" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="24.13" y="168.275" smashed="yes">
<attribute name="VALUE" x="21.209" y="171.831" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="86.995" y="168.91" smashed="yes">
<attribute name="VALUE" x="82.804" y="172.466" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="92.075" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="88.773" y="159.004" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="93.853" y="160.655" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R10" gate="G$1" x="92.075" y="146.05" smashed="yes" rot="R90">
<attribute name="NAME" x="88.773" y="148.844" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="93.853" y="149.225" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND16" gate="1" x="92.075" y="137.795"/>
<instance part="U2" gate="G$1" x="130.175" y="146.685" smashed="yes">
<attribute name="NAME" x="125.095" y="152.273" size="1.778" layer="95"/>
<attribute name="VALUE" x="121.92" y="139.065" size="1.778" layer="96"/>
</instance>
<instance part="J3" gate="G$1" x="160.02" y="29.21" rot="R180"/>
<instance part="C10" gate="G$1" x="155.575" y="151.765" smashed="yes" rot="MR180">
<attribute name="NAME" x="156.591" y="152.019" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="156.591" y="144.399" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND18" gate="1" x="155.575" y="137.16" smashed="yes">
<attribute name="VALUE" x="153.035" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="140.97" y="155.575" smashed="yes" rot="R90">
<attribute name="NAME" x="137.668" y="158.369" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="142.748" y="158.75" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C9" gate="G$1" x="81.28" y="151.13" smashed="yes" rot="MR180">
<attribute name="NAME" x="75.819" y="147.574" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="79.756" y="151.511" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND19" gate="1" x="81.28" y="137.795"/>
<instance part="SUPPLY6" gate="G$1" x="233.045" y="153.67" smashed="yes">
<attribute name="VALUE" x="228.854" y="157.226" size="1.778" layer="96"/>
</instance>
<instance part="U$1" gate="G$1" x="152.4" y="19.05" smashed="yes">
<attribute name="VALUE" x="150.876" y="17.145" size="1.27" layer="96"/>
</instance>
<instance part="U$10" gate="G$1" x="149.225" y="36.83" smashed="yes">
<attribute name="VALUE" x="147.701" y="34.925" size="1.27" layer="96"/>
</instance>
<instance part="U$13" gate="G$1" x="83.82" y="62.865" smashed="yes">
<attribute name="VALUE" x="82.296" y="60.96" size="1.27" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="64.77" y="116.205" smashed="yes" rot="R90">
<attribute name="NAME" x="61.468" y="117.729" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="66.548" y="119.38" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R15" gate="G$1" x="64.77" y="104.775" smashed="yes" rot="R90">
<attribute name="NAME" x="61.468" y="107.569" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="66.548" y="107.95" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND20" gate="1" x="64.77" y="95.25"/>
<instance part="C8" gate="G$1" x="237.49" y="90.805" smashed="yes" rot="R270">
<attribute name="NAME" x="237.109" y="93.091" size="1.778" layer="95"/>
<attribute name="VALUE" x="235.839" y="86.741" size="1.778" layer="96"/>
</instance>
<instance part="GND21" gate="1" x="274.955" y="88.265"/>
<instance part="LED2" gate="G$1" x="259.715" y="85.725" smashed="yes">
<attribute name="NAME" x="257.81" y="94.107" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="257.175" y="78.105" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="GND22" gate="1" x="192.405" y="83.185"/>
<instance part="D7" gate="G$1" x="194.945" y="102.87" smashed="yes" rot="R270">
<attribute name="NAME" x="193.04" y="102.235" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="196.85" y="106.045" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="VLED" gate="G$1" x="187.325" y="99.695" smashed="yes" rot="R90">
<attribute name="NAME" x="184.15" y="101.981" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="SUPPLY8" gate="G$1" x="194.945" y="106.68" smashed="yes">
<attribute name="VALUE" x="193.294" y="110.236" size="1.778" layer="96"/>
</instance>
<instance part="U$6" gate="G$1" x="181.61" y="109.22" smashed="yes">
<attribute name="VALUE" x="180.086" y="110.236" size="1.6764" layer="96"/>
</instance>
<instance part="U4" gate="G$1" x="115.57" y="46.99" smashed="yes">
<attribute name="NAME" x="97.79" y="80.01" size="1.778" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="97.79" y="11.43" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND23" gate="1" x="92.71" y="11.43" smashed="yes">
<attribute name="VALUE" x="92.71" y="11.176" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="PAD1" gate="P" x="220.345" y="147.32" rot="R90"/>
<instance part="PAD2" gate="P" x="247.015" y="147.32" rot="R90"/>
<instance part="PAD3" gate="P" x="220.345" y="135.89" rot="R270"/>
<instance part="PAD4" gate="P" x="247.015" y="135.255" rot="R270"/>
<instance part="SUPPLY9" gate="G$1" x="153.035" y="66.675" smashed="yes">
<attribute name="VALUE" x="153.162" y="69.723" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U$14" gate="G$1" x="55.245" y="34.29" rot="R180"/>
<instance part="SDS" gate="1" x="33.655" y="22.86" smashed="yes">
<attribute name="NAME" x="31.242" y="19.177" size="1.778" layer="95"/>
<attribute name="VALUE" x="31.115" y="19.05" size="1.778" layer="96"/>
</instance>
<instance part="S5V" gate="1" x="168.91" y="26.67" smashed="yes">
<attribute name="VALUE" x="166.37" y="22.86" size="1.778" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="41.275" y="146.05" smashed="yes" rot="R90">
<attribute name="NAME" x="37.973" y="148.844" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="43.053" y="149.225" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R17" gate="G$1" x="41.275" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="37.973" y="160.274" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="43.053" y="160.655" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND24" gate="1" x="41.275" y="137.795"/>
<instance part="SUPPLY10" gate="G$1" x="41.275" y="168.275" smashed="yes">
<attribute name="VALUE" x="38.354" y="171.831" size="1.778" layer="96"/>
</instance>
<instance part="S3V" gate="1" x="168.91" y="29.21" smashed="yes">
<attribute name="VALUE" x="166.37" y="25.4" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="239.395" y1="32.385" x2="239.395" y2="31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="248.285" y1="37.465" x2="248.285" y2="31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="163.195" y1="153.035" x2="161.925" y2="153.035" width="0.1524" layer="91"/>
<wire x1="161.925" y1="153.035" x2="161.925" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="114.3" y1="139.7" x2="114.3" y2="135.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="36.83" y1="99.06" x2="36.83" y2="97.79" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="21.59" y1="25.4" x2="27.305" y2="25.4" width="0.1524" layer="91"/>
<wire x1="27.305" y1="25.4" x2="27.305" y2="22.86" width="0.1524" layer="91"/>
<label x="21.59" y="25.4" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="1"/>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="SDS" gate="1" pin="1"/>
<wire x1="27.305" y1="22.86" x2="27.305" y2="19.685" width="0.1524" layer="91"/>
<wire x1="28.575" y1="22.86" x2="27.305" y2="22.86" width="0.1524" layer="91"/>
<junction x="27.305" y="22.86"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="187.325" y1="139.7" x2="187.325" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="28.575" y1="97.79" x2="28.575" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J1" gate="J1" pin="GND"/>
</segment>
<segment>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="52.705" y1="102.235" x2="52.705" y2="97.79" width="0.1524" layer="91"/>
<wire x1="52.705" y1="97.79" x2="48.895" y2="97.79" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="48.895" y1="97.79" x2="45.72" y2="97.79" width="0.1524" layer="91"/>
<wire x1="45.72" y1="97.79" x2="45.72" y2="101.6" width="0.1524" layer="91"/>
<junction x="48.895" y="97.79"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="LED1" gate="G$1" pin="VSS"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="231.14" y1="90.805" x2="232.41" y2="90.805" width="0.1524" layer="91"/>
<wire x1="232.41" y1="90.805" x2="234.95" y2="90.805" width="0.1524" layer="91"/>
<wire x1="232.41" y1="89.535" x2="232.41" y2="90.805" width="0.1524" layer="91"/>
<junction x="232.41" y="90.805"/>
</segment>
<segment>
<pinref part="TGG" gate="G$1" pin="TP"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="49.53" y1="19.685" x2="49.53" y2="22.86" width="0.1524" layer="91"/>
<wire x1="49.53" y1="22.86" x2="52.705" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="RST" gate="G$1" pin="2"/>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="76.2" y1="26.035" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="U$8" gate="G$1" pin="GND"/>
<wire x1="83.82" y1="26.035" x2="83.82" y2="31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="122.555" y1="144.145" x2="120.015" y2="144.145" width="0.1524" layer="91"/>
<wire x1="120.015" y1="144.145" x2="120.015" y2="135.89" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="13.97" y1="143.51" x2="13.97" y2="142.24" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="13.97" y1="142.24" x2="24.13" y2="142.24" width="0.1524" layer="91"/>
<wire x1="24.13" y1="142.24" x2="33.655" y2="142.24" width="0.1524" layer="91"/>
<wire x1="33.655" y1="142.24" x2="33.655" y2="143.51" width="0.1524" layer="91"/>
<wire x1="24.13" y1="142.24" x2="24.13" y2="140.335" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<junction x="24.13" y="142.24"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="68.58" y1="140.335" x2="68.58" y2="155.575" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="55.88" y1="140.335" x2="55.88" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="220.345" y1="131.445" x2="220.345" y2="133.35" width="0.1524" layer="91"/>
<pinref part="PAD3" gate="P" pin="P"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="247.015" y1="131.445" x2="247.015" y2="132.715" width="0.1524" layer="91"/>
<pinref part="PAD4" gate="P" pin="P"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="92.075" y1="140.335" x2="92.075" y2="140.97" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="155.575" y1="139.7" x2="155.575" y2="146.685" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="81.28" y1="140.335" x2="81.28" y2="146.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="5"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="152.4" y1="21.59" x2="152.4" y2="24.13" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="79.375" y1="66.04" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="88.265" y1="66.04" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<wire x1="86.995" y1="66.04" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<wire x1="83.82" y1="66.04" x2="83.82" y2="65.405" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="GND"/>
<junction x="83.82" y="66.04"/>
</segment>
<segment>
<pinref part="IO9" gate="G$1" pin="2"/>
<wire x1="149.225" y1="39.37" x2="149.225" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="64.77" y1="97.79" x2="64.77" y2="99.695" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="272.415" y1="90.805" x2="274.955" y2="90.805" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="VSS"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="192.405" y1="86.995" x2="192.405" y2="85.725" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="95.25" y1="16.51" x2="92.71" y2="16.51" width="0.1524" layer="91"/>
<wire x1="92.71" y1="16.51" x2="92.71" y2="13.97" width="0.1524" layer="91"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="41.275" y1="140.335" x2="41.275" y2="140.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="29.21" y1="33.655" x2="29.21" y2="33.02" width="0.1524" layer="91"/>
<wire x1="21.59" y1="33.02" x2="29.21" y2="33.02" width="0.1524" layer="91"/>
<label x="21.59" y="33.02" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="4"/>
<junction x="29.21" y="33.02"/>
</segment>
<segment>
<wire x1="140.97" y1="57.15" x2="135.89" y2="57.15" width="0.1524" layer="91"/>
<label x="140.97" y="57.15" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="U4" gate="G$1" pin="MTDO/LP_SCL/IO7"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="36.83" y1="33.655" x2="36.83" y2="30.48" width="0.1524" layer="91"/>
<wire x1="29.21" y1="30.48" x2="36.83" y2="30.48" width="0.1524" layer="91"/>
<wire x1="21.59" y1="30.48" x2="29.21" y2="30.48" width="0.1524" layer="91"/>
<label x="21.59" y="30.48" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="3"/>
<junction x="29.21" y="30.48"/>
</segment>
<segment>
<wire x1="135.89" y1="59.69" x2="140.97" y2="59.69" width="0.1524" layer="91"/>
<label x="140.97" y="59.69" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="U4" gate="G$1" pin="MTCK/LP_SDA/IO6"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="29.21" y1="45.72" x2="36.83" y2="45.72" width="0.1524" layer="91"/>
<wire x1="29.21" y1="43.815" x2="29.21" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="36.83" y1="43.815" x2="36.83" y2="45.72" width="0.1524" layer="91"/>
<pinref part="P+7" gate="3V3-EXT" pin="3V3-EXT"/>
<wire x1="29.21" y1="46.99" x2="29.21" y2="45.72" width="0.1524" layer="91"/>
<junction x="29.21" y="45.72"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="248.285" y1="62.23" x2="248.285" y2="67.31" width="0.1524" layer="91"/>
<pinref part="3V3" gate="3V3-EXT" pin="3V3-EXT"/>
<wire x1="248.285" y1="67.31" x2="248.285" y2="68.58" width="0.1524" layer="91"/>
<pinref part="OK1" gate="G$1" pin="C"/>
<wire x1="236.855" y1="47.625" x2="240.665" y2="47.625" width="0.1524" layer="91"/>
<wire x1="240.665" y1="47.625" x2="240.665" y2="67.31" width="0.1524" layer="91"/>
<wire x1="248.285" y1="67.31" x2="240.665" y2="67.31" width="0.1524" layer="91"/>
<junction x="248.285" y="67.31"/>
<pinref part="SLED" gate="G$1" pin="2"/>
<wire x1="250.19" y1="67.31" x2="248.285" y2="67.31" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="OUT"/>
<pinref part="U$5" gate="G$1" pin="3.3V"/>
<wire x1="187.325" y1="164.465" x2="187.325" y2="163.195" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="187.325" y1="163.195" x2="180.975" y2="163.195" width="0.1524" layer="91"/>
<wire x1="187.325" y1="160.02" x2="187.325" y2="163.195" width="0.1524" layer="91"/>
<junction x="187.325" y="163.195"/>
</segment>
<segment>
<wire x1="21.59" y1="27.94" x2="29.21" y2="27.94" width="0.1524" layer="91"/>
<label x="21.59" y="27.94" size="1.778" layer="95"/>
<pinref part="J2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="G$1" pin="3.3V"/>
<wire x1="84.455" y1="74.93" x2="84.455" y2="80.01" width="0.1524" layer="91"/>
<junction x="84.455" y="74.93"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="88.265" y1="74.93" x2="88.265" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="79.375" y1="74.93" x2="79.375" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="3V3"/>
<wire x1="79.375" y1="74.93" x2="84.455" y2="74.93" width="0.1524" layer="91"/>
<wire x1="84.455" y1="74.93" x2="88.265" y2="74.93" width="0.1524" layer="91"/>
<junction x="88.265" y="74.93"/>
<wire x1="88.265" y1="74.93" x2="95.25" y2="74.93" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="SUPPLY5" gate="G$1" pin="3.3V"/>
<wire x1="83.82" y1="53.34" x2="83.82" y2="52.07" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VLED" gate="G$1" pin="1"/>
<pinref part="U$6" gate="G$1" pin="3.3V"/>
<wire x1="181.61" y1="106.68" x2="181.61" y2="99.695" width="0.1524" layer="91"/>
<wire x1="181.61" y1="99.695" x2="182.245" y2="99.695" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="SUPPLY9" gate="G$1" pin="3.3V"/>
<wire x1="153.035" y1="66.675" x2="153.035" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="173.99" y1="29.21" x2="172.72" y2="29.21" width="0.1524" layer="91"/>
<label x="173.99" y="29.21" size="1.27" layer="95" xref="yes"/>
<pinref part="S3V" gate="1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="OK1" gate="G$1" pin="AC2"/>
<wire x1="213.995" y1="42.545" x2="207.645" y2="42.545" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="OK1" gate="G$1" pin="E"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="236.855" y1="42.545" x2="239.395" y2="42.545" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="239.395" y1="42.545" x2="243.205" y2="42.545" width="0.1524" layer="91"/>
<junction x="239.395" y="42.545"/>
</segment>
</net>
<net name="IO9" class="0">
<segment>
<pinref part="IO9" gate="G$1" pin="1"/>
<wire x1="149.225" y1="52.07" x2="149.225" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="IO9"/>
<wire x1="135.89" y1="52.07" x2="149.225" y2="52.07" width="0.1524" layer="91"/>
<label x="154.305" y="52.07" size="1.27" layer="95" font="vector" xref="yes"/>
<junction x="149.225" y="52.07"/>
<wire x1="149.225" y1="52.07" x2="154.305" y2="52.07" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<pinref part="J1" gate="J1" pin="D-"/>
<wire x1="28.575" y1="116.84" x2="33.655" y2="116.84" width="0.1524" layer="91"/>
<label x="29.21" y="116.84" size="1.778" layer="95"/>
<label x="33.655" y="116.84" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="USB_D-/IO12"/>
<wire x1="135.89" y1="44.45" x2="138.43" y2="44.45" width="0.1524" layer="91"/>
<label x="138.43" y="44.45" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<pinref part="J1" gate="J1" pin="D+"/>
<wire x1="28.575" y1="119.38" x2="33.655" y2="119.38" width="0.1524" layer="91"/>
<label x="29.21" y="119.38" size="1.778" layer="95"/>
<label x="33.655" y="119.38" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="USB_D+/IO13"/>
<wire x1="135.89" y1="41.91" x2="138.43" y2="41.91" width="0.1524" layer="91"/>
<label x="138.43" y="41.91" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="V_USB" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="53.975" y1="121.92" x2="64.77" y2="121.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="G$1" pin="VIN"/>
<wire x1="64.77" y1="121.285" x2="64.77" y2="121.92" width="0.1524" layer="91"/>
<wire x1="64.77" y1="123.825" x2="64.77" y2="121.92" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="64.77" y1="121.92" x2="82.55" y2="121.92" width="0.1524" layer="91"/>
<junction x="64.77" y="121.92"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="260.985" y1="56.515" x2="260.985" y2="55.88" width="0.1524" layer="91"/>
<pinref part="LTIC" gate="G$1" pin="+"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<label x="29.21" y="122.555" size="1.778" layer="95"/>
<wire x1="33.655" y1="121.92" x2="28.575" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J1" gate="J1" pin="VBUS"/>
<wire x1="33.655" y1="121.92" x2="43.815" y2="121.92" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<junction x="33.655" y="121.92"/>
</segment>
<segment>
<label x="174.625" y="26.67" size="1.27" layer="95" xref="yes"/>
<pinref part="S5V" gate="1" pin="2"/>
<wire x1="173.99" y1="26.67" x2="174.625" y2="26.67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="J1" gate="J1" pin="CC1"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="52.705" y1="112.395" x2="52.705" y2="114.3" width="0.1524" layer="91"/>
<wire x1="52.705" y1="114.3" x2="28.575" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J1" gate="J1" pin="CC2"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="45.72" y1="111.76" x2="28.575" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SHIELD" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="28.575" y1="109.22" x2="36.83" y2="109.22" width="0.1524" layer="91"/>
<pinref part="J1" gate="J1" pin="SHLD"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<wire x1="95.25" y1="69.85" x2="93.345" y2="69.85" width="0.1524" layer="91"/>
<wire x1="93.345" y1="69.85" x2="93.345" y2="41.275" width="0.1524" layer="91"/>
<wire x1="93.345" y1="41.275" x2="83.82" y2="41.275" width="0.1524" layer="91"/>
<wire x1="83.82" y1="41.275" x2="76.2" y2="41.275" width="0.1524" layer="91"/>
<wire x1="76.2" y1="38.1" x2="76.2" y2="41.275" width="0.1524" layer="91"/>
<pinref part="RST" gate="G$1" pin="1"/>
<wire x1="76.2" y1="41.275" x2="73.025" y2="41.275" width="0.1524" layer="91"/>
<label x="73.025" y="41.275" size="1.016" layer="95" rot="R180" xref="yes"/>
<junction x="76.2" y="41.275"/>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="83.82" y1="41.91" x2="83.82" y2="41.275" width="0.1524" layer="91"/>
<junction x="83.82" y="41.275"/>
<wire x1="83.82" y1="41.275" x2="83.82" y2="39.37" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="EN"/>
</segment>
</net>
<net name="TIC_RX" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="D"/>
<pinref part="R5" gate="G$1" pin="1"/>
<junction x="248.285" y="47.625"/>
<wire x1="267.335" y1="47.625" x2="260.985" y2="47.625" width="0.1524" layer="91"/>
<wire x1="260.985" y1="47.625" x2="248.285" y2="47.625" width="0.1524" layer="91"/>
<wire x1="248.285" y1="47.625" x2="248.285" y2="52.07" width="0.1524" layer="91"/>
<label x="267.335" y="47.625" size="1.27" layer="95" xref="yes"/>
<pinref part="LTIC" gate="G$1" pin="-"/>
<wire x1="260.985" y1="48.26" x2="260.985" y2="47.625" width="0.1524" layer="91"/>
<junction x="260.985" y="47.625"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="MTMS/LP_RX/IO4"/>
<wire x1="135.89" y1="64.77" x2="139.065" y2="64.77" width="0.1524" layer="91"/>
<label x="139.065" y="64.77" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="NEO" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="DI"/>
<wire x1="203.2" y1="83.185" x2="200.66" y2="83.185" width="0.1524" layer="91"/>
<label x="200.66" y="83.185" size="0.8128" layer="95" font="vector" ratio="15" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IO2"/>
<wire x1="135.89" y1="69.85" x2="140.97" y2="69.85" width="0.1524" layer="91"/>
<label x="140.97" y="69.85" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="MBAT" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="92.075" y1="152.4" x2="92.075" y2="151.765" width="0.1524" layer="91"/>
<wire x1="92.075" y1="151.765" x2="92.075" y2="151.13" width="0.1524" layer="91"/>
<wire x1="92.075" y1="151.765" x2="97.155" y2="151.765" width="0.1524" layer="91"/>
<junction x="92.075" y="151.765"/>
<label x="97.155" y="151.765" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="IO3"/>
<wire x1="135.89" y1="67.31" x2="140.97" y2="67.31" width="0.1524" layer="91"/>
<label x="140.97" y="67.31" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="IN"/>
<wire x1="163.195" y1="163.195" x2="155.575" y2="163.195" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="G$1" pin="VIN"/>
<wire x1="155.575" y1="163.195" x2="140.97" y2="163.195" width="0.1524" layer="91"/>
<wire x1="140.97" y1="163.195" x2="106.045" y2="163.195" width="0.1524" layer="91"/>
<wire x1="114.3" y1="149.225" x2="114.3" y2="147.32" width="0.1524" layer="91"/>
<junction x="106.045" y="163.195"/>
<wire x1="106.045" y1="163.195" x2="106.045" y2="168.91" width="0.1524" layer="91"/>
<wire x1="122.555" y1="149.225" x2="114.3" y2="149.225" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="106.045" y1="149.225" x2="114.3" y2="149.225" width="0.1524" layer="91"/>
<wire x1="87.63" y1="121.92" x2="106.045" y2="121.92" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="102.235" y1="163.195" x2="106.045" y2="163.195" width="0.1524" layer="91"/>
<junction x="106.045" y="163.195"/>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="106.045" y1="121.92" x2="106.045" y2="149.225" width="0.1524" layer="91"/>
<junction x="106.045" y="149.225"/>
<wire x1="106.045" y1="163.195" x2="106.045" y2="149.225" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="155.575" y1="154.305" x2="155.575" y2="163.195" width="0.1524" layer="91"/>
<junction x="155.575" y="163.195"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="140.97" y1="160.655" x2="140.97" y2="163.195" width="0.1524" layer="91"/>
<junction x="140.97" y="163.195"/>
<junction x="114.3" y="149.225"/>
</segment>
<segment>
<pinref part="SUPPLY8" gate="G$1" pin="VIN"/>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="194.945" y1="106.68" x2="194.945" y2="105.41" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="EN"/>
<wire x1="137.795" y1="146.685" x2="140.97" y2="146.685" width="0.1524" layer="91"/>
<wire x1="140.97" y1="146.685" x2="149.225" y2="146.685" width="0.1524" layer="91"/>
<wire x1="149.225" y1="146.685" x2="149.225" y2="158.115" width="0.1524" layer="91"/>
<wire x1="149.225" y1="158.115" x2="163.195" y2="158.115" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VOUT"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="140.97" y1="150.495" x2="140.97" y2="146.685" width="0.1524" layer="91"/>
<junction x="140.97" y="146.685"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="13.97" y1="148.59" x2="13.97" y2="150.495" width="0.1524" layer="91"/>
<wire x1="13.97" y1="150.495" x2="13.97" y2="153.035" width="0.1524" layer="91"/>
<wire x1="13.97" y1="150.495" x2="16.51" y2="150.495" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="1"/>
<junction x="13.97" y="150.495"/>
<label x="18.415" y="150.495" size="1.016" layer="95" xref="yes"/>
<wire x1="16.51" y1="150.495" x2="18.415" y2="150.495" width="0.1524" layer="91"/>
<junction x="16.51" y="150.495"/>
</segment>
</net>
<net name="I1" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="33.655" y1="148.59" x2="33.655" y2="150.495" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="1"/>
<wire x1="33.655" y1="150.495" x2="33.655" y2="153.67" width="0.1524" layer="91"/>
<junction x="33.655" y="150.495"/>
<label x="29.21" y="150.495" size="1.016" layer="95" rot="R180" xref="yes"/>
<wire x1="29.21" y1="150.495" x2="30.48" y2="150.495" width="0.1524" layer="91"/>
<junction x="30.48" y="150.495"/>
<wire x1="30.48" y1="150.495" x2="33.655" y2="150.495" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="OK1" gate="G$1" pin="AC1"/>
<wire x1="213.995" y1="47.625" x2="199.39" y2="47.625" width="0.1524" layer="91"/>
<label x="199.39" y="47.625" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2" class="0">
<segment>
<wire x1="24.13" y1="150.495" x2="24.13" y2="148.59" width="0.1524" layer="91"/>
<label x="24.13" y="148.59" size="1.016" layer="95" rot="R270" xref="yes"/>
<pinref part="U$9" gate="G$1" pin="1"/>
<junction x="24.13" y="150.495"/>
</segment>
<segment>
<wire x1="199.39" y1="37.465" x2="202.565" y2="37.465" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="A"/>
<label x="199.39" y="37.465" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V_SCAP" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VOUT"/>
<wire x1="78.74" y1="163.195" x2="81.28" y2="163.195" width="0.1524" layer="91"/>
<wire x1="81.28" y1="163.195" x2="86.995" y2="163.195" width="0.1524" layer="91"/>
<wire x1="86.995" y1="163.195" x2="92.075" y2="163.195" width="0.1524" layer="91"/>
<wire x1="92.075" y1="163.195" x2="97.155" y2="163.195" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<pinref part="SUPPLY4" gate="G$1" pin="VIN"/>
<wire x1="86.995" y1="168.91" x2="86.995" y2="163.195" width="0.1524" layer="91"/>
<junction x="86.995" y="163.195"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="92.075" y1="162.56" x2="92.075" y2="163.195" width="0.1524" layer="91"/>
<junction x="92.075" y="163.195"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="81.28" y1="153.67" x2="81.28" y2="163.195" width="0.1524" layer="91"/>
<junction x="81.28" y="163.195"/>
</segment>
<segment>
<wire x1="220.345" y1="149.86" x2="233.045" y2="149.86" width="0.1524" layer="91"/>
<wire x1="233.045" y1="149.86" x2="247.015" y2="149.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="G$1" pin="VIN"/>
<wire x1="233.045" y1="149.86" x2="233.045" y2="153.67" width="0.1524" layer="91"/>
<junction x="233.045" y="149.86"/>
<pinref part="PAD1" gate="P" pin="P"/>
<junction x="220.345" y="149.86"/>
<pinref part="PAD2" gate="P" pin="P"/>
<junction x="247.015" y="149.86"/>
</segment>
</net>
<net name="VTIC" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="13.97" y1="158.115" x2="13.97" y2="160.02" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="13.97" y1="160.02" x2="24.13" y2="160.02" width="0.1524" layer="91"/>
<wire x1="24.13" y1="160.02" x2="33.655" y2="160.02" width="0.1524" layer="91"/>
<wire x1="33.655" y1="160.02" x2="33.655" y2="158.75" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VIN"/>
<wire x1="58.42" y1="163.195" x2="55.88" y2="163.195" width="0.1524" layer="91"/>
<wire x1="55.88" y1="163.195" x2="41.275" y2="163.195" width="0.1524" layer="91"/>
<wire x1="41.275" y1="163.195" x2="24.13" y2="163.195" width="0.1524" layer="91"/>
<wire x1="24.13" y1="163.195" x2="24.13" y2="160.02" width="0.1524" layer="91"/>
<junction x="24.13" y="160.02"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="55.88" y1="154.94" x2="55.88" y2="163.195" width="0.1524" layer="91"/>
<junction x="55.88" y="163.195"/>
<pinref part="SUPPLY1" gate="G$1" pin="VIN"/>
<wire x1="24.13" y1="163.195" x2="24.13" y2="168.275" width="0.1524" layer="91"/>
<junction x="24.13" y="163.195"/>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="G$1" pin="VIN"/>
<wire x1="41.275" y1="168.275" x2="41.275" y2="163.195" width="0.1524" layer="91"/>
<junction x="41.275" y="163.195"/>
<wire x1="41.275" y1="163.195" x2="41.275" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="4"/>
<wire x1="163.83" y1="26.67" x2="152.4" y2="26.67" width="0.1524" layer="91"/>
<pinref part="S5V" gate="1" pin="1"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<wire x1="136.525" y1="31.75" x2="152.4" y2="31.75" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
<label x="139.7" y="31.75" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="RX0/IO17"/>
<wire x1="135.89" y1="31.75" x2="136.525" y2="31.75" width="0.1524" layer="91"/>
<label x="165.735" y="31.75" size="1.27" layer="95" font="vector" xref="yes"/>
<junction x="136.525" y="31.75"/>
<wire x1="136.525" y1="31.75" x2="165.735" y2="31.75" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="152.4" y1="34.29" x2="137.16" y2="34.29" width="0.1524" layer="91"/>
<label x="139.7" y="34.29" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="TX0/IO16"/>
<wire x1="135.89" y1="34.29" x2="137.16" y2="34.29" width="0.1524" layer="91"/>
<label x="165.735" y="34.29" size="1.27" layer="95" font="vector" xref="yes"/>
<junction x="137.16" y="34.29"/>
<wire x1="137.16" y1="34.29" x2="165.735" y2="34.29" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UBAT" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="64.77" y1="111.125" x2="64.77" y2="110.49" width="0.1524" layer="91"/>
<wire x1="64.77" y1="110.49" x2="64.77" y2="109.855" width="0.1524" layer="91"/>
<wire x1="64.77" y1="110.49" x2="67.945" y2="110.49" width="0.1524" layer="91"/>
<junction x="64.77" y="110.49"/>
<label x="67.945" y="110.49" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="XTAL_32K_N/IO1"/>
<wire x1="135.89" y1="72.39" x2="140.97" y2="72.39" width="0.1524" layer="91"/>
<label x="140.97" y="72.39" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="260.985" y1="66.675" x2="260.985" y2="67.31" width="0.1524" layer="91"/>
<pinref part="SLED" gate="G$1" pin="1"/>
<wire x1="260.985" y1="67.31" x2="260.35" y2="67.31" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NEO1" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="DI"/>
<pinref part="LED1" gate="G$1" pin="DO"/>
<wire x1="231.14" y1="83.185" x2="244.475" y2="83.185" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VLED" class="0">
<segment>
<wire x1="202.565" y1="94.615" x2="192.405" y2="94.615" width="0.1524" layer="91"/>
<wire x1="202.565" y1="90.805" x2="202.565" y2="94.615" width="0.1524" layer="91"/>
<junction x="210.185" y="99.06"/>
<label x="224.155" y="100.965" size="1.778" layer="95" rot="R180"/>
<pinref part="LED1" gate="G$1" pin="VDD"/>
<wire x1="202.565" y1="94.615" x2="202.565" y2="99.06" width="0.1524" layer="91"/>
<wire x1="202.565" y1="99.06" x2="210.185" y2="99.06" width="0.1524" layer="91"/>
<wire x1="203.2" y1="90.805" x2="202.565" y2="90.805" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="P+1" gate="3V3-EXT" pin="3V3-EXT"/>
<wire x1="210.185" y1="99.06" x2="210.185" y2="107.315" width="0.1524" layer="91"/>
<wire x1="243.205" y1="99.06" x2="243.205" y2="90.805" width="0.1524" layer="91"/>
<wire x1="243.205" y1="90.805" x2="242.57" y2="90.805" width="0.1524" layer="91"/>
<junction x="243.205" y="90.805"/>
<pinref part="LED2" gate="G$1" pin="VDD"/>
<wire x1="244.475" y1="90.805" x2="243.205" y2="90.805" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="243.205" y1="99.06" x2="210.185" y2="99.06" width="0.1524" layer="91"/>
<pinref part="VLED" gate="G$1" pin="2"/>
<wire x1="187.325" y1="94.615" x2="192.405" y2="94.615" width="0.1524" layer="91"/>
<junction x="192.405" y="94.615"/>
<junction x="202.565" y="94.615"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<pinref part="VLED" gate="G$1" pin="3"/>
<wire x1="194.945" y1="100.33" x2="194.945" y2="99.695" width="0.1524" layer="91"/>
<wire x1="194.945" y1="99.695" x2="192.405" y2="99.695" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO8" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="IO8"/>
<wire x1="135.89" y1="54.61" x2="153.035" y2="54.61" width="0.1524" layer="91"/>
<label x="154.305" y="54.61" size="1.27" layer="95" xref="yes"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="153.035" y1="54.61" x2="154.305" y2="54.61" width="0.1524" layer="91"/>
<wire x1="153.035" y1="54.61" x2="153.035" y2="55.88" width="0.1524" layer="91"/>
<junction x="153.035" y="54.61"/>
</segment>
</net>
<net name="IO5" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="MTDI/LP_TX/IO5"/>
<wire x1="135.89" y1="62.23" x2="140.97" y2="62.23" width="0.1524" layer="91"/>
<label x="140.97" y="62.23" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="41.275" y1="152.4" x2="41.275" y2="151.765" width="0.1524" layer="91"/>
<wire x1="41.275" y1="151.765" x2="41.275" y2="151.13" width="0.1524" layer="91"/>
<wire x1="41.275" y1="151.765" x2="44.45" y2="151.765" width="0.1524" layer="91"/>
<label x="44.45" y="151.765" size="1.016" layer="95" xref="yes"/>
<junction x="41.275" y="151.765"/>
</segment>
</net>
<net name="IO18" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SDIO_CMD/IO18"/>
<wire x1="135.89" y1="29.21" x2="138.43" y2="29.21" width="0.1524" layer="91"/>
<label x="138.43" y="29.21" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="TP"/>
<wire x1="52.705" y1="30.48" x2="51.435" y2="30.48" width="0.1524" layer="91"/>
<label x="51.435" y="30.48" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IO19" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SDIO_CLK/IO19"/>
<wire x1="135.89" y1="26.67" x2="138.43" y2="26.67" width="0.1524" layer="91"/>
<label x="138.43" y="26.67" size="1.27" layer="95" font="vector" xref="yes"/>
</segment>
<segment>
<wire x1="52.705" y1="34.29" x2="51.435" y2="34.29" width="0.1524" layer="91"/>
<label x="51.435" y="34.29" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="U$14" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="IO23" class="0">
<segment>
<wire x1="135.89" y1="16.51" x2="138.43" y2="16.51" width="0.1524" layer="91"/>
<label x="138.43" y="16.51" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="U4" gate="G$1" pin="SDIO_D3/IO23"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="TP"/>
<wire x1="52.705" y1="26.67" x2="51.435" y2="26.67" width="0.1524" layer="91"/>
<label x="51.435" y="26.67" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SDS" gate="1" pin="2"/>
<wire x1="38.735" y1="22.86" x2="39.37" y2="22.86" width="0.1524" layer="91"/>
<label x="39.37" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="IO14" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="IO14"/>
<wire x1="135.89" y1="39.37" x2="137.795" y2="39.37" width="0.1524" layer="91"/>
<label x="137.795" y="39.37" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="3V" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="3"/>
<wire x1="163.83" y1="29.21" x2="152.4" y2="29.21" width="0.1524" layer="91"/>
<pinref part="S3V" gate="1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="102,1,29.21,46.99,3V3-EXT,3V3,,,,"/>
<approved hash="102,1,248.285,68.58,3V3-EXT,3V3,,,,"/>
<approved hash="102,1,64.77,123.825,VIN,V_USB,,,,"/>
<approved hash="102,1,187.325,164.465,3.3V,3V3,,,,"/>
<approved hash="102,1,84.455,80.01,3.3V,3V3,,,,"/>
<approved hash="102,1,83.82,53.34,3.3V,3V3,,,,"/>
<approved hash="102,1,210.185,107.315,3V3-EXT,VLED,,,,"/>
<approved hash="102,1,24.13,168.275,VIN,VTIC,,,,"/>
<approved hash="102,1,86.995,168.91,VIN,V_SCAP,,,,"/>
<approved hash="102,1,233.045,153.67,VIN,V_SCAP,,,,"/>
<approved hash="102,1,181.61,106.68,3.3V,3V3,,,,"/>
<approved hash="102,1,153.035,66.675,3.3V,3V3,,,,"/>
<approved hash="102,1,41.275,168.275,VIN,VTIC,,,,"/>
<approved hash="201,1,210.185,107.315,3V3-EXT,3V3\, VLED,,,,"/>
<approved hash="201,1,248.285,68.58,3V3-EXT,3V3\, VLED,,,,"/>
<approved hash="201,1,29.21,46.99,3V3-EXT,3V3\, VLED,,,,"/>
<approved hash="201,1,41.275,168.275,VIN,V_USB\, VIN\, VTIC\, V_SCAP,,,,"/>
<approved hash="201,1,233.045,153.67,VIN,V_USB\, VIN\, VTIC\, V_SCAP,,,,"/>
<approved hash="201,1,86.995,168.91,VIN,V_USB\, VIN\, VTIC\, V_SCAP,,,,"/>
<approved hash="201,1,24.13,168.275,VIN,V_USB\, VIN\, VTIC\, V_SCAP,,,,"/>
<approved hash="201,1,64.77,123.825,VIN,V_USB\, VIN\, VTIC\, V_SCAP,,,,"/>
<approved hash="202,1,180.975,153.035,U3,BP,,,,"/>
<approved hash="104,1,21.59,25.4,J2,1,GND,,,"/>
<approved hash="104,1,21.59,27.94,J2,2,3V3,,,"/>
<approved hash="113,1,255.27,65.7945,SLED,,,,,"/>
<approved hash="113,1,155.617,27.8723,J3,,,,,"/>
<approved hash="113,1,219.172,149.581,PAD1,,,,,"/>
<approved hash="113,1,245.842,149.581,PAD2,,,,,"/>
<approved hash="113,1,221.518,133.629,PAD3,,,,,"/>
<approved hash="113,1,248.188,132.994,PAD4,,,,,"/>
<approved hash="113,1,33.655,21.662,SDS,,,,,"/>
<approved hash="113,1,168.91,26.67,S5V,,,,,"/>
<approved hash="113,1,168.91,29.21,S3V,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
