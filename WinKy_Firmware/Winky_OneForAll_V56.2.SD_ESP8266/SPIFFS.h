
//****************************//
//*** Prototypes fonctions ***//
//****************************//

void writeFile(fs::FS &fs, const char * path, const char * message);
String readFile(fs::FS &fs, const char * path);
void init_SPIFFS(void);

//**************************//
//*** Variables et autre ***//
//**************************//

String struseoptimwifi;
String struseled;
String strmidtrywificon;
String strDeepSleepSecondsOpti;
String struseinflux;
String strINFLUXDB_URL;
String strINFLUXDB_TOKEN;
String strINFLUXDB_ORG;
String strINFLUXDB_BUCKET;
String strwifi_ssid;
String strwifi_password;
String strwifi_passwordhtmlclean;
String strusemqtt;
String strmqtt_server;
String strmqtt_user;
String strmqtt_password;
String strmaintopicmqtt;
String strlinky_histstard;
