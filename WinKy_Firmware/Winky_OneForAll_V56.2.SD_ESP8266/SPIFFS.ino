

void init_SPIFFS(){
  if (!SPIFFS.begin()) {
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }
}



  

void majVariablesSPIFFS(){

  struseoptimwifi = readFile(SPIFFS, "/inputActiverOptimWifi.txt");
  struseled = readFile(SPIFFS, "/inputActiverLed.txt");
  strmidtrywificon = readFile(SPIFFS, "/inputMidConnexion.txt");
  strDeepSleepSecondsOpti = readFile(SPIFFS, "/inputOptimiDSSeconds.txt");
  delay(50);
  struseinflux = readFile(SPIFFS, "/inputActiverInflux.txt");
  strINFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
  strINFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
  strINFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
  strINFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");
  delay(50);
  strwifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
  strwifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
  strwifi_passwordhtmlclean = strwifi_password;
  strwifi_passwordhtmlclean.replace("%", "&#37;");
  delay(50);
  strusemqtt = readFile(SPIFFS, "/inputActiverMqtt.txt");
  strmqtt_server = readFile(SPIFFS, "/inputUrlMqtt.txt");
  strmqtt_user = readFile(SPIFFS, "/inputUserMqtt.txt");
  strmqtt_password = readFile(SPIFFS, "/inputPasswordMqtt.txt");
  strmaintopicmqtt = readFile(SPIFFS, "/inputMainTopicMqtt.txt");
  strlinky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt");

  delay(50);

  useoptimwifi = readFile(SPIFFS, "/inputActiverOptimWifi.txt") == "Active";
  useled = readFile(SPIFFS, "/inputActiverLed.txt") == "Active";
  midtrywificon = 10 * ((readFile(SPIFFS, "/inputMidConnexion.txt")).toInt());
  DeepSleepSecondsOpti = 1000000 * ((readFile(SPIFFS, "/inputOptimiDSSeconds.txt")).toFloat());
  delay(50);
  useinflux = readFile(SPIFFS, "/inputActiverInflux.txt") == "Active";
  INFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
  INFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
  INFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
  INFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");
  delay(50);
  wifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
  wifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
  usemqtt = readFile(SPIFFS, "/inputActiverMqtt.txt") == "Active";
  mqtt_server = readFile(SPIFFS, "/inputUrlMqtt.txt");
  mqtt_user = readFile(SPIFFS, "/inputUserMqtt.txt");
  mqtt_password = readFile(SPIFFS, "/inputPasswordMqtt.txt");
  strmaintopicmqtt = readFile(SPIFFS, "/inputMainTopicMqtt.txt");
  channel = readFile(SPIFFS, "/inputChannel.txt").toInt();
  AdressMacRouteur = readFile(SPIFFS, "/inputMacadress.txt");
  delay(100);
}



String readFile(fs::FS &fs, const char * path) {
  Serial.print("[SPIFFS] Reading file: ");
  Serial.print(path);
  File file = fs.open(path, "r");
  String fileContent;
  if (!file || file.isDirectory()) {
    Serial.println(" : empty file or failed to open file");
    Serial.println("[SPIFFS] WARNING");
    file.close();
    return String();
  }else{
    Serial.print(" : read from file: ");
    while (file.available()) {
      fileContent += String((char)file.read());
    }
    file.close();
  }
  Serial.println(fileContent);
  delay(10);
  return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.print("[SPIFFS] Writing file: ");
  Serial.print(path);
  File file = fs.open(path, "w");
  if (!file) {
    Serial.println("[SPIFFS] failed to open file for writing...");
    Serial.println("[SPIFFS] ERROR");
    file.close();
    return;
  }
  if (file.print(message)) {
    Serial.println(" : file written");
  } else {
    Serial.println(" : write failed");
    Serial.println("[SPIFFS] ERROR");
  }
  delay(10);
  file.close();
}