//****************************//
//*** Prototypes fonctions ***//
//****************************//


//**************************//
//*** Variables et autre ***//
//**************************//

/*Parametres InfluxDB*/
boolean useinflux = 0;
String INFLUXDB_URL;
// InfluxDB 2 server or cloud API authentication token (Use: InfluxDB UI -> Load Data -> Tokens -> <select token>)
String INFLUXDB_TOKEN;
// InfluxDB 2 organization id (Use: InfluxDB UI -> Settings -> Profile -> <name under tile> )
String INFLUXDB_ORG;
// InfluxDB 2 bucket name (Use: InfluxDB UI -> Load Data -> Buckets)
String INFLUXDB_BUCKET;