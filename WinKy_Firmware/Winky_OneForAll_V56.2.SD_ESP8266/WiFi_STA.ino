

void printMACAdrr(){
if (AdressMacRouteur != "") {
      int str_len = AdressMacRouteur.length() + 1;
      char str[str_len];
      AdressMacRouteur.toCharArray(str, str_len);
      bssid[0] = strtol(str, &ptr, HEX );
      for ( uint8_t i = 1; i < 6; i++ )
      {
        bssid[i] = strtol(ptr + 1, &ptr, HEX );
      }
      Serial.println("[WIFI-STA] Affichage de l'adresse mac du routeur :");
      Serial.print(bssid[0], HEX);
      for ( uint8_t i = 1; i < 6; i++)
      {
        Serial.print(':');
        Serial.print( bssid[i], HEX);
      }
      //original character array is preserved
      Serial.println();
      Serial.print(str);
    }
}




String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

String macToStrsimple(const uint8_t* mac)
{
  String result;
  for (int i = 3; i < 6; ++i) {
    result += String(mac[i], 16);
  }
  return result;
}

String composeClientID() {
  uint8_t mac[6];
  WiFi.macAddress(mac);
  String clientId;
  clientId += "xky-";
  clientId += macToStr(mac);
  return clientId;
}

void appendToHtmlLog(String message) {
  htmlLog += "<br>" + message + "\n"; // Ajoute le message dans la variable en HTML
}

void wifiSTATest() {
  Serial.println("[WIFI-STA] Essai du WiFi en mode STA...");
  appendToHtmlLog("[WIFI-STA] Essai du WiFi en mode STA...");

  uint8_t mac[6]; // Tableau de 6 octets pour la mac adresse
  WiFi.macAddress(mac); // Recupere adrr mac pour la stocker dans le tableau
  WiFi.hostname("winky-" + macToStrsimple(mac)); // Définit le nom d'hôte du module
  WiFi.enableInsecureWEP(); // Active le support pour le protocole WEP (obsolète)

  // Lit l'état précédent du Wi-Fi stocké dans la mémoire RTC
  ESP.rtcUserMemoryRead(RTC_USER_DATA_SLOT_WIFI_STATE, reinterpret_cast<uint32_t*>(&state), sizeof(state));

  // Tenter de reprendre la connexion Wi-Fi avec l'état précédent
  if (!WiFi.resumeFromShutdown(state)) {
    Serial.println("[WIFI-STA] Échec lors de la reprise de la connexion avec l'état précédent.");
    appendToHtmlLog("[WIFI-STA] Échec lors de la reprise de la connexion avec l'état précédent.");
  }

  // Vérifier si la connexion a réussi après 10 secondes
  int8_t wifiStatus = WiFi.waitForConnectResult(10000);
  if (wifiStatus != WL_CONNECTED) {
    Serial.print("[WIFI-STA] La connexion a échoué avec le code : ");
    Serial.println(wifiStatus);
    appendToHtmlLog("[WIFI-STA] La connexion a échoué avec le code : " + String(wifiStatus));

    // Désactiver l'enregistrement persistant des paramètres Wi-Fi
    WiFi.persistent(false);

    // Tentative de connexion manuelle avec SSID et mot de passe
    Serial.println("[WIFI-STA] Tentative de connexion via WiFi.begin()...");
    appendToHtmlLog("[WIFI-STA] Tentative de connexion via WiFi.begin()...");

    if (!WiFi.mode(WIFI_STA)) {
      Serial.println("[WIFI-STA] Échec : Impossible de passer en mode STA.");
      appendToHtmlLog("[WIFI-STA] Échec : Impossible de passer en mode STA.");
    }

    if (!WiFi.begin(wifi_ssid, wifi_password)) {
      Serial.println("[WIFI-STA] Échec : WiFi.begin() a échoué.");
      appendToHtmlLog("[WIFI-STA] Échec : WiFi.begin() a échoué.");
    }

    wifiStatus = WiFi.waitForConnectResult(10000);
    if (wifiStatus != WL_CONNECTED) {
      Serial.print("[WIFI-STA] Échec de connexion avec le code : ");
      Serial.println(wifiStatus);
      appendToHtmlLog("[WIFI-STA] Échec de connexion avec le code : " + String(wifiStatus));

      switch (wifiStatus) {
        case WL_NO_SSID_AVAIL:
          Serial.println("[WIFI-STA] SSID non disponible.");
          appendToHtmlLog("[WIFI-STA] SSID non disponible.");
          break;
        case WL_CONNECT_FAILED:
          Serial.println("[WIFI-STA] Connexion échouée (vérifier SSID/MDP).");
          appendToHtmlLog("[WIFI-STA] Connexion échouée (vérifier SSID/MDP).");
          break;
        case WL_CONNECTION_LOST:
          Serial.println("[WIFI-STA] Connexion perdue.");
          appendToHtmlLog("[WIFI-STA] Connexion perdue.");
          break;
        case WL_DISCONNECTED:
          Serial.println("[WIFI-STA] Déconnecté.");
          appendToHtmlLog("[WIFI-STA] Déconnecté.");
          break;
        default:
          Serial.println("[WIFI-STA] Erreur inconnue.");
          appendToHtmlLog("[WIFI-STA] Erreur inconnue.");
          break;
      }

      Serial.println("[WIFI-STA] Impossible de se connecter.");
      appendToHtmlLog("[WIFI-STA] <font color='FF5733'>Impossible de se connecter.</font>");
      return;
    } else {
      Serial.println("[WIFI-STA] Connexion réussie via WiFi.begin() !");
      appendToHtmlLog("[WIFI-STA]<font color='9FE855'> Connexion réussie via WiFi.begin() !</font>");
    }
  } else {
    Serial.println("[WIFI-STA] Connexion reprise avec succès !");
    appendToHtmlLog("[WIFI-STA]<font color='9FE855'> Connexion reprise avec succès !</font>");
  }

  delay(100);

  // Si l'ESP est connecté en mode STA (Station), on le déconnecte pour sortir du test
  if (WiFi.isConnected()) {
    Serial.println("[WIFI-STA] Déconnexion du réseau STA...");
    WiFi.disconnect();   // Déconnecter du réseau Wi-Fi
    delay(100);          // Petit délai pour s'assurer que la déconnexion est complète
  }
}


//Fonction de connexion utilisée lors de l'envoi des données par InfluxBD/MQTT 
void setup_wifi() {
  uint8_t mac[6]; //Tableau de 6 octets pour la mac adresse
  WiFi.macAddress(mac); //Recupere adrr mac pour la stocker dans le tableau
  WiFi.hostname("winky-" + macToStrsimple(mac)); // Définit le nom d'hôte du module
  WiFi.enableInsecureWEP(); // Active le support pour le protocole WEP / Obsolète et peu sécur ... pour de la compatibilitée ?
  // Lit l'état précédent du Wi-Fi stocké dans la mémoire RTC (mémoire conservée entre les redémarrages 'deepsleep') dans la variable 'state'. //Gain de temp et d'energie
  ESP.rtcUserMemoryRead(RTC_USER_DATA_SLOT_WIFI_STATE, reinterpret_cast<uint32_t*>(&state), sizeof(state));
  // Tenter de reprendre la connexion Wi-Fi avec l'état précédent
  if(!WiFi.resumeFromShutdown(state)) {
    Serial.println("[WIFI-STA] Échec lors de la reprise de la connexion avec l'état précédent.");
  }
  // Vérifier si la connexion a réussi après 10 secondes
  int8_t wifiStatus = WiFi.waitForConnectResult(10000);
  if(wifiStatus != WL_CONNECTED) {
    // Si la connexion échoue, afficher le code d'erreur spécifique
    Serial.print("[WIFI-STA] La connexion a échoué avec le code : ");
    Serial.println(wifiStatus);
    // Désactiver l'enregistrement persistant des paramètres Wi-Fi
    WiFi.persistent(false);
    // Tentative de connexion manuelle avec SSID et mot de passe
    Serial.println("[WIFI-STA] Tentative de connexion via WiFi.begin()...");
    // Passer en mode station et démarrer la connexion Wi-Fi
    if(!WiFi.mode(WIFI_STA)) {
      Serial.println("[WIFI-STA] Échec : Impossible de passer en mode STA.");
    }
    if(!WiFi.begin(wifi_ssid, wifi_password)) {
      Serial.println("[WIFI-STA] Échec : WiFi.begin() a échoué.");
    }
    // Vérifier à nouveau si la connexion est établie après 10 secondes
    wifiStatus = WiFi.waitForConnectResult(10000);
    if(wifiStatus != WL_CONNECTED) {
      Serial.print("[WIFI-STA] Échec de connexion avec le code : ");
      Serial.println(wifiStatus);
      switch (wifiStatus) {
        case WL_NO_SSID_AVAIL:
          Serial.println("[WIFI-STA] SSID non disponible.");
          break;
        case WL_CONNECT_FAILED:
          Serial.println("[WIFI-STA] Connexion échouée (vérifier SSID/MDP).");
          break;
        case WL_CONNECTION_LOST:
          Serial.println("[WIFI-STA] Connexion perdue.");
          break;
        case WL_DISCONNECTED:
          Serial.println("[WIFI-STA] Déconnecté.");
          break;
        default:
          Serial.println("[WIFI-STA] Erreur inconnue.");
          break;
      }
      // Désactiver le Wi-Fi si la connexion échoue
      WiFi.mode(WIFI_OFF);
      Serial.println("[WIFI-STA] Impossible de se connecter. Module Wi-Fi désactivé.");
      Serial.flush(); // Vider le buffer série
      // Mettre l'ESP en veille profonde pendant 10 secondes
      Serial.println("[ESP] Deep sleep pour 10s");
      delay(100);
      ESP.deepSleep(10e6, RF_DISABLED); //Si WiFi ne fonctionne pas, on redémarre et on recommence !
      return; // inutile car deepsleep
    }else{
      Serial.println("[WIFI-STA] Connexion réussie via WiFi.begin() !");
    }
  }else{
    Serial.println("[WIFI-STA] Connexion reprise avec succès !");
  }
}




//Connexion au reseau WiFi
void setup_wifi2() {
  //  WiFi.persistent(false); //Ajout
  //  WiFi.mode(WIFI_OFF); //Ajout
  //  delay(100); //Ajout
  WiFi.mode(WIFI_STA);
  delay(10);
  WiFi.setOutputPower(20.5);
  nbtrywificon = 0;
  uint8_t mac[6];
  WiFi.macAddress(mac);
  WiFi.hostname("winky-" + macToStrsimple(mac));
  WiFi.enableInsecureWEP();
  //  WiFi.disconnect(); //Ajout
  delay(100); //Ajout
  useoptimwifi = 0;
  // WiFi.setPhyMode(WIFI_PHY_MODE_11B);
  //WiFi.setPhyMode(WIFI_PHY_MODE_11N);

  WiFi.setPhyMode(WIFI_PHY_MODE_11G);
  if (AdressMacRouteur != "" && useoptimwifi) {
    WiFi.begin(wifi_ssid, wifi_password, channel, bssid);
    while (WiFi.status() != WL_CONNECTED) {
      delay(100);
      Serial.print(".");
      nbtrywificon = nbtrywificon + 1;
      if (nbtrywificon == midtrywificon) {
        SPIFFS.remove("/inputMacadress.txt");
      }
    }
  }
  else {
    WiFi.begin(wifi_ssid, wifi_password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(100);
      Serial.print(".");
    }
  }
  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());
  Serial.printf("\nWiFi channel: %d\n", WiFi.channel());
  Serial.printf("WiFi BSSID: %s\n", WiFi.BSSIDstr().c_str());
  if (useoptimwifi) {
    if (AdressMacRouteur != WiFi.BSSIDstr().c_str()) {
      writeFile(SPIFFS, "/inputMacadress.txt", WiFi.BSSIDstr().c_str());
    }
    if (String(channel) != String(WiFi.channel())) {
      writeFile(SPIFFS, "/inputChannel.txt", String(WiFi.channel()).c_str());
    }
  }
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'a obtenir une reconnexion
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    String clientnumber = composeClientID();
    client.setBufferSize(2048);
    if (client.connect(clientnumber.c_str(), mqtt_user.c_str(), mqtt_password.c_str())) {
      Serial.println("OK");
    } else {
      Serial.print("KO, erreur : ");
      Serial.print(client.state());
      Serial.println(" On attend 1 secondes avant de recommencer");
      delay(1000);
      nbtrymqttcon = nbtrymqttcon + 1;
      if (nbtrymqttcon == maxtrymqttcon) {
        ESP.deepSleep(30000000);
      }
    }
  }
}