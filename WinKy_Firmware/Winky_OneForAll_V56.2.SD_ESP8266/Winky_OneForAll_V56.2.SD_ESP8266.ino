#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <InfluxDbClient.h>

//MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD ---
//#include <ESPAsyncTCP.h>
//#include <ESPAsyncWebServeSDr.h>
#include <ESP8266WebServer.h>        //Librairie WiFi pour le mode AP
//#include <WiFiClientSecure.h> //Librairie pour HTTPS
#include <ESP8266HTTPClient.h>       //Librairie pour gestion HTTP
//MAJ V56.2.SD --- MAJ V56.2. --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD ---

#include <Hash.h>
#include <FS.h>
//#include <AsyncElegantOTA.h>  //Lib semble obsolète ? 
#include <ArduinoJson.h>
#include <include/WiFiState.h>  // WiFiState structure details

//Stocke l'état du WiFi dans la mémoire RTC lors d'un DeepSleep (gagne du temps et économie d'énergie).
#ifndef RTC_USER_DATA_SLOT_WIFI_STATE
#define RTC_USER_DATA_SLOT_WIFI_STATE 33u
#endif

#include "WiFi_AP.h"
#include "WiFi_STA.h"
#include "MQTT.h"
#include "InfluxDB.h"
#include "SPIFFS.h"
#include "IHM.h"


#define shuntLinkyHistMeas    1 // Fonction de debug, permet de faire fonctionner le programme sans que le système soit connecté au linky si 1.
#define deepSleepToDelay      1 // Fonction de debug, permet de remplacer un deepSleep par un delais si 1 (delais de 10s).
#define forceProgOrNormalMode 2 // Si à 0 = Selection auto du mode / Si à 1 = Prog mode / Si à 2 = Normal mode 


String FWVersion = "V56.2.SD";
String BoardVersion = "ESP8266";


long startloop;
long scanloop;
bool flagConfigModeForLoop = 1;

/*Parametres Linky*/
boolean progmode = 0;
boolean linky_histstard = 0;

/*Parametres Validation*/
boolean okpapp = 0;
boolean okpappi = 0;
boolean okbaseprod = 0;

/*Calibre tic auto*/
long calibtimefault = 4999;
long timestartfunc;// = millis();
boolean teleInfoproblem = 0;





/*Parametres xKy*/
long MQAll;
long DeepModulateSleeptime;
boolean useoptimwifi = 0;
boolean useled = 0;
int DeepSleepSecondsOpti = 0;
unsigned long timestartup;
unsigned long timedecode;
unsigned long timeconnexion;

//#include "linkyhist.h"
#include "linky.h"

void setup() {

  Serial.begin(115200);

  Serial.println("[ESP-SETUP] Start program !");

  init_IHM();    //Init Bp, leds, etc ...
  init_SPIFFS(); //Init fonctions de travail dans la mémoire flash
  
  //Si carte déconnectée du systéme de démodulation TIC, l'esp passe automatique en mode prog (ap, prog, etc ...).
  if(forceProgOrNormalMode == 1){
    progmode = 1;
  }else if(forceProgOrNormalMode == 2){
    progmode = 0;
  }else{
    progmode = digitalRead(hardSelectMode); 
  }

  if (progmode) {
    //On sort toutes les valeurs des fiches SPIFFS pour màj les varables et autres paramètres de fonctionnement
    majVariablesSPIFFS(); 
    //Test du WiFi en mode STA
    wifiSTATest();
    // Changer le mode WiFi pour Access Point (AP)
    Serial.println("[WIFI-AP] Passage en mode Access Point...");
    init_WifiAP(); //Prépare à passer en mode AP
    //AsyncElegantOTA.begin(&server); // Start AsyncElegantOTA // Lib OBSOLETE !!! 
    startloop = millis();
    scanloop = millis();
    scanwifi(); // Fait un scan des réseaux wifi dispo
  }else {

    Serial.println("[ESP-SETUP] Start init normal mode !");
    delay(10);
    //Mécanisme de gestion de l'état de redémarrage de l'ESP8266
    String resetCause = ESP.getResetReason();
    Serial.print("[ESP-SETUP] Dernier reset : ");
    Serial.println(resetCause);
    //Si le dernier redémarrage ne provient pas d'un deep sleep, c'est que l'esp à probalemet planté car pas asser d'energie pour lancer le réseau wifi ...
    //On plonge donc l'esp en deep sleep pour 60s et on coupant la RF à son réveil, pour laisser le temps a la super capa d'accumuler suffisament d'energie pour un démarrage :) 
    if (resetCause != "Deep-Sleep Wake") { 
      if (!deepSleepToDelay){ 
        Serial.println("[ESP-SETUP] DeepSleep pendant 60s.");
        ESP.deepSleep(60000000, WAKE_RF_DISABLED); //60000000 microsecondes = 1 minute.
      }else{
        Serial.println("[ESP-SETUP] DeepSleep pendant 60s.");
        delay(10000);
      }
    }

    //On sort toutes les valeurs des fiches SPIFFS pour màj les varables et autres paramètres de fonctionnement
    majVariablesSPIFFS(); 

    printMACAdrr(); //lecture adresse mac routeur

    linky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt") == "Standard";
    //Si le linky est prog en mode Standard :
    if (linky_histstard == 1) {
      Serial.println("[ESP-SETUP] Setup standard linky mode.");
      linky_setup();
    }
  }

  timestartup = millis();
  timestartfunc = millis();
  Serial.println("[ESP-SETUP] FIn de la configuration");
  delay(10);
}

void loop() {
  if(!progmode) {
    Serial.println("[ESP] Start normal mode !");
    if(linky_histstard == 0) {
      Serial.println("[ESP] Lecture des données sur le Linky en mode historique]"); 
      delay(10);
      linky_hist_loop();
    }else {
      Serial.println("[ESP] Lecture des données sur le Linky en mode standard]");
      delay(10);
      linky_loop();
    }
  }else {
    if (flagConfigModeForLoop){
      Serial.println("[ESP] Start config mode !");
      flagConfigModeForLoop = 0;
    }
    webApInterfaceRefresh();  //L'appel de la fonction interfaceAP doit être faite régulierement dans le loop. 
    ledwifiprogmode();
  }

}
