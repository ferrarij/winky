//****************************//
//*** Prototypes fonctions ***//
//****************************//

void publishMQTT(String maintTopic, String linkyOutput);

//**************************//
//*** Variables et autre ***//
//**************************//

/*Parametres MQTT*/
boolean usemqtt = 0;
String mqtt_server;
String mqtt_user;
String mqtt_password;
String maintopic;
PubSubClient client(espClient);
int maxtrymqttcon = 5;
int nbtrymqttcon = 0;