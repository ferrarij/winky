

void init_IHM(){
  pinMode(blueLED, OUTPUT);
  digitalWrite(blueLED, HIGH);
  delay(500);
  pinMode(hardSelectMode, INPUT);
}

void ledReadTeleInfo(){
  digitalWrite(blueLED, LOW);
  delay(100);
  digitalWrite(blueLED, HIGH);
  delay(100);
}

void ledwifiprogmode() {
  if ((millis() - startloop) < 499) {
    digitalWrite(blueLED, HIGH);
  } else if (((millis() - startloop) < 1000)) {
    digitalWrite(blueLED, LOW);
  } else {
    startloop = millis();
  }
}