//****************************//
//*** Prototypes fonctions ***//
//****************************//

void setup_wifi(void);
String composeClientID(void);
void reconnect(void);
void printMACAdrr(void);
void wifiSTATest(void);
void appendToHtmlLog(String message);

//**************************//
//*** Variables et autre ***//
//**************************//

WiFiState state;
//POUR LE MODE AP A DEPLACER !!! -------------------------------------------------------------------------------------------------------------------------------------------------------
//AsyncWebServer server(80);


/*Parametres Wifi*/
String wifi_ssid;
String wifi_password;
WiFiClient espClient;
int midtrywificon = 150;
int nbtrywificon = 0;
int channel = 11;
String AdressMacRouteur;
char str[] = "00:00:00:00:00:00";
uint8_t bssid[6];
char* ptr; //start and end pointer for strtol

String htmlLog = ""; // Variable globale pour stocker les logs conect WIFI mùode STA au format HTML

