




//MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD ---




//Prépart l'ESP32 en mode AP.
void init_WifiAP(){

  Serial.println("---------------------- Wifi AP pre-start --------------------------");
  Serial.println();
  Serial.print("[WIFI-AP] -> Setting soft-AP configuration ... ");
  Serial.println(WiFi.softAPConfig(local_ip, gateway, subnet) ? "Ready" : "Failed!");
  Serial.print("[WIFI-AP] -> Setting soft-AP ... ");
  Serial.println(WiFi.softAP(ssid,password) ? "Ready" : "Failed!");
  Serial.print("[WIFI-AP] -> Soft-AP IP address = ");
  Serial.println(WiFi.softAPIP());
  Serial.println("-------------------------------------------------------------------");
  Serial.println("----------------- Wifi AP maj global variable  --------------------");
  majVariablesSPIFFS();
  Serial.println("-------------------------------------------------------------------");
  delay(10);
  link(); //Etabli lien entre path et fonctions.
  server.begin(); //Init du serveur AP
  Serial.println("[WIFI-AP] -> HTTP server started !");
}

//Fonction utilisée dans init_WifiAP() et qui établie un lien entre le /path demandé par le client et la fonction qui va construire la page HTML. 
//server.on("/unURL", uneFonction); // "/unURL" <- URL demandé par l'utilisateur | "uneFonction" <- Fonction à appeler 
void link(){

  //Liste des pages et des fonctions qui leurs sont associées 
  //server.on("/", index);
  server.on("/", [](){index();});
  server.on("/reset", handle_reset);
  server.onNotFound(handle_NotFound);
  server.on("/influxdb", handle_influxDB);
  server.on("/wifistate", handle_wifiStatePage);
  server.on("/avance", handle_avanceConfiguration);
  server.on("/mqtt", handle_mqttConfiguration);

  //Pour les retours de formulaires :
  server.on("/receptionFormulaireIndex", receptionFormulaireIndex);
  server.on("/receptionFormulaireInfluxdb", receptionFormulaireInfluxdb); 
  server.on("/receptionFormulaireAvance", receptionFormulaireAvance);
  server.on("/receptionFormulaireMqtt", receptionFormulaireMqtt);

  //Pour les requetes AJAX :
  server.on("/getScanWifi", handleGetScanWifi);

}


//Renvoie la liste des réseaux wifi
void handleGetScanWifi() {
    scanwifi(); // Met à jour strscanwifi avec les réseaux disponibles
    server.send(200, "text/plain", strscanwifi); // Renvoie la liste des réseaux Wi-Fi
}


//Mise en mode AP, cette fonction doit être appelé régulierement dans le loop()
void webApInterfaceRefresh(){
  server.handleClient();
  exitWithDelay(); //Si l'utilisateur à oublier de sortir du mode AP, l'ESP sortira automatiquement de ce mode en fonction du delais configuré. 
}

//Fonction de sortie du mode AP. On redemarre l'esp pour sortire du mode AP et repartire en mode normal. 
//La fonction sera appelée dans webApInterfaceRefresh(); et le flag "tempsExitWithDelay" sera remis à 0 à chaque appel de fonction qui construit une page HTML
void exitWithDelay(){
  delay(1);
  tempsExitWithDelay++;
  if (tempsExitWithDelay > dureeExitWithDelay){
    Serial.println("[RESET] -> AP-MODE exit");
    ESP.restart(); //On redemarre l'esp pour sortire du mode AP et repartire en mode normal. 
  }
}

//Si l'utilisateur entre une URL non reconnue 
void handle_NotFound(){
  tempsExitWithDelay = 0;
  server.send(404, "text/plain", "Not found");
}

//Fonction de sortie du mode AP si demandé par l'utilisateur. On redemarre l'esp pour sortire du mode AP et repartire en mode normal. 
void handle_reset(){
  //On redirige sur la page d'index avant le redémarrage pour éviter que l'utilise quitte le mode AP par méguarde lors de sa prochaine connexion (page de sortie du mode enregistrée dans la barre d'url de l'utilisateur).
  String html = "<html>";
  html +="<head>";
  html +="<title>Redirection ...</title>";
  html +="<meta http-equiv=\"refresh\" content=\"1; URL=http://"+local_ip_redirect+"\">";
  html +="</head>";
  html +="<body>";
  html +="</body></html>";
  server.send(200, "text/html", html);
  delay(1000);
  Serial.println("[RESET] -> AP-MODE exit");
  ESP.restart();
}

/*@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@*/
/*@=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=@*/
/*@=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=  Pages CSS  =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=@*/
/*@=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=@*/
/*@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@*/



//Feuilles de style CSS
//On utilise les " " à chaque lignes pour que le compilateur ne nous embête pas avec les ';' du CSS. 

String CSS = "<style>"

  "body { "
    "font-family: Arial, sans-serif; "
    "color: #333;"
    "margin: 0; "
    "padding: 0; "
  "}"

  "h1, h2, h3, h4, h5, h6, p { "
    "text-align: center; "
    "color: #333; "
  "}"

  /* Div dans le style des form */
  ".divform, form { "
    "max-width: 600px; "
    "margin: auto; "
    "padding: 20px; "
    "background: #fff; "
    "border-radius: 10px; "
    "box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); "
  "}"

  /* Style des champs de formulaire */
  "input[type='text'], select, button, input[type='submit'] { "
    "margin: 10px 0; "
    "padding: 10px; "
    "width: 100%; "
    "border: 1px solid #ccc; "
    "border-radius: 5px; "
    "box-sizing: border-box; "
  "}"

  "button, input[type='submit'] { "
    "background-color: #4CAF50; "
    "color: white; "
    "border: none; "
    "cursor: pointer; "
    "padding: 10px; "
    "border-radius: 5px;"
  "}"

  ".menu_button_restart { "
    "background-color: #FD4626; "
    "color: white; "
    "border: none; "
    "cursor: pointer; "
    "padding: 10px; "
    "border-radius: 5px;"
  "}"

  "button:hover, input[type='submit']:hover { "
    "background-color: #45a049; "
  "}"

  ".menu-button {"
    "max-width: 600px;"
    "margin: 10px auto;"
    "padding: 10px;"
    "background-color: #636363;"
    "color: white;"
    "text-align: center;"
    "border: none;"
    "border-radius: 5px;"
    "width: calc(100% - 40px);"
    "cursor: pointer;"
  "}"

  ".menu-button:hover {"
    "background-color: #45a049;"
  "}"

  ".spacer {"
    "margin-bottom: 20px;"
  "}"

  "body {"
    "background-color: #d9d9d9; "
  "}"

  /* Style du tableau Wi-Fi */
  "table {"
    "width: 100%;"
    "border-collapse: collapse;"
    "margin: 20px 0;"
    "font-size: 12px;" /* Réduction de la taille du texte */
  "}"

  "th, td {"
    "padding: 8px;" /* Réduction des marges pour rendre plus compact */
    "border: 1px solid #ddd;"
    "text-align: center;"
  "}"

  "th {"
    "background-color: #4CAF50;"
    "color: white;"
  "}"

  "tr:nth-child(even) {"
    "background-color: #f2f2f2;"
  "}"

  "tr:hover {"
    "background-color: #ddd;"
  "}"

  /* Réduire la taille globale pour les petits écrans */
  "table {"
    "max-width: 100%;"
    "width: calc(100% - 40px);"
    "margin: auto;"
  "}"

  /* Ajout du overflow pour permettre le défilement horizontal si nécessaire */
  ".table-container {"
    "overflow-x: auto;" /* Permet de défiler horizontalement si nécessaire */
  "}"

  /* Style général pour tous les champs de formulaire */
  "input[list], select {"
  "    width: 100%;"
  "    padding: 10px;"
  "    margin: 8px 0;"
  "    display: inline-block;"
  "    border: 1px solid #ccc;"
  "    border-radius: 4px;"
  "    box-sizing: border-box;"
  "    font-size: 16px;"
  "    font-family: Arial, sans-serif;"
  "}"

  /* Style au survol pour donner un effet visuel */
  "input[list]:hover, select:hover {"
  "    border-color: #888;"
  "}"

  /* Apparence quand l'élément est sélectionné/focus */
  "input[list]:focus, select:focus {"
  "    outline: none;"
  "    border-color: #555;"
  "    box-shadow: 0 0 5px rgba(81, 203, 238, 1);"
  "}"



"</style>";
  
/*@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@*/
/*@=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=@*/
/*@=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Fonctions de création des pages HTML  =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=@*/
/*@=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=@*/
/*@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@=-=@*/

//Note : Dans la construction HTLM, pour echapper un ' " ' on fait : ' \" '.


//Si un client est connecté, on le dirige vers la page d'index.
void index() {
  tempsExitWithDelay = 0;
  scanwifi(); // Mets à jour la variable "strscanwifi" pour afficher la liste des wifi disponibles

  String html = "<!DOCTYPE HTML><html><head>";
  html += "<title>Configuration XKY</title>";
  html += "<meta charset='UTF-8'>";
  html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  html += "<script>";
  html += "setInterval(function(){getData01();}, 3000);\n"; // 3000ms update rate
  html += "function getData01() {\n";
  html += "  var xhttp = new XMLHttpRequest();\n";
  html += "  xhttp.onreadystatechange = function() {if (this.readyState == 4 && this.status == 200){document.getElementById('getScanWifi').innerHTML = this.responseText;}};\n";
  html += "  xhttp.open('GET', 'getScanWifi', true);\n";
  html += "  xhttp.send();\n";
  html += "}\n";
  html += "</script>";
  html += CSS;
  html += "</head>";
  html += "<body>";

  html += "<div class='section-white'>";
  html += "<h1>Configuration du module XKY</h1>";
  html += "<p><b>Wi-Fi</b></p>";
  html += "</div>";

  html += "<form action='/receptionFormulaireIndex'>";
  html += "<p><b>Configuration WiFi :</p></b>";
  html += "Nom du réseau Wi-Fi (SSID) : ";
  html += "<input list='list_ssid' id='input1' name='inputWifiSsid' value='"+strwifi_ssid+"' />\n";
  html += "<datalist id='list_ssid'>\n";
  html += strScanwifiDataList; //Précharge les réseaux Wi-Fi avant l'exécution de la fonction AJAX 
  html += "</datalist>\n";

  html += "Mot de passe : <input type='text' name='inputWifiPassword' value='"+strwifi_password+"'><br><br>";

  //SPAN pour fonction AJAX de mise à jour des réseaux Wi-Fi
  html += "<span id='getScanWifi'><p>\n";
  html += strscanwifi; //Précharge les réseaux Wi-Fi avant l'exécution de la fonction AJAX 
  html += "</p></span>\n";

  html += "<br><p><b>Configuration Linky</b></p>";
  html += "<label for='Mode'>Mode actuel :</label>";
  html += "<select name='inputModeHistStand' id='Mode'>";

  // Vérifiez si la variable strlinky_histstard contient "Historique" ou "Standard" pour définir l'option sélectionnée
  if (strlinky_histstard == "Historique") {
    html += "<option value='Historique' selected>Historique</option>";
    html += "<option value='Standard'>Standard</option>";
  } else {
    html += "<option value='Historique'>Historique</option>";
    html += "<option value='Standard' selected>Standard</option>";
  }

  html += "</select>";
  html += "<br><br>";
  //html += "<input type='submit' value='Enregistrer les modifications' onclick='submitMessage()'>";
  html +="<input class='button button-off' type='submit' value='Enregistrer les modifications'>\n"; //Bouton pour envoyer le formulaire
  html += "</form><br>";

  html += "<div class='menu-button' onclick='window.location.href = \"/influxdb\";'>Configuration InfluxDB</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/mqtt\";'>Configuration MQTT</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/avance\";'>Configuration avancée</div>";
  //html += "<div class='menu-button' onclick='window.location.href = \"/maj\";'>Mise à jour du logiciel</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/wifistate\";'>État du réseau</div><br>";

  html += "<p class='spacer'>One For All version 1er détenteur Board : "+BoardVersion+" Version logicielle : "+FWVersion+"</p>";
  html += "<iframe style='display:none' name='hidden-form'></iframe>";

  html += "</body></html>";

  server.send(200, "text/html", html);
}


void receptionFormulaireIndex(){
  tempsExitWithDelay = 0; //On remets à 0 le compteurs de sortie du mode AP automatique.

  Serial.println("[HTML] -> Index data set");
  String html = "<html>";
  html +="<head>";
  html +="<title>UN TITRE | Redirection ...</title>";
  html += "<meta charset='UTF-8'>";
  html +="<meta http-equiv=\"refresh\" content=\"1; URL=http://"+local_ip_redirect+"\">"; //Redirige sur la page d'index au bout d'une seconde
  html += CSS;
  html +="</head>";
  html +="<body>";
  html += "<br><br><center><h1 style=\"color:#7FBB1B;\">Les saisies du formulaire ont bien &eacute;t&eacute; prises en compte</h1></center><br>\n";
  html += "<center><h4>Redirection ...</h4></center>\n";

  //Boucle d'extraction des données :)
  for (int i = 0; i < server.args(); i++) { //serve.args() contient le nb de données passées par le formulaire
    Serial.println("[AP-MODE] Variables recus par le formulaire de la page précédente : ");
    Serial.print("  ---> Data [");Serial.print(i);Serial.print("] : ");Serial.print(server.argName(i));Serial.print(" ---> value : ");Serial.println(server.arg(i));
    if (i == 0){
      if (server.arg(i) != "") {
        strwifi_ssid = server.arg(i);
        writeFile(SPIFFS, "/inputWifiSsid.txt", strwifi_ssid.c_str());
      }
    }else if (i == 1){
      if (server.arg(i) != "") {
        strwifi_password = server.arg(i);
        writeFile(SPIFFS, "/inputWifiPassword.txt", strwifi_password.c_str());
      }
    }else if (i == 2){
      if (server.arg(i) != "") {
        strlinky_histstard = server.arg(i);
        writeFile(SPIFFS, "/inputModeHistStand.txt", strlinky_histstard.c_str());
      }
    }else{     
    }
  }
  //Ici, l'on peut appeler d'autres d'autres fonctions et jouer avec les variables recus.
  server.send(200, "text/html", html); //Response to the HTTP request
}


void handle_influxDB() {
  tempsExitWithDelay = 0;

  String html = "<!DOCTYPE HTML><html><head>";
  html += "<title>Configuration XKY</title>";
  html += "<meta charset='UTF-8'>";
  html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  html += "<link rel='stylesheet' type='text/css' href='/style.css'>";
  html += "<script>";
  html += "function submitMessage() {";
  html += "alert('Les valeurs vont être sauvegardées dans le XKY');";
  html += "setTimeout(function(){ document.location.reload(false); }, 5000);";
  html += "}";
  html += "</script>";
  html += CSS;
  html += "</head>";
  html += "<body>";
  html += "<div class='section-white'>";
  html += "<h1>Configuration du module XKY</h1>";
  html += "<p><b>Serveur InfluxDB</b></p>";
  html += "</div>";

  html += "<form action='/receptionFormulaireInfluxdb'>"; 
  html += "<label for='Mode'>Activation InfluxDB :</label>";
  html += "<select name='inputActiverInflux' id='Mode'>";

  // Vérifiez si la variable struseinflux contient "Active" ou "Desactive" pour définir l'option sélectionnée
  if (struseinflux == "Active") {
    html += "<option value='Active' selected>Activé</option>";
    html += "<option value='Desactive'>Desactivé</option>";
  } else {
    html += "<option value='Active'>Activé</option>";
    html += "<option value='Desactive' selected>Desactivé</option>";
  }
  //html += "<option value='Active' %selectedActiveInfluxDB%>Activé</option>";
  //html += "<option value='Desactive' %selectedDesactiveInfluxDB%>Désactivé</option>";
  html += "</select><br><br>";

  html += "URL : <input type='text' name='inputUrl' value='"+strINFLUXDB_URL+"'><br><br>";
  html += "Jeton : <input type='text' name='inputToken' value='"+strINFLUXDB_TOKEN+"'><br><br>";
  html += "Organisation : <input type='text' name='inputOrg' value='"+strINFLUXDB_ORG+"'><br><br>";
  html += "Seau (Bucket) : <input type='text' name='inputBucket' value='"+strINFLUXDB_BUCKET+"'><br><br>";

  //html += "<input type='submit' value='Enregistrer les modifications' onclick='submitMessage()'>";
  html +="<input class='button button-off' type='submit' value='Enregistrer les modifications'>\n"; //Bouton pour envoyer le formulaire
  html += "</form><br>";
  html += "<div class='menu-button' onclick='window.location.href = \"/\";'>Configuration principale</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/mqtt\";'>Configuration MQTT</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/avance\";'>Configuration avancée</div>";
  //html += "<div class='menu-button' onclick='window.location.href = \"/update\";'>Mise à jour logicielle</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/wifistate\";'>État du réseau</div><br>";

  html += "<p class='spacer'>One For All version 1er détenteur Board : "+BoardVersion+" Version logicielle : "+FWVersion+"</p>";
  html += "<iframe style='display:none' name='hidden-form'></iframe>";
  html += "</body></html>";

  server.send(200, "text/html", html);
}

void receptionFormulaireInfluxdb(){
  tempsExitWithDelay = 0; //On remets à 0 le compteurs de sortie du mode AP automatique.

  Serial.println("[HTML] -> InfluxDB data set");
  String html = "<html>";
  html +="<head>";
  html +="<title>InfluxDB | Redirection ...</title>";
  html += "<meta charset='UTF-8'>";
  html +="<meta http-equiv='refresh' content='1; URL=http://"+local_ip_redirect+"/influxdb'>"; //Redirige sur la page d'influxdb au bout d'une seconde
  html += CSS;
  html +="</head>";
  html +="<body>";
  html += "<br><br><center><h1 style='color:#7FBB1B;'>Les saisies du formulaire ont bien &eacute;t&eacute; prises en compte</h1></center><br>\n";
  html += "<center><h4>Redirection ...</h4></center>\n";

  //Boucle d'extraction des données :)
  for (int i = 0; i < server.args(); i++) { //serve.args() contient le nb de données passées par le formulaire
    Serial.println("[AP-MODE] Variables recus par le formulaire de la page précédente : ");
    Serial.print("  ---> Data [");Serial.print(i);Serial.print("] : ");Serial.print(server.argName(i));Serial.print(" ---> value : ");Serial.println(server.arg(i));
    if (i == 0){
      if (server.arg(i) != "") {
        struseinflux = server.arg(i);
        writeFile(SPIFFS, "/inputActiverInflux.txt", struseinflux.c_str());
      }
    }else if (i == 1){
      if (server.arg(i) != "") {
        strINFLUXDB_URL = server.arg(i);
        writeFile(SPIFFS, "/inputUrl.txt", strINFLUXDB_URL.c_str());
      }
    }else if (i == 2){
      if (server.arg(i) != "") {
        strINFLUXDB_TOKEN = server.arg(i);
        writeFile(SPIFFS, "/inputToken.txt", strINFLUXDB_TOKEN.c_str());
      }
    }else if (i == 3){
      if (server.arg(i) != "") {
        strINFLUXDB_ORG = server.arg(i);
        writeFile(SPIFFS, "/inputOrg.txt", strINFLUXDB_ORG.c_str());
      }
    }else if (i == 4){
      if (server.arg(i) != "") {
        strINFLUXDB_BUCKET = server.arg(i);
        writeFile(SPIFFS, "/inputBucket.txt", strINFLUXDB_BUCKET.c_str());
      }
    }else{     
    }
  }

  server.send(200, "text/html", html); 
}




void handle_mqttConfiguration() {
  tempsExitWithDelay = 0;

  String html = "<!DOCTYPE HTML><html><head>";
  html += "<title>Configuration XKY</title>";
  html += "<meta charset='UTF-8'>";
  html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  html += "<link rel='stylesheet' type='text/css' href='/style.css'>";
  html += "<script>";
  html += "function submitMessage() {";
  html += "alert('Les valeurs vont être sauvegardées dans le XKY');";
  html += "setTimeout(function(){ document.location.reload(false); }, 5000);";
  html += "}";
  html += "</script>";
  html += CSS;
  html += "</head>";
  html += "<body>";
  html += "<div class='section-white'>";
  html += "<h1>Configuration du module XKY</h1>";
  html += "<p><b>Serveur MQTT</b></p>";
  html += "</div>";

  html += "<form action='/receptionFormulaireMqtt'>";
  html += "<label for='Mode'>Activation MQTT :</label>";
  html += "<select name='inputActiverMqtt' id='Mode'>";
  // Vérifiez si la variable strusemqtt contient "Active" ou "Desactive" pour définir l'option sélectionnée
  if (strusemqtt == "Active") {
    html += "<option value='Active' selected>Activé</option>";
    html += "<option value='Desactive'>Desactivé</option>";
  } else {
    html += "<option value='Active'>Activé</option>";
    html += "<option value='Desactive' selected>Desactivé</option>";
  }
  //html += "<option value='Active' %selectedActiveMqtt%>Activé</option>";
  //html += "<option value='Desactive' %selectedDesactiveMqtt%>Désactivé</option>";
  html += "</select><br><br>";

  html += "URL : <input type='text' name='inputUrlMqtt' value='"+strmqtt_server+"'><br><br>";
  html += "Utilisateur : <input type='text' name='inputUserMqtt' value='"+strmqtt_user+"'><br><br>";
  html += "Mot de passe : <input type='text' name='inputPasswordMqtt' value='"+strmqtt_password+"'><br><br>";
  html += "Sujet (Topic) : <input type='text' name='inputMainTopicMqtt' value='"+strmaintopicmqtt+"'><br><br>";

  //html += "<input type='submit' value='Enregistrer les modifications' onclick='submitMessage()'>";
  html +="<input class='button button-off' type='submit' value='Enregistrer les modifications'>\n"; //Bouton pour envoyer le formulaire
  html += "</form><br>";
  html += "<div class='menu-button' onclick='window.location.href = \"/\";'>Configuration principale</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/influxdb\";'>Configuration InfluxDB</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/avance\";'>Configuration avancée</div>";
  //html += "<div class='menu-button' onclick='window.location.href = \"/update\";'>Mise à jour logicielle</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/wifistate\";'>État du réseau</div><br>";

  html += "<p class='spacer'>One For All version 1er détenteur Board : " + BoardVersion + " Version logicielle : " + FWVersion + "</p>";
  html += "<iframe style='display:none' name='hidden-form'></iframe>";
  html += "</body></html>";

  server.send(200, "text/html", html);
}


void receptionFormulaireMqtt(){
  tempsExitWithDelay = 0; //On remets à 0 le compteurs de sortie du mode AP automatique.

  Serial.println("[HTML] -> InfluxDB data set");
  String html = "<html>";
  html +="<head>";
  html +="<title>InfluxDB | Redirection ...</title>";
  html += "<meta charset='UTF-8'>";
  html +="<meta http-equiv='refresh' content='1; URL=http://"+local_ip_redirect+"/mqtt'>"; //Redirige sur la page d'index au bout d'une seconde
  html += CSS;
  html +="</head>";
  html +="<body>";
  html += "<br><br><center><h1 style='color:#7FBB1B;'>Les saisies du formulaire ont bien &eacute;t&eacute; prises en compte</h1></center><br>\n";
  html += "<center><h4>Redirection ...</h4></center>\n";

  //Boucle d'extraction des données :)
  for (int i = 0; i < server.args(); i++) { //serve.args() contient le nb de données passées par le formulaire
    Serial.println("[AP-MODE] Variables recus par le formulaire de la page précédente : ");
    Serial.print("  ---> Data [");Serial.print(i);Serial.print("] : ");Serial.print(server.argName(i));Serial.print(" ---> value : ");Serial.println(server.arg(i));
    if (i == 0){
      if (server.arg(i) != "") {
        strusemqtt = server.arg(i);
        writeFile(SPIFFS, "/inputActiverMqtt.txt", strusemqtt.c_str());
      }
    }else if (i == 1){
      if (server.arg(i) != "") {
        strmqtt_server = server.arg(i);
        writeFile(SPIFFS, "/inputUrlMqtt.txt", strmqtt_server.c_str());
      }
    }else if (i == 2){
      if (server.arg(i) != "") {
        strmqtt_user = server.arg(i);
        writeFile(SPIFFS, "/inputUserMqtt.txt", strmqtt_user.c_str());
      }
    }else if (i == 3){
      if (server.arg(i) != "") {
        strmqtt_password = server.arg(i);
        writeFile(SPIFFS, "/inputPasswordMqtt.txt", strmqtt_password.c_str());
      }
    }else if (i == 4){
      if (server.arg(i) != "") {
        strmaintopicmqtt = server.arg(i);
        writeFile(SPIFFS, "/inputMainTopicMqtt.txt", strmaintopicmqtt.c_str());
      }
    }else{     
    }
  }

  server.send(200, "text/html", html); 
}



void handle_avanceConfiguration() {
  tempsExitWithDelay = 0;

  String html = "<!DOCTYPE HTML><html><head>";
  html += "<title>Configuration XKY</title>";
  html += "<meta charset='UTF-8'>";
  html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  html += "<link rel='stylesheet' type='text/css' href='/style.css'>";
  html += "<script>";
  html += "function submitMessage() {";
  html += "alert('Les valeurs vont être sauvegardées dans le XKY');";
  html += "setTimeout(function(){ document.location.reload(false); }, 5000);";
  html += "}";
  html += "</script>";
  html += CSS;
  html += "</head>";
  html += "<body>";
  html += "<div class='section-white'>";
  html += "<h1>Configuration du module XKY</h1>";
  html += "<p><b>Paramètres internes XKY</b></p>";
  html += "<p>Attention. Ces paramètres sont disponibles à titre expérimental pour vous permettre de modifier le comportement du XKY.</p>";
  html += "<p>Un mauvais réglage peut engendrer des instabilités.</p>";
  html += "<p>En cas de doute, vous pouvez désactiver les options.</p>";
  html += "</div>";

  html += "<form action='/receptionFormulaireAvance'>";
  html += "<label for='Mode'>Optimisation Wi-Fi :</label>";
  html += "<select name='inputActiverOptimWifi' id='Mode'>";
  // Vérifiez si la variable struseoptimwifi contient "Active" ou "Desactive" pour définir l'option sélectionnée
  if (struseoptimwifi == "Active") {
    html += "<option value='Active' selected>Activée</option>";
    html += "<option value='Desactive'>Desactivée</option>";
  } else {
    html += "<option value='Active'>Activée</option>";
    html += "<option value='Desactive' selected>Desactivée</option>";
  }
  //html += "<option value='Active' %selectedActiveOptWifi%>Activée</option>";
  //html += "<option value='Desactive' %selectedDesactiveOptWifi%>Désactivée</option>";
  html += "</select><br><br>";

  html += "Nombre de secondes pour validation de l'optimisation Wi-Fi (par defaut 15s) : <input type='text' name='inputMidConnexion' value='"+strmidtrywificon+"'><br><br>";
  html += "Nombre de secondes pour l'optimisation Deep Sleep (Avec signe + ou -, exemple '+1.5s' / Par defaut 0s) : <input type='text' name='inputOptimiDSSeconds' value='"+strDeepSleepSecondsOpti+"'><br><br>";

  html += "<label for='led'>Activation de la LED :</label>";
  html += "<select name='inputActiverLed' id='led'>";
  // Vérifiez si la variable struseled contient "Active" ou "Desactive" pour définir l'option sélectionnée
  if (struseled == "Active") {
    html += "<option value='Active' selected>Activée</option>";
    html += "<option value='Desactive'>Desactivée</option>";
  } else {
    html += "<option value='Active'>Activée</option>";
    html += "<option value='Desactive' selected>Desactivée</option>";
  }
  //html += "<option value='Active' %selectedActiveLed%>Activée</option>";
  //html += "<option value='Desactive' %selectedDesactiveLed%>Désactivée</option>";
  html += "</select><br><br>";

  html +="<input class='button button-off' type='submit' value='Enregistrer les modifications'>\n"; //Bouton pour envoyer le formulaire
  //html += "<input type='submit' value='Enregistrer les modifications' onclick='submitMessage()'>";
  html += "</form><br>";

  html += "<div class='menu-button' onclick='window.location.href = \"/\";'>Configuration principale</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/influxdb\";'>Configuration InfluxDB</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/mqtt\";'>Configuration MQTT</div>";
  //html += "<div class='menu-button' onclick='window.location.href = \"/update\";'>Mise à jour logicielle</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/wifistate\";'>État du réseau</div><br>";

  html += "<p class='spacer'>One For All version 1er détenteur Board : " + BoardVersion + " Version logicielle : " + FWVersion + "</p>";
  html += "<iframe style='display:none' name='hidden-form'></iframe>";
  html += "</body></html>";

  server.send(200, "text/html", html);
}

void receptionFormulaireAvance(){
  tempsExitWithDelay = 0; //On remets à 0 le compteurs de sortie du mode AP automatique.

  Serial.println("[HTML] -> InfluxDB data set");
  String html = "<html>";
  html +="<head>";
  html +="<title>InfluxDB | Redirection ...</title>";
  html += "<meta charset='UTF-8'>";
  html +="<meta http-equiv='refresh' content='1; URL=http://"+local_ip_redirect+"/avance'>"; //Redirige sur la page d'index au bout d'une seconde
  html += CSS;
  html +="</head>";
  html +="<body>";
  html += "<br><br><center><h1 style='color:#7FBB1B;'>Les saisies du formulaire ont bien &eacute;t&eacute; prises en compte</h1></center><br>\n";
  html += "<center><h4>Redirection ...</h4></center>\n";

  //Boucle d'extraction des données :)
  for (int i = 0; i < server.args(); i++) { //serve.args() contient le nb de données passées par le formulaire
    Serial.println("[AP-MODE] Variables recus par le formulaire de la page précédente : ");
    Serial.print("  ---> Data [");Serial.print(i);Serial.print("] : ");Serial.print(server.argName(i));Serial.print(" ---> value : ");Serial.println(server.arg(i));
    if (i == 0){
      if (server.arg(i) != "") {
        struseoptimwifi = server.arg(i);
        writeFile(SPIFFS, "/inputActiverOptimWifi.txt", struseoptimwifi.c_str());
      }
    }else if (i == 1){
      if (server.arg(i) != "") {
        strmidtrywificon = server.arg(i);
        writeFile(SPIFFS, "/inputMidConnexion.txt", strmidtrywificon.c_str());
      }
    }else if (i == 2){
      if (server.arg(i) != "") {
        strDeepSleepSecondsOpti = server.arg(i);
        writeFile(SPIFFS, "/inputOptimiDSSeconds.txt", strDeepSleepSecondsOpti.c_str());
      }
    }else if (i == 3){
      if (server.arg(i) != "") {
        struseled = server.arg(i);
        writeFile(SPIFFS, "/inputActiverLed.txt", struseled.c_str());
      }
    }else{     
    }
  }

  server.send(200, "text/html", html); 
}


void handle_wifiStatePage() {
  tempsExitWithDelay = 0;

  String html = "<!DOCTYPE HTML><html><head>";
  html += "<title>Configuration XKY</title>";
  html += "<meta charset='UTF-8'>";
  html += "<meta name='viewport' content='width=device-width, initial-scale=1'>";
  html += "<link rel='stylesheet' type='text/css' href='/style.css'>";
  html += CSS;
  html += "</head>";
  html += "<body>";
  html += "<div class='section-white'>";
  html += "<h1>Configuration du module XKY</h1>";
  html += "<p><b>Paramètres de connexion WiFi depuis le dernier redémarrage du module.</b></p>";
  html += "<p>Attention, il s'agit du résultat de connexion du dernier redémarrage du module. ";
  html += "<br>Si vous souhaitez vérifier que le module se connecte bien à votre réseau, ";
  html += "<br>vous devez mettre à jour vos identifiants sur la page de configuration principale, ";
  html += "<br>les enregistrer, redémarrer le module, puis revenir sur cette page.</p>";
  html += "<div class='divform'>";
  html += htmlLog; // Variable contenant le contenu de log
  html += "<br>";
  html += "</div>";
  html += "<br>";
  html += "</div>";

  html += "<div class='menu-button' onclick='window.location.href = \"/\";'>Configuration principale</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/influxdb\";'>Configuration InfluxDB</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/mqtt\";'>Configuration MQTT</div>";
  html += "<div class='menu-button' onclick='window.location.href = \"/avance\";'>Configuration avancée</div>";
  //html += "<div class='menu-button' onclick='window.location.href = \"/update\";'>Mise à jour logicielle</div>";
  html += "<br>";

  html += "<p class='spacer'>One For All version 1er détenteur Board : " + BoardVersion + " Version logicielle : " + FWVersion + "</p>";
  html += "<iframe style='display:none' name='hidden-form'></iframe>";
  html += "</body></html>";

  server.send(200, "text/html", html);
}


//Exemple d'un page HTML type qui renvoie des valeurs vers un forulaire 

// /!\ Ne pas oublier d'ajouter les lien fonction et path dans la fonction link() :
//server.on("/test", pageHtmlAvecFormulaire);
//server.on("/receptionFormulaireEtTraitementDesDOnnees", receptionFormulaireEtTraitementDesDOnnees);

/* //Fonction qui créé une page HTML
void pageHtmlAvecFormulaire(){
    tempsExitWithDelay = 0; //On remets à 0 le compteurs de sortie du mode AP automatique.
    String html = "<!DOCTYPE html><html>\n";
    html += "<meta charset='UTF-8'>";
    html +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"> \n";
    //On peut inclure une page de CSS ici :)
    html +="<title>UN TITRE</title>\n";
    html +="<body>\n";
    
    html +="<h1>Titre principal de la page</h1> \n";

    html +="<p>Date : \n";
    html +="<span id='unID'>\n";
    html +="<b>Du texte ici :D</b>";
    html +="</span>\n";
    html +="</p>\n";
    // Le form redirigera sur la page : receptionFormulaireEtTraitementDesDOnneess.html
    html +="<form action='/receptionFormulaireEtTraitementDesDOnnees'>\n";
    
    //REGARDER ICI L'UTILISATION DE LIST ? 
    html +="<label for='input01'>Donnée 01 : <br><br></label><input list='Je ne sais plus pourquoi l'on utilise list ici ?!' id='input01' name='input01' value='\n";
    html +="UnDonneeICI"; //Fonction qui sort une donnée pour préremplire le champ du formulaire. 
    html +="'/>\n";

    html +="<label for='input02'>Donnée 02 : <br><br></label><input list='Je ne sais plus pourquoi l'on utilise list ici ?!' id='input02' name='input02' value='\n";
    html +="UnDonneeICI"; //Fonction qui sort une donnée pour préremplire le champ du formulaire. 
    html +="'/>\n";

    html +="<label for='input01'>Donnée 03 : <br><br></label><input list='Je ne sais plus pourquoi l'on utilise list ici ?!' id='input03' name='input03' value='\n";
    html +="UnDonneeICI"; //Fonction qui sort une donnée pour préremplire le champ du formulaire. 
    html +="'/>\n";

    html +="<label for='input01'>Donnée 04 : <br><br></label><input list='Je ne sais plus pourquoi l'on utilise list ici ?!' id='input04' name='input04' value='\n";
    html +="UnDonneeICI"; //Fonction qui sort une donnée pour préremplire le champ du formulaire. 
    html +="'/>\n";

    html +="<label for='input01'>Donnée 05 : <br><br></label><input list='Je ne sais plus pourquoi l'on utilise list ici ?!' id='input05' name='input05' value='\n";
    html +="UnDonneeICI"; //Fonction qui sort une donnée pour préremplire le champ du formulaire. 
    html +="'/>\n";

    html +="<label for='input01'>Donnée 06 : <br><br></label><input list='Je ne sais plus pourquoi l'on utilise list ici ?!' id='input06' name='input06' value='\n";
    html +="UnDonneeICI"; //Fonction qui sort une donnée pour préremplire le champ du formulaire. 
    html +="'/>\n";

    html +="<br><br><input class='button button-off' type='submit' value='Enregistrer'>\n"; //Bouton pour envoyer le formulaire
    
    html +="<p></p><a class='button button-reset' href='/'><- BACK</a> \n"; //Bouton de retour sur la page précédente
    
    html +="</form>\n";    

    //Inclure un scripte AJAX Ici ? :)
    
    html +="</body></html>\n";
    Serial.println("[HTTP] -> Page principale demandée");
    server.send(200, "text/html", html);
}

void receptionFormulaireEtTraitementDesDOnnees(){
  tempsExitWithDelay = 0; //On remets à 0 le compteurs de sortie du mode AP automatique.
  Serial.println("[HTML] -> Data set");
  String html = "<html>";
  html +="<head>";
  html +="<title>UN TITRE | Redirection ...</title>";
  html += "<meta charset='UTF-8'>";
  html +="<meta http-equiv='refresh' content='1; URL=http://"+local_ip_redirect+"'>"; //Redirige sur la page d'index au bout d'une seconde
  //On peut inclure une page de CSS ici :)
  html +="</head>";
  html +="<body>";
  html += "<br><br><center><h1 style='color:#7FBB1B;'>Les saisies du formulaire ont bien &eacute;t&eacute; prises en compte</h1></center><br>\n";
  html += "<center><h4>Redirection ...</h4></center>\n";

  int data01, data02, data03, data04, data05, data06;
  //Boucle d'extraction des données :)
  for (int i = 0; i < server.args(); i++) { //serve.args() contient le nb de données passées par le formulaire
    Serial.println("[AP-MODE] Variables recus par le formulaire de la page précédente : ");
    Serial.print("  ---> Data [");Serial.print(i);Serial.print("] : ");Serial.print(server.argName(i));Serial.print(" ---> value : ");Serial.println(server.arg(i));
    if (i == 0){
      data01 = server.arg(i).toInt();
    }else if (i == 1){
      data02 = server.arg(i).toInt();
    }else if (i == 2){
      data03 = server.arg(i).toInt();
    }else if (i == 3){
      data04 = server.arg(i).toInt();
    }else if (i == 4){
      data05 = server.arg(i).toInt();
    }else if (i == 5){
      data06 = server.arg(i).toInt();
    }else{     
    }
  }
  //Ici, l'on peut appeler d'autres d'autres fonctions et jouer avec les variables recus.
  server.send(200, "text/html", html); //Response to the HTTP request
}





//Pour ajouter de l'AJAX (maj auto de petites portions de code dans une page HTML) : 

  //Extrait de code pour créé le script AJAX à mettre dans le header de la page html 
  html += "<script>";
  html +="setInterval(function(){getData01();}, 1000);\n"; //1000ms update rate
  html +="function getData01() \n";
  html +="  {\n";
  html +="    var xhttp = new XMLHttpRequest();\n";
  html +="    xhttp.onreadystatechange = function(){if (this.readyState == 4 && this.status == 200){document.getElementById('getScanWifi').innerHTML = this.responseText;}};\n";
  html +="    xhttp.open('GET', 'getScanWifi', true);\n"; 
  html +="    xhttp.send();\n";
  html +="  }\n";
  html += "</script>";


  //Extrait de code d'un SPAN a mettre dans le body d'une page html pour fonction AJAX de maj automatique
  html +="<span id='getScanWifi'><p>\n";
  html += uneValeur; //Précharge une valeur pour l'afficher avant l'execution de la fonction AJAX 
  html +="</p></span>\n";


  //Action à effectuer lors d'une requete AJAX
  void handleGetUneValeur() {
      uneFonctionQuiMetsAJouruneValeur();
      server.send(200, "text/plain", uneValeur);
  }

  //extrait de code pour les requetes AJAX, faire le lien avec path et fonction, à mettre dans link() :
  server.on("/getUneValeur", handleGetUneValeur);

  //Une fonction classique qui renvoie une nouvelle valeur à la variable uneValeur
  void uneFonctionQuiMetsAJouruneValeur(){
    uneValeur = uneNouvelleValeure;
  }


*/



        




void scanwifi() {
  String ssid;
  int32_t rssi;
  uint8_t encryptionType;
  int32_t channel;
  bool hidden;
  int scanResult;

  strScanwifiDataList = "";  // Réinitialisation à chaque scan

  Serial.println(F("[WIFI-AP] Starting WiFi scan..."));
  ledwifiprogmode();
  scanResult = WiFi.scanNetworks(false, true); // scanResult = WiFi.scanNetworks(/async=/false, /hidden=/true);
  ledwifiprogmode();

  if (scanResult == 0) {
    Serial.println(F("[WIFI-AP] No networks found"));
    strscanwifitemp = "[WIFI-AP] Pas de réseau Wi-Fi accessible";
  } else if (scanResult > 0) {
    Serial.printf(PSTR("%d networks found:\n"), scanResult);
    strscanwifitemp = "<b><center>" + String(scanResult) + " réseaux trouvés: <br>" + "</center></b><br>";

    // Initialisation du tableau HTML
    strscanwifitemp += "<table border='1' style='width:100%; text-align:center; border-collapse:collapse;'>";
    strscanwifitemp += "<tr>";
    strscanwifitemp += "<th>#</th>";  
    strscanwifitemp += "<th>SSID</th>";  
    strscanwifitemp += "<th>RSSI (Signal)</th>";  
    strscanwifitemp += "<th>Canal</th>";  
    strscanwifitemp += "<th>BSSID</th>";  
    strscanwifitemp += "<th>Mode</th>";  
    strscanwifitemp += "<th>Sécurité</th>";  
    strscanwifitemp += "<th>Caché</th>";  
    strscanwifitemp += "</tr>";

    // Boucle sur les résultats du scan Wi-Fi
    for (int8_t i = 0; i < scanResult; i++) {
      ssid = WiFi.SSID(i);
      rssi = WiFi.RSSI(i);
      encryptionType = WiFi.encryptionType(i);
      channel = WiFi.channel(i);
      hidden = WiFi.isHidden(i);
      String bssid = WiFi.BSSIDstr(i);

      String phyMode;
      // Détection du mode physique
      switch (WiFi.getPhyMode()) {
        case WIFI_PHY_MODE_11B: //WiFi 2
          phyMode = "802.11b"; 
          break;
        case WIFI_PHY_MODE_11G: //WiFi 3
          phyMode = "802.11g";
          break;
        case WIFI_PHY_MODE_11N: //WiFi 4
          phyMode = "802.11n";
          break;
        default:
          phyMode = "Unknown"; // Wi-Fi 5 (802.11ac) ou Wi-Fi 6 (802.11ax) non gérés par le module
          break;
      }


      String securityMethod;
      // Détection des méthodes de sécurité
      switch (encryptionType) {
        case ENC_TYPE_NONE:
          securityMethod = "None";
          break;
        case ENC_TYPE_WEP:
          securityMethod = "WEP";
          break;
        case ENC_TYPE_TKIP:
          securityMethod = "WPA";
          break;
        case ENC_TYPE_CCMP:
          securityMethod = "WPA2";
          break;
        //case ENC_TYPE_WPA3:         Méthode WPA3 non géré par le modul
        //  securityMethod = "WPA3";
        //  break;
        default:
          securityMethod = "Unknown";
          break;
      }

      // Affichage dans le moniteur série (ajout de la méthode de sécurité)
      Serial.printf(
        PSTR("  %02d: [CH %02d] [%s] %ddBm %c %c %s %s\n"), 
        i, channel, 
        bssid.c_str(), 
        rssi, 
        (encryptionType == ENC_TYPE_NONE) ? ' ' : '*', 
        hidden ? 'H' : 'V', 
        phyMode.c_str(), 
        securityMethod.c_str()
      );

      // Construction d'une ligne du tableau pour ce réseau Wi-Fi
      strscanwifitemp += "<tr>";
      strscanwifitemp += "<td>" + String(i) + "</td>";  
      strscanwifitemp += "<td><b>" + String(ssid.c_str()) + "</b></td>";  
      strscanwifitemp += "<td>" + String(rssi) + " dBm</td>";  
      strscanwifitemp += "<td>" + String(channel) + "</td>";  
      strscanwifitemp += "<td>" + bssid + "</td>";  
      strscanwifitemp += "<td>" + phyMode + "</td>";  
      strscanwifitemp += "<td>" + securityMethod + "</td>";  
      strscanwifitemp += "<td>" + String(hidden ? "Oui" : "Non") + "</td>";  
      strscanwifitemp += "</tr>";

      // Ajout de l'option dans la datalist pour le SSID
      strScanwifiDataList += "<option value='" + ssid + "'></option>";

      yield();  // Laisser de la place pour d'autres processus
      ledwifiprogmode();  // Indicateur de progression
    }
    // Fermeture du tableau
    strscanwifitemp += "</table>";
  } else {
    Serial.printf(PSTR("[WIFI-AP] WiFi scan error %d"), scanResult);
    strscanwifitemp = "WiFi erreur scan";
  }

  strscanwifi = strscanwifitemp;
}




//MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD --- MAJ V56.2.SD ---

