//****************************//
//*** Prototypes fonctions ***//
//****************************//

void init_IHM(void);
void ledwifiprogmode(void);
void ledReadTeleInfo(void);

//**************************//
//*** Variables et autre ***//
//**************************//

#define hardSelectMode 4 //GPIO 4  //Broche état bas ou haut pour definire le normal mode ou le prog mode
#define blueLED        2 //GPIO 2  