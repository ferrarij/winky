#include <ArduinoOTA.h>
#include <Preferences.h>
#include <EspMQTTClient.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <LibTeleinfo.h>
#include "winky.h"

EspMQTTClient mqtt;
Preferences preferences;
char hostname[32];
int _retry = 0;
String topic = TOPIC_BASE;

uint16_t vusb ;
uint16_t vcap ;
uint16_t vlky ;
bool usb_connected = false;
bool has_frame = false;

// Uptime timer
boolean tick1sec=0;// one for interrupt, don't mess with 
unsigned long uptime=0; // save value we can use in sketch even if we're interrupted

// Pour clignotement LED asynchrone
unsigned long blinkLed  = 0;
int blinkDelay= 0;

// Teleinfo object
TInfo tinfo; 

// Default Teleinfo mode, can be switched back and forth with button
// with Denky D4 push button
_Mode_e tinfo_mode = TIC_MODE; 


void delay_loop(unsigned long _delay) 
{
    while (_delay >= 10) {
        mqtt.loop();
        _delay-=10;
        delay(_delay);
    }
}

void show_wakeup_reason()
{
    esp_sleep_wakeup_cause_t wakeup_reason;

    wakeup_reason = esp_sleep_get_wakeup_cause();

    switch(wakeup_reason) {
        case ESP_SLEEP_WAKEUP_EXT0 : SerialDebug.print("external signal using RTC_IO"); break;
        case ESP_SLEEP_WAKEUP_EXT1 : SerialDebug.print("external signal using RTC_CNTL"); break;
        case ESP_SLEEP_WAKEUP_TIMER : SerialDebug.print("timer"); break;
        case ESP_SLEEP_WAKEUP_TOUCHPAD : SerialDebug.print("touchpad"); break;
        case ESP_SLEEP_WAKEUP_ULP : SerialDebug.print("ULP program"); break;
        default : SerialDebug.printf_P(PSTR("not from sleep:%d"),wakeup_reason); break;
    }
    SerialDebug.print(CRLF);
}

bool GetVoltages(uint16_t * _vcap, uint16_t * _vusb, uint16_t * _vlky) {

    static unsigned long nextVoltage = millis() + 1000;

    // only check voltage every 100ms
    if ( nextVoltage - millis() > 0 ) {
        uint32_t raw;
        nextVoltage = millis() + 100; 

        raw = analogReadMilliVolts(CAP_VOLTAGE); 
        *_vcap = raw * (LOWER_DIVIDER+UPPER_DIVIDER) / LOWER_DIVIDER;

        raw = analogReadMilliVolts(USB_VOLTAGE);  
        *_vusb = raw * (LOWER_DIVIDER+UPPER_DIVIDER) / LOWER_DIVIDER;
        // VUSB is here or not we may consider below 3.5V it's not there
        if (*_vusb < 3500) {
            *_vusb = 0;
        }

        #ifdef LKY_VOLTAGE
        raw = analogReadMilliVolts(LKY_VOLTAGE);  
        *_vlky = raw * (100+470) / 100;
        #else
        *_vlky = 0;
        #endif 

        return true;
        //SerialDebug.printf_P(PSTR(" VIN:%dmV\r\n"), mv);
    }
    return false;
}


void deep_sleep(uint32_t seconds)
{
	SerialDebug.printf_P(PSTR("Going to deep sleep mode for %d seconds" CRLF), seconds);
	esp_sleep_enable_timer_wakeup(  ((uint64_t) seconds) * uS_TO_S_FACTOR);
    DotStar_Clear();
    delay_loop(50);
    //while(true) {
    //    delay_loop(50);
    //}
    WiFi.disconnect();
	esp_deep_sleep_start();
}

 
void onConnectionEstablished()
{
    SerialDebug.print(CRLF "***** Connected to MQTT Broker *****" CRLF);

    ArduinoOTA.onStart([]() {
        SerialDebug.print("Start updating ");
        if (ArduinoOTA.getCommand() == U_FLASH) {
            SerialDebug.print("sketch" CRLF);
        } else { // U_FS
            SerialDebug.print("filesystem" CRLF);
        }
        // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    });
    
    ArduinoOTA.onEnd([]() {
        SerialDebug.print(CRLF "End updating." CRLF);
    });
    
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        SerialDebug.printf_P(PSTR("%u%%" CRLF), (progress / (total / 100)));
    });
    
    ArduinoOTA.onError([](ota_error_t error) {
        SerialDebug.printf_P(PSTR("Error[%u]: "), error);
        if (error == OTA_AUTH_ERROR) {
            SerialDebug.print("Auth Failed" CRLF);
        } else if (error == OTA_BEGIN_ERROR) {
            SerialDebug.print("Begin Failed" CRLF);
        } else if (error == OTA_CONNECT_ERROR) {
            SerialDebug.print("Connect Failed" CRLF);
        } else if (error == OTA_RECEIVE_ERROR) {
            SerialDebug.print("Receive Failed" CRLF);
        } else if (error == OTA_END_ERROR) {
            SerialDebug.print("End Failed" CRLF);
        }
    });

    ArduinoOTA.setHostname(hostname);
    ArduinoOTA.begin();
}

void stop_error(uint32_t wakeup) 
{
    // Blink RED 2 times
    for (int i =0; i<2; i++) {
        delay_loop(240);
        DotStar_SetPixelColor(DOTSTAR_RED, true);
        delay_loop(10);
        DotStar_Clear();
    }
    // Sleep forever or not
    deep_sleep(WAKE_EVERY);
}

double round2(double value) 
{
   return (int)(value * 100 + 0.5) / 100.0;
}

void new_frame(ValueList * me)
{
  // Light First led in Blue
  DotStar_SetPixelColor(RGB1, DOTSTAR_BLUE, true);
  has_frame = true;
  SerialDebug.print(CRLF "***** Complete Teleinfo Frame *****" CRLF);
}

void teleinfo_init()
{
  // Init teleinfo
  tinfo.init(tinfo_mode);

  // Cleanup
  SerialTIC.flush();
  SerialTIC.end();

  // Configure Teleinfo 
  SerialDebug.printf_P(PSTR("TIC RX=GPIO%d  Mode:"), TIC_RX_PIN);
  SerialTIC.begin(tinfo_mode == TINFO_MODE_HISTORIQUE ? 1200 : 9600, SERIAL_7E1, TIC_RX_PIN);

  if ( tinfo_mode == TINFO_MODE_HISTORIQUE ) {
    SerialDebug.println(F("Historique"));
  } else {
    SerialDebug.println(F("Standard"));
  }
}

_State_e teleinfo_loop()
{
    static char c;
    _State_e state;
    // On a reçu un caractère ?
    while ( SerialTIC.available() && !has_frame) {
        // Le lire
        c = SerialTIC.read();
        // Gérer
        state = tinfo.process(c);
        // L'affcher dans la console
        /*
        if (c==TINFO_STX) {
            SerialDebug.print("<STX>");
        } else if (c==TINFO_ETX) {
            SerialDebug.print("<ETX>");
        } else if (c==TINFO_HT) {
            SerialDebug.print("<TAB>");
        } else if (c==TINFO_EOT) {
            SerialDebug.print("<INTERRUPT>");
        } else {
            // Display only when receiving state OK
            if (state == TINFO_READY) {
                SerialDebug.print(c);
            }
        }
        */
    }
    return state;
}



void setup()
{
    StaticJsonDocument<512> doc;
    String output;

    if ( GetVoltages(&vcap, &vusb, &vlky) ) {
        if ( vusb > 4500) {
            usb_connected = true;
        }
    }    

    SerialDebug.begin(115200);

    // this resets all the neopixels to an off state
    #ifdef BUILTIN_RGBLED_PIN
    if (!rmtInit(BUILTIN_RGBLED_PIN, RMT_TX_MODE, RMT_MEM_NUM_BLOCKS_1, 10000000)) {
        SerialDebug.println("init RGB Led failed\n");
    }
    DotStar_Clear();
    #endif

    //DotStar_SetBrightness( MY_RGB_BRIGHTNESS );    

    Wire.begin(I2C_SDA, I2C_SCL);

    // Set hostanme
    uint32_t chipId = 0;
    for (int i=0; i<17; i=i+8) {
        chipId |= ((ESP.getEfuseMac() >> (40 - i)) & 0xff) << i;
    }
    // Set Network Hostname
    sprintf_P(hostname, PSTR("winky-%06x"), chipId & 0xFFFFFF );
    topic += String(hostname) + "/";

    // Button
    pinMode(BTN1, INPUT_PULLUP);     

    // Wait for USB serial to be up in 2s
    if ( usb_connected ) {
        while (millis()<2000) {
            delay(200);
            DotStar_SetPixelColor(RGB1, DOTSTAR_YELLOW, true);
            delay(50);
            DotStar_Clear();
        }
    } else {

    }

    // Open Preferences, Namespace name is limited to 15 chars.
    preferences.begin("winky", false);
    // Remove all preferences under the opened namespace
    //preferences.clear();
    // Or remove the key only
    //preferences.remove("mykey");
    SerialDebug.println();
    SerialDebug.println("===========================");
    SerialDebug.printf("Device Name   : %s" CRLF, hostname);
    SerialDebug.print ("Wakeup by     : "); show_wakeup_reason();
    SerialDebug.printf_P(PSTR("Button on GPIO%d" CRLF), BTN1);
    SerialDebug.println("===========================");

    SerialDebug.printf_P(PSTR("Linky:%dmV  USB:%dmV  SCap:%dmV" CRLF), vlky, vusb, vcap);

    teleinfo_init();

    // Attacher les callback dont nous avons besoin
    // pour cette demo, ADPS et TRAME modifiée
    tinfo.attachNewFrame(new_frame); 

    // Start network stuff
    mqtt.setMqttClientName(hostname);
    mqtt.setWifiCredentials(WIFI_SSID, WIFI_PASS);
    mqtt.setMqttServer(MQTT_SERVER, MQTT_USER, MQTT_PASS, MQTT_PORT); // default no user/pass and port 1883
    mqtt.setMaxPacketSize(1024);
    // Optional functionalities of EspMQTTClient
    //mqtt.enableDebuggingMessages(); // Enable debugging messages sent to serial output
    mqtt.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overridded with enableHTTPWebUpdater("user", "password").
    mqtt.enableOTA(); // Enable OTA (Over The Air) updates. Password defaults to MQTTPassword. Port is the default OTA port. Can be overridden with enableOTA("password", port).
    //mqtt.enableLastWillMessage(String(topic + "lastwill").c_str(), "offline", true);  


    // Wait for WiFi and MQTT connected 
    int time_out = 0;
    SerialDebug.print(F("Trying to connect WiFi") );
    while (!mqtt.isConnected() ) {
        mqtt.loop();
        teleinfo_loop();
        delay(450);
        DotStar_SetPixelColor(RGB1, DOTSTAR_GREEN, true);
        if (mqtt.isWifiConnected() ) {
            SerialDebug.print('*');
            DotStar_SetPixelColor(RGB2, DOTSTAR_GREEN, true);
        } else {
            SerialDebug.print('.');
        }
        delay(50);
        if (mqtt.isWifiConnected()) {
            if (!mqtt.isConnected()) {
                // Clear only second led
                DotStar_Clear(RGB2);
            }
        } else {
            // Clear all
            DotStar_Clear();
        }

        // >30s (500ms loop)
        if (++time_out >= 60) {
            SerialDebug.print(F(CRLF "Unable to connect in 30s" CRLF) );
            // Stop 
            stop_error(WAKE_EVERY);
        }
    }

    // Let some time to see 2nd led connected for MQTT
    delay(250);
    // Clear all
    DotStar_Clear();
    SerialDebug.printf_P(PSTR(CRLF "Connected in %ds" CRLF), time_out/2);

    GetVoltages(&vcap, &vusb, &vlky);
}

void loop()
{   
    static unsigned long previousMillis = 0;
    static uint8_t buttonState = 0;
    static unsigned long lastDebounceTime = 0;  

    _State_e state ;

    unsigned long currentMillis = millis();

    // Button to switch mode
    uint8_t button = digitalRead(BTN1);

    // New Press 
    if ( button==LOW && buttonState==0) {
        buttonState = 1; 
        lastDebounceTime = millis(); 

    // Pressed enought (debounced)
    } else if ( buttonState==1 && button==LOW && (millis()-lastDebounceTime)>50 ) {
        buttonState = 2; 

    // Release (no need debouce here)
    } else if ( buttonState==2 && button==HIGH ) {

        // Invert mode
        if ( tinfo_mode == TINFO_MODE_HISTORIQUE ) {
            tinfo_mode = TINFO_MODE_STANDARD;
        } else  {
            tinfo_mode = TINFO_MODE_HISTORIQUE;
        }

        // Start long led blink
        DotStar_SetPixelColor(RGB2, DOTSTAR_MAGENTA, true);
        blinkLed = millis();
        blinkDelay = 100; // 100ms

        // Init Teleinfo Serial Port
        teleinfo_init();

        lastDebounceTime = millis(); 
        buttonState = 0;
    } 

    state = teleinfo_loop();

    // Avons nous recu un ticker de seconde?
    if (tick1sec) {
        tick1sec = false;
        uptime++;

        GetVoltages(&vcap, &vusb, &vlky);
        // No USB and supercaps low
        if (!usb_connected && vcap < 3500) {
            // sleep until next time
            deep_sleep(WAKE_EVERY);
        }
    }

    // Wait for full teleinfo frame
    if (has_frame) {
        StaticJsonDocument<512> doc;
        String output;

        SerialDebug.print("Sending Teleinfo Frame" CRLF);
        
        // Read winky status
        doc["vcap"] = vcap ;
        doc["vusb"] = vusb ;
        doc["vlky"] = vlky ;

        // Send some values (example)
        char buff[64];
        if ( tinfo_mode == TINFO_MODE_HISTORIQUE ) {
           doc["ADCO"] = tinfo.valueGet_P(PSTR("ADCO"), buff);
           doc["PAPP"] = tinfo.valueGet_P(PSTR("PAPP"), buff);
        } else  {
            doc["ADSC"] = tinfo.valueGet_P(PSTR("ADSC"), buff);
        }

        serializeJson(doc, output);
        mqtt.publish(topic + "json", output, true); // timestamp since epoch in UTC
        delay_loop(100); // Do not remove
        DotStar_Clear();

        // If you need specific individual values (not json object) please selec
        // the one you need below
        #ifdef PUBLISH_RAW
        //mqtt.publish(topic + "EAST", blabla, true);
        //delay_loop(50); // Do not remove
        //mqtt.publish(topic + "ADSC", blabla, true);
        //delay_loop(50); // Do not remove
        #endif

        delay_loop(250);

        // sleep until next time
        deep_sleep(WAKE_EVERY);
    }
    
 

    // Verifier si le clignotement LED doit s'arreter 
    if (blinkLed && ((millis()-blinkLed) >= blinkDelay))  {
        DotStar_Clear(RGB2);
        blinkLed = 0;
    }

    if (currentMillis - previousMillis > 1000 ) {
        // save the last time you blinked the LED 
        previousMillis = currentMillis;   
        tick1sec = true;
    }
}
