#include <Arduino.h>

#define NR_OF_ALL_BITS 24*NEOPIXEL_LEDS

// TinyPico LED Color
#define DOTSTAR_BLACK     0x000000
#define DOTSTAR_RED       0x800000
#define DOTSTAR_ORANGE    0x802200
#define DOTSTAR_YELLOW    0x805000
#define DOTSTAR_GREEN     0x008000
#define DOTSTAR_CYAN      0x008080
#define DOTSTAR_BLUE      0x000080
#define DOTSTAR_VIOLET    0x400080
#define DOTSTAR_MAGENTA   0x800033
#define DOTSTAR_PINK      0x803377
#define DOTSTAR_AQUA      0x557D80
#define DOTSTAR_WHITE     0x808080

#define RGB1    1
#define RGB2    0

// Simulate used TinyPico Function
// To be done, fill theese fonction to relevant code specific 
// to board used (Hardware GPIO)
void DotStar_SetBrightness(uint8_t b);
void DotStar_SetPixelColor(uint32_t c);
void DotStar_SetPixelColor(uint32_t c, bool show);
void DotStar_SetPixelColor(uint16_t index, uint32_t c, bool show=true);
void DotStar_Clear();
void DotStar_Clear(uint16_t index); 
void DotStar_Show();
