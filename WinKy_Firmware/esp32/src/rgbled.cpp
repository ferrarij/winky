#include "rgbled.h"
#include <Arduino.h>


#ifdef BUILTIN_RGBLED_PIN

rmt_data_t led_data[NR_OF_ALL_BITS];
uint32_t led_color[NEOPIXEL_LEDS];

void DotStar_SetBrightness(uint8_t b)  { 
  //strip.SetBrightness(b);
}


void DotStar_Show() 
{ 
  int led, c, bit;
  int i=0;
  uint32_t color;
  uint8_t col[3];
  for (led=0; led<NEOPIXEL_LEDS; led++) {
    color = led_color[led];
    col[0] = (color >> 8 ) & 0xFF ; // Red
    col[1] = (color >> 16) & 0xFF ; // Green
    col[2] = (color      ) & 0xFF ; // Blue
    //SerialDebug.printf_P(PSTR("#%d %02X%02X%02X\r\n"), led, col[0],col[1],col[2]);
    for (bit=0; bit<8; bit++){
      for (c=0; c<3; c++){
        if ( col[c] & (1<<(7-bit)) ) {
          led_data[i].level0 = 1;
          led_data[i].duration0 = 8;
          led_data[i].level1 = 0;
          led_data[i].duration1 = 4;
        } else {
          led_data[i].level0 = 1;
          led_data[i].duration0 = 4;
          led_data[i].level1 = 0;
          led_data[i].duration1 = 8;
        }
        i++;
      }
    }
  }
  // Send the data and wait until it is done
  rmtWrite(BUILTIN_RGBLED_PIN, led_data, NR_OF_ALL_BITS, RMT_WAIT_FOR_EVER);
}


void DotStar_SetPixelColor(uint32_t c, bool show ) { 
  led_color[0] = c;
  if (show) {
    DotStar_Show();
  }
}

void DotStar_SetPixelColor(uint32_t c ) { 
  led_color[0] = c;
}

void DotStar_SetPixelColor(uint16_t index, uint32_t c, bool show) { 
  led_color[index] = c;
  if (show) {
    DotStar_Show();
  }
}

void DotStar_Clear() { 

  for (int led=0; led<NEOPIXEL_LEDS; led++) {
    led_color[led] = 0;
    DotStar_Show();
  }
}

void DotStar_Clear(uint16_t index) { 
  led_color[index] = 0;
  DotStar_Show();
}

#else 

void DotStar_SetBrightness(uint8_t b) {}
void DotStar_SetPixelColor(uint32_t c, bool show ) {}
void DotStar_SetPixelColor(uint32_t c ) {}
void DotStar_SetPixelColor(uint16_t index, uint32_t c, bool show) {}
void DotStar_Clear() {}
void DotStar_Clear(uint16_t index) {} 
void DotStar_Show() {}

#endif // BUILTIN_RGBLED_PIN
