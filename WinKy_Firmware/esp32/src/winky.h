#include "Arduino.h"
#include "time.h"
#include "stdio.h"
#include "stdarg.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"
#include "rgbled.h"

#ifndef LED_BUILTIN
// Change this pin if needed
#define LED_BUILTIN NOT_A_PIN
#else
#ifdef BUILTIN_RGBLED_PIN
// Avoid same pin led buildin and NeoPixel
#undef LED_BUILTIN
#define LED_BUILTIN NOT_A_PIN
#endif
#endif

#define CRLF "\r\n"
#define uS_TO_S_FACTOR 1000000ULL 

char * getDate() ;

#if defined (CONFIG_IDF_TARGET_ESP32C3)

#include "driver/adc.h"
#include "esp_adc_cal.h"
#define VUSB_CHANNEL ADC1_CHANNEL_1  // VUSB voltage ADC input
#define VCAP_CHANNEL ADC1_CHANNEL_4  // VCAP voltage ADC input

#elif defined (CONFIG_IDF_TARGET_ESP32C2)
// To Be done
//#include "driver/adc.h"
//#include "esp_adc_cal.h"
#define VUSB_CHANNEL ADC1_CHANNEL_1  // VUSB voltage ADC input
#define VCAP_CHANNEL ADC1_CHANNEL_3  // VCAP voltage ADC input
#define VLKY_CHANNEL ADC1_CHANNEL_5 // VLinky voltage ADC input
// divider resistor values

#endif

#define UPPER_DIVIDER 470
#define LOWER_DIVIDER 470

