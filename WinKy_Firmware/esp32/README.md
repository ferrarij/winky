# Winky esp32 - Teleinfo usage data for MQTT

Fetch energy values from French Linky smart meter using Teleinfo protocol. Integrated with MQTT. 

## Hardware

The project runs on ESP32-C3 and ESP32-C6 Winky Board, just select the target into VScode


## Configuration

All configuration is done thru `platformio.ini` file


Set the ms to sleep between wake/connect/read
```ini
    -D WAKE_MS=2500
```

Here, each day at 08H15, if read fails it will retry 15min later and do that until it worked or 6 times maximum.

So in case of all reads fail, last retry will be at 09H45 and then stop retries. 

Process will start back at 08H15 next day.

Set your WiFi credentials and time zone see [timezone.h](timezone.h)
```ini
    -D WIFI_SSID=\"YOUR-SSID\"
    -D WIFI_PASS=\"YOUR-PASSWORD\"
    -D TIME_ZONE=TZ_Europe_Paris ; See timezone.h of ./src folder
```

And then MQTT related 
```ini
    -D MQTT_SERVER=\"192.168.1.8\"
    -D TOPIC_BASE=\"linky/\"
    -D MQTT_USER=\"\"
    -D MQTT_PASS=\"\"
    -D MQTT_PORT=1883
    ;-D PUBLISH_RAW  ; Enable to publish also RAW values (keeping JSON)
```

If you need MQTT auth please do like that
```ini
    -D MQTT_SERVER=\"192.168.1.8\"
    -D TOPIC_BASE=\"linky/\"
    -D MQTT_USER=\"MQTT_USER\"
    -D MQTT_PASS=\"MQTT_PASS\"
    -D MQTT_PORT=1883
    ;-D PUBLISH_RAW  ; Enable to publish also RAW values (keeping JSON)
```

## MQTT Topics and data

### Base Topic

Base topic is `linky` followed by smart meter serial number (`ADCO` or `ADSC`) followed by `-espabcd`, because I've also device with PI so I need to know who is sending data, of course you can remove it if needed.

```
linky/031428067147-espbf84
```

### Reading OK

When read suceeded, results are sent in `JSON`format to the base topic plus `json`

Example received on `linky/031428067147-espbf84/json`

```json
{
    "ts": 1689656420,
    "date": "Tue Jul 18 07:00:20 2023",
    "vcap": 3.5,
    "vusb": 5.0
}
```

Values can also be sent in raw format enabling setting `PUBLISH_RAW` in `platformio.ini`

```ini
    -D PUBLISH_RAW  ; Enable to publish also RAW values (keeping JSON)
```

- `linky/031428067147-espbf84/EAST`
- `linky/031428067147-espbf84/URMS1`
- `linky/031428067147-espbf84/IRMS1`
- `linky/031428067147-espbf84/VUSB`
- `linky/031428067147-espbf84/VCAP`


### Read fail

When read failed, results are sent in `JSON`

Example received on `linky/031428067147-espbf84/error`

```json
{
    "ts": 1689626877,
    "date": "Mon Jul 17 22:47:57 2023",
    "vcap": 3.5,
    "vusb": 5.0,
    "type": "No Data"
}
```

### Next Wake

Before going into deep sleep until next measure `WAKE_MEASURE` of `platformio.ini` the device will send a `JSON` message to topic `sleep_until` 

Example received on `linky/031428067147-espbf84/sleep_until`

```json
{
    "seconds": 29526,
    "ts": 1689656406,
    "date": "Tue Jul 18 07:00:06 2023"
}
```

## Misc

See news and other projects on my [blog][1] 

[1]: https://hallard.me


