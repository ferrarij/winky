#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <InfluxDbClient.h>
#include <ESPAsyncTCP.h>
#include <Hash.h>
#include <FS.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <ArduinoJson.h>
#include <include/WiFiState.h>  // WiFiState structure details

#ifndef RTC_USER_DATA_SLOT_WIFI_STATE
#define RTC_USER_DATA_SLOT_WIFI_STATE 33u
#endif

WiFiState state;
AsyncWebServer server(80);


long startloop;
long scanloop;

/*Parametres Wifi*/
String wifi_ssid;
String wifi_password;
const char *ssid = "XKY";
const char *password = "oneforall";
WiFiClient espClient;
int midtrywificon = 150;
int nbtrywificon = 0;
int channel = 11;
String AdressMacRouteur;
char str[] = "00:00:00:00:00:00";
uint8_t bssid[6];
char* ptr; //start and end pointer for strtol

/*Parametres Linky*/
boolean progmode = 0;
boolean linky_histstard = 0;

/*Parametres Validation*/
boolean okpapp = 0;
boolean okpappi = 0;
boolean okbaseprod = 0;

/*Calibre tic auto*/
long calibtimefault = 4999;
long timestartfunc;// = millis();
boolean teleInfoproblem = 0;

/*Parametres InfluxDB*/
boolean useinflux = 0;
String INFLUXDB_URL;
// InfluxDB 2 server or cloud API authentication token (Use: InfluxDB UI -> Load Data -> Tokens -> <select token>)
String INFLUXDB_TOKEN;
// InfluxDB 2 organization id (Use: InfluxDB UI -> Settings -> Profile -> <name under tile> )
String INFLUXDB_ORG;
// InfluxDB 2 bucket name (Use: InfluxDB UI -> Load Data -> Buckets)
String INFLUXDB_BUCKET;

/*Parametres MQTT*/
boolean usemqtt = 0;
String mqtt_server;
String mqtt_user;
String mqtt_password;
String maintopic;
PubSubClient client(espClient);
int maxtrymqttcon = 5;
int nbtrymqttcon = 0;

/*Parametres xKy*/
long MQAll;
long DeepModulateSleeptime;
boolean useoptimwifi = 0;
boolean useled = 0;
int DeepSleepSecondsOpti = 0;
String FWVersion = "V54";
String BoardVersion = "ESP8266";
unsigned long timestartup;
unsigned long timedecode;
unsigned long timeconnexion;

/*Parametres Page WEB*/
const char* PARAM_URL = "inputUrl";
const char* PARAM_TOKEN = "inputToken";
const char* PARAM_ORG = "inputOrg";
const char* PARAM_BUCKET = "inputBucket";
const char* PARAM_WIFI_SSID = "inputWifiSsid";
const char* PARAM_WIFI_PASSWORD = "inputWifiPassword";
const char* PARAM_ACTIVE_INFLUX = "inputActiverInflux";
const char* PARAM_ACTIVE_MQTT = "inputActiverMqtt";
const char* PARAM_URL_MQTT = "inputUrlMqtt";
const char* PARAM_USER_MQTT = "inputUserMqtt";
const char* PARAM_PASSWORD_MQTT = "inputPasswordMqtt";
const char* PARAM_MAINTOPIC_MQTT = "inputMainTopicMqtt";
const char* PARAM_MODEHISTSTAND = "inputModeHistStand";
const char* PARAM_ACTIVE_OPTIMWIFI = "inputActiverOptimWifi";
const char* PARAM_MIDCONNEXION = "inputMidConnexion";
const char* PARAM_OPTIMDSSECONDS = "inputOptimiDSSeconds";
const char* PARAM_ACTIVE_LED = "inputActiverLed";

String struseoptimwifi;
String struseled;
String strmidtrywificon;
String strDeepSleepSecondsOpti;
String struseinflux;
String strINFLUXDB_URL;
String strINFLUXDB_TOKEN;
String strINFLUXDB_ORG;
String strINFLUXDB_BUCKET;
String strwifi_ssid;
String strwifi_password;
String strwifi_passwordhtmlclean;
String strusemqtt;
String strmqtt_server;
String strmqtt_user;
String strmqtt_password;
String strmaintopicmqtt;
String strlinky_histstard;
String strscanwifitemp;
String strscanwifi;


// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta  />
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Wifi</b></p>
  <form action="/get" target="hidden-form">
    Nom du reseau wifi (SSID) actuel: %inputWifiSsid% <input type="text" name="inputWifiSsid"><br><br>
    Mot de passe actuel: %inputWifiPassword% <input type="text" name="inputWifiPassword">
    <p>%inputScanWifi%</p>
    <button onclick="window.location.href = 'http://192.168.4.1/';">Rescanner wifi</button>
    <p><b>Configuration Linky</b></p>
    <label for="Mode">Mode actuel: %inputModeHistStand%</label>
      <select name="inputModeHistStand" id="Mode">
        <option value="Historique">Historique</option>
        <option value="Standard">Standard</option>
        <option selected hidden>%inputModeHistStand%</option>
      </select><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">  
  </form><br>
  <button onclick="window.location.href = 'http://192.168.4.1/influx';">Configuration InfluxDB</button>
  <button onclick="window.location.href = 'http://192.168.4.1/mqtt';">Configuration MQTT</button>
  <button onclick="window.location.href = 'http://192.168.4.1/avance';">Configuration Avancee</button>
  <button onclick="window.location.href = 'http://192.168.4.1/update';">Mise a jour Software</button>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char influx_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Server InfluxDB</b></p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Activation InfluxDB actuel: %inputActiverInflux%</label>
      <select name="inputActiverInflux" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverInflux%</option>
      </select><br><br>
    Url actuelle: %inputUrl% <input type="text" name="inputUrl"><br><br>
    Token actuel: %inputToken% <input type="text " name="inputToken"><br><br>
    Organisation actuelle: %inputOrg% <input type="text " name="inputOrg"><br><br>
    Bucket actuel: %inputBucket% <input type="text " name="inputBucket"><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">
  </form>
  <br>
  <button onclick="window.location.href = 'http://192.168.4.1/';">Configuration Principale</button>
  <button onclick="window.location.href = 'http://192.168.4.1/mqtt';">Configuration MQTT</button>
  <button onclick="window.location.href = 'http://192.168.4.1/avance';">Configuration Avancee</button>
  <button onclick="window.location.href = 'http://192.168.4.1/update';">Mise a jour Software</button>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char mqtt_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Server MQTT</b></p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Activation MQTT actuel: %inputActiverMqtt%</label>
      <select name="inputActiverMqtt" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverMqtt%</option>
      </select><br><br>
    Url actuelle: %inputUrlMqtt% <input type="text" name="inputUrlMqtt"><br><br>
    User actuel: %inputUserMqtt% <input type="text " name="inputUserMqtt"><br><br>
    Password actuel: %inputPasswordMqtt% <input type="text " name="inputPasswordMqtt"><br><br>
    Topic actuel: %inputMainTopicMqtt% <input type="text " name="inputMainTopicMqtt"><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">
  </form>
  <br>  
  <button onclick="window.location.href = 'http://192.168.4.1/';">Configuration Principale</button>
  <button onclick="window.location.href = 'http://192.168.4.1/influx';">Configuration InfluxDB</button>
  <button onclick="window.location.href = 'http://192.168.4.1/avance';">Configuration Avancee</button>
  <button onclick="window.location.href = 'http://192.168.4.1/update';">Mise a jour Software</button>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char avance_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Parametres internes XKY</b></p>
  <p>Attention. Ces parametres sont disponible a titre experimental pour vous permettre de modifier le comportement du XKY.</p>
  <p>Un mauvais reglage peut engendrer des instabilites.</p>
  <p>En cas de doute, vous pouvez mettre les activations sur Desactive.</p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Option optimisation wifi actuel: %inputActiverOptimWifi%</label>
      <select name="inputActiverOptimWifi" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverOptimWifi%</option>
      </select><br><br>
    Nb de secondes validation optimisation wifi (par defaut 15s) valeur actuelle: %inputMidConnexion% <input type="text" name="inputMidConnexion"><br><br>
    Nb de secondes deepsleep optimisation offset (Avec signe + ou - et ici la decimale s'ecrit avec un point exemple 1.5s) (par defaut 0s) valeur actuelle: %inputOptimiDSSeconds% <input type="text" name="inputOptimiDSSeconds"><br><br>
    <label for="led">Option activation LED actuel: %inputActiverLed%</label>
      <select name="inputActiverLed" id="led">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverLed%</option>
      </select><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">
  </form><br>
  <br>
  <button onclick="window.location.href = 'http://192.168.4.1/';">Configuration Principale</button>
  <button onclick="window.location.href = 'http://192.168.4.1/influx';">Configuration InfluxDB</button>
  <button onclick="window.location.href = 'http://192.168.4.1/mqtt';">Configuration MQTT</button>
  <button onclick="window.location.href = 'http://192.168.4.1/update';">Mise a jour Software</button>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

String readFile(fs::FS &fs, const char * path) {
  Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if (!file || file.isDirectory()) {
    Serial.println("- empty file or failed to open file");
    return String();
  }
  Serial.println("- read from file:");
  String fileContent;
  while (file.available()) {
    fileContent += String((char)file.read());
  }
  file.close();
  Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if (!file) {
    Serial.println("- failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
  file.close();
}

void ledwifiprogmode() {
  if ((millis() - startloop) < 499) {
    digitalWrite(2, HIGH);
  } else if (((millis() - startloop) < 1000)) {
    digitalWrite(2, LOW);
  } else {
    startloop = millis();
  }
}

void scanwifi() {
  String ssid;
  int32_t rssi;
  uint8_t encryptionType;
  uint8_t *bssid;
  int32_t channel;
  bool hidden;
  int scanResult;

  Serial.println(F("Starting WiFi scan..."));
  ledwifiprogmode();
  scanResult = WiFi.scanNetworks(/*async=*/false, /*hidden=*/true);
  ledwifiprogmode();
  if (scanResult == 0) {
    Serial.println(F("No networks found"));
    strscanwifitemp = "Pas de reseau wifi accessible";
  } else if (scanResult > 0) {
    Serial.printf(PSTR("%d networks found:\n"), scanResult);
    strscanwifitemp = "<b>" + String(scanResult) + " reseaux trouves:" + "</b><br>";

    // Print unsorted scan results
    for (int8_t i = 0; i < scanResult; i++) {
      WiFi.getNetworkInfo(i, ssid, encryptionType, rssi, bssid, channel, hidden);

      // get extra info
      const bss_info *bssInfo = WiFi.getScanInfoByIndex(i);
      String phyMode;
      const char *wps = "";
      if (bssInfo) {
        phyMode.reserve(12);
        phyMode = F("802.11");
        String slash;
        if (bssInfo->phy_11b) {
          phyMode += 'b';
          slash = '/';
        }
        if (bssInfo->phy_11g) {
          phyMode += slash + 'g';
          slash = '/';
        }
        if (bssInfo->phy_11n) {
          phyMode += slash + 'n';
        }
        if (bssInfo->wps) {
          wps = PSTR("WPS");
        }
      }
      Serial.printf(PSTR("  %02d: [CH %02d] [%02X:%02X:%02X:%02X:%02X:%02X] %ddBm %c %c %-11s %3S %s\n"), i, channel, bssid[0], bssid[1], bssid[2], bssid[3], bssid[4], bssid[5], rssi, (encryptionType == ENC_TYPE_NONE) ? ' ' : '*', hidden ? 'H' : 'V', phyMode.c_str(), wps, ssid.c_str());
      strscanwifitemp = strscanwifitemp + String(i) + ": <b>" + ssid.c_str() + "</b> " + String(rssi) + "dBm [CH " + String(channel) + "] [" + String(bssid[0]) + ":" + String(bssid[1]) + ":" + String(bssid[2]) + ":" + String(bssid[3]) + ":" + String(bssid[4]) + ":" + String(bssid[5]) + "] " + phyMode.c_str() + " " + wps + "<br>";
      yield();
      ledwifiprogmode();
    }
  } else {
    Serial.printf(PSTR("WiFi scan error %d"), scanResult);
    strscanwifitemp = "WiFi erreur scan";
  }
  strscanwifi = strscanwifitemp;
}

// Replaces placeholder with stored values
String processor(const String& var) {
  //Serial.println(var);
  if (var == "inputUrl") {
    return strINFLUXDB_URL;
  }
  else if (var == "inputToken") {
    return strINFLUXDB_TOKEN;
  }
  else if (var == "inputOrg") {
    return strINFLUXDB_ORG;
  }
  else if (var == "inputBucket") {
    return strINFLUXDB_BUCKET;
  }
  else if (var == "inputWifiSsid") {
    return strwifi_ssid;
  }
  else if (var == "inputScanWifi") {
    return strscanwifi;
  }
  else if (var == "BoardVersion") {
    return BoardVersion;
  }
  else if (var == "FWVersion") {
    return FWVersion;
  }
  else if (var == "inputWifiPassword") {
    return strwifi_passwordhtmlclean;
  }
  else if (var == "inputActiverInflux") {
    return struseinflux;
  }
  else if (var == "inputActiverMqtt") {
    return strusemqtt;
  }
  else if (var == "inputUrlMqtt") {
    return strmqtt_server;
  }
  else if (var == "inputUserMqtt") {
    return strmqtt_user;
  }
  else if (var == "inputPasswordMqtt") {
    return strmqtt_password;
  }
  else if (var == "inputModeHistStand") {
    return strlinky_histstard;
  }
  else if (var == "inputActiverOptimWifi") {
    return struseoptimwifi;
  }
  else if (var == "inputMidConnexion") {
    return strmidtrywificon;
  }
  else if (var == "inputActiverLed") {
    return struseled;
  }
  else if (var == "inputOptimiDSSeconds") {
    return strDeepSleepSecondsOpti;
  }
  else if (var == "inputMainTopicMqtt") {
    return strmaintopicmqtt;
  }
  return String();
}



String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

String macToStrsimple(const uint8_t* mac)
{
  String result;
  for (int i = 3; i < 6; ++i) {
    result += String(mac[i], 16);
  }
  return result;
}

String composeClientID() {
  uint8_t mac[6];
  WiFi.macAddress(mac);
  String clientId;
  clientId += "xky-";
  clientId += macToStr(mac);
  return clientId;
}

void setup_wifi() {
  uint8_t mac[6];
  WiFi.macAddress(mac);
  WiFi.hostname("winky-" + macToStrsimple(mac));
  WiFi.enableInsecureWEP();
  ESP.rtcUserMemoryRead(RTC_USER_DATA_SLOT_WIFI_STATE, reinterpret_cast<uint32_t*>(&state), sizeof(state));
  if (!WiFi.resumeFromShutdown(state) || (WiFi.waitForConnectResult(10000) != WL_CONNECTED)) {
    Serial.println("Cannot resume WiFi connection, connecting via begin...");
    WiFi.persistent(false);

    if (!WiFi.mode(WIFI_STA) || !WiFi.begin(wifi_ssid, wifi_password) || (WiFi.waitForConnectResult(10000) != WL_CONNECTED)) {
      WiFi.mode(WIFI_OFF);
      Serial.println("Cannot connect!");
      Serial.flush();
      ESP.deepSleep(10e6, RF_DISABLED);
      return;
    }
  }
}



//Connexion au reseau WiFi
void setup_wifi2() {
  //  WiFi.persistent(false); //Ajout
  //  WiFi.mode(WIFI_OFF); //Ajout
  //  delay(100); //Ajout
  WiFi.mode(WIFI_STA);
  delay(10);
  WiFi.setOutputPower(20.5);
  nbtrywificon = 0;
  uint8_t mac[6];
  WiFi.macAddress(mac);
  WiFi.hostname("winky-" + macToStrsimple(mac));
  WiFi.enableInsecureWEP();
  //  WiFi.disconnect(); //Ajout
  delay(100); //Ajout
  useoptimwifi = 0;
  // WiFi.setPhyMode(WIFI_PHY_MODE_11B);
  //WiFi.setPhyMode(WIFI_PHY_MODE_11N);

  WiFi.setPhyMode(WIFI_PHY_MODE_11G);
  if (AdressMacRouteur != "" && useoptimwifi) {
    WiFi.begin(wifi_ssid, wifi_password, channel, bssid);
    while (WiFi.status() != WL_CONNECTED) {
      delay(100);
      Serial.print(".");
      nbtrywificon = nbtrywificon + 1;
      if (nbtrywificon == midtrywificon) {
        SPIFFS.remove("/inputMacadress.txt");
      }
    }
  }
  else {
    WiFi.begin(wifi_ssid, wifi_password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(100);
      Serial.print(".");
    }
  }
  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());
  Serial.printf("\nWiFi channel: %d\n", WiFi.channel());
  Serial.printf("WiFi BSSID: %s\n", WiFi.BSSIDstr().c_str());
  if (useoptimwifi) {
    if (AdressMacRouteur != WiFi.BSSIDstr().c_str()) {
      writeFile(SPIFFS, "/inputMacadress.txt", WiFi.BSSIDstr().c_str());
    }
    if (String(channel) != String(WiFi.channel())) {
      writeFile(SPIFFS, "/inputChannel.txt", String(WiFi.channel()).c_str());
    }
  }
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'a obtenir une reconnexion
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    String clientnumber = composeClientID();
    client.setBufferSize(2048);
    if (client.connect(clientnumber.c_str(), mqtt_user.c_str(), mqtt_password.c_str())) {
      Serial.println("OK");
    } else {
      Serial.print("KO, erreur : ");
      Serial.print(client.state());
      Serial.println(" On attend 1 secondes avant de recommencer");
      delay(1000);
      nbtrymqttcon = nbtrymqttcon + 1;
      if (nbtrymqttcon == maxtrymqttcon) {
        ESP.deepSleep(30000000);
      }
    }
  }
}

//#include "linkyhist.h"
#include "linky.h"

void setup() {
  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH);
  delay(500);
  pinMode(4, INPUT);
  progmode = digitalRead(4);
  //progmode = 0;
  if (progmode) {
    Serial.begin(9600);
    if (!SPIFFS.begin()) {
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }
    WiFi.mode(WIFI_AP_STA);
    WiFi.softAP(ssid, password);
    IPAddress myIP = WiFi.softAPIP();
    // Send web page with input fields to client

    struseoptimwifi = readFile(SPIFFS, "/inputActiverOptimWifi.txt");
    struseled = readFile(SPIFFS, "/inputActiverLed.txt");
    strmidtrywificon = readFile(SPIFFS, "/inputMidConnexion.txt");
    strDeepSleepSecondsOpti = readFile(SPIFFS, "/inputOptimiDSSeconds.txt");

    struseinflux = readFile(SPIFFS, "/inputActiverInflux.txt");
    strINFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
    strINFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
    strINFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
    strINFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");

    strwifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
    strwifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
    strwifi_passwordhtmlclean = strwifi_password;
    strwifi_passwordhtmlclean.replace("%", "&#37;");

    strusemqtt = readFile(SPIFFS, "/inputActiverMqtt.txt");
    strmqtt_server = readFile(SPIFFS, "/inputUrlMqtt.txt");
    strmqtt_user = readFile(SPIFFS, "/inputUserMqtt.txt");
    strmqtt_password = readFile(SPIFFS, "/inputPasswordMqtt.txt");
    strmaintopicmqtt = readFile(SPIFFS, "/inputMainTopicMqtt.txt");
    strlinky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt");

    server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
      //      scanwifi();
      request->send_P(200, "text/html", index_html, processor);
    });

    server.on("/influx", HTTP_GET, [](AsyncWebServerRequest * request) {
      request->send_P(200, "text/html", influx_html, processor);
    });

    server.on("/mqtt", HTTP_GET, [](AsyncWebServerRequest * request) {
      request->send_P(200, "text/html", mqtt_html, processor);
    });

    server.on("/avance", HTTP_GET, [](AsyncWebServerRequest * request) {
      request->send_P(200, "text/html", avance_html, processor);
    });

    // Send a GET request to <ESP_IP>/get?inputUrl=<inputMessage>
    server.on("/get", HTTP_GET, [] (AsyncWebServerRequest * request) {
      String inputMessage;
      // GET inputUrl value on <ESP_IP>/get?inputUrl=<inputMessage>
      if (request->hasParam(PARAM_URL)) {
        inputMessage = request->getParam(PARAM_URL)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputUrl.txt", inputMessage.c_str());
          strINFLUXDB_URL = inputMessage.c_str();
        }
      }
      // GET inputToken value on <ESP_IP>/get?inputToken=<inputMessage>
      if (request->hasParam(PARAM_TOKEN)) {
        inputMessage = request->getParam(PARAM_TOKEN)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputToken.txt", inputMessage.c_str());
          strINFLUXDB_TOKEN = inputMessage.c_str();
        }
      }
      // GET inputOrg value on <ESP_IP>/get?inputOrg=<inputMessage>
      if (request->hasParam(PARAM_ORG)) {
        inputMessage = request->getParam(PARAM_ORG)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputOrg.txt", inputMessage.c_str());
          strINFLUXDB_ORG = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_BUCKET)) {
        inputMessage = request->getParam(PARAM_BUCKET)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputBucket.txt", inputMessage.c_str());
          strINFLUXDB_BUCKET = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_WIFI_SSID)) {
        inputMessage = request->getParam(PARAM_WIFI_SSID)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputWifiSsid.txt", inputMessage.c_str());
          strwifi_ssid = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_WIFI_PASSWORD)) {
        inputMessage = request->getParam(PARAM_WIFI_PASSWORD)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputWifiPassword.txt", inputMessage.c_str());
          strwifi_password = inputMessage.c_str();
          strwifi_passwordhtmlclean = strwifi_password;
          strwifi_passwordhtmlclean.replace("%", "&#37;");
        }
      }
      if (request->hasParam(PARAM_ACTIVE_INFLUX)) {
        inputMessage = request->getParam(PARAM_ACTIVE_INFLUX)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputActiverInflux.txt", inputMessage.c_str());
          struseinflux = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_ACTIVE_MQTT)) {
        inputMessage = request->getParam(PARAM_ACTIVE_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputActiverMqtt.txt", inputMessage.c_str());
          strusemqtt = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_URL_MQTT)) {
        inputMessage = request->getParam(PARAM_URL_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputUrlMqtt.txt", inputMessage.c_str());
          strmqtt_server = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_USER_MQTT)) {
        inputMessage = request->getParam(PARAM_USER_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputUserMqtt.txt", inputMessage.c_str());
          strmqtt_user = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_PASSWORD_MQTT)) {
        inputMessage = request->getParam(PARAM_PASSWORD_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputPasswordMqtt.txt", inputMessage.c_str());
          strmqtt_password = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_MODEHISTSTAND)) {
        inputMessage = request->getParam(PARAM_MODEHISTSTAND)->value();
        writeFile(SPIFFS, "/inputModeHistStand.txt", inputMessage.c_str());
        strlinky_histstard = inputMessage.c_str();
      }
      if (request->hasParam(PARAM_ACTIVE_OPTIMWIFI)) {
        inputMessage = request->getParam(PARAM_ACTIVE_OPTIMWIFI)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputActiverOptimWifi.txt", inputMessage.c_str());
          struseoptimwifi = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_MIDCONNEXION)) {
        inputMessage = request->getParam(PARAM_MIDCONNEXION)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputMidConnexion.txt", inputMessage.c_str());
          strmidtrywificon = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_ACTIVE_LED)) {
        inputMessage = request->getParam(PARAM_ACTIVE_LED)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputActiverLed.txt", inputMessage.c_str());
          struseled = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_OPTIMDSSECONDS)) {
        inputMessage = request->getParam(PARAM_OPTIMDSSECONDS)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputOptimiDSSeconds.txt", inputMessage.c_str());
          strDeepSleepSecondsOpti = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_MAINTOPIC_MQTT)) {
        inputMessage = request->getParam(PARAM_MAINTOPIC_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputMainTopicMqtt.txt", inputMessage.c_str());
          strmaintopicmqtt = inputMessage.c_str();
        }
      }
      //  else {
      //    inputMessage = "No message sent";
      //  }
      Serial.println(inputMessage);
      request->send(200, "text/text", inputMessage);
    });
    server.onNotFound(notFound);
    AsyncElegantOTA.begin(&server);    // Start AsyncElegantOTA
    server.begin();
    startloop = millis();
    scanloop = millis();
    scanwifi();
  }
  else {
    String resetCause = ESP.getResetReason();
    Serial.println(resetCause);
    //  resetCount = 0;
    if (resetCause != "Deep-Sleep Wake") {
      ESP.deepSleep(30000000, WAKE_RF_DISABLED); //100ms
      //    Serial.println(F("I'm awake and starting the Low Power tests"));
    }
    if (!SPIFFS.begin()) {
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }
    useoptimwifi = readFile(SPIFFS, "/inputActiverOptimWifi.txt") == "Active";
    useled = readFile(SPIFFS, "/inputActiverLed.txt") == "Active";
    midtrywificon = 10 * ((readFile(SPIFFS, "/inputMidConnexion.txt")).toInt());
    DeepSleepSecondsOpti = 1000000 * ((readFile(SPIFFS, "/inputOptimiDSSeconds.txt")).toFloat());

    useinflux = readFile(SPIFFS, "/inputActiverInflux.txt") == "Active";
    INFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
    INFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
    INFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
    INFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");

    wifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
    wifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
    usemqtt = readFile(SPIFFS, "/inputActiverMqtt.txt") == "Active";
    mqtt_server = readFile(SPIFFS, "/inputUrlMqtt.txt");
    mqtt_user = readFile(SPIFFS, "/inputUserMqtt.txt");
    mqtt_password = readFile(SPIFFS, "/inputPasswordMqtt.txt");
    strmaintopicmqtt = readFile(SPIFFS, "/inputMainTopicMqtt.txt");
    channel = readFile(SPIFFS, "/inputChannel.txt").toInt();
    AdressMacRouteur = readFile(SPIFFS, "/inputMacadress.txt");
    //lecture adresse mac routeur
    if (AdressMacRouteur != "") {
      int str_len = AdressMacRouteur.length() + 1;
      char str[str_len];
      AdressMacRouteur.toCharArray(str, str_len);
      bssid[0] = strtol(str, &ptr, HEX );
      for ( uint8_t i = 1; i < 6; i++ )
      {
        bssid[i] = strtol(ptr + 1, &ptr, HEX );
      }
      Serial.print(bssid[0], HEX);
      for ( uint8_t i = 1; i < 6; i++)
      {
        Serial.print(':');
        Serial.print( bssid[i], HEX);
      }

      //original character array is preserved
      Serial.println();
      Serial.print(str);
    }
    linky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt") == "Standard";
    Serial.begin(9600);
    if (linky_histstard == 1) {
      linky_setup();
    }
  }
  timestartup = millis();
  timestartfunc = millis();
}

void loop() {
  if (!progmode) {
    if (linky_histstard == 0) {
      linky_hist_loop();
    }
    else {
      linky_loop();
    }
  } else {
    ledwifiprogmode();
    if ((millis() - scanloop) > 5000) {
      scanwifi();
      scanloop = millis();
    }
  }
}
