#include <Arduino.h>

#define BUILTIN_RGBLED_PIN 2
#define NEOPIXEL_LEDS 2
#define NR_OF_ALL_BITS 24 * NEOPIXEL_LEDS
rmt_data_t led_data[NR_OF_ALL_BITS];

// TinyPico LED Color
#define DOTSTAR_BLACK     0x000000
#define DOTSTAR_RED       0x800000
#define DOTSTAR_ORANGE    0x802200
#define DOTSTAR_YELLOW    0x805000
#define DOTSTAR_GREEN     0x008000
#define DOTSTAR_CYAN      0x008080
#define DOTSTAR_BLUE      0x000080
#define DOTSTAR_VIOLET    0x400080
#define DOTSTAR_MAGENTA   0x800033
#define DOTSTAR_PINK      0x803377
#define DOTSTAR_AQUA      0x557D80
#define DOTSTAR_WHITE     0x808080

#define RGB1    1
#define RGB2    0

int led_color[] = { 0xFF0000, 0x00FF00 };

//int color[] = { 0xFF, 0x00, 0x00 };  // Green Red Blue values
int led_index = 0;

void DotStar_Show() {
  int led, c, bit;
  int i = 0;
  uint32_t color;
  uint8_t col[3] = { 0xFF, 0x00, 0x00 };
  for (led = 0; led < NEOPIXEL_LEDS; led++) {
    color = led_color[led];
    col[0] = (color >> 8) & 0xFF;   // Red
    col[1] = (color >> 16) & 0xFF;  // Green
    col[2] = (color)&0xFF;          // Blue
    //SerialDebug.printf_P(PSTR("#%d %02X%02X%02X\r\n"), led, col[0],col[1],col[2]);
    for (c = 0; c < 3; c++) {
      for (bit = 0; bit < 8; bit++) {
        if (col[c] & (1 << (7 - bit))) {
          led_data[i].level0 = 1;
          led_data[i].duration0 = 8;
          led_data[i].level1 = 0;
          led_data[i].duration1 = 4;
        } else {
          led_data[i].level0 = 1;
          led_data[i].duration0 = 4;
          led_data[i].level1 = 0;
          led_data[i].duration1 = 8;
        }
        i++;
      }
    }
  }
  // Send the data and wait until it is done
  rmtWrite(BUILTIN_RGBLED_PIN, led_data, NR_OF_ALL_BITS, RMT_WAIT_FOR_EVER);
}

void DotStar_SetPixelColor(uint32_t c, bool show ) { 
  led_color[0] = c;
  if (show) {
    DotStar_Show();
  }
}

void DotStar_SetPixelColor(uint32_t c ) { 
  led_color[0] = c;
}

void DotStar_SetPixelColor(uint16_t index, uint32_t c, bool show) { 
  led_color[index] = c;
  if (show) {
    DotStar_Show();
  }
}

void DotStar_Clear() { 
  for (int led=0; led<NEOPIXEL_LEDS; led++) {
    led_color[led] = 0;
    DotStar_Show();
  }
}

void DotStar_Clear(uint16_t index) { 
  led_color[index] = 0;
  DotStar_Show();
}
