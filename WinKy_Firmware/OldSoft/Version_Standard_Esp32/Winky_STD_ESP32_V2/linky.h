// **********************************************************************************
// Arduino Teleinfo sample, display information on teleinfo values received
// **********************************************************************************
// Creative Commons Attrib Share-Alike License
// You are free to use/extend this library but please abide with the CC-BY-SA license:
// http://creativecommons.org/licenses/by-sa/4.0/
//
// for detailled explanation of this library see dedicated article
// https://hallard.me/libteleinfo/
//
// For any explanation about teleinfo or use, see my blog
// https://hallard.me/category/tinfo
//
// connect Teleinfo RXD pin To Arduin D3
// see schematic here https://hallard.me/demystifier-la-teleinfo/
// and dedicated article here
//
// Written by Charles-Henri Hallard (https://hallard.me)
//
// History : V1.00 2015-06-14 - First release
//
// All text above must be included in any redistribution.
//
// **********************************************************************************
//
// adaptation pour LINKY mode Standart sur ESP32

#include "Teleinfo.h"
#include "Teleinfo_cpp.h"
#include <PubSubClient.h>

WiFiClient espClient;
PubSubClient client(espClient);

TInfo    tinfo; // Teleinfo object
ValueList * me_fist;
bool  ok_me_fist = false;

char * PAPP;
char * BASE;
char * HCHP;

int maxtrywificon = 20;
int nbtrywificon = 0;

int maxtrymqttcon = 5;
int nbtrymqttcon = 0;

/* ======================================================================
  Function: printUptime
  Purpose : print pseudo uptime value
  Input   : -
  Output  : -
  Comments: compteur de secondes basique sans controle de dépassement
          En plus SoftwareSerial rend le compteur de millis() totalement
          A la rue, donc la precision de ce compteur de seconde n'est
          pas fiable du tout, dont acte !!!
  ====================================================================== */
void printUptime(void)
{
  Serial.print(millis() / 1000);
  Serial.print(F("s\t"));
}

/* ======================================================================
  Function: DataCallback
  Purpose : callback when we detected new or modified data received
  Input   : linked list pointer on the concerned data
          current flags value
  Output  : -
  Comments: -
  ====================================================================== */
void DataCallback(ValueList * me, uint8_t  flags)
{
  // Show our not accurate second counter
  //printUptime();

  if (flags & TINFO_FLAGS_ADDED || flags & TINFO_FLAGS_UPDATED)
    if  ( !ok_me_fist) {
      ok_me_fist = true;
      me_fist = me;
    }

  // Display values
  Serial.print("==> ");
  Serial.print(me->name);
  if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
  Serial.print(F("\t")) ;
  if ( me->value[0] != '*') {
    Serial.print(F(" = ")) ;
    Serial.print(me->value) ;
  }
  if ( me->date[0] != '*' ) {
    Serial.print(" : ");
    Serial.print(me->date);
  }
  if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'S') && (me->value[3] != '0') && (me->value[4] != '0')) {

    PAPP = me->value;
    Serial.println(F(""));
    Serial.println("Condition validée");
    Serial.println(PAPP);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'T') && (me->value[7] != '0') && (me->value[8] != '0')) {

    BASE = me->value;
    Serial.println(F(""));
    Serial.println("Condition validée");
    Serial.println(BASE);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '2') && (me->value[7] != '0') && (me->value[8] != '0')) {

    HCHP = me->value;
    Serial.println(F(""));
    Serial.println("Condition validée");
    Serial.println(HCHP);

  }
  Serial.println("");

}

/* ======================================================================
  Function: sendJSON
  Purpose : dump teleinfo values on serial
  Input   : linked list pointer on the concerned data
          true to dump all values, false for only modified ones
  Output  : -
  Comments: -
  ====================================================================== */
void sendAll()
{
  ValueList * me;
  me = me_fist;                                   // reprendre le 1er de la chaine
  // Got at least one ?
  if (me) {

    Serial.print(F("\"all\":"));
    Serial.print(me->name) ;
    Serial.print(F("\t")) ;
    if ( me->value[0] != '*') {
      Serial.print(F(" = ")) ;
      Serial.print(me->value) ;
    }
    if ( me->date[0] != '*') {
      Serial.print(" : ");
      Serial.print(me->date);
    }
    Serial.println("");

    // Loop thru the node
    while (me->next) {

      // go to next node
      me = me->next;                                // suivant
      //sprintf(msg, "%s=%s", me->name, me->value );
      //Mqtt_emettre(me->name, me->value);

      Serial.print(F("\"all\":"));
      Serial.print(me->name) ;
      //if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
      Serial.print(F("\t")) ;
      if ( me->value[0] != '*') {
        Serial.print(F(" = ")) ;
        Serial.print(me->value) ;
      }
      if (me->date[0] != '*') {
        Serial.print(" : ");
        Serial.print(me->date);
      }
      Serial.println("");

    }
  }
}

/* ======================================================================
  Function: setup
  Purpose : Setup I/O and other one time startup stuff
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void linky_setup()
{

  Serial.println(F("======== Setup Teleinfo Linky ======="));
  delay(200);

  Serial2.begin(9600, SERIAL_7N1, RXD2, TXD2); // Teleinfo Serial pin Rx16, Tx17

  // Init teleinfo
  tinfo.init();

  // Attacher les callback dont nous avons besoin
  // pour cette demo, ici attach data
  tinfo.attachData(DataCallback);

}

/* ======================================================================
  Function: macToStr
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
String macToStr(const uint8_t* mac) {
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

/* ======================================================================
  Function: composeClientID
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
String composeClientID() {
  uint8_t mac[6];
  WiFi.macAddress(mac);
  String clientId;
  clientId += "esp-";
  clientId += macToStr(mac);
  return clientId;
}

/* ======================================================================
  Function: reconnect
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void reconnect() {
  //Boucle jusqu'à obtenur une reconnexion
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    String clientnumber = composeClientID();
    if (client.connect(clientnumber.c_str(), mqtt_user, mqtt_password)) {
      Serial.println("OK");
    } else {
      Serial.print("KO, erreur : ");
      Serial.print(client.state());
      Serial.println(" On attend 1 secondes avant de recommencer");
      delay(1000);
      nbtrymqttcon = nbtrymqttcon + 1;
      if (nbtrymqttcon == maxtrymqttcon) {
        ESP.deepSleep(30000000);
      }
    }
  }
}


/* ======================================================================
  Function: loop
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
long MQAll;
void linky_loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // Teleinformation processing
  if ( Serial2.available() ) {
    /*
      char inByte = Serial2.read();
      Serial.print(inByte);
      Serial.print(".");
      Serial.print(inByte,HEX);
      Serial.print(";");
    */
    tinfo.process(Serial2.read());
  }

  String adressmacesp = composeClientID();
  String topic_power = "winky/" + adressmacesp + "/power";
  String topic_base = "winky/" + adressmacesp + "/base";
  String topic_hchp = "winky/" + adressmacesp + "/hchp";
  
  // Afficher la liste complete toutes les minutes et met à jour Jeedom
  if (millis() > MQAll + 60000 ) {
    MQAll = millis();
    sendAll();
    client.publish(topic_power.c_str(), String(PAPP).c_str(), true);
    client.publish(topic_base.c_str(), String(BASE).c_str(), true);
    client.publish(topic_hchp.c_str(), String(HCHP).c_str(), true);
  }
}
