#include "Arduino.h"
#include "time.h"
#include "stdio.h"
#include "stdarg.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"
#include "rgbled.h"

#ifndef LED_BUILTIN
// Change this pin if needed
#define LED_BUILTIN NOT_A_PIN
#else
#ifdef MOD_NEOPIXEL
// Avoid same pin led buildin and NeoPixel
#undef LED_BUILTIN
#define LED_BUILTIN NOT_A_PIN
#endif
#endif

#define CRLF "\r\n"
#define uS_TO_S_FACTOR 1000000ULL 

char * getDate() ;

#include "driver/adc.h"
#include "esp_adc_cal.h"
#define VUSB_CHANNEL ADC1_CHANNEL_1  // VUSB voltage ADC input
#define VCAP_CHANNEL ADC1_CHANNEL_4  // VCAP voltage ADC input
// divider resistor values
#define UPPER_DIVIDER 470
#define LOWER_DIVIDER 470
