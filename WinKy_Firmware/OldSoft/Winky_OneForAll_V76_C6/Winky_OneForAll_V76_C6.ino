#include <Arduino.h>
#include <AsyncTCP.h>
//#include <AsyncElegantOTA.h>
#include <PubSubClient.h>
#include <InfluxDbClient.h>
#include "WiFi.h"
#include "esp_wifi.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"
#include <ArduinoJson.h>
#include "rgbled.h"
AsyncWebServer server(80);

//Parametres leds

int first = 0;
long startloop;
long scanloop;
long looptransmis;
long waitloop;


#define VUSB_CHANNEL ADC1_CHANNEL_1  // VUSB voltage ADC input
#define VCAP_CHANNEL ADC1_CHANNEL_3  // VCAP voltage ADC input
#define VLKY_CHANNEL ADC1_CHANNEL_5  // VLinky voltage ADC input

// divider resistor values
#define UPPER_DIVIDER 470
#define LOWER_DIVIDER 470

#define CRLF "\r\n"

#define uS_TO_S_FACTOR 1000000ULL /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 30          /* Time ESP32 will go to sleep (in seconds) */
#define TIME_TO_SLEEP_MID 10

#define FORMAT_SPIFFS_IF_FAILED true

uint16_t vusb;
uint16_t vcap;
uint16_t vlky;
bool usb_connected = false;
bool firststart = 1;

int FrequenceEnvoi = 10000;

/*Parametres Wifi*/
bool wificonnectedusb = 0;
String wifi_ssid;
String wifi_password;
const char* ssid = "XKY";
const char* password = "oneforall";
WiFiClient espClient;
int midtrywificon = 150;
int nbtrywificon = 0;
int channel = 11;
String AdressMacRouteur;
char str[] = "00:00:00:00:00:00";
uint8_t bssid[6];
char* ptr;  //start and end pointer for strtol

boolean modetestwifi = 0;
boolean modetestwififirst = 1;
String IPlocalwifi = "Non connecte";

/*Parametres TIC*/
boolean linky_histstard = 0;
boolean modetestwotic = 0;

/*Parametres Validation*/
boolean okpapp = 0;
boolean okpappi = 0;
boolean okbaseprod = 0;

/*Parametres InfluxDB*/
int nbtryinfluxdbcon = 0;
boolean useinflux = 0;
String INFLUXDB_URL;
// InfluxDB 2 server or cloud API authentication token (Use: InfluxDB UI -> Load Data -> Tokens -> <select token>)
String INFLUXDB_TOKEN;
// InfluxDB 2 organization id (Use: InfluxDB UI -> Settings -> Profile -> <name under tile> )
String INFLUXDB_ORG;
// InfluxDB 2 bucket name (Use: InfluxDB UI -> Load Data -> Buckets)
String INFLUXDB_BUCKET;

/*Parametres MQTT*/
boolean usemqtt = 0;
String mqtt_server;
String mqtt_user;
String mqtt_password;
String maintopic;
PubSubClient client(espClient);
int maxtrymqttcon = 5;
int nbtrymqttcon = 0;

/*Parametres xKy*/
long MQAll;
long DeepModulateSleeptime;
//boolean useoptimwifi = 0;
String FWVersion = "V76";
String BoardVersion = "C6";
unsigned long timestartup;
unsigned long timedecode;
unsigned long timeconnexion;

/*Parametres Page WEB*/
const char* PARAM_URL = "inputUrl";
const char* PARAM_TOKEN = "inputToken";
const char* PARAM_ORG = "inputOrg";
const char* PARAM_BUCKET = "inputBucket";
const char* PARAM_WIFI_SSID = "inputWifiSsid";
const char* PARAM_WIFI_PASSWORD = "inputWifiPassword";
const char* PARAM_ACTIVE_INFLUX = "inputActiverInflux";
const char* PARAM_ACTIVE_MQTT = "inputActiverMqtt";
const char* PARAM_URL_MQTT = "inputUrlMqtt";
const char* PARAM_USER_MQTT = "inputUserMqtt";
const char* PARAM_PASSWORD_MQTT = "inputPasswordMqtt";
const char* PARAM_MAINTOPIC_MQTT = "inputMainTopicMqtt";
const char* PARAM_MODEHISTSTAND = "inputModeHistStand";
const char* PARAM_ACTIVE_OPTIMWIFI = "inputActiverOptimWifi";
const char* PARAM_FREQENVOI = "inputFrequenceEnvoi";
const char* PARAM_MODETESTWIFI = "inputModeTestWifi";
const char* PARAM_MODETESTTIC = "inputModeTestTIC";

//String struseoptimwifi;
String strfrequencenvoi;
String struseinflux;
String wifi_passwordhtmlclean;
String strusemqtt;
String maintopicmqtt;
String strlinky_histstard;
String strscanwifitemp;
String strscanwifi;

// Variables Linky historique
char HHPHC;
int ISOUSC;  // intensité souscrite
int PEJP;
int IINST;  // intensité instantanée    en A
int IINST1;
int IINST2;
int IINST3;
int ADPS;
int PAPP;               // puissance apparente      en VA
unsigned long PMAX;     // compteur Heures Creuses  en Wh
unsigned long HCHC;     // compteur Heures Creuses  en Wh
unsigned long HCHP;     // compteur Heures Pleines  en Wh
unsigned long EJPHN;    // compteur Heures Normales  en Wh
unsigned long EJPHPM;   // compteur Heures de Pointe Mobile  en Wh
unsigned long BASE;     // index BASE               en Wh
unsigned long BBRHCJB;  // compteur Heures Creuses  en Wh
unsigned long BBRHPJB;  // compteur Heures Pleines  en Wh
unsigned long BBRHCJW;  // compteur Heures Creuses  en Wh
unsigned long BBRHPJW;  // compteur Heures Pleines  en Wh
unsigned long BBRHCJR;  // compteur Heures Creuses  en Wh
unsigned long BBRHPJR;  // compteur Heures Pleines  en Wh
String DEMAIN;          // demain rouge
String PTEC;            // période tarif en cours
String ADCO;            // adresse du compteur
String OPTARIF;         // option tarifaire
int IMAX;               // intensité maxi = 90A
int IMAX1;
int IMAX2;
int IMAX3;
String MOTDETAT;  // status word

//Variables linky standard
char* ADSC;
char* VTIC;
char* DATE;
char* NGTF;
char* LTARF;
char* EAST;
char* EASF01;
char* EASF02;
char* EASF03;
char* EASF04;
char* EASF05;
char* EASF06;
char* EASF07;
char* EASF08;
char* EASF09;
char* EASF10;
char* EASD01;
char* EASD02;
char* EASD03;
char* EASD04;
char* ERQ1;
char* ERQ2;
char* ERQ3;
char* ERQ4;
char* EAIT;
char* IRMS1;
char* IRMS2;
char* IRMS3;
char* URMS1;
char* URMS2;
char* URMS3;
char* PREF;
char* PCOUP;
char* SINSTS;
char* SINSTS1;
char* SINSTS2;
char* SINSTS3;
char* SMAXSN;
char* SMAXSN1;
char* SMAXSN2;
char* SMAXSN3;
char* SMAXSN_1;
char* SMAXSN1_1;
char* SMAXSN2_1;
char* SMAXSN3_1;
char* SINSTI;
char* SMAXIN;
char* SMAXIN_1;
char* CCAIN;
char* CCAIN_1;
char* CCASN_1;
char* CCASN;
char* UMOY1;
char* UMOY2;
char* UMOY3;
char* STGE;
char* DPM1;
char* FPM1;
char* DPM2;
char* FPM2;
char* DPM3;
char* FPM3;
char* MSG1;
char* MSG2;
char* PRM;
char* RELAIS;
char* NTARF;
char* NJOURF;
char* NJOURF1;
char* PJOURF1;
char* PPOINTE;

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Wifi</b></p>
  <form action="/get" target="hidden-form">
    Nom du reseau wifi (SSID) actuel: %inputWifiSsid% <input type="text" name="inputWifiSsid"><br><br>
    Mot de passe actuel: %inputWifiPassword% <input type="text" name="inputWifiPassword">
    <p>%inputScanWifi%</p>
    <button onclick="window.location.href = 'http://192.168.4.1/';">Rescanner wifi</button>
    <p><b>Etat wifi</b></p>
      <p>Adresse ip actuelle sur votre reseau %inputIPlocalwifi%</p>
    <p><b>Configuration Linky</b></p>
    <label for="Mode">Mode actuel: %inputModeHistStand%</label>
      <select name="inputModeHistStand" id="Mode">
        <option value="Historique">Historique</option>
        <option value="Standard">Standard</option>
        <option selected hidden>%inputModeHistStand%</option>
      </select><br><br>
      Si vous changer le mode TIC, il faut rebooter le winky apres la sauvegarde de ce parametre<br>
    <p><b>Configuration Avancee</b></p>
    Ses parametres sont a regler avec prudence<br><br>
    La decimale s'ecrit avec un point (exemple 1.5 correspond a 1,5 secondes)<br><br>
    Frequence de remontee des donnees en mode USB (par defaut 10, minimum autorise 10) valeur actuelle: %inputFrequenceEnvoi% s <input type="text" name="inputFrequenceEnvoi"><br><br>
    <label for="Mode">Mode test connexion serveur uniquement sans lecture TIC: %inputModeTestTIC%</label>
      <select name="inputModeTestTIC" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputModeTestTIC%</option>
      </select><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">  
  </form><br>
  <a href="influx">Configuration InfluxDB</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="compthist">TIC Historique</a>
  <a href="comptstd">TIC Standard</a>
  <a href="update">Mise a jour Software</a>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char influx_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Server InfluxDB</b></p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Activation InfluxDB actuel: %inputActiverInflux%</label>
      <select name="inputActiverInflux" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverInflux%</option>
      </select><br><br>
    Url actuelle: %inputUrl% <input type="text" name="inputUrl"><br><br>
    Token actuel: %inputToken% <input type="text " name="inputToken"><br><br>
    Organisation actuelle: %inputOrg% <input type="text " name="inputOrg"><br><br>
    Bucket actuel: %inputBucket% <input type="text " name="inputBucket"><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">
  </form>
  <br>
  <a href="index">Configuration Generale</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="compthist">TIC Historique</a>
  <a href="comptstd">TIC Standard</a>
  <a href="update">Mise a jour Software</a>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char mqtt_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b><u>Configuration du module XKY</u></b></p>
  <p><b>Server MQTT</b></p>
  <form action="/get" target="hidden-form">
  <label for="Mode">Activation MQTT actuel: %inputActiverMqtt%</label>
      <select name="inputActiverMqtt" id="Mode">
        <option value="Active">Active</option>
        <option value="Desactive">Desactive</option>
        <option selected hidden>%inputActiverMqtt%</option>
      </select><br><br>
    Url actuelle: %inputUrlMqtt% <input type="text" name="inputUrlMqtt"><br><br>
    User actuel: %inputUserMqtt% <input type="text " name="inputUserMqtt"><br><br>
    Password actuel: %inputPasswordMqtt% <input type="text " name="inputPasswordMqtt"><br><br>
    Topic actuel: %inputMainTopicMqtt% <input type="text " name="inputMainTopicMqtt"><br><br>
    <input type="submit" value="Enregistrer les modifications" onclick="submitMessage()">
  </form>
  <br>  
  <a href="index">Configuration Generale</a>
  <a href="influx">Configuration InfluxDB</a>
  <a href="compthist">TIC Historique</a>
  <a href="comptstd">TIC Standard</a>
  <a href="update">Mise a jour Software</a>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char comptstd_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
    function rebootMessage() {
      alert("Le Module va rebooter");
      setTimeout(function(){ document.location.replace("reboot"); }, 1000);   
    }
  </script></head><body>
    <a href="index">Configuration Generale</a>
  <a href="influx">Configuration InfluxDB</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="compthist">TIC Historique</a>
  <a href="update">Mise a jour Software</a>
  <p><b>Valeurs Linky actuelles</b></p>
  <p><b>Communes</b></p>
  <form action="/get" target="hidden-form">
       <label for="Mode">ADSC actuel: %ADSC%</label><br>
       <label for="Mode">VTIC actuel: %VTIC%</label><br>
       <label for="Mode">DATE actuel: %DATE%</label><br>
       <label for="Mode">NGTF actuel: %NGTF%</label><br>
       <label for="Mode">LTARF actuel: %LTARF%</label><br>
       <label for="Mode">EAST actuel: %EAST%</label><br>
       <label for="Mode">EASF01 actuel: %EASF01%</label><br>
       <label for="Mode">EASF02 actuel: %EASF02%</label><br>
       <label for="Mode">EASF03 actuel: %EASF03%</label><br>
       <label for="Mode">EASF04 actuel: %EASF04%</label><br>
       <label for="Mode">EASF05 actuel: %EASF05%</label><br>
       <label for="Mode">EASF06 actuel: %EASF06%</label><br>
       <label for="Mode">EASF07 actuel: %EASF07%</label><br>
       <label for="Mode">EASF08 actuel: %EASF08%</label><br>
       <label for="Mode">EASF09 actuel: %EASF09%</label><br>
       <label for="Mode">EASF10 actuel: %EASF10%</label><br>
       <label for="Mode">EASD01 actuel: %EASD01%</label><br>
       <label for="Mode">EASD02 actuel: %EASD02%</label><br>
       <label for="Mode">EASD03 actuel: %EASD03%</label><br>
       <label for="Mode">EASD04 actuel: %EASD04%</label><br>
       <label for="Mode">ERQ1 actuel: %ERQ1%</label><br>
       <label for="Mode">ERQ2 actuel: %ERQ2%</label><br>
       <label for="Mode">ERQ3 actuel: %ERQ3%</label><br>
       <label for="Mode">ERQ4 actuel: %ERQ4%</label><br>
       <label for="Mode">EAIT actuel: %EAIT%</label><br>
       <label for="Mode">IRMS1 actuel: %IRMS1%</label><br>
       <label for="Mode">IRMS2 actuel: %IRMS2%</label><br>
       <label for="Mode">IRMS3 actuel: %IRMS3%</label><br>
       <label for="Mode">URMS1 actuel: %URMS1%</label><br>
       <label for="Mode">URMS2 actuel: %URMS2%</label><br>
       <label for="Mode">URMS3 actuel: %URMS3%</label><br>
       <label for="Mode">PREF actuel: %PREF%</label><br>
       <label for="Mode">PCOUP actuel: %PCOUP%</label><br>
       <label for="Mode">SINSTS actuel: %SINSTS%</label><br>
       <label for="Mode">SINSTS1 actuel: %SINSTS1%</label><br>
       <label for="Mode">SINSTS2 actuel: %SINSTS2%</label><br>
       <label for="Mode">SINSTS3 actuel: %SINSTS3%</label><br>
       <label for="Mode">SMAXSN actuel: %SMAXSN%</label><br>
       <label for="Mode">SMAXSN1 actuel: %SMAXSN1%</label><br>
       <label for="Mode">SMAXSN2 actuel: %SMAXSN2%</label><br>
       <label for="Mode">SMAXSN3 actuel: %SMAXSN3%</label><br>
       <label for="Mode">SMAXSN_1 actuel: %SMAXSN_1%</label><br>
       <label for="Mode">SMAXSN1_1 actuel: %SMAXSN1_1%</label><br>
       <label for="Mode">SMAXSN2_1 actuel: %SMAXSN2_1%</label><br>
       <label for="Mode">SMAXSN3_1 actuel: %SMAXSN3_1%</label><br>
       <label for="Mode">SINSTI actuel: %SINSTI%</label><br>
       <label for="Mode">SMAXIN actuel: %SMAXIN%</label><br>
       <label for="Mode">SMAXIN_1 actuel: %SMAXIN_1%</label><br>
       <label for="Mode">CCAIN actuel: %CCAIN%</label><br>
       <label for="Mode">CCAIN_1 actuel: %CCAIN_1%</label><br>
       <label for="Mode">CCASN_1 actuel: %CCASN_1%</label><br>
       <label for="Mode">CCASN actuel: %CCASN%</label><br>
       <label for="Mode">UMOY1 actuel: %UMOY1%</label><br>
       <label for="Mode">UMOY2 actuel: %UMOY2%</label><br>
       <label for="Mode">UMOY3 actuel: %UMOY3%</label><br>
       <label for="Mode">STGE actuel: %STGE%</label><br>
       <label for="Mode">DPM1 actuel: %DPM1%</label><br>
       <label for="Mode">FPM1 actuel: %FPM1%</label><br>
       <label for="Mode">DPM2 actuel: %DPM2%</label><br>
       <label for="Mode">FPM2 actuel: %FPM2%</label><br>
       <label for="Mode">DPM3 actuel: %DPM3%</label><br>
       <label for="Mode">FPM3 actuel: %FPM3%</label><br>
       <label for="Mode">MSG1 actuel: %MSG1%</label><br>
       <label for="Mode">MSG2 actuel: %MSG2%</label><br>
       <label for="Mode">PRM actuel: %PRM%</label><br>
       <label for="Mode">RELAIS actuel: %RELAIS%</label><br>
       <label for="Mode">NTARF actuel: %NTARF%</label><br>
       <label for="Mode">NJOURF actuel: %NJOURF%</label><br>
       <label for="Mode">NJOURF1 actuel: %NJOURF1%</label><br>
       <label for="Mode">PJOURF1 actuel: %PJOURF1%</label><br>
       <label for="Mode">PPOINTE actuel: %PPOINTE%</label><br>
  </form>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char compthist_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Les valeurs vont etre sauvegardees dans le XKY");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
    function rebootMessage() {
      alert("Le Module va rebooter");
      setTimeout(function(){ document.location.replace("reboot"); }, 1000);   
    }
  </script></head><body>
  <a href="index">Configuration Generale</a>
  <a href="influx">Configuration InfluxDB</a>
  <a href="mqtt">Configuration MQTT</a>
  <a href="comptstd">TIC Standard</a>
  <a href="update">Mise a jour Software</a>
  <p><b>Valeurs Linky actuelles</b></p>
  <p><b>Communes</b></p>
  <form action="/get" target="hidden-form">
      <label for="Mode">ADCO actuel: %ADCO%</label><br>
      <label for="Mode">OPTARIF actuel: %OPTARIF%</label><br>
      <label for="Mode">PAPP actuel: %PAPP%</label><br>
      <label for="Mode">BASE actuel: %BASE%</label><br>
      <label for="Mode">HHPHC actuel: %HHPHC%</label><br>
      <label for="Mode">ISOUSC actuel: %ISOUSC%</label><br>
      <label for="Mode">PTEC actuel: %PTEC%</label><br>
      <label for="Mode">MOTDETAT actuel: %MOTDETAT%</label><br>
  </form>
  <p><b>Valeurs speciales Heures Creuses Heures Pleines</b></p>
      <label for="Mode">HCHC actuel: %HCHC%</label><br>
      <label for="Mode">HCHP actuel: %HCHP%</label><br>
  </form>
  <p><b>Valeurs speciales Tempo</b></p>
      <label for="Mode">BBRHCJB actuel: %BBRHCJB%</label><br>
      <label for="Mode">BBRHPJB actuel: %BBRHPJB%</label><br>
      <label for="Mode">BBRHCJW actuel: %BBRHCJW%</label><br>
      <label for="Mode">BBRHPJW actuel: %BBRHPJW%</label><br>
      <label for="Mode">BBRHCJR actuel: %BBRHCJR%</label><br>
      <label for="Mode">BBRHPJR actuel: %BBRHPJR%</label><br>
      <label for="Mode">DEMAIN actuel: %DEMAIN%</label><br>
  </form>
  <p><b>Valeurs speciales EJP</b></p>
      <label for="Mode">EJPHN actuel: %EJPHN%</label><br>
      <label for="Mode">EJPHPM actuel: %EJPHPM%</label><br>
      <label for="Mode">PEJP actuel: %PEJP%</label><br>
  </form>
  <p><b>Speciales Monophase</b></p>
  <form action="/get" target="hidden-form">
      <label for="Mode">IINST actuel: %IINST%</label><br>
      <label for="Mode">IMAX actuel: %IMAX%</label><br>
      <label for="Mode">ADPS actuel: %ADPS%</label><br>
  </form>
  <p><b>Speciales Triphase</b></p>
  <form action="/get" target="hidden-form">
      <label for="Mode">IINST1 actuel: %IINST1%</label><br>
      <label for="Mode">IINST2 actuel: %IINST2%</label><br>
      <label for="Mode">IINST3 actuel: %IINST3%</label><br>
      <label for="Mode">IMAX1 actuel: %IMAX1%</label><br>
      <label for="Mode">IMAX2 actuel: %IMAX2%</label><br>
      <label for="Mode">IMAX3 actuel: %IMAX3%</label><br>
      <label for="Mode">PMAX actuel: %PMAX%</label><br>
  </form><br>
  <p>One For All version 1er detenteur Board: %BoardVersion% Version soft: %FWVersion% </p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

void notFound(AsyncWebServerRequest* request) {
  request->send(404, "text/plain", "Not found");
}

String readFile(fs::FS& fs, const char* path) {
  Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if (!file || file.isDirectory()) {
    Serial.println("- empty file or failed to open file");
    return String();
  }
  Serial.println("- read from file:");
  String fileContent;
  while (file.available()) {
    fileContent += String((char)file.read());
  }
  file.close();
  Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS& fs, const char* path, const char* message) {
  Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if (!file) {
    Serial.println("- failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
  file.close();
}

void ledwifiprogmode() {
  if ((millis() - startloop) < 499) {
    //led_color[1] = DOTSTAR_BLUE;
    led_color[1] = DOTSTAR_GREEN;
    DotStar_Show();
  } else if (((millis() - startloop) < 1000)) {
    led_color[1] = DOTSTAR_BLUE;
    DotStar_Show();
  } else {
    startloop = millis();
  }
}

void scanwifi() {
  String ssid;
  int32_t rssi;
  uint8_t encryptionType;
  uint8_t* bssid;
  int32_t channel;
  bool hidden;
  int scanResult;
  ledwifiprogmode;
  Serial.println(F("Starting WiFi scan..."));
  scanResult = WiFi.scanNetworks(/*async=*/false, /*hidden=*/true);
  ledwifiprogmode;
  if (scanResult == 0) {
    strscanwifitemp = "Pas de reseau wifi accessible";
  } else if (scanResult > 0) {
    strscanwifitemp = "<b>" + String(scanResult) + " reseaux trouves:" + "</b><br>";

    // Print unsorted scan results
    for (int8_t i = 0; i < scanResult; i++) {
      WiFi.getNetworkInfo(i, ssid, encryptionType, rssi, bssid, channel);

      //    Serial.printf(PSTR("  %02d: [CH %02d] [%02X:%02X:%02X:%02X:%02X:%02X] %ddBm %c %c %-11s %3S %s\n"), i, channel, bssid[0], bssid[1], bssid[2], bssid[3], bssid[4], bssid[5], rssi, (encryptionType == ENC_TYPE_NONE) ? ' ' : '*', hidden ? 'H' : 'V', phyMode.c_str(), wps, ssid.c_str());
      strscanwifitemp = strscanwifitemp + String(i) + ": <b>" + ssid.c_str() + "</b> " + String(rssi) + "dBm [CH " + String(channel) + "] [" + String(bssid[0]) + ":" + String(bssid[1]) + ":" + String(bssid[2]) + ":" + String(bssid[3]) + ":" + String(bssid[4]) + ":" + String(bssid[5]) + "] " + "<br>";
      ledwifiprogmode;
      yield();
    }
  } else {
    //  Serial.printf(PSTR("WiFi scan error %d"), scanResult);
    strscanwifitemp = "WiFi erreur scan";
  }
  strscanwifi = strscanwifitemp;
  WiFi.scanDelete();
}

// Replaces placeholder with stored values
String processor(const String& var) {
  //Serial.println(var);
  if (var == "inputUrl") {
    return INFLUXDB_URL;
  } else if (var == "inputToken") {
    return INFLUXDB_TOKEN;
  } else if (var == "inputOrg") {
    return INFLUXDB_ORG;
  } else if (var == "inputBucket") {
    return INFLUXDB_BUCKET;
  } else if (var == "inputWifiSsid") {
    return wifi_ssid;
  } else if (var == "inputWifiPassword") {
    return wifi_passwordhtmlclean;
  } else if (var == "inputActiverInflux") {
    return struseinflux;
  } else if (var == "inputActiverMqtt") {
    return strusemqtt;
  } else if (var == "inputUrlMqtt") {
    return mqtt_server;
  } else if (var == "inputUserMqtt") {
    return mqtt_user;
  } else if (var == "inputPasswordMqtt") {
    return mqtt_password;
  } else if (var == "inputModeHistStand") {
    return strlinky_histstard;
    //  } else if (var == "inputActiverOptimWifi") {
    //    return struseoptimwifi;
  } else if (var == "inputFrequenceEnvoi") {
    return strfrequencenvoi;
  } else if (var == "inputMainTopicMqtt") {
    return maintopicmqtt;
  } else if (var == "inputScanWifi") {
    return strscanwifi;
  } else if (var == "BoardVersion") {
    return BoardVersion;
  } else if (var == "FWVersion") {
    return FWVersion;
  } else if (var == "inputIPlocalwifi") {
    return IPlocalwifi;
  } else if (var == "inputIPlocalwifi") {
    return IPlocalwifi;
  } else if (var == "inputModeTestWifi") {
    if (modetestwifi == 0) {
      return "Desactive";
    } else {
      return "Active";
    }
  } else if (var == "inputModeTestTIC") {
    if (modetestwotic == 0) {
      return "Desactive";
    } else {
      return "Active";
    }
  } else if (var == "ADCO") {
    return ADCO;
  } else if (var == "OPTARIF") {
    return OPTARIF;
  } else if (var == "PEJP") {
    return String(PEJP);
  } else if (var == "ISOUSC") {
    return String(ISOUSC);
  } else if (var == "HCHC") {
    return String(HCHC);
  } else if (var == "HCHP") {
    return String(HCHP);
  } else if (var == "BASE") {
    return String(BASE);
  } else if (var == "EJPHN") {
    return String(EJPHN);
  } else if (var == "EJPHPM") {
    return String(EJPHPM);
  } else if (var == "BBRHCJB") {
    return String(BBRHCJB);
  } else if (var == "BBRHPJB") {
    return String(BBRHPJB);
  } else if (var == "BBRHCJW") {
    return String(BBRHCJW);
  } else if (var == "BBRHPJW") {
    return String(BBRHPJW);
  } else if (var == "BBRHCJR") {
    return String(BBRHCJR);
  } else if (var == "BBRHPJR") {
    return String(BBRHPJR);
  } else if (var == "BBRHCJR") {
    return String(BBRHCJR);
  } else if (var == "PTEC") {
    return PTEC;
  } else if (var == "DEMAIN") {
    return DEMAIN;
  } else if (var == "HHPHC") {
    return String(HHPHC);
  } else if (var == "IINST") {
    return String(IINST);
  } else if (var == "IMAX") {
    return String(IMAX);
  } else if (var == "PAPP") {
    return String(PAPP);
  } else if (var == "MOTDETAT") {
    return MOTDETAT;
  } else if (var == "IMAX1") {
    return String(IMAX1);
  } else if (var == "IMAX2") {
    return String(IMAX);
  } else if (var == "IMAX3") {
    return String(IMAX3);
  } else if (var == "IINST1") {
    return String(IINST1);
  } else if (var == "IINST2") {
    return String(IINST2);
  } else if (var == "IINST3") {
    return String(IINST3);
  } else if (var == "ADPS") {
    return String(ADPS);
  } else if (var == "PMAX") {
    return String(PMAX);
  } else if (var == "ADSC") {
    return String(ADSC);
  } else if (var == "VTIC") {
    return String(VTIC);
  } else if (var == "DATE") {
    return String(DATE);
  } else if (var == "NGTF") {
    return String(NGTF);
  } else if (var == "LTARF") {
    return String(LTARF);
  } else if (var == "EAST") {
    return String(EAST);
  } else if (var == "EASF01") {
    return String(EASF01);
  } else if (var == "EASF02") {
    return String(EASF02);
  } else if (var == "EASF03") {
    return String(EASF03);
  } else if (var == "EASF04") {
    return String(EASF04);
  } else if (var == "EASF05") {
    return String(EASF05);
  } else if (var == "EASF06") {
    return String(EASF06);
  } else if (var == "EASF07") {
    return String(EASF07);
  } else if (var == "EASF08") {
    return String(EASF08);
  } else if (var == "EASF09") {
    return String(EASF09);
  } else if (var == "EASF10") {
    return String(EASF10);
  } else if (var == "EASD01") {
    return String(EASD01);
  } else if (var == "EASD02") {
    return String(EASD02);
  } else if (var == "EASD03") {
    return String(EASD03);
  } else if (var == "EASD04") {
    return String(EASD04);
  } else if (var == "ERQ1") {
    return String(ERQ1);
  } else if (var == "ERQ2") {
    return String(ERQ2);
  } else if (var == "ERQ3") {
    return String(ERQ3);
  } else if (var == "ERQ4") {
    return String(ERQ4);
  } else if (var == "EAIT") {
    return String(EAIT);
  } else if (var == "IRMS1") {
    return String(IRMS1);
  } else if (var == "IRMS2") {
    return String(IRMS2);
  } else if (var == "IRMS3") {
    return String(IRMS3);
  } else if (var == "URMS1") {
    return String(URMS1);
  } else if (var == "URMS2") {
    return String(URMS2);
  } else if (var == "URMS3") {
    return String(URMS3);
  } else if (var == "PREF") {
    return String(PREF);
  } else if (var == "PCOUP") {
    return String(PCOUP);
  } else if (var == "SINSTS") {
    return String(SINSTS);
  } else if (var == "SINSTS1") {
    return String(SINSTS1);
  } else if (var == "SINSTS2") {
    return String(SINSTS2);
  } else if (var == "SINSTS3") {
    return String(SINSTS3);
  } else if (var == "SMAXSN") {
    return String(SMAXSN);
  } else if (var == "SMAXSN1") {
    return String(SMAXSN1);
  } else if (var == "SMAXSN2") {
    return String(SMAXSN2);
  } else if (var == "SMAXSN3") {
    return String(SMAXSN3);
  } else if (var == "SMAXSN_1") {
    return String(SMAXSN_1);
  } else if (var == "SMAXSN1_1") {
    return String(SMAXSN1_1);
  } else if (var == "SMAXSN2_1") {
    return String(SMAXSN2_1);
  } else if (var == "SMAXSN3_1") {
    return String(SMAXSN3_1);
  } else if (var == "SINSTI") {
    return String(SINSTI);
  } else if (var == "SMAXIN") {
    return String(SMAXIN);
  } else if (var == "SMAXIN_1") {
    return String(SMAXIN_1);
  } else if (var == "CCAIN") {
    return String(CCAIN);
  } else if (var == "CCAIN_1") {
    return String(CCAIN_1);
  } else if (var == "CCASN_1") {
    return String(CCASN_1);
  } else if (var == "CCASN") {
    return String(CCASN);
  } else if (var == "UMOY1") {
    return String(UMOY1);
  } else if (var == "UMOY2") {
    return String(UMOY2);
  } else if (var == "UMOY3") {
    return String(UMOY3);
  } else if (var == "STGE") {
    return String(STGE);
  } else if (var == "DPM1") {
    return String(DPM1);
  } else if (var == "FPM1") {
    return String(FPM1);
  } else if (var == "DPM2") {
    return String(DPM2);
  } else if (var == "FPM2") {
    return String(FPM2);
  } else if (var == "DPM3") {
    return String(DPM3);
  } else if (var == "FPM3") {
    return String(FPM3);
  } else if (var == "MSG1") {
    return String(MSG1);
  } else if (var == "MSG2") {
    return String(MSG2);
  } else if (var == "PRM") {
    return String(PRM);
  } else if (var == "RELAIS") {
    return String(RELAIS);
  } else if (var == "NTARF") {
    return String(NTARF);
  } else if (var == "NJOURF") {
    return String(NJOURF);
  } else if (var == "NJOURF1") {
    return String(NJOURF1);
  } else if (var == "PJOURF1") {
    return String(PJOURF1);
  } else if (var == "PPOINTE") {
    return String(PPOINTE);
  }
  return String();
}

bool GetVoltages(uint16_t* _vcap, uint16_t* _vusb, uint16_t* _vlky) {
  static unsigned long nextVoltage = millis() + 1000;

  // only check voltage every 1 second
  if (nextVoltage - millis() > 0) {
    uint32_t raw;
    nextVoltage = millis() + 1000;

    raw = analogReadMilliVolts(3);
    *_vcap = raw * (LOWER_DIVIDER + UPPER_DIVIDER) / LOWER_DIVIDER;

    raw = analogReadMilliVolts(1);
    *_vusb = raw * (LOWER_DIVIDER + UPPER_DIVIDER) / LOWER_DIVIDER;

    raw = analogReadMilliVolts(5);
    *_vlky = raw * (LOWER_DIVIDER + UPPER_DIVIDER) / LOWER_DIVIDER;

    return true;
    //SerialDebug.printf_P(PSTR(" VIN:%dmV\r\n"), mv);
  }
  return false;
}

void CheckLowBat() {
  GetVoltages(&vcap, &vusb, &vlky);
  if (vcap <= 3750) {
    led_color[1] = DOTSTAR_RED;
    DotStar_Show();
    esp_wifi_stop();
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP_MID * uS_TO_S_FACTOR);
    esp_deep_sleep_start();
  }
}

String macToStr(const uint8_t* mac) {
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

String composeClientID() {
  uint8_t mac[6];
  WiFi.macAddress(mac);
  String clientId;
  clientId += "xky-";
  clientId += macToStr(mac);
  return clientId;
}

//Connexion au reseau WiFi
void setup_wifi_Test() {
  delay(10);
  nbtrywificon = 0;
  int nbretrymax = 50;
  int nbretry = 0;
  WiFi.begin(wifi_ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    nbretry = nbretry + 1;
    delay(100);
    Serial.print(".");
    if (nbretry > nbretrymax) {
      break;
    }
  }
  if (nbretrymax > nbretry) {
    wificonnectedusb = 1;
  }
  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());
  if (wificonnectedusb) {
    IPlocalwifi = WiFi.localIP().toString();
  } else {
    IPlocalwifi = "Non connecte verifiez vos identifiants wifi";
    WiFi.disconnect();
  }
  Serial.printf("\nWiFi channel: %d\n", WiFi.channel());
  Serial.printf("WiFi BSSID: %s\n", WiFi.BSSIDstr().c_str());
}

//Connexion au reseau WiFi
void setup_wifi() {
  delay(10);
  nbtrywificon = 0;
  WiFi.begin(wifi_ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
    nbtrywificon = nbtrywificon + 1;
    if (nbtrywificon >= 10) {
      CheckLowBat();
      nbtrywificon = 0;
    }
  }
  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());
  IPlocalwifi = WiFi.localIP().toString();
  Serial.printf("\nWiFi channel: %d\n", WiFi.channel());
  Serial.printf("WiFi BSSID: %s\n", WiFi.BSSIDstr().c_str());
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'a obtenir une reconnexion
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    String clientnumber = composeClientID();
    client.setBufferSize(2048);
    if (client.connect(clientnumber.c_str(), mqtt_user.c_str(), mqtt_password.c_str())) {
      Serial.println("OK");
    } else {
      Serial.print("KO, erreur : ");
      Serial.print(client.state());
      Serial.println(" On attend 1 secondes avant de recommencer");
      delay(1000);
      nbtrymqttcon = nbtrymqttcon + 1;
      if (nbtrymqttcon == maxtrymqttcon) {
        CheckLowBat();
        nbtrymqttcon = 0;
      }
    }
  }
}

void ReadMemory() {
  /*Lecture des variables stockees en memoire TIC*/
  strlinky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt");
  linky_histstard = strlinky_histstard == "Standard";
  modetestwotic = readFile(SPIFFS, "/inputActiverModetesttic.txt") == "Active";
  FrequenceEnvoi = 1000 * ((readFile(SPIFFS, "/inputFrequenceEnvoi.txt")).toFloat());

  /*Lecture des variables stockees en memoire Internes*/
  strfrequencenvoi = readFile(SPIFFS, "/inputFrequenceEnvoi.txt");
  midtrywificon = 10 * ((readFile(SPIFFS, "/inputFrequenceEnvoi.txt")).toInt());

  /*Lecture des variables stockees en memoire WiFi*/
  wifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
  wifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
  wifi_passwordhtmlclean = wifi_password;
  wifi_passwordhtmlclean.replace("%", "&#37;");
  channel = readFile(SPIFFS, "/inputChannel.txt").toInt();

  /*Lecture des variables stockees en memoire INFLUXDB*/
  struseinflux = readFile(SPIFFS, "/inputActiverInflux.txt");
  useinflux = readFile(SPIFFS, "/inputActiverInflux.txt") == "Active";
  INFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
  INFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
  INFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
  INFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");

  /*Lecture des variables stockees en memoire MQTT*/
  strusemqtt = readFile(SPIFFS, "/inputActiverMqtt.txt");
  usemqtt = strusemqtt == "Active";
  mqtt_server = readFile(SPIFFS, "/inputUrlMqtt.txt");
  mqtt_user = readFile(SPIFFS, "/inputUserMqtt.txt");
  mqtt_password = readFile(SPIFFS, "/inputPasswordMqtt.txt");
  maintopicmqtt = readFile(SPIFFS, "/inputMainTopicMqtt.txt");
}


#include "linky.h";

void setup() {
  /*Arret du module Wifi*/
  esp_wifi_stop();
  delay(100);

  /*Mesure des diverses tensions*/
  if (GetVoltages(&vcap, &vusb, &vlky)) {
    if (vusb > 4800) {
      usb_connected = true;
    }
  }

  if (usb_connected) {
    /*Initialisation de la communication*/
    Serial.begin(9600);
    if (!rmtInit(BUILTIN_RGBLED_PIN, RMT_TX_MODE, RMT_MEM_NUM_BLOCKS_1, 10000000)) {
      Serial.println("init sender failed\n");
    }
    Serial.println("real tick set to: 100ns");
    if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)) {
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }

    /*Initialisation du wifi en mode */
    WiFi.mode(WIFI_AP_STA);
    WiFi.softAP(ssid, password);
    led_color[0] = DOTSTAR_GREEN;
    led_color[1] = DOTSTAR_GREEN;
    DotStar_Show();

    ReadMemory();

    server.on("/", HTTP_GET, [](AsyncWebServerRequest* request) {
      request->send_P(200, "text/html", index_html, processor);
    });

    server.on("/index", HTTP_GET, [](AsyncWebServerRequest* request) {
      request->send_P(200, "text/html", index_html, processor);
    });

    server.on("/compthist", HTTP_GET, [](AsyncWebServerRequest* request) {
      request->send_P(200, "text/html", compthist_html, processor);
    });

    server.on("/comptstd", HTTP_GET, [](AsyncWebServerRequest* request) {
      request->send_P(200, "text/html", comptstd_html, processor);
    });

    server.on("/influx", HTTP_GET, [](AsyncWebServerRequest* request) {
      request->send_P(200, "text/html", influx_html, processor);
    });

    server.on("/mqtt", HTTP_GET, [](AsyncWebServerRequest* request) {
      request->send_P(200, "text/html", mqtt_html, processor);
    });

    // Send a GET request to <ESP_IP>/get?inputUrl=<inputMessage>
    server.on("/get", HTTP_GET, [](AsyncWebServerRequest* request) {
      String inputMessage;
      // GET inputUrl value on <ESP_IP>/get?inputUrl=<inputMessage>
      if (request->hasParam(PARAM_URL)) {
        inputMessage = request->getParam(PARAM_URL)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputUrl.txt", inputMessage.c_str());
          INFLUXDB_URL = inputMessage.c_str();
        }
      }
      // GET inputToken value on <ESP_IP>/get?inputToken=<inputMessage>
      if (request->hasParam(PARAM_TOKEN)) {
        inputMessage = request->getParam(PARAM_TOKEN)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputToken.txt", inputMessage.c_str());
          INFLUXDB_TOKEN = inputMessage.c_str();
        }
      }
      // GET inputOrg value on <ESP_IP>/get?inputOrg=<inputMessage>
      if (request->hasParam(PARAM_ORG)) {
        inputMessage = request->getParam(PARAM_ORG)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputOrg.txt", inputMessage.c_str());
          INFLUXDB_ORG = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_BUCKET)) {
        inputMessage = request->getParam(PARAM_BUCKET)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputBucket.txt", inputMessage.c_str());
          INFLUXDB_BUCKET = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_WIFI_SSID)) {
        inputMessage = request->getParam(PARAM_WIFI_SSID)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputWifiSsid.txt", inputMessage.c_str());
          wifi_ssid = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_WIFI_PASSWORD)) {
        inputMessage = request->getParam(PARAM_WIFI_PASSWORD)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputWifiPassword.txt", inputMessage.c_str());
          wifi_password = inputMessage.c_str();
          wifi_passwordhtmlclean = wifi_password;
          wifi_passwordhtmlclean.replace("%", "&#37;");
        }
      }
      if (request->hasParam(PARAM_ACTIVE_INFLUX)) {
        inputMessage = request->getParam(PARAM_ACTIVE_INFLUX)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputActiverInflux.txt", inputMessage.c_str());
          struseinflux = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_ACTIVE_MQTT)) {
        inputMessage = request->getParam(PARAM_ACTIVE_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputActiverMqtt.txt", inputMessage.c_str());
          strusemqtt = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_URL_MQTT)) {
        inputMessage = request->getParam(PARAM_URL_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputUrlMqtt.txt", inputMessage.c_str());
          mqtt_server = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_USER_MQTT)) {
        inputMessage = request->getParam(PARAM_USER_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputUserMqtt.txt", inputMessage.c_str());
          mqtt_user = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_PASSWORD_MQTT)) {
        inputMessage = request->getParam(PARAM_PASSWORD_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputPasswordMqtt.txt", inputMessage.c_str());
          mqtt_password = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_MODEHISTSTAND)) {
        inputMessage = request->getParam(PARAM_MODEHISTSTAND)->value();
        writeFile(SPIFFS, "/inputModeHistStand.txt", inputMessage.c_str());
        strlinky_histstard = inputMessage.c_str();
      }
      if (request->hasParam(PARAM_FREQENVOI)) {
        inputMessage = request->getParam(PARAM_FREQENVOI)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputFrequenceEnvoi.txt", inputMessage.c_str());
          strfrequencenvoi = inputMessage.c_str();
          FrequenceEnvoi = 1000 * strfrequencenvoi.toFloat();
        }
      }
      if (request->hasParam(PARAM_MAINTOPIC_MQTT)) {
        inputMessage = request->getParam(PARAM_MAINTOPIC_MQTT)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputMainTopicMqtt.txt", inputMessage.c_str());
          maintopicmqtt = inputMessage.c_str();
        }
      }
      if (request->hasParam(PARAM_MODETESTWIFI)) {
        inputMessage = request->getParam(PARAM_MODETESTWIFI)->value();
        modetestwifi = inputMessage == "Active";
      }
      if (request->hasParam(PARAM_MODETESTTIC)) {
        inputMessage = request->getParam(PARAM_MODETESTTIC)->value();
        if (inputMessage != "") {
          writeFile(SPIFFS, "/inputActiverModetesttic.txt", inputMessage.c_str());
          modetestwotic = inputMessage == "Active";
        }
      }
      Serial.println(inputMessage);
      request->send(200, "text/text", inputMessage);
    });
    server.onNotFound(notFound);
    server.begin();

    if (linky_histstard == 1) {
      linky_setup();
    }
    startloop = millis();
    scanloop = millis();
    looptransmis = millis();
    scanwifi();
  } else {
    esp_sleep_wakeup_cause_t wakeup_reason;
    wakeup_reason = esp_sleep_get_wakeup_cause();
    if (wakeup_reason == 0) {
      esp_wifi_stop();
      esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
      esp_deep_sleep_start();
    } else if (vcap <= 4750) {
      esp_wifi_stop();
      esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP_MID * uS_TO_S_FACTOR);
      esp_deep_sleep_start();
    }
    if (!rmtInit(BUILTIN_RGBLED_PIN, RMT_TX_MODE, RMT_MEM_NUM_BLOCKS_1, 10000000)) {
      Serial.println("init sender failed\n");
    }
    Serial.println("real tick set to: 100ns");
    if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)) {
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }

    led_color[0] = DOTSTAR_VIOLET;
    //led_color[1] = DOTSTAR_GREEN;
    DotStar_Show();

    ReadMemory();

    if (linky_histstard == 1) {
      linky_setup();
    }
  }
  timestartup = millis();
}

void loop() {
  if (usb_connected) {
    if (linky_histstard == 0) {
      linky_hist_test_loop();
    } else {
      linky_test_loop();
    }
  } else {
    if (linky_histstard == 0) {
      linky_hist_loop();
    } else {
      linky_loop();
    }
  }
}
