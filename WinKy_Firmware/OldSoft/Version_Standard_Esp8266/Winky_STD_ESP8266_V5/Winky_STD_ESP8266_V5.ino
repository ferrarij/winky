#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
//#include <WiFi.h>

#define wifi_ssid "linksys"
#define wifi_password "G2ElabMonitoringHabitat"

#define mqtt_server "192.168.1.114"
#define mqtt_user "guest"     //s'il a été configuré sur Mosquitto
#define mqtt_password "guest" //idem

#define RXD2   16             // linky rx  = Serial2 Gpio 16 
#define TXD2   17

#include "linky.h"

//Buffer qui permet de décoder les messages MQTT reçus
char message_buff[100];

long lastMsg = 0;   //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;  //Affiche sur la console si True

uint32_t resetCount = 0;  // keeps track of the number of Deep Sleep tests / resets

/* ======================================================================
  Function: Setup
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void setup() { 
  Serial.begin(9600);     //  sortie moniteur 
  delay(500);
  String resetCause = ESP.getResetReason();
  Serial.println(resetCause);
//  resetCount = 0;
  if ((resetCause == "External System") || (resetCause == "Power On")) {
    ESP.deepSleep(90000000, WAKE_RF_DISABLED); //100ms
//    Serial.println(F("I'm awake and starting the Low Power tests"));
  }
/*
  // Read previous resets (Deep Sleeps) from RTC memory, if any
  uint32_t crcOfData = crc32((uint8_t*) &nv->rtcData.rstCount, sizeof(nv->rtcData.rstCount));
  if ((crcOfData = nv->rtcData.crc32) && (resetCause == "Deep-Sleep Wake")) {
    resetCount = nv->rtcData.rstCount;  // read the previous reset count
    resetCount++;
  }
  nv->rtcData.rstCount = resetCount; // update the reset count & CRC
  updateRTCcrc();
*/
/*  
  Serial.begin(9600);     //  sortie moniteur              
//  delay(500);
  Serial.println(" ");
  Serial.println(F("Boot: version Linky_standart 9600"));
  Serial.print(ESP.getFullVersion()); 
   
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  delay(60000);
  WiFi.forceSleepWake();
  delay(1);
*/



//  delay(1000);
//  cm1106_i2c.read_serial_number();
//  delay(1000);
//  cm1106_i2c.check_sw_version();
//  delay(1000);
  
  linky_setup();
}


//--------------------------------------- LOOP -------------------------------
void loop() {
  
            linky_loop();
}
//-------------------------- fin du programme 
