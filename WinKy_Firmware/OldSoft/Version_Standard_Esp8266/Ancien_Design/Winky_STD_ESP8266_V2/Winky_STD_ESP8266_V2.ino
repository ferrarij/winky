#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
//#include <WiFi.h>

#define wifi_ssid "linksys"
#define wifi_password "G2ElabMonitoringHabitat"

#define mqtt_server "192.168.1.114"
#define mqtt_user "guest"     //s'il a été configuré sur Mosquitto
#define mqtt_password "guest" //idem

#define RXD2   16             // linky rx  = Serial2 Gpio 16 
#define TXD2   17

#include "linky.h"

//Buffer qui permet de décoder les messages MQTT reçus
char message_buff[100];

long lastMsg = 0;   //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;  //Affiche sur la console si True

/* ======================================================================
  Function: Setup
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void setup() { 
  
  Serial.begin(9600);     //  sortie moniteur              
  delay(500);
  Serial.println(" ");
  Serial.println(F("Boot: version Linky_standart 9600"));
  Serial.print(ESP.getFullVersion()); 
   
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  delay(10000);
  WiFi.forceSleepWake();
  delay(1);
  WiFi.mode(WIFI_STA);
  delay(1);
//  Serial.println(F("Ligne 47"));
  
//  //cm1106_i2c.begin();
//  //On se connecte au réseau wifi
   
  delay(1000);
  Serial.println();
  Serial.print("Connexion a ");
  Serial.println(wifi_ssid);
//  WiFi.setOutputPower(20.5);
  WiFi.mode(WIFI_STA); //Wifi en mode station pour échange de requête
  WiFi.begin(wifi_ssid, wifi_password);
  
  nbtrywificon = 0;
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    nbtrywificon = nbtrywificon+1;
    if(nbtrywificon == maxtrywificon){
      ESP.deepSleep(30000000);
    }
  }
 
  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.println(WiFi.localIP());
//  String clientId = composeClientID() ;
//  #define power_topic "winky/" + composeClientID() + "/power"  //Topic co2


  client.setServer(mqtt_server, 1883);    //Configuration de la connexion au serveur MQTT

  delay(1000);
//  cm1106_i2c.read_serial_number();
  delay(1000);
//  cm1106_i2c.check_sw_version();
  delay(1000);
  
  linky_setup();
}


//--------------------------------------- LOOP -------------------------------
void loop() {
  
            linky_loop();
}
//-------------------------- fin du programme 
