Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

# ESP8266 Pinout

![ESP8266_Dev_board_Pinout](/uploads/6c51b0da7e28fdc75017d936dbb2b83f/ESP8266_Dev_board_Pinout.png)

# Wireing

|ESP8266|---|Winky|
|:-:|:-:|:-:|
|GPIO3|---|RX|
|GND|---|GND|
|3V3|---|3V3|  

![Schéma_de_câblage_ESP8266](/uploads/7ed5dcfff578d0d8a797bb792ad2f48a/Schéma_de_câblage_ESP8266.png)
![Photo_de_câblage_ESP8266](/uploads/0dbfd2187770abf162cb907bb050ca40/Photo_de_câblage_ESP8266.png)

# Original code

We use the code of [@6galou][1] as base and the historial version of the [Winky][2]

[1] : https://github.com/6galou/TIC-Linky-mode-standart
[2] : https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/winky/-/tree/main/WinKy_Firmware/WinKy_Firmware_V5


![CNRS-logo][]
![G2elab-logo][]
![UGA-logo][]
![GINP-logo][]


[GINP-logo]: https://www.grenoble-inp.fr/uas/alias1/LOGO/Grenoble+INP+-+%C3%89tablissement+%28couleur%2C+RVB%2C+120px%29.png
[CNRS-logo]: https://www.cnrs.fr/themes/custom/cnrs/logo.svg
[G2elab-logo]: https://g2elab.grenoble-inp.fr/uas/alias31/LOGO/G2Elab-logo.jpg
[UGA-logo]:https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1D0TxrJin49KlU9Oo263OgjsanU9vKQZhKJ9LZsMjxxggnCEKO7NDsN-rW6CEXGJbLEE&usqp=CAU
