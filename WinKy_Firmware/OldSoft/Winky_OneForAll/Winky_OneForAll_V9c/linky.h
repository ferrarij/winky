// **********************************************************************************
// Arduino Teleinfo sample, display information on teleinfo values received
// **********************************************************************************
// Creative Commons Attrib Share-Alike License
// You are free to use/extend this library but please abide with the CC-BY-SA license:
// http://creativecommons.org/licenses/by-sa/4.0/
//
// for detailled explanation of this library see dedicated article
// https://hallard.me/libteleinfo/
//
// For any explanation about teleinfo or use, see my blog
// https://hallard.me/category/tinfo
//
// connect Teleinfo RXD pin To Arduin D3
// see schematic here https://hallard.me/demystifier-la-teleinfo/
// and dedicated article here
//
// Written by Charles-Henri Hallard (https://hallard.me)
//
// History : V1.00 2015-06-14 - First release
//
// All text above must be included in any redistribution.
//
// **********************************************************************************
//
// adaptation pour LINKY mode Standart sur ESP32

#include "Teleinfo.h"
#include "Teleinfo_cpp.h"

TInfo    tinfo; // Teleinfo object
ValueList * me_fist;
bool  ok_me_fist = false;

char * PAPPSTAR;
char * EAST;
char * HCHPSTAR;
char * HCHCSTAR;
char * EAIT;

/* ======================================================================
  Function: printUptime
  Purpose : print pseudo uptime value
  Input   : -
  Output  : -
  Comments: compteur de secondes basique sans controle de dépassement
          En plus SoftwareSerial rend le compteur de millis() totalement
          A la rue, donc la precision de ce compteur de seconde n'est
          pas fiable du tout, dont acte !!!
  ====================================================================== */
void printUptime(void)
{
  Serial.print(millis() / 1000);
  Serial.print(F("s\t"));
}

/* ======================================================================
  Function: DataCallback
  Purpose : callback when we detected new or modified data received
  Input   : linked list pointer on the concerned data
          current flags value
  Output  : -
  Comments: -
  ====================================================================== */
void DataCallback(ValueList * me, uint8_t  flags)
{
  // Show our not accurate second counter
  //printUptime();

  if (flags & TINFO_FLAGS_ADDED || flags & TINFO_FLAGS_UPDATED)
    if  ( !ok_me_fist) {
      ok_me_fist = true;
      me_fist = me;
    }

  // Display values
  Serial.print("==> ");
  Serial.print(me->name);
  if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
  Serial.print(F("\t")) ;
  if ( me->value[0] != '*') {
    Serial.print(F(" = ")) ;
    Serial.print(me->value) ;
  }
  if ( me->date[0] != '*' ) {
    Serial.print(" : ");
    Serial.print(me->date);
  }
  if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') 
  && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'S')) {

    PAPPSTAR = me->value;
    Serial.print("PAPPSTAR:");
    Serial.println(PAPPSTAR);

  }
  if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') 
  && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'I')) {

    PAPPSTAR = '-' + me->value;
    Serial.print("PAPPSTAR:");
    Serial.println(PAPPSTAR);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') 
  && (me->name[3] == 'T')) {

    EAST = me->value;
    Serial.print("EAST:");
    Serial.println(EAST);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'I') 
  && (me->name[3] == 'T')) {
    EAIT = me->value;
    Serial.print("EAIT:");
    Serial.println(EAIT);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') 
  && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '2')) {

    HCHPSTAR = me->value;
    Serial.print("HCHPSTAR:");
    Serial.println(HCHPSTAR);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') 
  && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '1')) {

    HCHCSTAR = me->value;
    Serial.print("HCHCSTAR:");
    Serial.println(HCHCSTAR);

  }
  Serial.println("");

}

/* ======================================================================
  Function: sendJSON
  Purpose : dump teleinfo values on serial
  Input   : linked list pointer on the concerned data
          true to dump all values, false for only modified ones
  Output  : -
  Comments: -
  ====================================================================== */
void sendAll()
{
  ValueList * me;
  me = me_fist;                                   // reprendre le 1er de la chaine
  // Got at least one ?
  if (me) {

    Serial.print(F("\"all\":"));
    Serial.print(me->name) ;
    Serial.print(F("\t")) ;
    if ( me->value[0] != '*') {
      Serial.print(F(" = ")) ;
      Serial.print(me->value) ;
    }
    if ( me->date[0] != '*') {
      Serial.print(" : ");
      Serial.print(me->date);
    }
    Serial.println("");

    // Loop thru the node
    while (me->next) {

      // go to next node
      me = me->next;                                // suivant
      //sprintf(msg, "%s=%s", me->name, me->value );
      //Mqtt_emettre(me->name, me->value);

      Serial.print(F("\"all\":"));
      Serial.print(me->name) ;
      //if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
      Serial.print(F("\t")) ;
      if ( me->value[0] != '*') {
        Serial.print(F(" = ")) ;
        Serial.print(me->value) ;
      }
      if (me->date[0] != '*') {
        Serial.print(" : ");
        Serial.print(me->date);
      }
      Serial.println("");

    }
  }
}

/* ======================================================================
  Function: setup
  Purpose : Setup I/O and other one time startup stuff
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void linky_setup()
{

  Serial.println(F("======== Setup Teleinfo Linky ======="));
  delay(200);

  Serial.begin(9600);
  // Init teleinfo
  tinfo.init();

  // Attacher les callback dont nous avons besoin
  // pour cette demo, ici attach data
  tinfo.attachData(DataCallback);

}

/* ======================================================================
  Function: loop
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */

void linky_loop() {
  // Teleinformation processing
  if ( Serial.available() ) {
    /*
      char inByte = Serial2.read();
      Serial.print(inByte);
      Serial.print(".");
      Serial.print(inByte,HEX);
      Serial.print(";");
    */
    tinfo.process(Serial.read());
  }

  String adressmacesp = composeClientID();
  
  if (atoi(String(PAPPSTAR).c_str())!= 0 && atoi(String(EAST).c_str())!= 0 && (atoi(String(EAIT).c_str())!= 0 || modeprod == 0)) {
    MQAll = millis();
    WiFi.mode(WIFI_STA);
    delay(1);
    setup_wifi();           //On se connecte au réseau wifi
    if (useinflux ==1){
      InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
      // Data point
      Point sensor("xKy");
      sensor.addTag("MacAdress", adressmacesp);
      clientinflux.setInsecure();
      // Check server connection
      if (clientinflux.validateConnection()) {
        Serial.print("Connected to InfluxDB: ");
        Serial.println(clientinflux.getServerUrl());
      } else {
        Serial.print("InfluxDB connection failed: ");
        Serial.println(clientinflux.getLastErrorMessage());
        }
      // Store measured value into point
      sensor.clearFields();
      // Report RSSI of currently connected network
      sensor.addField("PAPP", atoi(PAPPSTAR));
      sensor.addField("EAST", atoi(EAST));
      sensor.addField("EAIT", atoi(EAIT));
      sensor.addField("RSSI", WiFi.RSSI());
      clientinflux.writePoint(sensor);
      }
      if (usemqtt==1){
        client.setServer(mqtt_server.c_str(), 1883);    //Configuration de la connexion au serveur MQTT
        if (!client.connected()) {
          reconnect();
        }
        client.loop();
/*    String adressmacesp = composeClientID();    */
        String topic_power = "winky/" + adressmacesp + "/power";
        String topic_base = "winky/" + adressmacesp + "/base";
//    String topic_hchc = "winky/" + adressmacesp + "/hchc";
//    String topic_hchp = "winky/" + adressmacesp + "/hchp";    
        client.publish(topic_power.c_str(), String(PAPP).c_str(), true);
        client.publish(topic_base.c_str(), String(BASE).c_str(), true);
//    client.publish(topic_hchc.c_str(), String(HCHC).c_str(), true);
//    client.publish(topic_hchp.c_str(), String(HCHP).c_str(), true);
      }
    delay(1000);
    WiFi.disconnect();
    delay(500);
    DeepModulateSleeptime = (millis()-MQAll)*1000;
    ESP.deepSleep(DeepModulateSleeptime, WAKE_RF_DISABLED); //100ms  
   // ESP.deepSleep(30000000, WAKE_RF_DISABLED); //100ms
  }
}
