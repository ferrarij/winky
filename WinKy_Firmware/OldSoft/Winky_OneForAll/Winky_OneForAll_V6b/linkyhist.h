char HHPHC;
int ISOUSC;               // intensité souscrite
int IINST;                // intensité instantanée    en A
int PAPP;                 // puissance apparente      en VA
int PEJP;
unsigned long HCHC;       // compteur Heures Creuses  en W
unsigned long HCHP;       // compteur Heures Pleines  en W
unsigned long BASE;       // index BASE               en W
unsigned long EJPHN;       // compteur Heures Normales  en Wh
unsigned long EJPHPM;       // compteur Heures de Pointe Mobile  en Wh
unsigned long BBRHCJB;       // compteur Heures Creuses  en W
unsigned long BBRHPJB;       // compteur Heures Pleines  en W
unsigned long BBRHCJW;       // compteur Heures Creuses  en W
unsigned long BBRHPJW;       // compteur Heures Pleines  en W
unsigned long BBRHCJR;       // compteur Heures Creuses  en W
unsigned long BBRHPJR;       // compteur Heures Pleines  en W
String DEMAIN;              // demain rouge
String PTEC;              // période tarif en cours
String ADCO;              // adresse du compteur
String OPTARIF;           // option tarifaire
int IMAX;                 // intensité maxi = 90A
String MOTDETAT;          // status word
boolean teleInfoReceived;
char msg[50];

char chksum(char *buff, uint8_t len);
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber);

// ---------------------------------------------- //
//        Basic constructor for LoKyTIC           //
void TeleInfo() {
  // variables initializations
  ADCO = "000000000000";
  OPTARIF = "----";
  PEJP = 0;
  ISOUSC = 0;
  HCHC = 0L;
  HCHP = 0L;
  BASE = 0L;
  EJPHN = 0L;
  EJPHPM = 0L;
  BBRHCJB = 0L;
  BBRHPJB = 0L;
  BBRHCJW = 0L;
  BBRHPJW = 0L;
  BBRHCJR = 0L;
  BBRHPJR = 0L;
  PTEC = "----";
  DEMAIN = "----";
  HHPHC = '-';
  IINST = 0;
  IMAX = 0;
  PAPP = 0;
  MOTDETAT = "------";
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//           TIC frame capture from Linky         //
boolean readTeleInfo()  {
  //boolean readTeleInfo(bool TIC_state)  {
#define startFrame  0x02
#define endFrame    0x03
#define startLine   0x0A
#define endLine     0x0D
#define maxFrameLen 280

  int comptChar = 0; // variable de comptage des caractères reçus
  char charIn = 0;  // variable de mémorisation du caractère courant en réception
  char bufferTeleinfo[21] = "";
  int bufferLen = 0;
  int checkSum;
  int sequenceNumnber = 0;    // number of information group

  //--- wait for starting frame character
  while (charIn != startFrame)
  { // "Start Text" STX (002 h) is the beginning of the frame
    if (Serial.available())
      charIn = Serial.read() & 0x7F; // Serial.read() vide buffer au fur et à mesure
  } // fin while (tant que) pas caractère 0x02
  //--- wait for the ending frame character
  while (charIn != endFrame)
  { // tant que des octets sont disponibles en lecture : on lit les caractères
    if (Serial.available()) {
      charIn = Serial.read() & 0x7F;
      // incrémente le compteur de caractère reçus
      comptChar++;
      if (charIn == startLine)  bufferLen = 0;
      bufferTeleinfo[bufferLen] = charIn;
      // on utilise une limite max pour éviter String trop long en cas erreur réception
      // ajoute le caractère reçu au String pour les N premiers caractères
      if (charIn == endLine)  {
        checkSum = bufferTeleinfo[bufferLen - 1];
        if (chksum(bufferTeleinfo, bufferLen) == checkSum)  {// we clear the 1st character
          strncpy(&bufferTeleinfo[0], &bufferTeleinfo[1], bufferLen - 3);
          bufferTeleinfo[bufferLen - 3] = 0x00;
          sequenceNumnber++;
          if (! handleBuffer(bufferTeleinfo, sequenceNumnber))  {
            // Serial.println(F("Sequence error ..."));
            return false;
          }
        }
        else  {
          // Serial.println(F("Checksum error!"));
          return false;
        }
      }
      else
        bufferLen++;
    }
    if (comptChar > maxFrameLen)  {
      // Serial.println(F("Overflow error ..."));
      return false;
    }
  }
  return true;
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//   Update new values from TIC for the next TX   //
void updateParameters() {
  Serial.begin(1200);  // Important!!! -> RESTART LoKyTIC
  teleInfoReceived = readTeleInfo();
  Serial.end(); // Important!!! -> STOP LoKyTIC to send packet.
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//               TIC frame parsing                //
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber) {
  // create a pointer to the first char after the space
  char* resultString = strchr(bufferTeleinfo, ' ') + 1;
  boolean sequenceIsOK;
  switch (sequenceNumnber) {
    case 1:
      if (sequenceIsOK = bufferTeleinfo[0] == 'A')  ADCO = String(resultString);
      break;
    case 2:
      if (sequenceIsOK = bufferTeleinfo[0] == 'O')  OPTARIF = String(resultString);
      break;
    case 3:
      if (sequenceIsOK = bufferTeleinfo[1] == 'S')  ISOUSC = atol(resultString);
      break;

    case 4:
      if (sequenceIsOK = bufferTeleinfo[3] == 'C') {
#define Linky_HCHP true
#ifdef Linky_HCHP
        HCHC = atol(resultString);
#endif
      }
      else if (sequenceIsOK = (bufferTeleinfo[0] == 'B' && bufferTeleinfo[1] == 'A')) {
#define Linky_BASE true
#ifdef Linky_BASE
        BASE = atol(resultString);
#endif
      }
      else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'B')) {
        BBRHCJB = atol(resultString);
      }
      break;

    case 5:
      if (sequenceIsOK = bufferTeleinfo[3] == 'P') {
#define Linky_HCHP true
#ifdef Linky_HCHP
        HCHP = atol(resultString);
#endif
      }
      else if (sequenceIsOK = bufferTeleinfo[1] == 'T') {
#define Linky_BASE true
#ifdef Linky_BASE
        PTEC = String(resultString);
#endif
      }
      else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'B')) {
        BBRHPJB = atol(resultString);
      }
      break;

    case 6:
      if (sequenceIsOK = bufferTeleinfo[1] == 'T') {
#define Linky_HCHP true
#ifdef Linky_HCHP
        PTEC = String(resultString);
#endif
      }
      else if (sequenceIsOK = bufferTeleinfo[1] == 'I') {
#define Linky_BASE true
#ifdef Linky_BASE
        IINST = atol(resultString);
#endif
      }
      else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'W')) {
        BBRHCJW = atol(resultString);
      }
      break;

    case 7:
      if (sequenceIsOK = bufferTeleinfo[1] == 'I') {
#define Linky_HCHP true
#ifdef Linky_HCHP
        IINST = atol(resultString);
#endif
      }
      else if (sequenceIsOK = bufferTeleinfo[1] == 'M') {
#define Linky_BASE true
#ifdef Linky_BASE
        IMAX = atol(resultString);
#endif
      }
      else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'W')) {
        BBRHPJW = atol(resultString);
      }
      break;

    case 8:
      if (sequenceIsOK = bufferTeleinfo[1] == 'M') {
#define Linky_HCHP true
#ifdef Linky_HCHP
        IMAX = atol(resultString);
#endif
      }
      else if (sequenceIsOK = bufferTeleinfo[1] == 'A') {
#define Linky_BASE true
#ifdef Linky_BASE
        PAPP = atol(resultString);
        okpapp = 1;
#endif
      }
      else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'R')) {
        BBRHCJR = atol(resultString);
      }
      break;

    case 9:
      if (sequenceIsOK = bufferTeleinfo[1] == 'A') {
#define Linky_HCHP true
#ifdef Linky_HCHP
        PAPP = atol(resultString);
        okpapp = 1;
#endif
      }
      else if (sequenceIsOK = bufferTeleinfo[1] == 'H') {
#define Linky_BASE true
#ifdef Linky_BASE
        HHPHC = resultString[0];
#endif
      }
      else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'R')) {
        BBRHPJR = atol(resultString);
      }
      break;

    case 10:
      if (sequenceIsOK = bufferTeleinfo[1] == 'H') {
#define Linky_HCHP true
#ifdef Linky_HCHP
        HHPHC = resultString[0];
#endif
      }
      else if (sequenceIsOK = bufferTeleinfo[1] == 'O') {
#define Linky_BASE true
#ifdef Linky_BASE
        MOTDETAT = String(resultString);
#endif
      }
      else if (sequenceIsOK = bufferTeleinfo[1] == 'T') {
        PTEC = String(resultString);
      }
      break;

    case 11:
      if (sequenceIsOK = bufferTeleinfo[1] == 'O')  MOTDETAT = String(resultString);
      else if (sequenceIsOK = bufferTeleinfo[0] == 'D')  DEMAIN = String(resultString);
      break;

    case 12:
      if (sequenceIsOK = bufferTeleinfo[1] == 'I')  IINST = atol(resultString);
      break;
    case 13:
      if (sequenceIsOK = bufferTeleinfo[1] == 'M')  IMAX = atol(resultString);
      break;
    case 14:
      if (sequenceIsOK = bufferTeleinfo[1] == 'A') {
        PAPP = atol(resultString);
        okpapp = 1;
      }
      break;
    case 15:
      if (sequenceIsOK = bufferTeleinfo[1] == 'H')  HHPHC = resultString[0];
      break;
    case 16:
      if (sequenceIsOK = bufferTeleinfo[0] == 'M')  MOTDETAT = resultString[0];
      break;

  }
  if (!sequenceIsOK) {
    // Serial.print(F("Out of sequence ..."));
    //  Serial.println(bufferTeleinfo);
  }
  return sequenceIsOK;
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//            Checksum Calculation                //
char chksum(char *buff, uint8_t len)  {
  int i;
  char sum = 0;
  for (i = 1; i < (len - 2); i++)
    sum = sum + buff[i];
  sum = (sum & 0x3F) + 0x20;
  return (sum);
}
// ---------------------------------------------- //

void linky_hist_loop() {

  TeleInfo();
  if (abonnement == "Base" || abonnement == "HCHP") {
    while (okpapp == 0 || (BASE == 0 && (HCHP == 0 || HCHC == 0))) {
      updateParameters();
    }
    if (BASE == 0) {
      BASE = HCHP + HCHC;
    }
  }
  else if (abonnement == "Tempo") {
    while ( okpapp == 0 || BBRHCJB == 0 || BBRHPJB == 0 || BBRHCJW == 0 || BBRHPJW == 0 || BBRHCJR == 0 || BBRHPJR == 0) {
      updateParameters();
    }
    if (BASE == 0) {
      BASE = BBRHCJB + BBRHPJB + BBRHCJW + BBRHPJW + BBRHCJR + BBRHPJR;
    }
  }
  Point sensor("xKy");
  InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
  DynamicJsonDocument JSONbuffer(1024);
  String output;

  if (useinflux == 1) {

    sensor.clearFields();
    if (HCHP != 0 || HCHC != 0) {
      sensor.addField("HCHC", HCHC);
      sensor.addField("HCHP", HCHP);
    }
    if (EJPHN != 0 || EJPHPM != 0) {
      sensor.addField("EJPHN", EJPHN);
      sensor.addField("EJPHPM", EJPHPM);
      sensor.addField("PEJP", PEJP);
    }
    if (BBRHCJB != 0 || BBRHPJB != 0 || BBRHCJW != 0 || BBRHPJW != 0 || BBRHCJR != 0 || BBRHPJR != 0) {
      sensor.addField("BBRHCJB", BBRHCJB);
      sensor.addField("BBRHPJB", BBRHPJB);
      sensor.addField("BBRHCJW", BBRHCJW);
      sensor.addField("BBRHPJW", BBRHPJW);
      sensor.addField("BBRHCJR", BBRHCJR);
      sensor.addField("BBRHPJR", BBRHPJR);
    }
    if (PTEC != "----") {
      sensor.addField("PTEC", PTEC);
    }
    if (DEMAIN != "----") {
      sensor.addField("DEMAIN", DEMAIN);
    }
    sensor.addField("PAPP", PAPP);
    sensor.addField("OPTARIF", OPTARIF);
    sensor.addField("BASE", BASE);
  }
  if (usemqtt == 1) {

    JSONbuffer["ADCO"] = String(ADCO);
    JSONbuffer["OPTARIF"] = String(OPTARIF);
    JSONbuffer["PAPP"] = String(PAPP);
    JSONbuffer["BASE"] = String(BASE);

    if (HCHP != 0 || HCHC != 0) {
      JSONbuffer["HCHC"] = String(HCHC);
      JSONbuffer["HCHP"] = String(HCHP);
    }

    if (EJPHN != 0 || EJPHPM != 0) {
      JSONbuffer["EJPHN"] = String(EJPHN);
      JSONbuffer["EJPHPM"] = String(EJPHPM);
      JSONbuffer["PEJP"] = String(PEJP);
    }

    if (BBRHCJB != 0 || BBRHPJB != 0 || BBRHCJW != 0 || BBRHPJW != 0 || BBRHCJR != 0 || BBRHPJR != 0) {
      JSONbuffer["BBRHCJB"] = String(BBRHCJB);
      JSONbuffer["BBRHPJB"] = String(BBRHPJB);
      JSONbuffer["BBRHCJW"] = String(BBRHCJW);
      JSONbuffer["BBRHPJW"] = String(BBRHPJW);
      JSONbuffer["BBRHCJR"] = String(BBRHCJR);
      JSONbuffer["BBRHPJR"] = String(BBRHPJR);
    }

    if (PTEC != "----") {
      JSONbuffer["PTEC"] = String(PTEC);
    }

    if (DEMAIN != "----") {
      JSONbuffer["DEMAIN"] = String(DEMAIN);
    }
  }

  MQAll = millis();
  WiFi.mode(WIFI_STA);
  delay(1);
  setup_wifi();           //On se connecte au réseau wifi
  delay(1000);
  String adressmacesp = composeClientID();

  if (useinflux == 1) {
    // Add constant tags - only once
    sensor.addTag("MacAdress", adressmacesp);
    clientinflux.setInsecure();
    // Check server connection
    if (clientinflux.validateConnection()) {
      Serial.print("Connected to InfluxDB: ");
      Serial.println(clientinflux.getServerUrl());
    } else {
      Serial.print("InfluxDB connection failed: ");
      Serial.println(clientinflux.getLastErrorMessage());
    }
    sensor.addField("RSSI", WiFi.RSSI());
    clientinflux.writePoint(sensor);
  }
  if (usemqtt == 1) {
    client.setServer(mqtt_server.c_str(), 1883);    //Configuration de la connexion au serveur MQTT
    if (!client.connected()) {
      reconnect();
    }
    client.loop();
    serializeJson(JSONbuffer, output);
    String maintopic = "winky/" + adressmacesp;
    client.publish(maintopic.c_str(), output.c_str(), true);
  }

  delay(1000);
  WiFi.disconnect();
  delay(500);
//  DeepModulateSleeptime = (millis() - MQAll) * 1000;
  DeepModulateSleeptime = (millis() - MQAll);
  sprintf(msg, "%ld", DeepModulateSleeptime);
  writeFile(SPIFFS, "/timesleep.txt", msg);
  ESP.deepSleep(100000, WAKE_RF_DISABLED); //100ms
 // ESP.deepSleep(DeepModulateSleeptime, WAKE_RF_DISABLED); //100ms
}
