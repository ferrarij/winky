#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <InfluxDbClient.h>
#include <ESPAsyncTCP.h>
#include <Hash.h>
#include <FS.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <ArduinoJson.h>
AsyncWebServer server(80);

/*Parametres Wifi*/
String wifi_ssid;
String wifi_password;
const char *ssid = "XKY";
const char *password = "oneforall";
WiFiClient espClient;
int midtrywificon = 150;
int nbtrywificon = 0;
int channel = 11;
String AdressMacRouteur;
char str[] = "00:00:00:00:00:00";
uint8_t bssid[6];
char* ptr; //start and end pointer for strtol

/*Parametres Linky*/
boolean progmode =0;
String abonnement; 
int typeabo=0;
boolean linky_histstard =0;
boolean modeprod=0;

/*Parametres Validation*/
boolean okpapp =0;
boolean okpappi = 0;

/*Parametres InfluxDB*/
boolean useinflux=0;
String INFLUXDB_URL; 
// InfluxDB 2 server or cloud API authentication token (Use: InfluxDB UI -> Load Data -> Tokens -> <select token>)
String INFLUXDB_TOKEN;
// InfluxDB 2 organization id (Use: InfluxDB UI -> Settings -> Profile -> <name under tile> )
String INFLUXDB_ORG;
// InfluxDB 2 bucket name (Use: InfluxDB UI -> Load Data -> Buckets)
String INFLUXDB_BUCKET;

/*Parametres MQTT*/
boolean usemqtt=0;
String mqtt_server; 
String mqtt_user; 
String mqtt_password; 
PubSubClient client(espClient);
int maxtrymqttcon = 5;
int nbtrymqttcon = 0;

/*Parametres xKy*/
long MQAll;
long DeepModulateSleeptime;
boolean useoptimwifi=0;
boolean useled=0;
int DeepSleepSecondsOpti =0;
String FWVersion = "V17";
unsigned long timestartup;
unsigned long timedecode;
unsigned long timeconnexion;

/*Parametres Page WEB*/
const char* PARAM_URL = "inputUrl";
const char* PARAM_TOKEN = "inputToken";
const char* PARAM_ORG = "inputOrg";
const char* PARAM_BUCKET = "inputBucket";
const char* PARAM_WIFI_SSID = "inputWifiSsid";
const char* PARAM_WIFI_PASSWORD = "inputWifiPassword";
const char* PARAM_ACTIVE_INFLUX = "inputActiverInflux";
const char* PARAM_ACTIVE_MQTT = "inputActiverMqtt";
const char* PARAM_URL_MQTT = "inputUrlMqtt";
const char* PARAM_USER_MQTT = "inputUserMqtt";
const char* PARAM_PASSWORD_MQTT = "inputPasswordMqtt";
const char* PARAM_ABONNEMENT = "inputAbonnements";
const char* PARAM_MODEPROD = "inputModeProd";
const char* PARAM_MODEHISTSTAND = "inputModeHistStand";
const char* PARAM_ACTIVE_OPTIMWIFI = "inputActiverOptimWifi";
const char* PARAM_MIDCONNEXION = "inputMidConnexion";
const char* PARAM_OPTIMDSSECONDS = "inputOptimiDSSeconds";
const char* PARAM_ACTIVE_LED = "inputActiverLed";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Saved value to XKY SPIFFS");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Wifi</p>
  <form action="/get" target="hidden-form">
    SSID actuel: %inputWifiSsid% <input type="text" name="inputWifiSsid">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Mot de passe actuel: %inputWifiPassword% <input type="text" name="inputWifiPassword">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <p>Configuration Linky</p>
  <form action="/get" target="hidden-form">
      <label for="Mode">Mode actuel: %inputModeHistStand%</label>
      <select name="inputModeHistStand" id="abon">
        <option value="Historique">Historique</option>
        <option value="Standard">Standard</option>
      </select>
      <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <p>Configuration Abonnement</p>
  <form action="/get" target="hidden-form">
      <label for="abon">Abonnement actuel: %inputAbonnements%</label>
      <select name="inputAbonnements" id="abon">
        <option value="Base">Base</option>
        <option value="HCHP">HCHP</option>
        <option value="Tempo">Tempo</option>
      </select>
      <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <p>Mode Production</p>
   <form action="/get" target="hidden-form">
    Activer (0= desactive/1= active) valeur actuelle: %inputModeProd% <input type="text" name="inputModeProd">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>  
  <button onclick="window.location.href = 'http://192.168.4.1/influx';">Configuration InfluxDB</button>
  <button onclick="window.location.href = 'http://192.168.4.1/mqtt';">Configuration MQTT</button>
  <button onclick="window.location.href = 'http://192.168.4.1/avance';">Configuration Avancee</button>
  <button onclick="window.location.href = 'http://192.168.4.1/update';">Mise a jour Software</button>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char influx_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Saved value to XKY SPIFFS");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Server InfluxDB</p>
  <form action="/get" target="hidden-form">
    Activer (0= desactive/1= active) valeur actuelle: %inputActiverInflux% <input type="text" name="inputActiverInflux">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Url actuelle: %inputUrl% <input type="text" name="inputUrl">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Token actuel: %inputToken% <input type="text " name="inputToken">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Organisation actuelle: %inputOrg% <input type="text " name="inputOrg">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form>
  <br>
  <form action="/get" target="hidden-form">
    Bucket actuel: %inputBucket% <input type="text " name="inputBucket">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form>
  <br>
  <button onclick="window.location.href = 'http://192.168.4.1/';">Configuration Principale</button>
  <button onclick="window.location.href = 'http://192.168.4.1/mqtt';">Configuration MQTT</button>
  <button onclick="window.location.href = 'http://192.168.4.1/avance';">Configuration Avancee</button>
  <button onclick="window.location.href = 'http://192.168.4.1/update';">Mise a jour Software</button>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char mqtt_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Saved value to XKY SPIFFS");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Server MQTT</p>
  <form action="/get" target="hidden-form">
    Activer (0= desactive/1= active) valeur actuelle: %inputActiverMqtt% <input type="text" name="inputActiverMqtt">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Url actuelle: %inputUrlMqtt% <input type="text" name="inputUrlMqtt">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    User actuel: %inputUserMqtt% <input type="text " name="inputUserMqtt">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Password actuel: %inputPasswordMqtt% <input type="text " name="inputPasswordMqtt">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form>
  <br>  
  <button onclick="window.location.href = 'http://192.168.4.1/';">Configuration Principale</button>
  <button onclick="window.location.href = 'http://192.168.4.1/influx';">Configuration InfluxDB</button>
  <button onclick="window.location.href = 'http://192.168.4.1/avance';">Configuration Avancee</button>
  <button onclick="window.location.href = 'http://192.168.4.1/update';">Mise a jour Software</button>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

// HTML web page to handle 3 input fields (inputUrl, inputToken, inputOrg, inputBucket)
const char avance_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>XKY Configuration</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    function submitMessage() {
      alert("Saved value to XKY SPIFFS");
      setTimeout(function(){ document.location.reload(false); }, 5000);   
    }
  </script></head><body>
  <p><b>Configuration du module XKY</b></p>
  <p>Parametres internes XKY</p>
  <p>Attention. Ces parametres sont la a titre experimental pour vous permettre de modifier le comportement du XKY.</p>
  <p>Un mauvais reglage peut engendrer des instabilites.</p>
  <p>En cas de doute, vous pouvez mettre les activations a 0.</p>
  <form action="/get" target="hidden-form">
    Activer optimisation wifi (0= desactive/1= active) valeur actuelle: %inputActiverOptimWifi% <input type="text" name="inputActiverOptimWifi">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Nb de secondes validation optimisation wifi (par defaut 15s) valeur actuelle: %inputMidConnexion% <input type="text" name="inputMidConnexion">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Nb de secondes deepsleep optimisation offset (Avec signe + ou - et ici la decimale s'ecrit avec un point exemple 1.5s) (par defaut 0s) valeur actuelle: %inputOptimiDSSeconds% <input type="text" name="inputOptimiDSSeconds">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <form action="/get" target="hidden-form">
    Activer Led secondaire (0= desactive/1= active) valeur actuelle: %inputActiverLed% <input type="text" name="inputActiverLed">
    <input type="submit" value="Submit" onclick="submitMessage()">
  </form><br>
  <br>
  <button onclick="window.location.href = 'http://192.168.4.1/';">Configuration Principale</button>
  <button onclick="window.location.href = 'http://192.168.4.1/influx';">Configuration InfluxDB</button>
  <button onclick="window.location.href = 'http://192.168.4.1/mqtt';">Configuration MQTT</button>
  <button onclick="window.location.href = 'http://192.168.4.1/update';">Mise a jour Software</button>
  <p>One For All version 1er detenteur</p>
  <iframe style="display:none" name="hidden-form"></iframe>
</body></html>)rawliteral";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

String readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if(!file || file.isDirectory()){
    Serial.println("- empty file or failed to open file");
    return String();
  }
  Serial.println("- read from file:");
  String fileContent;
  while(file.available()){
    fileContent+=String((char)file.read());
  }
  file.close();
  Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if(!file){
    Serial.println("- failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
  file.close();
}

// Replaces placeholder with stored values
String processor(const String& var){
  //Serial.println(var);
  if(var == "inputUrl"){
    return readFile(SPIFFS, "/inputUrl.txt");
  }
  else if(var == "inputToken"){
    return readFile(SPIFFS, "/inputToken.txt");
  }
  else if(var == "inputOrg"){
    return readFile(SPIFFS, "/inputOrg.txt");
  }
  else if(var == "inputBucket"){
    return readFile(SPIFFS, "/inputBucket.txt");
  }
  else if(var == "inputWifiSsid"){
    return readFile(SPIFFS, "/inputWifiSsid.txt");
  }
  else if(var == "inputWifiPassword"){
    return readFile(SPIFFS, "/inputWifiPassword.txt");
  }
  else if(var == "inputActiverInflux"){
    return readFile(SPIFFS, "/inputActiverInflux.txt");
  }
  else if(var == "inputActiverMqtt"){
    return readFile(SPIFFS, "/inputActiverMqtt.txt");
  }
  else if(var == "inputUrlMqtt"){
    return readFile(SPIFFS, "/inputUrlMqtt.txt");
  }
  else if(var == "inputUserMqtt"){
    return readFile(SPIFFS, "/inputUserMqtt.txt");
  }
  else if(var == "inputPasswordMqtt"){
    return readFile(SPIFFS, "/inputPasswordMqtt.txt");
  }
  else if(var == "inputAbonnements"){
    return readFile(SPIFFS, "/inputAbonnements.txt");
  }
  else if(var == "inputModeProd"){
    return readFile(SPIFFS, "/inputModeProd.txt");
  }
  else if(var == "inputModeHistStand"){
    return readFile(SPIFFS, "/inputModeHistStand.txt");
  }
  else if(var == "inputActiverOptimWifi"){
    return readFile(SPIFFS, "/inputActiverOptimWifi.txt");
  }
  else if(var == "inputMidConnexion"){
    return readFile(SPIFFS, "/inputMidConnexion.txt");
  }
  else if(var == "inputActiverLed"){
    return readFile(SPIFFS, "/inputActiverLed.txt");
  }
  else if(var == "inputOptimiDSSeconds"){
    return readFile(SPIFFS, "/inputOptimiDSSeconds.txt");
  }
  return String();
}



String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

String composeClientID() {
  uint8_t mac[6];
  WiFi.macAddress(mac);
  String clientId;
  clientId += "esp-";
  clientId += macToStr(mac);
  return clientId;
}

//Connexion au réseau WiFi
void setup_wifi() {
  delay(10);
  WiFi.setOutputPower(20.5);
  nbtrywificon = 0;
  if(AdressMacRouteur!="" && useoptimwifi){
    WiFi.begin(wifi_ssid, wifi_password,channel,bssid);
    while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
    nbtrywificon = nbtrywificon+1;
    if(nbtrywificon == midtrywificon){
      SPIFFS.remove("/inputMacadress.txt");
    }
  }
  }
  else{
    WiFi.begin(wifi_ssid, wifi_password);
    while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  }
  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());
  Serial.printf("\nWiFi channel: %d\n", WiFi.channel());
  Serial.printf("WiFi BSSID: %s\n", WiFi.BSSIDstr().c_str());
  if (AdressMacRouteur != WiFi.BSSIDstr().c_str()){
    writeFile(SPIFFS, "/inputMacadress.txt", WiFi.BSSIDstr().c_str());
  }
  if (String(channel) != String(WiFi.channel())){
    writeFile(SPIFFS, "/inputChannel.txt", String(WiFi.channel()).c_str());
  }
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'à obtenir une reconnexion
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    String clientnumber = composeClientID();
    if (client.connect(clientnumber.c_str(), mqtt_user.c_str(), mqtt_password.c_str())) {
      Serial.println("OK");
    } else {
      Serial.print("KO, erreur : "); 
      Serial.print(client.state());
      Serial.println(" On attend 1 secondes avant de recommencer");
      delay(1000);
      nbtrymqttcon = nbtrymqttcon +1;
      if (nbtrymqttcon == maxtrymqttcon) {
        ESP.deepSleep(30000000);
      }
    }
  }
}

#include "linkyhist.h"
#include "linky.h"

void setup() {
  pinMode(2,OUTPUT);
  digitalWrite(2,HIGH);
  delay(500);
  pinMode(4,INPUT);
  progmode = digitalRead(4);
//progmode = 0;
  if (progmode){
      Serial.begin(9600);
    if(!SPIFFS.begin()){
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }
  WiFi.softAP(ssid, password);
  IPAddress myIP = WiFi.softAPIP();
  // Send web page with input fields to client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  server.on("/influx", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", influx_html, processor);
  });

  server.on("/mqtt", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", mqtt_html, processor);
  });

  server.on("/avance", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", avance_html, processor);
  });

  // Send a GET request to <ESP_IP>/get?inputUrl=<inputMessage>
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage;
    // GET inputUrl value on <ESP_IP>/get?inputUrl=<inputMessage>
    if (request->hasParam(PARAM_URL)) {
      inputMessage = request->getParam(PARAM_URL)->value();
      writeFile(SPIFFS, "/inputUrl.txt", inputMessage.c_str());
    }
    // GET inputToken value on <ESP_IP>/get?inputToken=<inputMessage>
    else if (request->hasParam(PARAM_TOKEN)) {
      inputMessage = request->getParam(PARAM_TOKEN)->value();
      writeFile(SPIFFS, "/inputToken.txt", inputMessage.c_str());
    }
    // GET inputOrg value on <ESP_IP>/get?inputOrg=<inputMessage>
    else if (request->hasParam(PARAM_ORG)) {
      inputMessage = request->getParam(PARAM_ORG)->value();
      writeFile(SPIFFS, "/inputOrg.txt", inputMessage.c_str());
    }
    else if (request->hasParam(PARAM_BUCKET)) {
      inputMessage = request->getParam(PARAM_BUCKET)->value();
      writeFile(SPIFFS, "/inputBucket.txt", inputMessage.c_str());
    }
    else if (request->hasParam(PARAM_WIFI_SSID)) {
      inputMessage = request->getParam(PARAM_WIFI_SSID)->value();
      writeFile(SPIFFS, "/inputWifiSsid.txt", inputMessage.c_str());
    }
    else if (request->hasParam(PARAM_WIFI_PASSWORD)) {
      inputMessage = request->getParam(PARAM_WIFI_PASSWORD)->value();
      writeFile(SPIFFS, "/inputWifiPassword.txt", inputMessage.c_str());
    }  
    else if (request->hasParam(PARAM_ACTIVE_INFLUX)) {
      inputMessage = request->getParam(PARAM_ACTIVE_INFLUX)->value();
      writeFile(SPIFFS, "/inputActiverInflux.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_ACTIVE_MQTT)) {
      inputMessage = request->getParam(PARAM_ACTIVE_MQTT)->value();
      writeFile(SPIFFS, "/inputActiverMqtt.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_URL_MQTT)) {
      inputMessage = request->getParam(PARAM_URL_MQTT)->value();
      writeFile(SPIFFS, "/inputUrlMqtt.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_USER_MQTT)) {
      inputMessage = request->getParam(PARAM_USER_MQTT)->value();
      writeFile(SPIFFS, "/inputUserMqtt.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_PASSWORD_MQTT)) {
      inputMessage = request->getParam(PARAM_PASSWORD_MQTT)->value();
      writeFile(SPIFFS, "/inputPasswordMqtt.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_ABONNEMENT)) {
      inputMessage = request->getParam(PARAM_ABONNEMENT)->value();
      writeFile(SPIFFS, "/inputAbonnements.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_MODEPROD)) {
      inputMessage = request->getParam(PARAM_MODEPROD)->value();
      writeFile(SPIFFS, "/inputModeProd.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_MODEHISTSTAND)) {
      inputMessage = request->getParam(PARAM_MODEHISTSTAND)->value();
      writeFile(SPIFFS, "/inputModeHistStand.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_ACTIVE_OPTIMWIFI)) {
      inputMessage = request->getParam(PARAM_ACTIVE_OPTIMWIFI)->value();
      writeFile(SPIFFS, "/inputActiverOptimWifi.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_MIDCONNEXION)) {
      inputMessage = request->getParam(PARAM_MIDCONNEXION)->value();
      writeFile(SPIFFS, "/inputMidConnexion.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_ACTIVE_LED)) {
      inputMessage = request->getParam(PARAM_ACTIVE_LED)->value();
      writeFile(SPIFFS, "/inputActiverLed.txt", inputMessage.c_str());
    } 
    else if (request->hasParam(PARAM_OPTIMDSSECONDS)) {
      inputMessage = request->getParam(PARAM_OPTIMDSSECONDS)->value();
      writeFile(SPIFFS, "/inputOptimiDSSeconds.txt", inputMessage.c_str());
    } 
    else {
      inputMessage = "No message sent";
    }
    Serial.println(inputMessage);
    request->send(200, "text/text", inputMessage);
  });
  server.onNotFound(notFound);
  AsyncElegantOTA.begin(&server);    // Start AsyncElegantOTA
  server.begin();   
  
  }
  else{
  String resetCause = ESP.getResetReason();
  Serial.println(resetCause);
//  resetCount = 0;
  if (resetCause != "Deep-Sleep Wake") {
    ESP.deepSleep(30000000, WAKE_RF_DISABLED); //100ms
//    Serial.println(F("I'm awake and starting the Low Power tests"));
  }
  if(!SPIFFS.begin()){
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }
  useoptimwifi= readFile(SPIFFS, "/inputActiverOptimWifi.txt") == "1";
  useled= readFile(SPIFFS, "/inputActiverLed.txt") == "1";
  midtrywificon = 10*((readFile(SPIFFS, "/inputMidConnexion.txt")).toInt());
  DeepSleepSecondsOpti = 1000000*((readFile(SPIFFS, "/inputOptimiDSSeconds.txt")).toFloat());
  
  useinflux= readFile(SPIFFS, "/inputActiverInflux.txt") == "1";
  INFLUXDB_URL = readFile(SPIFFS, "/inputUrl.txt");
  INFLUXDB_TOKEN = readFile(SPIFFS, "/inputToken.txt");
  INFLUXDB_ORG = readFile(SPIFFS, "/inputOrg.txt");
  INFLUXDB_BUCKET = readFile(SPIFFS, "/inputBucket.txt");

  wifi_ssid = readFile(SPIFFS, "/inputWifiSsid.txt");
  wifi_password = readFile(SPIFFS, "/inputWifiPassword.txt");
  usemqtt= readFile(SPIFFS, "/inputActiverMqtt.txt") == "1";
  mqtt_server= readFile(SPIFFS, "/inputUrlMqtt.txt");
  mqtt_user= readFile(SPIFFS, "/inputUserMqtt.txt");
  mqtt_password= readFile(SPIFFS, "/inputPasswordMqtt.txt");
  abonnement = readFile(SPIFFS, "/inputAbonnements.txt");
  channel = readFile(SPIFFS, "/inputChannel.txt").toInt();
  AdressMacRouteur = readFile(SPIFFS, "/inputMacadress.txt");
  //lecture adresse mac routeur
  if(AdressMacRouteur!=""){
  int str_len = AdressMacRouteur.length() + 1;
  char str[str_len];
  AdressMacRouteur.toCharArray(str, str_len);
  bssid[0] = strtol(str, &ptr, HEX );
  for( uint8_t i = 1; i < 6; i++ )
  {bssid[i] = strtol(ptr+1, &ptr, HEX );}
  Serial.print(bssid[0], HEX);
  for( uint8_t i = 1; i < 6; i++)
  {
    Serial.print(':');
    Serial.print( bssid[i], HEX);
  }

//original character array is preserved
  Serial.println();
  Serial.print(str);
  }
  if(abonnement == "Base" || abonnement == "HCHP"){
    typeabo =0;
  }
  else if(abonnement == "Tempo"){
    typeabo =1;
  }
  linky_histstard = readFile(SPIFFS, "/inputModeHistStand.txt") == "Standard";
  modeprod = readFile(SPIFFS, "/inputModeProd.txt") == "1"; 
  Serial.begin(9600);
  if(linky_histstard==1){
    linky_setup();
    }
  }
  timestartup = millis();
}

void loop() {
  if(linky_histstard==0){
    linky_hist_loop();
  }
  else{
    linky_loop();
    }
}
