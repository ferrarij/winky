// **********************************************************************************
// Arduino Teleinfo sample, display information on teleinfo values received
// **********************************************************************************
// Creative Commons Attrib Share-Alike License
// You are free to use/extend this library but please abide with the CC-BY-SA license:
// http://creativecommons.org/licenses/by-sa/4.0/
//
// for detailled explanation of this library see dedicated article
// https://hallard.me/libteleinfo/
//
// For any explanation about teleinfo or use, see my blog
// https://hallard.me/category/tinfo
//
// connect Teleinfo RXD pin To Arduin D3
// see schematic here https://hallard.me/demystifier-la-teleinfo/
// and dedicated article here
//
// Written by Charles-Henri Hallard (https://hallard.me)
//
// History : V1.00 2015-06-14 - First release
//
// All text above must be included in any redistribution.
//
// **********************************************************************************
//
// adaptation pour LINKY mode Standart sur ESP32

#include "Teleinfo.h"
#include "Teleinfo_cpp.h"

TInfo    tinfo; // Teleinfo object
ValueList * me_fist;
bool  ok_me_fist = false;

bool boucleok=0;
int compttour = 0;

char * NGTF;
char * SINSTS;
char * SINSTI;
char * EAST;
char * EASF01;
char * EASF02;
char * EASF03;
char * EASF04;
char * EASF05;
char * EASF06;
char * EAIT;
char * LTARF;
String output;
Point sensor("xKy");
DynamicJsonDocument JSONbuffer(1024);
String adressmacesp;
   
/* ======================================================================
  Function: printUptime
  Purpose : print pseudo uptime value
  Input   : -
  Output  : -
  Comments: compteur de secondes basique sans controle de dépassement
          En plus SoftwareSerial rend le compteur de millis() totalement
          A la rue, donc la precision de ce compteur de seconde n'est
          pas fiable du tout, dont acte !!!
  ====================================================================== */
void printUptime(void)
{
  Serial.print(millis() / 1000);
  Serial.print(F("s\t"));
}

/* ======================================================================
  Function: DataCallback
  Purpose : callback when we detected new or modified data received
  Input   : linked list pointer on the concerned data
          current flags value
  Output  : -
  Comments: -
  ====================================================================== */
void DataCallback(ValueList * me, uint8_t  flags)
{
  // Show our not accurate second counter
  //printUptime();

  if (flags & TINFO_FLAGS_ADDED || flags & TINFO_FLAGS_UPDATED)
    if  ( !ok_me_fist) {
      ok_me_fist = true;
      me_fist = me;
    }

  // Display values
  Serial.print("==> ");
  Serial.print(me->name);
  if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
  Serial.print(F("\t")) ;
  if ( me->value[0] != '*') {
    Serial.print(F(" = ")) ;
    Serial.print(me->value) ;
  }
  if ( me->date[0] != '*' ) {
    Serial.print(" : ");
    Serial.print(me->date);
    compttour =compttour+1;
  }
  if ((me->name[0] == 'N') && (me->name[1] == 'G') && (me->name[2] == 'T') && (me->name[3] == 'F')) {

    NGTF = me->value;
    Serial.print("NGTF:");
    Serial.println(NGTF);
  }
  if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'S')) {

    SINSTS = me->value;
    okpapp = 1;
    Serial.print("SINSTS:");
    Serial.println(SINSTS);

  }
  if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'I')) {

    SINSTI = me->value;
    okpappi = 1;
    Serial.print("SINSTI:");
    Serial.println(SINSTI);

  }
  if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'I')) {

    SINSTS = '-' + me->value;
    Serial.print("SINSTS:");
    Serial.println(SINSTS);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'T')) {

    EAST = me->value;
    Serial.print("EAST:");
    Serial.println(EAST);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'I') && (me->name[3] == 'T')) {
    EAIT = me->value;
    Serial.print("EAIT:");
    Serial.println(EAIT);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '1')) {
    EASF01 = me->value;
    Serial.print("EASF01:");
    Serial.println(EASF01);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '2')) {
    EASF02 = me->value;
    Serial.print("EASF02:");
    Serial.println(EASF02);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '3')) {
    EASF03 = me->value;
    Serial.print("EASF03:");
    Serial.println(EASF03);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '4')) {
    EASF04 = me->value;
    Serial.print("EASF04:");
    Serial.println(EASF04);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '5')) {
    EASF05 = me->value;
    Serial.print("EASF05:");
    Serial.println(EASF05);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '6')) {
    EASF06 = me->value;
    Serial.print("EASF06:");
    Serial.println(EASF06);
  }
  if ((me->name[0] == 'L') && (me->name[1] == 'T') && (me->name[2] == 'A') && (me->name[3] == 'R') && (me->name[4] == 'F')) {
    LTARF = me->value;
    Serial.print("LTARF:");
    Serial.println(LTARF);
  }
  if ((me->name[0] == 'U') && (me->name[1] == 'M') && (me->name[2] == 'O') && (me->name[3] == 'Y') && (me->name[4] == '1')) {
    boucleok = 1;
  }
  Serial.println("");
}

/* ======================================================================
  Function: sendJSON
  Purpose : dump teleinfo values on serial
  Input   : linked list pointer on the concerned data
          true to dump all values, false for only modified ones
  Output  : -
  Comments: -
  ====================================================================== */
void sendAll()
{
  ValueList * me;
  me = me_fist;                                   // reprendre le 1er de la chaine
  // Got at least one ?
  if (me) {

    Serial.print(F("\"all\":"));
    Serial.print(me->name) ;
    Serial.print(F("\t")) ;
    if ( me->value[0] != '*') {
      Serial.print(F(" = ")) ;
      Serial.print(me->value) ;
    }
    if ( me->date[0] != '*') {
      Serial.print(" : ");
      Serial.print(me->date);
    }
    Serial.println("");

    // Loop thru the node
    while (me->next) {

      // go to next node
      me = me->next;                                // suivant
      //sprintf(msg, "%s=%s", me->name, me->value );
      //Mqtt_emettre(me->name, me->value);

      Serial.print(F("\"all\":"));
      Serial.print(me->name) ;
      //if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
      Serial.print(F("\t")) ;
      if ( me->value[0] != '*') {
        Serial.print(F(" = ")) ;
        Serial.print(me->value) ;
      }
      if (me->date[0] != '*') {
        Serial.print(" : ");
        Serial.print(me->date);
      }
      Serial.println("");

    }
  }
}

void DataformatComPart1(){
  timedecode = millis();
   sensor.clearFields();

   sensor.addField("ModeTic", "Standard");
   JSONbuffer["ModeTic"] = String("Standard");
   sensor.addField("FWVersion", FWVersion); 
   JSONbuffer["FWVersion"] = String(FWVersion);

   if(useled){
    digitalWrite(2,LOW);
   }
   MQAll = millis();
   WiFi.mode(WIFI_STA);
   delay(1);
   setup_wifi();           //On se connecte au réseau wifi
   delay(500);
   timeconnexion = millis();
   adressmacesp = composeClientID();
   sensor.addField("RSSI", WiFi.RSSI());
   JSONbuffer["RSSI"] = String(WiFi.RSSI());
   sensor.addField("TimeStartup", timestartup);
   sensor.addField("TimeDecode",timedecode);
   sensor.addField("TimePrepa", MQAll);
   sensor.addField("TimeConn", timeconnexion);
   sensor.addField("UseOptimWifi", useoptimwifi);
   sensor.addField("MidTryWifiCon",midtrywificon);
   sensor.addField("OffsetDeepsleep",DeepSleepSecondsOpti);
   sensor.addField("UseLed", useled);
   sensor.addField("NGTF", String(NGTF));
   sensor.addField("OPTARIF", String(LTARF));
   sensor.addField("PTEC", String(LTARF));
   sensor.addField("BASE", atoi(EAST));
   sensor.addField("EAST", atoi(EAST));
   if(okpapp == 1){
    sensor.addField("SINSTS", atoi(SINSTS));
    sensor.addField("PAPP", atoi(SINSTS));  
   }
   if(okpappi == 1){
    sensor.addField("SINSTI", atoi(SINSTI));
    sensor.addField("EAIT", atoi(EAIT));
   }  
}

void DataformatComPart2(){
  serializeJson(JSONbuffer, output);
   if (useinflux ==1){
    // Data point  
    sensor.addTag("MacAdress", adressmacesp);
    InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
    clientinflux.setInsecure();
    boolean influxconnec = 0;
    while (!influxconnec) {
      influxconnec = clientinflux.validateConnection();
      delay(100);
    }
    clientinflux.writePoint(sensor);
   }
   if (usemqtt==1){
    client.setServer(mqtt_server.c_str(), 1883);    //Configuration de la connexion au serveur MQTT
    if (!client.connected()) {
      reconnect();
    }
    client.loop();
    String maintopic = "winky/" + adressmacesp;
    client.publish(maintopic.c_str(), output.c_str(), true);
   }
   delay(1000);
   WiFi.disconnect();
   delay(500);
   DeepModulateSleeptime = (millis()-MQAll)*1000 + DeepSleepSecondsOpti;
   ESP.deepSleep(DeepModulateSleeptime, WAKE_RF_DISABLED); //100ms  
  //ESP.deepSleep(25000000, WAKE_RF_DISABLED); //100ms  
}




/* ======================================================================
  Function: setup
  Purpose : Setup I/O and other one time startup stuff
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void linky_setup()
{

  Serial.println(F("======== Setup Teleinfo Linky ======="));
  delay(200);

  Serial.begin(9600);
  // Init teleinfo
  tinfo.init();

  // Attacher les callback dont nous avons besoin
  // pour cette demo, ici attach data
  tinfo.attachData(DataCallback);

}

/* ======================================================================
  Function: loop
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */

void linky_loop() {
  // Teleinformation processing
  if ( Serial.available() ) {
    /*
      char inByte = Serial2.read();
      Serial.print(inByte);
      Serial.print(".");
      Serial.print(inByte,HEX);
      Serial.print(";");
    */
    tinfo.process(Serial.read());
  }

  if ((String(NGTF).indexOf("BA")== 0)&& boucleok == 1){
   DataformatComPart1();
   DataformatComPart2();
  }
  else if ((String(NGTF).indexOf('H')== 0) && boucleok == 1){
   DataformatComPart1();
   sensor.addField("EASF01", atoi(String(EASF01).c_str()));
   sensor.addField("HCHC", atoi(String(EASF01).c_str()));
   sensor.addField("EASF02", atoi(String(EASF02).c_str()));
   sensor.addField("HCHP", atoi(String(EASF02).c_str())); 
   DataformatComPart2();
  }
  else if ((String(NGTF).indexOf('E')== 0) && boucleok == 1){
   DataformatComPart1();
   sensor.addField("EASF01", atoi(String(EASF01).c_str()));
   sensor.addField("EJPHN", atoi(String(EASF01).c_str()));
   sensor.addField("EASF02", atoi(String(EASF02).c_str()));
   sensor.addField("EJPHPM", atoi(String(EASF02).c_str())); 
   DataformatComPart2();
  }
  else if ((String(NGTF).indexOf('T')== 0) && boucleok == 1){
   DataformatComPart1();
   sensor.addField("EASF01", atoi(String(EASF01).c_str()));
   sensor.addField("BBRHCJB", atoi(String(EASF01).c_str()));
   sensor.addField("EASF02", atoi(String(EASF02).c_str()));
   sensor.addField("BBRHPJB", atoi(String(EASF02).c_str())); 
   sensor.addField("EASF03", atoi(String(EASF03).c_str()));
   sensor.addField("BBRHCJW", atoi(String(EASF03).c_str()));
   sensor.addField("EASF04", atoi(String(EASF04).c_str()));
   sensor.addField("BBRHPJW", atoi(String(EASF04).c_str())); 
   sensor.addField("EASF05", atoi(String(EASF05).c_str()));
   sensor.addField("BBRHCJR", atoi(String(EASF05).c_str()));
   sensor.addField("EASF06", atoi(String(EASF06).c_str()));
   sensor.addField("BBRHPJR", atoi(String(EASF06).c_str())); 
   DataformatComPart2();
  }
}
