// **********************************************************************************
// Arduino Teleinfo sample, display information on teleinfo values received
// **********************************************************************************
// Creative Commons Attrib Share-Alike License
// You are free to use/extend this library but please abide with the CC-BY-SA license:
// http://creativecommons.org/licenses/by-sa/4.0/
//
// for detailled explanation of this library see dedicated article
// https://hallard.me/libteleinfo/
//
// For any explanation about teleinfo or use, see my blog
// https://hallard.me/category/tinfo
//
// connect Teleinfo RXD pin To Arduin D3
// see schematic here https://hallard.me/demystifier-la-teleinfo/
// and dedicated article here
//
// Written by Charles-Henri Hallard (https://hallard.me)
//
// History : V1.00 2015-06-14 - First release
//
// All text above must be included in any redistribution.
//
// **********************************************************************************
//
// adaptation pour LINKY mode Standart sur ESP32
// add version of historique by Jerome Ferrari CNRS

#include "Teleinfo.h"
#include "Teleinfo_cpp.h"

TInfo    tinfo; // Teleinfo object
ValueList * me_fist;
bool  ok_me_fist = false;
bool ticmissing = 0;

bool boucleok = 0;
int compttour = 0;

char * NGTF;
char * SINSTS;
char * SINSTI;
char * EAST;
char * EASF01;
char * EASF02;
char * EASF03;
char * EASF04;
char * EASF05;
char * EASF06;
char * EAIT;
char * LTARF;
String output;
Point sensor("xKy");
DynamicJsonDocument JSONbuffer(1024);
String adressmacesp;

// Variables Linky historique
char HHPHC;
int ISOUSC;               // intensité souscrite
int PEJP;
int IINST;                // intensité instantanée    en A
int IINST1;
int IINST2;
int IINST3;
int ADPS;
int PAPP;                 // puissance apparente      en VA
unsigned long PMAX;       // compteur Heures Creuses  en Wh
unsigned long HCHC;       // compteur Heures Creuses  en Wh
unsigned long HCHP;       // compteur Heures Pleines  en Wh
unsigned long EJPHN;       // compteur Heures Normales  en Wh
unsigned long EJPHPM;       // compteur Heures de Pointe Mobile  en Wh
unsigned long BASE;       // index BASE               en Wh
unsigned long BBRHCJB;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJB;       // compteur Heures Pleines  en Wh
unsigned long BBRHCJW;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJW;       // compteur Heures Pleines  en Wh
unsigned long BBRHCJR;       // compteur Heures Creuses  en Wh
unsigned long BBRHPJR;       // compteur Heures Pleines  en Wh
String DEMAIN;              // demain rouge
String PTEC;              // période tarif en cours
String ADCO;              // adresse du compteur
String OPTARIF;           // option tarifaire
int IMAX;                 // intensité maxi = 90A
int IMAX1;
int IMAX2;
int IMAX3;
String MOTDETAT;          // status word
boolean teleInfoReceived;

char chksum(char *buff, uint8_t len);
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber);


/* ======================================================================
  Function: printUptime
  Purpose : print pseudo uptime value
  Input   : -
  Output  : -
  Comments: compteur de secondes basique sans controle de dépassement
          En plus SoftwareSerial rend le compteur de millis() totalement
          A la rue, donc la precision de ce compteur de seconde n'est
          pas fiable du tout, dont acte !!!
  ====================================================================== */
void printUptime(void)
{
  Serial.print(millis() / 1000);
  Serial.print(F("s\t"));
}

/* ======================================================================
  Function: DataCallback
  Purpose : callback when we detected new or modified data received
  Input   : linked list pointer on the concerned data
          current flags value
  Output  : -
  Comments: -
  ====================================================================== */
void DataCallback(ValueList * me, uint8_t  flags)
{
  // Show our not accurate second counter
  //printUptime();

  if (flags & TINFO_FLAGS_ADDED || flags & TINFO_FLAGS_UPDATED)
    if  ( !ok_me_fist) {
      ok_me_fist = true;
      me_fist = me;
    }

  // Display values
  Serial.print("==> ");
  Serial.print(me->name);
  if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
  Serial.print(F("\t")) ;
  if ( me->value[0] != '*') {
    Serial.print(F(" = ")) ;
    Serial.print(me->value) ;
  }
  if ( me->date[0] != '*' ) {
    Serial.print(" : ");
    Serial.print(me->date);
    compttour = compttour + 1;
  }
  if ((me->name[0] == 'N') && (me->name[1] == 'G') && (me->name[2] == 'T') && (me->name[3] == 'F')) {

    NGTF = me->value;
    Serial.print("NGTF:");
    Serial.println(NGTF);
  }
  if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'S')) {

    SINSTS = me->value;
    okpapp = 1;
    Serial.print("SINSTS:");
    Serial.println(SINSTS);

  }
  if ((me->name[0] == 'S') && (me->name[1] == 'I') && (me->name[2] == 'N') && (me->name[3] == 'S') && (me->name[4] == 'T') && (me->name[5] == 'I')) {

    SINSTI = me->value;
    okpappi = 1;
    Serial.print("SINSTI:");
    Serial.println(SINSTI);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'T')) {

    EAST = me->value;
    Serial.print("EAST:");
    Serial.println(EAST);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'I') && (me->name[3] == 'T')) {
    EAIT = me->value;
    Serial.print("EAIT:");
    Serial.println(EAIT);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '1')) {
    EASF01 = me->value;
    Serial.print("EASF01:");
    Serial.println(EASF01);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '2')) {
    EASF02 = me->value;
    Serial.print("EASF02:");
    Serial.println(EASF02);

  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '3')) {
    EASF03 = me->value;
    Serial.print("EASF03:");
    Serial.println(EASF03);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '4')) {
    EASF04 = me->value;
    Serial.print("EASF04:");
    Serial.println(EASF04);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '5')) {
    EASF05 = me->value;
    Serial.print("EASF05:");
    Serial.println(EASF05);
  }
  if ((me->name[0] == 'E') && (me->name[1] == 'A') && (me->name[2] == 'S') && (me->name[3] == 'F') && (me->name[4] == '0') && (me->name[5] == '6')) {
    EASF06 = me->value;
    Serial.print("EASF06:");
    Serial.println(EASF06);
  }
  if ((me->name[0] == 'L') && (me->name[1] == 'T') && (me->name[2] == 'A') && (me->name[3] == 'R') && (me->name[4] == 'F')) {
    LTARF = me->value;
    Serial.print("LTARF:");
    Serial.println(LTARF);
  }
  if ((me->name[0] == 'P') && (me->name[1] == 'R') && (me->name[2] == 'M')) {
    boucleok = 1;
  }
  Serial.println("");
}

/* ======================================================================
  Function: sendJSON
  Purpose : dump teleinfo values on serial
  Input   : linked list pointer on the concerned data
          true to dump all values, false for only modified ones
  Output  : -
  Comments: -
  ====================================================================== */
void sendAll()
{
  ValueList * me;
  me = me_fist;                                   // reprendre le 1er de la chaine
  // Got at least one ?
  if (me) {

    Serial.print(F("\"all\":"));
    Serial.print(me->name) ;
    Serial.print(F("\t")) ;
    if ( me->value[0] != '*') {
      Serial.print(F(" = ")) ;
      Serial.print(me->value) ;
    }
    if ( me->date[0] != '*') {
      Serial.print(" : ");
      Serial.print(me->date);
    }
    Serial.println("");

    // Loop thru the node
    while (me->next) {

      // go to next node
      me = me->next;                                // suivant
      //sprintf(msg, "%s=%s", me->name, me->value );
      //Mqtt_emettre(me->name, me->value);

      Serial.print(F("\"all\":"));
      Serial.print(me->name) ;
      //if (strlen(me->name) < 4 ) Serial.print(F("\t")) ;
      Serial.print(F("\t")) ;
      if ( me->value[0] != '*') {
        Serial.print(F(" = ")) ;
        Serial.print(me->value) ;
      }
      if (me->date[0] != '*') {
        Serial.print(" : ");
        Serial.print(me->date);
      }
      Serial.println("");

    }
  }
}

void DataformatComPart1() {
  timedecode = millis();
  sensor.clearFields();

  sensor.addField("ModeTic", "Standard");
  JSONbuffer["ModeTic"] = String("Standard");
  sensor.addField("FWVersion", FWVersion);
  JSONbuffer["FWVersion"] = String(FWVersion);
  sensor.addField("BoardVersion", BoardVersion);
  JSONbuffer["BoardVersion"] = String(BoardVersion);

  // if(useled){
  //  digitalWrite(2,LOW);
  // }
  MQAll = millis();
  WiFi.mode(WIFI_STA);
  delay(1);
  setup_wifi();           //On se connecte au réseau wifi
  delay(500);
  timeconnexion = millis();
  adressmacesp = composeClientID();
  sensor.addField("RSSI", WiFi.RSSI());
  JSONbuffer["RSSI"] = String(WiFi.RSSI());
  sensor.addField("TimeStartup", timestartup);
  sensor.addField("TimeDecode", timedecode);
  sensor.addField("TimePrepa", MQAll);
  sensor.addField("TimeConn", timeconnexion);
  sensor.addField("UseOptimWifi", useoptimwifi);
  sensor.addField("MidTryWifiCon", midtrywificon);
  sensor.addField("OffsetDeepsleep", DeepSleepSecondsOpti);
  sensor.addField("UseLed", useled);
  sensor.addField("NGTF", String(NGTF));
  JSONbuffer["NGTF"] = String(NGTF);
  sensor.addField("OPTARIF", String(LTARF));
  sensor.addField("PTEC", String(LTARF));
  JSONbuffer["PTEC"] = String(PTEC);
  sensor.addField("BASE", atoi(EAST));
  sensor.addField("EAST", atoi(EAST));
  JSONbuffer["EAST"] = String(EAST);
  if (okpapp == 1) {
    sensor.addField("SINSTS", atoi(SINSTS));
    JSONbuffer["SINSTS"] = String(SINSTS);
    sensor.addField("PAPP", atoi(SINSTS));
  }
  if (okpappi == 1) {
    sensor.addField("SINSTI", atoi(SINSTI));
    sensor.addField("EAIT", atoi(EAIT));
    JSONbuffer["EAIT"] = String(EAIT);
  }
}

void DataformatComPart2() {
  serializeJson(JSONbuffer, output);
  if (useinflux == 1) {
    // Data point
    sensor.addTag("MacAdress", adressmacesp);
    InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
    clientinflux.setInsecure();
    boolean influxconnec = 0;
    while (!influxconnec) {
      influxconnec = clientinflux.validateConnection();
      delay(100);
    }
    clientinflux.writePoint(sensor);
  }
  if (usemqtt == 1) {
    client.setServer(mqtt_server.c_str(), 1883);    //Configuration de la connexion au serveur MQTT
    if (!client.connected()) {
      reconnect();
    }
    client.loop();
    String maintopic = "winky/" + adressmacesp;
    client.publish(maintopic.c_str(), output.c_str(), true);
  }
  delay(1000);
  WiFi.disconnect();
  delay(500);
  DeepModulateSleeptime = (millis() - MQAll) * 1000 + DeepSleepSecondsOpti;
  ESP.deepSleep(DeepModulateSleeptime, WAKE_RF_DISABLED); //100ms
  //ESP.deepSleep(25000000, WAKE_RF_DISABLED); //100ms
}




/* ======================================================================
  Function: setup
  Purpose : Setup I/O and other one time startup stuff
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void linky_setup()
{

  Serial.println(F("======== Setup Teleinfo Linky ======="));
  delay(200);

  Serial.begin(9600);
  // Init teleinfo
  tinfo.init();

  // Attacher les callback dont nous avons besoin
  // pour cette demo, ici attach data
  tinfo.attachData(DataCallback);

}

/* ======================================================================
  Function: loop
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */

void linky_loop() {
  // Teleinformation processing
  if ( Serial.available() && ticmissing ) {
    timestartfunc = millis();
    ticmissing = 0;
  }
  if ( Serial.available() && !ticmissing ) {
    if (useled) {
      digitalWrite(2, LOW);
    }
    /*
      char inByte = Serial2.read();
      Serial.print(inByte);
      Serial.print(".");
      Serial.print(inByte,HEX);
      Serial.print(";");
    */
    tinfo.process(Serial.read());
    if (5000 < (millis() - timestartfunc)) {
      writeFile(SPIFFS, "/inputModeHistStand.txt", "Historique");
      delay(500);
      ESP.restart();
    }
    if (useled) {
      digitalWrite(2, HIGH);
    }
  }
  else if (calibtimefault < (millis() - timestartfunc)) {
    ticmissing = 1;
    digitalWrite(2, LOW);
    delay(100);
    digitalWrite(2, HIGH);
    delay(100);
  }

  if ((String(NGTF).indexOf("BA") != -1) && okpapp == 1 && boucleok == 1) {
    DataformatComPart1();
    DataformatComPart2();
  }
  else if ((String(NGTF).indexOf('H') != -1) && okpapp == 1 && boucleok == 1) {
    DataformatComPart1();
    sensor.addField("EASF01", atoi(String(EASF01).c_str()));
    JSONbuffer["EASF01"] = String(EASF01);
    sensor.addField("HCHC", atoi(String(EASF01).c_str()));
    sensor.addField("EASF02", atoi(String(EASF02).c_str()));
    JSONbuffer["EASF02"] = String(EASF02);
    sensor.addField("HCHP", atoi(String(EASF02).c_str()));
    DataformatComPart2();
  }
  else if ((String(NGTF).indexOf('E') == 0) && okpapp == 1 && boucleok == 1) {
    DataformatComPart1();
    sensor.addField("EASF01", atoi(String(EASF01).c_str()));
    sensor.addField("HCHC", atoi(String(EASF01).c_str()));
    sensor.addField("EJPHN", atoi(String(EASF01).c_str()));
    sensor.addField("EASF02", atoi(String(EASF02).c_str()));
    JSONbuffer["EASF02"] = String(EASF02);
    sensor.addField("EJPHPM", atoi(String(EASF02).c_str()));
    DataformatComPart2();
  }
  else if ((String(NGTF).indexOf('T') == 0) && okpapp == 1 && boucleok == 1) {
    DataformatComPart1();
    sensor.addField("EASF01", atoi(String(EASF01).c_str()));
    JSONbuffer["EASF01"] = String(EASF01);
    sensor.addField("BBRHCJB", atoi(String(EASF01).c_str()));
    sensor.addField("EASF02", atoi(String(EASF02).c_str()));
    JSONbuffer["EASF02"] = String(EASF02);
    sensor.addField("BBRHPJB", atoi(String(EASF02).c_str()));
    sensor.addField("EASF03", atoi(String(EASF03).c_str()));
    JSONbuffer["EASF03"] = String(EASF03);
    sensor.addField("BBRHCJW", atoi(String(EASF03).c_str()));
    sensor.addField("EASF04", atoi(String(EASF04).c_str()));
    JSONbuffer["EASF04"] = String(EASF04);
    sensor.addField("BBRHPJW", atoi(String(EASF04).c_str()));
    sensor.addField("EASF05", atoi(String(EASF05).c_str()));
    JSONbuffer["EASF05"] = String(EASF05);
    sensor.addField("BBRHCJR", atoi(String(EASF05).c_str()));
    sensor.addField("EASF06", atoi(String(EASF06).c_str()));
    JSONbuffer["EASF06"] = String(EASF06);
    sensor.addField("BBRHPJR", atoi(String(EASF06).c_str()));
    DataformatComPart2();
  }
}

//Programme Linky Historique
// ---------------------------------------------- //
//        Basic constructor for LoKyTIC           //
void TeleInfo() {
  // variables initializations
  ADCO = "000000000000";
  OPTARIF = "----";
  ISOUSC = 0;
  PEJP = 0;
  HCHC = 0L;
  HCHP = 0L;
  EJPHN = 0L;
  EJPHPM = 0L;
  BASE = 0L;
  BBRHCJB = 0L;
  BBRHPJB = 0L;
  BBRHCJW = 0L;
  BBRHPJW = 0L;
  BBRHCJR = 0L;
  BBRHPJR = 0L;
  PMAX = 0L;
  PTEC = "----";
  DEMAIN = "----";
  HHPHC = '-';
  IINST = 0;
  IINST1 = 0;
  IINST2 = 0;
  IINST3 = 0;
  ADPS = 0;
  IMAX = 0;
  IMAX1 = 0;
  IMAX2 = 0;
  IMAX3 = 0;
  PAPP = 0;
  MOTDETAT = "------";
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//           TIC frame capture from Linky         //
boolean readTeleInfo()  {
  //boolean readTeleInfo(bool TIC_state)  {
#define startFrame  0x02
#define endFrame    0x03
#define startLine   0x0A
#define endLine     0x0D
#define maxFrameLen 280

  int comptChar = 0; // variable de comptage des caractères reçus
  char charIn = 0;  // variable de mémorisation du caractère courant en réception
  char bufferTeleinfo[21] = "";
  int bufferLen = 0;
  int checkSum;
  int sequenceNumnber = 0;    // number of information group
  //--- wait for starting frame character
  while (charIn != startFrame)
  { // "Start Text" STX (002 h) is the beginning of the frame
    if (Serial.available()) {
      charIn = Serial.read() & 0x7F; // Serial.read() vide buffer au fur et à mesure
    }
    else if (calibtimefault < (millis() - timestartfunc)) {
      digitalWrite(2, LOW);
      delay(100);
      digitalWrite(2, HIGH);
      delay(100);
    }
  } // fin while (tant que) pas caractère 0x02
  //--- wait for the ending frame character
  while (charIn != endFrame)
  { // tant que des octets sont disponibles en lecture : on lit les caractères
    if (Serial.available()) {
      charIn = Serial.read() & 0x7F;
      // incrémente le compteur de caractère reçus
      comptChar++;
      if (charIn == startLine)  bufferLen = 0;
      bufferTeleinfo[bufferLen] = charIn;
      // on utilise une limite max pour éviter String trop long en cas erreur réception
      // ajoute le caractère reçu au String pour les N premiers caractères
      if (charIn == endLine)  {
        checkSum = bufferTeleinfo[bufferLen - 1];
        if (chksum(bufferTeleinfo, bufferLen) == checkSum)  {// we clear the 1st character
          strncpy(&bufferTeleinfo[0], &bufferTeleinfo[1], bufferLen - 3);
          bufferTeleinfo[bufferLen - 3] = 0x00;
          sequenceNumnber++;
          if (! handleBuffer(bufferTeleinfo, sequenceNumnber))  {
            // Serial.println(F("Sequence error ..."));
            return false;
          }
        }
        else  {
          // Serial.println(F("Checksum error!"));
          return false;
        }
      }
      else
        bufferLen++;
    }
    if (comptChar > maxFrameLen)  {
      // Serial.println(F("Overflow error ..."));
      return false;
    }
  }
  return true;
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//   Update new values from TIC for the next TX   //
void updateParameters() {
  teleInfoReceived = readTeleInfo();
  // Serial.end(); // Important!!! -> STOP LoKyTIC to send packet.
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//               TIC frame parsing                //
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber) {
  // create a pointer to the first char after the space
  char* resultString = strchr(bufferTeleinfo, ' ') + 1;
  boolean sequenceIsOK =0;

  if (sequenceIsOK = (bufferTeleinfo[0] == 'A' && bufferTeleinfo[2] == 'C')) {
    ADCO = String(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'B' && bufferTeleinfo[1] == 'A')) {
    BASE = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'A' && bufferTeleinfo[2] == 'P')) {
    ADPS = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'H' && bufferTeleinfo[3] == 'C')) {
    HCHC = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'H' && bufferTeleinfo[3] == 'P')) {
    HCHP = atol(resultString);
  }
  else if (sequenceIsOK = bufferTeleinfo[1] == 'T') {
    PTEC = String(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'P' && bufferTeleinfo[1] == 'E')) {
    PEJP = atol(resultString);
  }
  else if (sequenceIsOK = bufferTeleinfo[1] == 'M') {
    IMAX = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'P' && bufferTeleinfo[1] == 'A')) {
    PAPP = atol(resultString);
    okpapp = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == 'I' && bufferTeleinfo[5] == '1')) {
    IINST1 = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == 'I' && bufferTeleinfo[5] == '2')) {
    IINST2 = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == 'I' && bufferTeleinfo[5] == '3')) {
    IINST3 = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'I' && bufferTeleinfo[1] == 'I')) {
    IINST = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == '0' && bufferTeleinfo[4] == '1')) {
    IMAX1 = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == '0' && bufferTeleinfo[4] == '2')) {
    IMAX2 = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == '0' && bufferTeleinfo[4] == '3')) {
    IMAX3 = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[1] == 'M' && bufferTeleinfo[3] == 'X')) {
    PMAX = atol(resultString);
  }
  else if (sequenceIsOK = bufferTeleinfo[1] == 'H') {
    HHPHC = resultString[0];
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'E' && bufferTeleinfo[4] == 'N')) {
    EJPHN = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'E' && bufferTeleinfo[4] == 'P')) {
    EJPHPM = atol(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[0] == 'I' && bufferTeleinfo[1] == 'S')) {
    ISOUSC = atol(resultString);
  }
  else if (sequenceIsOK = bufferTeleinfo[0] == 'D') {
    DEMAIN = String(resultString);
  }
  else if (sequenceIsOK = bufferTeleinfo[0] == 'O') {
    OPTARIF = String(resultString);
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'B')) {
    BBRHCJB = atol(resultString);
    okbleuHC = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'B')) {
    BBRHPJB = atol(resultString);
    okbleuHP = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'W')) {
    BBRHCJW = atol(resultString);
    okblancHC = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'W')) {
    BBRHPJW = atol(resultString);
    okblancHP = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'C' && bufferTeleinfo[6] == 'R')) {
    BBRHCJR = atol(resultString);
    okrougeHC = 1;
  }
  else if (sequenceIsOK = (bufferTeleinfo[4] == 'P' && bufferTeleinfo[6] == 'R')) {
    BBRHPJR = atol(resultString);
    okrougeHP = 1;
  }
  else if (sequenceIsOK = bufferTeleinfo[1] == 'O') {
    MOTDETAT = String(resultString);
  }

  if (!sequenceIsOK) {
    // Serial.print(F("Out of sequence ..."));
    //  Serial.println(bufferTeleinfo);
  }
  return sequenceIsOK;
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//            Checksum Calculation                //
char chksum(char *buff, uint8_t len)  {
  int i;
  char sum = 0;
  for (i = 1; i < (len - 2); i++)
    sum = sum + buff[i];
  sum = (sum & 0x3F) + 0x20;
  return (sum);
}
// ---------------------------------------------- //

void linky_hist_loop() {
  Serial.begin(1200);  // Important!!! -> RESTART LoKyTIC
  TeleInfo();
  while (okpapp == 0 || (BASE == 0 && (HCHP == 0 || HCHC == 0) && (EJPHN == 0 || EJPHPM == 0) && (okbleuHC == 0 || okbleuHP == 0 || okblancHC == 0 || okblancHP == 0 || okrougeHC == 0 || okrougeHP == 0) )) {
    updateParameters();
    if (calibtimefault < (millis() - timestartfunc)) {
      writeFile(SPIFFS, "/inputModeHistStand.txt", "Standard");
      delay(500);
      timestartfunc = millis();
      linky_setup();
      linky_loop();
    }
  }
  timedecode = millis();
  if (BASE == 0) {
    BASE = HCHP + HCHC + EJPHN + EJPHPM + BBRHCJB + BBRHPJB + BBRHCJW + BBRHPJW + BBRHCJR + BBRHPJR;
  }

  Point sensor("xKy");
  DynamicJsonDocument JSONbuffer(1024);
  String output;

  // Add constant tags - only once
  // Store measured value into point
  sensor.clearFields();
  sensor.addField("ModeTic", "Historique");
  // Report RSSI of currently connected network
  sensor.addField("FWVersion", FWVersion);
  JSONbuffer["FWVersion"] = String(FWVersion);
  sensor.addField("BoardVersion", BoardVersion);
  JSONbuffer["BoardVersion"] = String(BoardVersion);
  sensor.addField("ADCO", ADCO);
  JSONbuffer["ADCO"] = String(ADCO);
  sensor.addField("OPTARIF", OPTARIF);
  JSONbuffer["OPTARIF"] = String(OPTARIF);
  sensor.addField("PAPP", PAPP);
  JSONbuffer["PAPP"] = String(PAPP);
  sensor.addField("BASE", BASE);
  JSONbuffer["BASE"] = String(BASE);
  sensor.addField("IINST", IINST);

  if (HCHP != 0 || HCHC != 0) {
    sensor.addField("HCHC", HCHC);
    JSONbuffer["HCHC"] = String(HCHC);
    sensor.addField("HCHP", HCHP);
    JSONbuffer["HCHP"] = String(HCHP);
  }

  if (EJPHN != 0 || EJPHPM != 0) {
    sensor.addField("EJPHN", EJPHN);
    JSONbuffer["EJPHN"] = String(EJPHN);
    sensor.addField("EJPHPM", EJPHPM);
    JSONbuffer["EJPHPM"] = String(EJPHPM);
    sensor.addField("PEJP", PEJP);
    JSONbuffer["PEJP"] = String(PEJP);
  }

  if (okbleuHC != 0 || okbleuHP != 0 || okblancHC != 0 || okblancHP != 0 || okrougeHC != 0 || okrougeHP != 0) {
    sensor.addField("BBRHCJB", BBRHCJB);
    JSONbuffer["BBRHCJB"] = String(BBRHCJB);
    sensor.addField("BBRHPJB", BBRHPJB);
    JSONbuffer["BBRHPJB"] = String(BBRHPJB);
    sensor.addField("BBRHCJW", BBRHCJW);
    JSONbuffer["BBRHCJW"] = String(BBRHCJW);
    sensor.addField("BBRHPJW", BBRHPJW);
    JSONbuffer["BBRHPJW"] = String(BBRHPJW);
    sensor.addField("BBRHCJR", BBRHCJR);
    JSONbuffer["BBRHCJR"] = String(BBRHCJR);
    sensor.addField("BBRHPJR", BBRHPJR);
    JSONbuffer["BBRHPJR"] = String(BBRHPJR);
  }

  if (PTEC != "----") {
    sensor.addField("PTEC", PTEC);
    JSONbuffer["PTEC"] = String(PTEC);
  }

  if (DEMAIN != "----") {
    sensor.addField("DEMAIN", DEMAIN);
    JSONbuffer["DEMAIN"] = String(DEMAIN);
  }
  if (useled) {
    digitalWrite(2, LOW);
  }
  MQAll = millis();
  WiFi.mode(WIFI_STA);
  delay(1);
  setup_wifi();           //On se connecte au réseau wifi
  delay(500);
  timeconnexion = millis();
  String adressmacesp = composeClientID();
  sensor.addField("RSSI", WiFi.RSSI());
  sensor.addField("TimeStartup", timestartup);
  sensor.addField("TimeDecode", timedecode);
  sensor.addField("TimePrepa", MQAll);
  sensor.addField("TimeConn", timeconnexion);
  sensor.addField("UseOptimWifi", useoptimwifi);
  sensor.addField("MidTryWifiCon", midtrywificon);
  sensor.addField("OffsetDeepsleep", DeepSleepSecondsOpti);
  sensor.addField("UseLed", useled);
  // JSONbuffer["RSSI"] = String(WiFi.RSSI());

  if (useinflux == 1) {
    // Data point
    sensor.addTag("MacAdress", adressmacesp);
    InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
    clientinflux.setInsecure();
    boolean influxconnec = 0;
    while (!influxconnec) {
      influxconnec = clientinflux.validateConnection();
      delay(100);
    }
    clientinflux.writePoint(sensor);
  }
  if (usemqtt == 1) {
    client.setServer(mqtt_server.c_str(), 1883);    //Configuration de la connexion au serveur MQTT
    if (!client.connected()) {
      reconnect();
    }
    client.loop();
    serializeJson(JSONbuffer, output);
    String maintopic = strmaintopicmqtt + "/" + adressmacesp;
    client.publish(maintopic.c_str(), output.c_str(), true);
  }
  delay(1000);
  WiFi.disconnect();
  delay(500);
  DeepModulateSleeptime = (millis() - MQAll) * 1000 + DeepSleepSecondsOpti;
  if (DeepModulateSleeptime > 25000000) {
    DeepModulateSleeptime = 25000000;
  }
  if (DeepModulateSleeptime < 0) {
    DeepModulateSleeptime = 1000000;
  }
  ESP.deepSleep(DeepModulateSleeptime, WAKE_RF_DISABLED); //100ms
  //ESP.deepSleep(25000000, WAKE_RF_DISABLED); //100ms
}
